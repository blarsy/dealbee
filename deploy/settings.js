var webPath = './web/';
module.exports = {
	packages:{
		baseAgent: './*(*.js|*.json)',
		cloudHooks: '.openshift/**/!(.gitignore|*.md)',
		api: 'api/**/*',
		web: 'web/app/**/*.*',
		admin: 'web/admin/**/*.*'
	},
	build: {
		WEBAPPPATH: webPath,
		CSSPATH: webPath + 'app/assets/css/',
		SCSSPATH: webPath + 'scss/',
		ADMINCSSPATH: webPath + 'admin/assets/css/',
		TESTSPATH: './tests/',
		DEPLOYREPOPATH: './../',
		NPMFILE: 'package.json',
		TMPPATH: './../tmp/'
	},
	configs: {
		local: {
			redisPath: 'd:/apps/redis/redis-server.exe',
			mongoPath: 'd:/apps/mongo/bin/mongod.exe',
			dataPath: 'd:/apps/mongo/data/db/'
		},
		dev:{
			appName: 'dev',
			urlRepo: 'ssh://54d49c07e0b8cd00fb00025b@dev-dealbee.rhcloud.com/~/git/dev.git/',
			rootPath: './',
			redisPassword: 'ZTNiMGM0NDI5OGZjMWMxNDlhZmJmNGM4OTk2ZmI5',
			fbAppId: '1553137084944074',
			fbAppSecret: 'ef9c8170a6bc9489dc678522d0f909aa',
			fbNamespace: 'dealbee-dev',
			logPath: '/../../logs/',
			forceSsl: true,
			webApiUrl: 'https://dev-dealbee.rhcloud.com/api/',
			environment: 'Dev',
			jobPollInterval: '10 seconds',
			mailApiKey: 'key-271e24d65c68377c634d835c5801202c',
			mailDomain: 'sandbox81cd5b6aade341be9daf698f7c780cef.mailgun.org',
			websiteUrl: 'https://dev-dealbee.rhcloud.com/',
			stripePublishableKey: 'pk_test_NYE7FIl5PpaUjqLSLeDe3lDr',
			stripeSecretKey: 'sk_test_XDLgeFcfUm1983hRdQYRwEgz',
			launchDate: '2015, 1, 2',
			backupsPath: './../../data/backup/',
			autoStartJobs: true
		},
		test:{
			appName: 'test',
			urlRepo: 'ssh://546ca678ecb8d46e1d00004a@test-dealbee.rhcloud.com/~/git/test.git/',
			rootPath: './',
			redisPassword: 'ZTNiMGM0NDI5OGZjMWMxNDlhZmJmNGM4OTk2ZmI5',
			fbAppId: '1510408459216937',
			fbAppSecret: '7786bac3094d9646e2b70ae685015919',
			fbNamespace: 'dealbee-test',
			logPath: '/../../logs/',
			forceSsl: true,
			webApiUrl: 'https://test-dealbee.rhcloud.com/api/',
			environment: 'Test',
			jobPollInterval: '10 seconds',
			mailApiKey: 'key-271e24d65c68377c634d835c5801202c',
			mailDomain: 'sandbox81cd5b6aade341be9daf698f7c780cef.mailgun.org',
			websiteUrl: 'https://test-dealbee.rhcloud.com/',
			stripePublishableKey: 'pk_test_NYE7FIl5PpaUjqLSLeDe3lDr',
			stripeSecretKey: 'sk_test_XDLgeFcfUm1983hRdQYRwEgz',
			launchDate: '2015, 1, 2',
			backupsPath: './../../data/backup/',
			autoStartJobs: true
		},
		prod:{
			appName: 'web',
			urlRepo: 'ssh://54574de0e0b8cd20440004b1@web-dealbee.rhcloud.com/~/git/web.git/',
			rootPath: './',
			redisPassword: 'ZTNiMGM0NDI5OGZjMWMxNDlhZmJmNGM4OTk2ZmI5',
			fbAppId: '1510408322550284',
			fbAppSecret: 'ab2b6f8ce1f99682b6a9afa2a4acdda6',
			fbNamespace: 'dealbee',
			logPath: '/../../logs/',
			forceSsl: true,
			webApiUrl: 'https://www.dealbee.net/api/',
			environment: '',
			jobPollInterval: '10 seconds',
			mailApiKey: 'key-271e24d65c68377c634d835c5801202c',
			mailDomain: 'dealbee.net',
			websiteUrl: 'https://www.dealbee.net/',
			stripePublishableKey: 'pk_live_k1iq48E07WJKh5sKqvQtMIGS',
			stripeSecretKey: 'sk_live_viofuO70DZJIMTtJn3y8mA5U',
			launchDate: '2015, 1, 24',
			backupsPath: './../../data/backup/',
			autoStartJobs: true
		}
	},
};

//prod   
// MongoDb
// Root User:     admin
//    Root Password: 8FKK76NuElUB
//    Database Name: web

// Connection URL: mongodb://$OPENSHIFT_MONGODB_DB_HOST:$OPENSHIFT_MONGODB_DB_PORT/