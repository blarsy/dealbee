var cliConfig = require(process.cwd() + '/deploy/cliConfig'),
	settings = require(process.cwd() + '/deploy/settings');

module.exports = function (){
	var result = {};

	if(cliConfig.deploy === 'test'){
		result.deployMetadata = settings.configs.test;
	} else if(cliConfig.deploy === 'dev') {
		result.deployMetadata = settings.configs.dev;
	} else if(cliConfig.deploy === 'prod'){
		result.deployMetadata = settings.configs.prod;
	} else {
		return null;
	}
	result.configName = cliConfig.deploy;
	result.targetBuildFolder = settings.build.DEPLOYREPOPATH + result.deployMetadata.appName;

	return result;
}();