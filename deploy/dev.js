module.exports = function() {
	var gulp = require('gulp'),
		sass = require('gulp-sass'),
		plumber = require('gulp-plumber'),
		settings = require(process.cwd() + '/deploy/settings'),
		karma = require('gulp-karma'),
		gulpwatch = require('gulp-watch');

	gulp.task('dev', ['launchServers'], function(taskCallback){
		gulpwatch(settings.build.SCSSPATH + 'app.scss')
			.pipe(plumber())
			.pipe(sass())
			.pipe(gulp.dest(settings.build.CSSPATH));

		gulpwatch(settings.build.SCSSPATH + 'app-admin.scss')
			.pipe(plumber())
			.pipe(sass())
			.pipe(gulp.dest(settings.build.ADMINCSSPATH));

		gulp.src([settings.build.TESTSPATH + 'app/components/**/*.js', settings.build.TESTSPATH + 'app/routes/**/*.js'])
			.pipe(plumber())
			.pipe(karma({
				configFile: settings.build.TESTSPATH + 'karma.conf.js',
				action: 'watch'
			}));

		taskCallback();
	});
};