

module.exports = function() {
	var gulp = require('gulp'),
		plumber = require('gulp-plumber'),
		settings = require(process.cwd() + '/deploy/settings'),
		karma = require('gulp-karma'),
		gulpwatch = require('gulp-watch'),
		protractor = require('gulp-protractor').protractor;

	gulp.task('test', ['launchServers'], function (taskCallback){
		//launch auto tests
		gulp.src([
			settings.build.TESTSPATH + 'app/components/**/*.js', 
			settings.build.TESTSPATH + 'app/routes/**/*.js'])
			.pipe(plumber())
			.pipe(karma({
				configFile: settings.build.TESTSPATH + 'karma.conf.js',
				action: 'run'
			}));

		gulpwatch([settings.build.TESTSPATH + 'features/*.feature', settings.build.TESTSPATH + 'features/**/*.js'])
			.pipe(gulp.src(settings.build.TESTSPATH))
			.pipe(plumber())
			.pipe(protractor({ configFile: settings.build.TESTSPATH + 'protractor.conf.js' }));
	});
};