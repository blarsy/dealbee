module.exports = function() {
	var gulp = require('gulp'),
		util = require('gulp-util'),
		plumber = require('gulp-plumber'),
		ngAnnotate = require('gulp-ng-annotate'),
		uglify = require('gulp-uglify'),
		concat = require('gulp-concat'),
		fs = require('fs'),
		hash = require('gulp-hash'),
		replace = require('gulp-replace'),
		_ = require('underscore'),
		config = require(process.cwd() + '/deploy/config'),
		settings = require(process.cwd() + '/deploy/settings'),
		rimraf = require('rimraf'),
		gulpRimraf = require('gulp-rimraf'),
		now = new Date(),
		tmpPath = settings.build.TMPPATH + now.getFullYear().toString() + now.getMonth().toString() + now.getDay().toString() + now.getMinutes().toString() + now.getSeconds().toString(),
		async = require('async');

	function getMinifiedFilePath(callback){
		fs.readFile(tmpPath + '/dist/hashes.json', function(err, data){
			if(err) return callback(err);
			var hashes = JSON.parse(data);
			callback(null, hashes['app.min.js']);
		});
	};

	gulp.task('compress', ['integrate-compressed'], function (taskCallback){
		util.log('Compression done.');
		taskCallback();
	});

	gulp.task('integrate-compressed', ['minify'], function (taskCallback){
		util.log('Starting integration of compressed files');

		getMinifiedFilePath(function (err, minifiedFilePath){
			if(err) return taskCallback(err);

			async.waterfall([
					function (callback){
						util.log('Copying minified script file...');
						gulp.src(tmpPath + '/dist/' + minifiedFilePath)
							.pipe(plumber())
							.pipe(gulp.dest(config.targetBuildFolder + '/app/'))
							.on('end', callback);
					},
					function (callback){
						util.log('Copied minified script file.');
						util.log('Overwritting index.html...');
						gulp.src(tmpPath + '/dist/index.html')
							.pipe(plumber())
							.pipe(gulp.dest(config.targetBuildFolder + '/app/'))
							.on('end', callback);
					},
					function (callback){
						util.log('Index.html overwritten.');
						util.log('Removing uncompressed source files...');
						//The piping of 'dest' at the end is not required for gulp-rimraf to do its job, but
						//it is the only way I have found to ensure the 'end' event is emitted upon completion
						//of this pipe
						gulp.src([ config.targetBuildFolder + '/app/routes/**/*.js'], {read: false})
							.pipe(plumber())
							.pipe(gulpRimraf({force: true}))
							.pipe(gulp.dest('.'))
							.on('end', function (){
								rimraf(config.targetBuildFolder + '/app/components', function (err){
									if(err) return callback(err);
									rimraf(config.targetBuildFolder + '/app/lib', function (err){
										if(err) return callback(err);
										callback();
									});
								});
							});
					}
				],
				function(err, results){
					if(err) return taskCallback(err);
					taskCallback();
				});
		});
	});

	gulp.task('minify', ['makePackage'], function (taskCallback){
		util.log('Starting minification ...');
		
		fs.readFile('web/app/index.html', function (err, data){
			if(err) throw err;

			var scriptRegex = /<script src=\"(.*)\"/gm,
				ngZoneRegex = /<\!-- ngFiles -->((.|\r\n)*)<\!-- end ngfiles -->/m,
				scriptsZoneRegex = /<\!-- scripts to minify -->((.|\r\n)*)<\!-- end scripts to minify -->/m,
				ngZone = data.toString().match(ngZoneRegex),
				scriptsZone = data.toString().match(scriptsZoneRegex),
				match,
				ngScripts = [],
				classicScripts = [];

			debugger;
			while(match = scriptRegex.exec(ngZone[1])){
				ngScripts.push(config.targetBuildFolder + '/app/' + match[1]);
			}
			while(match = scriptRegex.exec(scriptsZone[1])){
				var scriptToAdd = config.targetBuildFolder + '/app/' + match[1];
				if(!_.find(ngScripts, function (ngScript){
					return ngScript === scriptToAdd;
				})){
					classicScripts.push(scriptToAdd);
				}
			}

			util.log(ngScripts.length + ' angular scripts to annotate.');
			util.log(classicScripts.length + ' classic scripts to minify.');

			async.waterfall([
					function(callback){
						gulp.src(ngScripts)
							.pipe(plumber())
							.pipe(ngAnnotate({ single_quotes: true }))
							.pipe(concat('ngAnnotated.js'))
							.pipe(gulp.dest(tmpPath))
							.on('end', callback);
					},
					function(callback){
						util.log('Angular scripts annotated.');
						util.log('Minifying ...');
						classicScripts.push(tmpPath + '/*.js');
						gulp.src(classicScripts)
							.pipe(plumber())
							.pipe(concat('app.min.js'))
							.pipe(uglify())
							.pipe(gulp.dest(tmpPath + '/dist/'))
							.on('end', callback);
					},
					function(callback){
						util.log('Minification complete.');
						util.log('Hashing minified file...')
						gulp.src(tmpPath + '/dist/app.min.js')
							.pipe(plumber())
							.pipe(hash())
							.pipe(gulp.dest(tmpPath + '/dist/'))
							.pipe(hash.manifest('./hashes.json'))
							.pipe(gulp.dest(tmpPath + '/dist/'))
							.on('end', callback);
					},
					function(callback){
						getMinifiedFilePath(function(err, minifiedFilePath){
							if(err) return callback(err);
							util.log('Minified file hashed to "' + minifiedFilePath + '"');
							util.log('Rewriting index.html...');
							gulp.src(settings.build.WEBAPPPATH + 'app/index.html')
								.pipe(plumber())
								.pipe(replace(/(<\!-- scripts to minify -->(.|\r\n)*<\!-- end scripts to minify -->)/m, '<script src="' + minifiedFilePath+ '"></script>'))
								.pipe(gulp.dest(tmpPath + '/dist/'))
								.on('end', function (){
									util.log('index.html rewritten.')
									callback();
								});
						});
					}
				],
				function(err, results){
					if(err) return taskCallback(err);
					taskCallback();
				});
		});
	});
};