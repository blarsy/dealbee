var minimist = require('minimist');

module.exports = minimist(process.argv.slice(2), { string: ['deploy'], boolean: ['test'], default: { deploy: false, test: false }});