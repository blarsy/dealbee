module.exports = function() {
	var gulp = require('gulp'),
		plumber = require('gulp-plumber'),
		cliConfig = require(process.cwd() + '/deploy/cliConfig'),
		config = require(process.cwd() + '/deploy/config'),
		git = require('gulp-git'),
		rimraf = require('rimraf'),
		gulpRimraf = require('gulp-rimraf'),
		util = require('gulp-util'),
		ignore = require('gulp-ignore'),
		async = require('async'),
		replace = require('gulp-replace'),
		settings = require(process.cwd() + '/deploy/settings'),
		exec = require('child_process').exec,
		fs = require('fs');

	function cleanup(taskCallback){
		util.log('cleaning up empty directories in source directory... (no clue why they were created anyway)');
		fs.readdir(process.cwd(), function (err, files){
			if(err) return taskCallback(err);

			for(var i = 0; i < files.length; i++){
				var stat = fs.statSync(files[i]);
				if(stat.isDirectory()){
					var filesInSubdir = fs.readdirSync(files[i]);
					if(filesInSubdir.length === 0){
						fs.rmdirSync(files[i]);
					}
				}
			}
			util.log('cleaned up empty directories in source directory.')
			taskCallback();
		});
	};

	gulp.task('deploy-full', ['deploy'], function (taskCallback){
		util.log('Full deployment successful.');
		taskCallback();
	});

	gulp.task('compress-cleanup', ['compress'], function (taskCallback){
		cleanup(taskCallback);
	});

	gulp.task('deploy-copyOnly', ['compress-cleanup'], function (taskCallback){
		util.log('Copy-only deployment successful');
		taskCallback();
	});

	gulp.task('makePackage', function (taskCallback){
		var deploymentTasks;

		if(!config.configName) return taskCallback('No valid environment provided: don\'t know where to deploy. Stopping.');
		
		util.log('Using configuration ' + config.configName + '.');

		deploymentTasks = [
			function(callback){
				util.log('Removing local repo if exists.');
				rimraf(config.targetBuildFolder, callback);
			},
			function(callback){
				util.log('Cloning into local repo.');
				var cmd = "git clone " + config.deployMetadata.urlRepo;
				return exec(cmd, {cwd: settings.build.DEPLOYREPOPATH}, function(err, stdout, stderr){
					if(err) return callback(err);
					util.log(stdout, stderr);
					callback();
				});
			},
			function(callback){
				util.log('Deleting files from local repo.');
				//The piping of 'dest' at the end is not required for gulp-rimraf to do its job, but
				//it is the only way I have found to ensure the 'end' event is emitted upon completion
				//of this pipe
				gulp.src(config.targetBuildFolder + '/*', {read: false})
					.pipe(plumber())
					.pipe(gulpRimraf({force: true}))
					.pipe(gulp.dest('.'))
					.on('end', callback);
			},
			function(callback){
				util.log('Copying latest files to local repo.');
				var pck = settings.packages;
				async.parallel([
						function(callback){
							gulp.src([pck.cloudHooks, pck.api, pck.baseAgent], {base: '.'})
								.pipe(plumber())
								.pipe(gulp.dest(config.targetBuildFolder))
								.on('end', callback);
						},
						function(callback){
							gulp.src([pck.web], {base: './web'})
								.pipe(plumber())
								.pipe(gulp.dest(config.targetBuildFolder))
								.on('end', callback);
						},
						function(callback){
							gulp.src([pck.admin], {base: './web'})
								.pipe(plumber())
								.pipe(gulp.dest(config.targetBuildFolder))
								.on('end', callback);
						}
					],
					function(err, results){
						if(err) return callback(err);
						callback();
					});
			},
			function(callback){
				util.log('Customizing settings for deployment.');
				gulp.src([config.targetBuildFolder + '/settings.js'])
					.pipe(plumber())
					.pipe(replace(/webAppPath = '(.*)'/, 'webAppPath = \'' + './app/' + '\''))
					.pipe(replace(/ROOTPATH: '(.*)'/, 'ROOTPATH: \'' + config.deployMetadata.rootPath + '\''))
					.pipe(replace(/REDISPASSWORD: '(.*)'/, 'REDISPASSWORD: \'' + config.deployMetadata.redisPassword + '\''))
					.pipe(replace(/JOBPOLLINTERVAL: '(.*)'/, 'JOBPOLLINTERVAL: \'' + config.deployMetadata.jobPollInterval + '\''))
					.pipe(replace(/FBAPPID: '(.*)'/, 'FBAPPID: \'' + config.deployMetadata.fbAppId + '\''))
					.pipe(replace(/FBAPPSECRET: '(.*)'/, 'FBAPPSECRET: \'' + config.deployMetadata.fbAppSecret + '\''))
					.pipe(replace(/FBNAMESPACE: '(.*)'/, 'FBNAMESPACE: \'' + config.deployMetadata.fbNamespace + '\''))
					.pipe(replace(/LOGPATH: '(.*)'/, 'LOGPATH: \'' + config.deployMetadata.logPath + '\''))
					.pipe(replace(/FORCESSL: (.*),/, 'FORCESSL: ' + config.deployMetadata.forceSsl + ','))
					.pipe(replace(/MAILAPIKEY: '(.*)'/, 'MAILAPIKEY: \'' + config.deployMetadata.mailApiKey + '\''))
					.pipe(replace(/MAILDOMAIN: '(.*)'/, 'MAILDOMAIN: \'' + config.deployMetadata.mailDomain + '\''))
					.pipe(replace(/MAILAPIURL: '(.*)'/, 'MAILAPIURL: \'' + config.deployMetadata.mailApiUrl + '\''))
					.pipe(replace(/WEBSITEURL: '(.*)'/, 'WEBSITEURL: \'' + config.deployMetadata.websiteUrl + '\''))
					.pipe(replace(/STRIPEAPIKEY: '(.*)'/, 'STRIPEAPIKEY: \'' + config.deployMetadata.stripeSecretKey + '\''))
					.pipe(replace(/BACKUPSPATH: '(.*)'/, 'BACKUPSPATH: \'' + config.deployMetadata.backupsPath + '\''))
					.pipe(replace(/ENVIRONMENT: '(.*)'/, 'ENVIRONMENT: \'' + config.deployMetadata.environment + '\''))
					.pipe(replace(/AUTOSTARTJOBS: (.*),/, 'AUTOSTARTJOBS: ' + config.deployMetadata.autoStartJobs + ','))
					.pipe(gulp.dest(config.targetBuildFolder + '/'))
					.on('end', function (){
						gulp.src([config.targetBuildFolder + '/app/components/services.js'])
							.pipe(plumber())
							.pipe(replace(/webApi: '(.*)'/, 'webApi: \'' + config.deployMetadata.webApiUrl + '\''))
							.pipe(replace(/fbAppId: '(.*)'/, 'fbAppId: \'' + config.deployMetadata.fbAppId + '\''))
							.pipe(replace(/environment: '(.*)'/, 'environment: \'' + config.deployMetadata.environment + '\''))
							.pipe(replace(/stripePublishableKey: '(.*)'/, 'stripePublishableKey: \'' + config.deployMetadata.stripePublishableKey + '\''))
							.pipe(replace(/launchDate: (.*)\)/, 'launchDate: new Date(' + config.deployMetadata.launchDate + ')'))
							.pipe(gulp.dest(config.targetBuildFolder + '/app/components/'))
							.on('end',  function (){
							gulp.src([config.targetBuildFolder + '/admin/components/services.js'])
								.pipe(plumber())
								.pipe(replace(/webApi: '(.*)'/, 'webApi: \'' + config.deployMetadata.webApiUrl + '\''))
								.pipe(replace(/fbAppId: '(.*)'/, 'fbAppId: \'' + config.deployMetadata.fbAppId + '\''))
								.pipe(replace(/environment: '(.*)'/, 'environment: \'' + config.deployMetadata.environment + '\''))
								.pipe(gulp.dest(config.targetBuildFolder + '/admin/components/'))
								.on('end', callback)
						});
					});
			},
			function(callback){
				util.log('Removing dev dependencies.');
				gulp.src([config.targetBuildFolder + '/' + settings.build.NPMFILE])
					.pipe(plumber())
					.pipe(replace(/  \"devDependencies\": \{[\s\S]*\},/m, ' '))
					.pipe(gulp.dest(config.targetBuildFolder))
					.on('end', callback);
			}
		];	
		async.waterfall(deploymentTasks, function(err, results){
			if(err) return taskCallback(err);
			taskCallback();
		});
	});

	gulp.task('deploy', ['compress-cleanup'], function (taskCallback){
		var deploymentTasks = [];

		if(!cliConfig.copyOnly){
			deploymentTasks.push(function(callback){
				util.log('Adding files to local repo.');
				var cmd = "git add -A ";
				exec(cmd, {cwd: config.targetBuildFolder}, function(err, stdout, stderr){
					util.log(stdout, stderr);
					if(err) return callback(err);
					callback();
				});
			});
			deploymentTasks.push(function(callback){
				util.log('Committing to local repo.');
				var now = new Date(),
					cmd = 'git commit -a -m "Deployment ' + 
					[now.getDate(), now.getMonth(), now.getFullYear()].join('/') + ' ' +
					[now.getHours(), now.getMinutes(), now.getSeconds()].join(':') + '"';
				exec(cmd, {cwd: config.targetBuildFolder}, function(err, stdout, stderr){
					util.log(stdout, stderr);
					if(err) return callback(err);
					callback();
				});
			});
			deploymentTasks.push(function(callback){
				util.log('Pushing to remote repo.');
				var cmd = "git push";
				exec(cmd, {cwd: config.targetBuildFolder, maxBuffer: 1024 * 1024}, function(err, stdout, stderr){
					if(err) return callback(err);
					util.log(stdout, stderr);
					callback();
				});
			});
		}

		async.waterfall(deploymentTasks, function(err, results){
			if(err) return taskCallback(err);
			taskCallback();
		});
	});
};