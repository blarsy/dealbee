var util = require('gulp-util'),
	settings = require(process.cwd() + '/deploy/settings'),
	spawn = require('child_process').spawn,
	exec = require('child_process').execFile;

module.exports = {
	launch: function(){
		util.log('Launching Redis.');
		spawn(settings.configs.local.redisPath, [], { detached: true, stdio: 'ignore' });
		util.log('Redis launched.');
		util.log('Launching MongoDb.');
		spawn(settings.configs.local.mongoPath, ['--dbpath', settings.configs.local.dataPath ], { detached: true, stdio: 'ignore' });
		util.log('MongoDb launched.');
		util.log('Launching webserver.');
		spawn('node', ['server.js'], { detached: false, stdio: ['ignore', process.stdout, process.stderr] });
		util.log('Webserver launched.');
	}
};