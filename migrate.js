print('beginning');
var mongo = new Mongo('localhost'),
	dealbeeDb = mongo.getDB('web');

dealbeeDb.auth( 'admin', '8FKK76NuElUB' );

dealbeeDb.runCommand( { getLastError: 1, w: 1, j: true, wtimeout: 1000 } );
emitterCol = dealbeeDb.getCollection('emitters');
identifierCol = dealbeeDb.getCollection('identifiers');
clientCol = dealbeeDb.getCollection('clients');

emitterCursor = emitterCol.find({ payments: { $elemMatch: { uuid: { $exists: false } } }});

emitterCursor.forEach(function (emitter){
	emitterCol.update({ _id: emitter._id }, { $pull: { payments: { uuid: { $exists: false } } } }, false, false);
});

emitterCursor = emitterCol.find({});
emitterCursor.forEach(function (emitter){
	var client = {
		_id: new ObjectId(),
		subscription: emitter.subscription,
		created: new Date()
	};

	if(emitter.stripeCustomerId){
		client.stripeCustomerId = emitter.stripeCustomerId;
	}
	if(emitter.distributions){
		client.distributions = emitter.distributions;
	}
	if(emitter.packs){
		client.packs = emitter.packs;
	}
	if(emitter.payments){
		client.payments = emitter.payments;
	}
	if(emitter.invoicingData){
		client.invoicingData = emitter.invoicingData;
	}
	if(emitter.subscription){
		client.subscription = emitter.subscription;
	}

	var clientWriteResult = clientCol.insert(client);

	for(var i = 0; i < emitter.identifiers.length; i++){
		identifierCol.update({ _id: emitter.identifiers[i] }, { $set: { client: client._id } });
	}
});

emitterCol.update({}, { $unset: { refunds: '', stripeCustomerId: '', distributions: '', packs: '', payments: '', invoicingData: '', subscription: '', balance: '' } }, { multi: true });

print('done');