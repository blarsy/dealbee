module.exports = function (){
	var dealService = require(process.cwd() + '/api/lib/dealService'),
		settings = require(process.cwd() + '/settings'),
		htmlencode = require('htmlencode').htmlEncode,
		templateApplier = require(process.cwd() + '/api/lib/templateApplier'),
		imageRepository = require(process.cwd() + '/api/lib/imageRepository');

	return {
		scrapeDeal: function (req, dealId, done){
			dealService.getDealForDisplay(dealId, function (err, deal){
				if(err) return done(err);

				var emitterInfo = deal.emitter.address ? (
						deal.emitter.address.street + (deal.emitter.address.number ? ', ' + deal.emitter.address.number : '') +
						(deal.emitter.address.box ? ' - ' + deal.emitter.address.box : '') +
						' ' + deal.emitter.address.postcode + ' ' + deal.emitter.address.city + (deal.emitter.address.country ? ' ' + deal.emitter.address.country : '')
						) : '',
					dealData = {
						dealtitle: htmlencode(deal.benefit + ' - ' + deal.teaser), 
						viadealbee: ' via Dealbee',
						image: imageRepository.url(deal.imageRef.public_id + '.' + deal.imageRef.format, { width: 1200, height: 629, crop: 'crop', gravity: 'custom' }),
						emittername: htmlencode(deal.emitter.name),
						emitterinfo: htmlencode(emitterInfo),
						emitterwebsite: deal.emitter.website ? '<a href="' + deal.emitter.website + '">' + deal.emitter.website + '</a>' : '',
						logoimage: imageRepository.url(deal.emitter.imageRef.public_id + '.' + deal.emitter.imageRef.format, { height: 300, crop: 'fit' })
					};
				
				templateApplier.createFromTemplate('/api/lib/scraperTemplates/scrape-se-deal.html', '', dealData, {}, '', done);
			});
		},
		scrapeHomePage: function (req, done){
			var pageData = {
					appdescription: htmlencode('Créez des deals pour vos clients actuels et futurs ! Dealbee est votre partenaire de confiance pour gérer vos opérations de promotion et fidélisation envers vos clients. Grâce à notre site web spécialisé, rapide, simple à utiliser et efficace, vous pouvez créer des bons de réduction et des codes de promotion, appelés Deals, pour de nombreuses occasions.'),
					pagetitle: 'Dealbee: les deals faciles',
					motto: 'Des deals qui font gagner tout le monde',
				};
			
			templateApplier.createFromTemplate('/api/lib/scraperTemplates/scrape-se-home.html', '', pageData, {}, '', done);
		}
	}
}();