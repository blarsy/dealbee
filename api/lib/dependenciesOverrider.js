module.exports = function (){
	var _ = require('underscore'),
		objects = {},
		settings = require(process.cwd() + '/settings'),
		restify = require('restify'),
		setupCall = function (objectName, method, args, returnValue){
			var objectToOverride = require(objectName);
			if(!objects[objectName]){
				objects[objectName] = { methods: {} };
			}
			if(!objects[objectName].methods[method]){
				objects[objectName].methods[method] = { signatures: [] };
				objectToOverride[method] = function (){
					var argsToCompare = arguments,
						matchedSignature = _.find(objects[objectName].methods[method].signatures, function (signature){
						if(signature.args.length !== argsToCompare.length){
							return false;
						}
						for(var i = 0; i < signature.args.length; i ++){
							if(signature.args[i] === 'callback'){
								signature.callbackIndex = i;
							} else if(signature.args[i] === 'any'){
								
							} else if(argsToCompare[i] !== null && typeof argsToCompare[i] === 'object'){
								if(JSON.stringify(signature.args[i]) !== JSON.stringify(argsToCompare[i])){
									return false;
								}
							} else {
								if(argsToCompare[i] !== signature.args[i]){
									return false;
								}
							}
						}	
						return true;
					});

					if(!matchedSignature){
						throw new Error('Unexpected call to ' + objectName + '.' + method + ' with args ' + JSON.stringify(arguments));					
					}

					if(matchedSignature.returnValue){
						if(matchedSignature.callbackIndex !== undefined) {
							if(matchedSignature.returnValue.length){
								arguments[matchedSignature.callbackIndex].apply(null, matchedSignature.returnValue);
							} else {
								arguments[matchedSignature.callbackIndex](matchedSignature.returnValue);
							}
						} else {
							return matchedSignature.returnValue;
						}
					}
				};
			}
			objects[objectName].methods[method].signatures.push({ args: args, returnValue: returnValue });
		};

	if(settings.ENVIRONMENT === 'local'){
		var server = restify.createServer({
			name: 'DependenciesOverrider'
		});
		server.use(restify.bodyParser());

		server.post('call', function (req, res, next){
			if(!req.body.objectName) return next(new restify.InternalError('Missing_ObjectName'));
			if(!req.body.method) return next(new restify.InternalError('Missing_Method'));
			if(!req.body.args) return next(new restify.InternalError('Missing_Args'));
			if(!req.body.returnValue) return next(new restify.InternalError('Missing_ReturnValue'));

			setupCall(req.body.objectName, req.body.method, req.body.args, req.body.returnValue);
			res.send('Ok');
			next();
		});

		server.get('ping', function (res, res, next){
			res.send('Ok');
			next();
		});

		server.listen(8085, '127.0.0.1', function(){});
	}
}();