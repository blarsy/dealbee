var res = require(process.cwd() + '/api/res.json'),
	settings = require(process.cwd() + '/settings'),
	mailSender = require(process.cwd() + '/api/lib/mailassets/mailSender'),
	templateApplier = require(process.cwd() + '/api/lib/templateApplier'),
	models = require(process.cwd() + '/api/models'),
	_ = require('underscore');

module.exports = function(){
	var result = {},
		pad = function (s) { 
			return (s < 10) ? '0' + s : s;
		},
		toDateTime = function(date, langCode){
			if(langCode === 'en'){
				return [pad(date.getMonth()+1), pad(date.getDate()), date.getFullYear()].join('-') + ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes());
			} else {
				return [pad(date.getDate()), pad(date.getMonth()+1), date.getFullYear()].join('/') + ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes());
			}
		},
		checkSendAllowed = function(email, emitterId, done){
			models.preference.findOne({ email: email }, function (err, preference){
				if(err) return done(err);
				if(!preference) return done();

				if(preference.unsubscribedDate) return done('Send blocked: ' + email + ' is unsubscribed.');

				if(preference.blockedEmitters && _.find(preference.blockedEmitters, function (blockedEmitter){ 
					return blockedEmitter.toString() === emitterId.toString(); 
				})){
					return done('Send blocked: ' + email + ' is unsubscribed from emitter ' + emitterId.toString() + '.');
				}
				done();
			});
		},
		makeNiceEmitterEmailAddress = function (emitter){
			return emitter.name.replace(/\W/g, '-') + '@' + settings.MAILDOMAIN;;
		};


	result.composeAndSend = function (from, to, subject, htmlTemplate, textTemplate, mailData, resourceSetName, lang, done){
		var mail = {};
		try {
			mail.from = from;
			mail.to = to;

			templateApplier.createFromTemplate(htmlTemplate, lang, mailData, res, resourceSetName, function (err, content){
				if(err) return done(err);
				try{
					mail.subject = subject;
					mail.html = content;

					templateApplier.createFromTemplate(textTemplate, lang, mailData, res, resourceSetName, function (err, content){
						if(err) return done(err);
						try{
							mail.text = content;
							mailSender.send(mail, done);
						}
						catch(ex){
							return done(ex);
						}
					});					
				}
				catch(ex){
					return done(ex);
				}
			});							
		}
		catch(ex){
			return done(ex);
		}
	};

	result.distributeDeal = function (grant, done){
		try{
			var emitter = grant.emitter,
				mailRes = res.sendDeal,
				mailData = {
					title: emitter.name + mailRes.gaveYouADeal[emitter.language],
					p1: mailRes.dealSummary[emitter.language],
					clickToSee: mailRes.clickToSee[emitter.language],
					ctaUrl: settings.WEBSITEURL + 'viewdeal/' + grant.uuid.toString() + '/' + emitter.language,
					summary: grant.deal.benefit + ' ' + grant.deal.teaser,
					dealbeeSummary: res.dealbeeSummaryPrefix[emitter.language] + emitter.name + res.dealbeeSummary[emitter.language],
					privacyUrl: settings.WEBSITEURL + res.privacyUrl[emitter.language] + '/' + emitter.language,
					homePageUrl: settings.WEBSITEURL + '/' + emitter.language,
					unsubscribeUrl: settings.WEBSITEURL + 'pref/' + grant.uuid.toString() + '/' + emitter.language
				},
				mailSubject = emitter.name + mailRes.offersYou[emitter.language] + grant.deal.benefit + ' ' + grant.deal.teaser;

			checkSendAllowed(grant.email, emitter._id, function (err){
				if(err) return done(err);

				result.composeAndSend(makeNiceEmitterEmailAddress(emitter), grant.email, mailSubject, 
					'/api/lib/mailassets/dealsend.html', '/api/lib/mailassets/dealsend.txt', mailData, 'sendDeal',
					emitter.language, done);
			});
		}
		catch(ex){
			return done(ex);
		}		
	};
	
	result.giveDeal = function (grant, done){
		try{
			var emitter = grant.emitter,
				mailRes = res.sendDeal,
				previousHolder = grant.previousHolders[grant.previousHolders.length - 1],
				mailData = {
					title: (grant.senderName || previousHolder.email) + mailRes.gaveYouADealCreatedBy[emitter.language] + emitter.name,
					p1: mailRes.dealSummary[emitter.language],
					clickToSee: mailRes.clickToSee[emitter.language],
					ctaUrl: settings.WEBSITEURL + 'viewdeal/' + grant.uuid.toString() + '/' + emitter.language,
					summary: grant.deal.benefit + ' ' + grant.deal.teaser,
					dealbeeSummary: res.dealbeeSummaryPrefix[emitter.language] + emitter.name + res.dealbeeSummary[emitter.language],
					privacyUrl: settings.WEBSITEURL + res.privacyUrl[emitter.language] + '/' + emitter.language,
					homePageUrl: settings.WEBSITEURL + '/' + emitter.language,
					unsubscribeUrl: settings.WEBSITEURL + 'pref/' + grant.uuid.toString() + '/' + emitter.language
				},
				mailSubject = (grant.senderName || previousHolder.email) + mailRes.gaveYouADealCreatedBy[emitter.language] + emitter.name + ' : ' + grant.deal.benefit + ' ' + grant.deal.teaser;
				
			checkSendAllowed(grant.email, emitter._id, function (err){
				if(err) return done(err);

				result.composeAndSend(makeNiceEmitterEmailAddress(emitter), grant.email, mailSubject, 
					'/api/lib/mailassets/dealsend.html', '/api/lib/mailassets/dealsend.txt', mailData, 'sendDeal',
					emitter.language, done);
			});
		}
		catch(ex){
			return done(ex);
		}		
	};
	
	result.confirmDealGift = function (giftEmailConfirmation, done){
		try{
			var emitter = giftEmailConfirmation.emitter,
				grant = giftEmailConfirmation.grant,
				mailData = {
					destinator: grant.email,
					dealSummary: grant.deal.teaser + ' ' + grant.deal.benefit,
					emitter: emitter.name,
					ctaViewDealUrl: settings.WEBSITEURL + 'viewdeal/' + grant.uuid.toString() + '/' + emitter.language,
					ctaConfirmUrl: settings.WEBSITEURL + 'deal/giftconfirmed/' + giftEmailConfirmation.uuid.toString() + '/' + emitter.language,
					giftRequestExpiration: toDateTime(giftEmailConfirmation.expires, emitter.language),
					privacyUrl: settings.WEBSITEURL + res.privacyUrl[emitter.language] + '/' + emitter.language,
					homePageUrl: settings.WEBSITEURL + '/' + emitter.language
				};

			result.composeAndSend(makeNiceEmitterEmailAddress(emitter), grant.email, res.confirmGift.confirmDealGiftSubject[emitter.language], 
				'/api/lib/mailassets/confirmgivebymail.html', '/api/lib/mailassets/confirmgivebymail.txt', mailData, 'confirmGift',
				emitter.language, done);
		}
		catch(ex){
			done(ex);
		}
	};

	result.recoverPassword = function (passwordRecoveryRequest, done){
		try{
			var emitterLang = passwordRecoveryRequest.emitter.language,
				mailData = {
					ctaRecoverUrl: settings.WEBSITEURL + 'recoverpassword/' + passwordRecoveryRequest.uuid.toString() + '/' + emitterLang,
					contactSupportUrl: settings.WEBSITEURL + 'contact/' + emitterLang
				};

			result.composeAndSend('support@' + settings.MAILDOMAIN, passwordRecoveryRequest.email, res.recoverPassword.subject[emitterLang], 
				'/api/lib/mailassets/requestPasswordRecovery.html', '/api/lib/mailassets/requestPasswordRecovery.txt', mailData, 'recoverPassword',
				emitterLang, done);
		}
		catch(ex){
			done(ex);
		}
	};

	result.inviteManager = function (managerInvitation, done){
		models.managerInvitation.populate(managerInvitation, { path: 'emitter', select: 'name language' }, function (err, invitationWithEmitter){
			if(err) return done(err);

			models.managerInvitation.populate(invitationWithEmitter, { path: 'requestor', select: 'email' }, function (err, populatedInvitation){
				if(err) return done(err);

				try{
					var emitterLang = populatedInvitation.emitter.language,
						mailData = {
							managerRequestLead: managerInvitation.requestor.email + res.managerRequest.requestYouToManageDealsOf[emitterLang] + populatedInvitation.emitter.name + '\'',
							managerRequestExplain: managerInvitation.requestor.email + res.managerRequest.toldUsYouCouldManageDealsOf[emitterLang] + populatedInvitation.emitter.name + '\'' + res.managerRequest.butYouAreNotKnownToUs[emitterLang],
							ctaUrl: settings.WEBSITEURL + 'managers/accept/' + managerInvitation.uuid.toString() + '/' + emitterLang,
							dealbeeSummary: res.dealbeeSummaryPrefix[emitterLang] + populatedInvitation.emitter.name + res.dealbeeSummary[emitterLang],
							privacyUrl: settings.WEBSITEURL + res.privacyUrl[emitterLang] + '/' + emitterLang,
							homePageUrl: settings.WEBSITEURL + '/' + emitterLang
						};

					result.composeAndSend('support@' + settings.MAILDOMAIN, managerInvitation.newManagerEmail, mailData.managerRequestLead, 
						'/api/lib/mailassets/requestmanager.html', '/api/lib/mailassets/requestmanager.txt', mailData, 'managerRequest',
						emitterLang, done);
				} catch (ex){
					done(ex);
				}				
			});
		});
	};

	return result;
}();