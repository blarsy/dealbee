var settings = require(process.cwd() + '/settings'),
	mailgun = require('mailgun-js')({ apiKey: settings.MAILAPIKEY, domain: settings.MAILDOMAIN });

module.exports = {
	send: function (mail, done){
		mailgun.messages().send(mail, function (err, res) {
			if(err) return done(err);
			done();		
		});
	}
};