module.exports = function(){
	var restify = require('restify'),
		session = require(process.cwd() + '/api/configuredsession'),
		_ = require('underscore'),
		models = require(process.cwd() + '/api/models'),
		crypto = require('crypto'),
		fb = require('fb'),
		facebook = require(process.cwd() + '/api/lib/facebook'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		result = {},
		identifierService = require(process.cwd() + '/api/lib/identifierService');

	result.ensureAuthenticated = function(req, res, next){
		var restApiServer = require(process.cwd() + '/api/restapiserver');
		if(restApiServer.inMaintenance){
			result.ensureAdmin(req, res, function (err, sessionData){
				if(err && err.message === 'Unauthorized'){
					return next('Service_Unavailable');
				} else {
					return next(err, sessionData);
				}
			});
		} else {
			var sessionId = req.params.sessionId || req.body.sessionId;
			if(!sessionId) return next('Not_Authenticated');

			result.restoreSession(sessionId, req, res, next);
		}
	};

	result.ensureAdmin = function(req, res, next){
		var sessionId = req.params.sessionId || req.body.sessionId;
		if(!sessionId) return next('Not_Authenticated');

		result.restoreSession(sessionId, req, res, function (err, sessionData){
			if(err) return next(err);
			if(!req.session.loggedInIdentifier.god) return next('Unauthorized');
			next(null, sessionData);
		});
	};

	result.tryRestoreSession = function(req, res, next){
		var sessionId = req.params ? req.params.sessionId : req.body.sessionId;
		if(!sessionId) return next(null);

		result.restoreSession(sessionId, req, res, next);
	};

	result.restoreSession = function(sessionId, req, res, next){
		session.exists(sessionId, function (err, exists){
			if(err) return next(err);
			if(!exists) return next('SessionExpired');

			session.load(sessionId, function(err, data){
				if(err) return next(err);

				//restore date fields, as they have been stringified by redis
				if(data.loggedInEmitter){
					var emitter = data.loggedInEmitter;
					if(emitter.address && emitter.address.created){
						emitter.address.created = new Date(emitter.address.created);
					}
					emitter.created = new Date(emitter.created);
				}
				if(data.loggedInIdentifier && data.loggedInIdentifier.client){
					var client = data.loggedInClient;
					if(client.subscription){
						client.subscription.latestRenewal = new Date(client.subscription.latestRenewal);
					}
					_.each(client.payments, function (payment){
						if(payment.date){
							payment.date = new Date(payment.date);
						}
					});
					_.each(client.packs, function (pack){
						pack.begin = new Date(pack.begin);
						pack.end = new Date(pack.end);
					});
					_.each(client.distributions, function (distribution){
						distribution.date = new Date(distribution.date);
					});
				}

				session.setSessionData(sessionId, data, req, res, function(){
					next(null, data);
				});
			});
		});
	};

	result.createSessionForEmitter = function(req, emitter, identifier, done){
		req.session.loggedInEmitter = emitter;
		identifier.lastLoggedInEmitter = emitter._id;
		// Mess with JSON converter, so that the 'identifier' mongoose dto is stored in the session including its emitter collection.
		req.session.loggedInIdentifier = JSON.parse(JSON.stringify(identifier));
		req.session.loggedInIdentifier.emitters = identifier.emitters;
		req.session.loggedInIdentifier.name = identifier.name;
		req.session.loggedInIdentifier.powerUser = identifier.powerUser;
		req.session.loggedInIdentifier.god = identifier.god;

		models.client.findOne({ _id: identifier.client }, function (err, client){
			if(err) return done(err);

			req.session.loggedInClient = client;

			models.identifier.findOne({ _id: identifier._id }, function (err, identifierToSave){
				if(err) return done(err);

				identifierToSave.lastLoggedInEmitter = emitter._id;
				identifierToSave.save(function (err){
					if(err) return done(err);

					session.save(req.session.sid, req.session, function (err){
						if(err) return done(err);

						var sessionLog = {
							identifier: identifier,
							id: req.session.sid,
							created: bizTime.now(),
							activity: [],
							emitter: req.session.loggedInEmitter
						};
						
						models.sessionLog.create(sessionLog, function (err){
							if(err) return done(err);
							return done()
						});
					});
				});			
			});
		});
	};

	result.createSession = function(req, identifier, done){
		identifierService.linkEmitters(identifier, function (err, updatedIdentifier){
			if(err) return done(err);
			if(identifier.emitters.length == 0) return done('No emitter linked to identifier, or default emitter not found');

			var emitterToLogin = _.find(identifier.emitters, function (emitter){ return emitter._id === identifier.defaultEmitter }) ||
				identifier.emitters[0];

			result.createSessionForEmitter(req, emitterToLogin, identifier, done);
		});
	};

	result.switchEmitter = function(req, emitterId, done){
		var emitterToSwitchTo = _.find(req.session.loggedInIdentifier.emitters, function (emitter){
			return emitter._id === emitterId;
		});
		if(!emitterToSwitchTo) return done('Invalid_Emitter')

		models.identifier.findOne({ _id: req.session.loggedInIdentifier._id }, function (err, identifierFromDb){
			if(err) return done(err);

			identifierService.linkEmitters(identifierFromDb, function (err){
				if(err) return done(err);

				models.emitter.findOne({ _id: emitterId }, function (err, emitterFromDb){
					if(err) return done(err);

					if(req.session.loggedInIdentifier.name){
						//When the name has been gathered during login (fb login), perpetuate it
						identifierFromDb.name = req.session.loggedInIdentifier.name;
					}

					result.createSessionForEmitter(req, emitterFromDb, identifierFromDb, done);
				});

			});
		});
	};

	result.login = function(req, fbToken, email, password, done){
		if(fbToken){
			result.loginFb(req, fbToken, done);
		} else {
			result.loginEmail(req, email, password, done);
		}
	};

	result.loginFb = function (req, fbToken, done){
		facebook.getIdAndName(fbToken, function (err, fbInfo){
			if(err) return done(err);

			models.identifier.findOne({ 
				type: 0, 
				fbUid: fbInfo.id 
			}, function (err, identifier){
				if(err) return done(err);
				if(!identifier) return done('Identifier_Not_Found');

				facebook.getPowerUsers(fbToken, function (err, powerUsers){
					if(err) return done(err);

					var powerUser = _.find(powerUsers, function (powerUser){ return powerUser.user === fbInfo.id });
					if(powerUser){
						identifier.powerUser = true;
						identifier.god = powerUser.role === 'administrators';
					}
					identifier.name = fbInfo.name;

					result.createSession(req, identifier, done);
				})
			});
		});
	};

	result.loginEmail = function (req, email, pwd, done){
		models.identifier.findOne({ email: email, type: 1 }, function (err, identifier){
			if(err) return done(err);
			if(!identifier) return done('LoginFailed');

			crypto.pbkdf2(pwd, identifier.hashInfo.salt, identifier.hashInfo.iterations, identifier.hashInfo.keySize, function(err, derivedKey){
				if(err) return done(err);

				if(derivedKey.toString() == identifier.hashInfo.value){
					result.createSession(req, identifier, done);
				} else {
					return done('LoginFailed');
				}
			});				
		});
	};
	
	return result;
}();