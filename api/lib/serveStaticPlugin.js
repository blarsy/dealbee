
// Original file: Copyright 2012 Mark Cavage, Inc.  All rights reserved.
// Copied from the serveStatic Restify plugin, modified to allow custom handling of 'ResourceNotFound' condition

var fs = require('fs'),
    path = require('path'),
    escapeRE = require('escape-regexp-component'),
    assert = require('assert-plus'),
    mime = require('mime'),
    restify = require('restify'),
    serveStatic = require(process.cwd() + '/api/lib/serveStatic');

///--- Functions

function serveStaticPlugin(opts) {
    opts = opts || {};
    assert.object(opts, 'options');
    assert.string(opts.directory, 'options.directory');
    assert.optionalNumber(opts.maxAge, 'options.maxAge');
    assert.optionalObject(opts.match, 'options.match');
    assert.optionalString(opts.charSet, 'options.charSet');

    var p = path.normalize(opts.directory).replace(/\\/g, '/');
    var re = new RegExp('^' + escapeRE(p) + '/?.*');

    function serve(req, res, next) {
        var file = path.join(opts.directory,
            decodeURIComponent(req.path()));

        if (req.method !== 'GET' && req.method !== 'HEAD') {
            next(new restify.MethodNotAllowedError(req.method));
            return;
        }

        if (opts.match && !opts.match.test(file)) {
            next(new restify.NotAuthorizedError(req.path()));
            return;
        }

        if (opts.gzip && req.acceptsEncoding('gzip')) {
            fs.stat(file + '.gz', function (err, stats) {
                if (!err) {
                    res.setHeader('Content-Encoding', 'gzip');
                    serveStatic.serveFileFromStats(file,
                        err,
                        stats,
                        true,
                        opts,
                        req,
                        res,
                        next);
                } else {
                    serveStatic.serveNormal(file, opts, req, res, next);
                }
            });
        } else {
            serveStatic.serveNormal(file, opts, req, res, next);
        }

    }

    return (serve);
}

module.exports = serveStaticPlugin;
