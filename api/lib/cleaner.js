module.exports = function (){
	var models = require(process.cwd() + '/api/models'),
		async = require('async'),
		_ = require('underscore'),
		imageRepository = require(process.cwd() + '/api/lib/imageRepository');

	function deleteEmitter(emitter, done){
		var filter = emitter._id ? { _id: emitter._id } : { name: emitter.name };
		models.emitter.findOne(filter, function(err, emitterFound){
			if(err) return done(err);
			if(!emitterFound) return done('Cannot delete emitter with name "' + emitter.name + '", not found.' )
			var deleteOperations = [];

			models.deal.find({ emitter: emitterFound }, function(err, deals){
				if(err) return done(err);

				_.each(deals, function(deal){
					deleteOperations.push(function(callback){
						deleteDeal(deal, function(err){
							if(err) return callback(err);

							callback();
						});
					});
				});

				async.parallel(deleteOperations, function(err){
					if(err) return done(err);

					models.sessionLog.remove({ emitter: emitterFound }, function(err){
						if(err) return done(err);
						if(emitterFound.imageRef){
							imageRepository.api.delete_resources([ emitterFound.imageRef.public_id ], function (result){
								if(result.error) console.error(JSON.stringify(result.error));

								emitterFound.remove(function (err){
									if(err) return done(err);

									done();
								});
							});
						} else {
							emitterFound.remove(function (err){
								if(err) return done(err);

								done();
							});
						}
					});
				});
			});
		});
	};

	function deleteDeal(deal, done){
		models.grant.find({ deal: deal._id }, function (err, grants){
			var deleteGrantOperations = [];
			if(err) return done(err);

			_.each(grants, function(grant){
				deleteGrantOperations.push(function(callback){
					deleteGrant(grant._id, callback);
				});
			});

			async.parallel(deleteGrantOperations, function (err){
				if(err) return done(err);

				imageRepository.api.delete_resources([ deal.imageRef.public_id ], function (result){
					if(result.error) return done(result.error);

					deal.remove(function (err){
						if(err) return done(err);
						done();
					});
				});
			});
		});
	};

	function deleteGrant(grantId, done){
		models.giftEmailConfirmation.remove({ grant: grantId }, function (err){
			if(err) return done(err);

			models.grant.remove({ _id: grantId }, function(err){
				if(err) return done(err);

				done();
			});
		});
	};

	function deleteIdentifier(identifier, done){
		var deleteTasks = [];

		models.identifier.findOne({ email: identifier.email }, function (err, foundIdentifier){
			if(err) return done(err);

			deleteTasks.push(function (callback){
				models.client.remove({ _id: foundIdentifier.client }, callback);
			});

			deleteTasks.push(function (callback){
				models.identifier.remove({ _id: foundIdentifier._id }, callback);
			});

			models.emitter.find({ identifiers: foundIdentifier._id }, function (err, emitters){
				if(err) return done(err);

				_.each(emitters, function (emitter){
					deleteTasks.push(function (callback){
						emitter.identifiers = _.without(emitter.identifiers, foundIdentifier._id);
						emitter.save(callback);
					});
				});
			});

			async.parallel(deleteTasks, done);
		});
	};

	return {
		deleteEmitter: deleteEmitter,
		deleteIdentifier: deleteIdentifier,
		deleteManagerRequest: function(managerRequest, done){
			models.managerInvitation.remove({ uuid: managerRequest.uuid }, done);
		}
	};
}();