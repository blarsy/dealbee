module.exports = function(){
	var result = {},
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		uuid = require('node-uuid'),
		models = require(process.cwd() + '/api/models'),
		_ = require('underscore'),
		resources = require(process.cwd() + '/api/res.json'),
		facebook = require(process.cwd() + '/api/lib/facebook'),
		mailComposer = require(process.cwd() + '/api/lib/mailassets/mailComposer'),
		joblogs = require(process.cwd() + '/api/lib/jobs/joblogs'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		finalizeDealSend = function (err, grant, callback){
			var success = true,
				errorMessage;

			try{
				if(err){
					if(!grant.sendErrors) grant.sendErrors = [];
					if(!grant.consecutiveSendErrors){
						grant.consecutiveSendErrors = 1;
					} else {
						grant.consecutiveSendErrors++;
					}
					if(typeof err === 'object'){
						errorMessage = JSON.stringify(err);
					} else {
						errorMessage = err.toString();
					}
					grant.sendErrors.push({ date: bizTime.now(), error: errorMessage });
					success = false;
				} else {
					grant.consecutiveSendErrors = 0;
					grant.sent = bizTime.now();
				}
				grant.save(function (err, grantSaved){
					try{
						if(err){
							joblogs.errorLog.error({ task: 'send deal', message:'Deal sent, but saving the status of the grant failed', err: err, grant: grant });
							return callback(err);
						}
						callback(null, success);
					}
					catch(ex){
						callback(ex);
					}
				});
			}
			catch(ex){
				callback(ex);
			}
		};

	result.getNextNumber = function (emitterId, done){
		models.deal.find({ emitter: emitterId}, { _id: 1 }, function (err, deals){
			if(err) return done(err);

			models.grant.findOne({ deal: { $in: _.pluck(deals, '_id') } }, { number: 1 }).sort('-number').exec(function (err, grantWithMaxNumber){
				if(err) return done(err);
				var number = 1;

				if(grantWithMaxNumber && grantWithMaxNumber.number){
					number = grantWithMaxNumber.number + 1;
				}

				done(null, number);
			});
		});
	};

	result.createMany = function (dealId, grants, lang, done){
		var grantsToSave = [];
		models.deal.findOne({ _id: dealId }, { emitter: 1 }, function (err, deal){
			if(err) return done(err);

			result.getNextNumber(deal.emitter, function (err, nextNumber){
				if(err) return done(err);
				var now = bizTime.now();

				_.each(grants, function (grant){
					if(grant.fbDestinatorUser){
						grant.fbDestinatorUser.taggedMessage += resources.tagWith[lang] + '@tag';
					}

					var grantToSave = result.create(deal._id, grant.email, grant.fbDestinatorUser, grant.senderFbUid, nextNumber);
					grantsToSave.push(grantToSave);

					nextNumber ++;
				});

				models.grant.collection.insert(grantsToSave, function (err, grantsInserted){
					if(err) return done(err);

					done(null, grantsInserted.result.n);
				});
			});
		});
	};

	result.createOnRequest = function (dealId, email, done){
		models.grant.findOne({ deal: dealId, email: email }, function (err, grantForSameEmail){
			if(err) return done(err);
			if(grantForSameEmail) return done('Only_One_Deal_Per_Email');

			models.deal.findOne({ _id: dealId }, { nbOnRequest: 1, emitter: 1 }, function (err, deal){
				if(err) return done(err);

				if(deal.nbOnRequest === undefined) deal.nbOnRequest = 0;
				if(deal.nbOnRequest < 1) return done('No_More_Deals_Available');

				models.preference.findOne({ email: email }, function (err, pref){
					if(err) return done(err);

					if(pref){
						if(pref.unsubscribedDate) return done('EMail_Blocked');
						if(_.find(pref.blockedEmitters, function (emitter){ return emitter.id == deal.emitter.id; })) return done('Emitter_Blocked');
					}

					deal.nbOnRequest --;

					result.getNextNumber(deal.emitter, function (err, nextNumber){
						if(err) return done(err);

						var grantToSave = result.create(deal._id, email, null, null, nextNumber);
						
						deal.save(function (err){
							if(err) return done(err);
							models.grant.create(grantToSave, function (err){
								if(err) return done(err);
								done();
							});
						});
					});					
				});
			});
		});
	};

	result.create = function (dealId, email, fbDestinatorUser, senderFbUid, nextNumber){
		var grantToReturn = {
			deal: dealId,
			channelType: fbDestinatorUser ? 0 : 1,
			email: email,
			uuid: uuid.v4(),
			created: bizTime.now(),
			number: nextNumber
		};
		
		if(fbDestinatorUser){
			grantToReturn.fbDestinatorUser = fbDestinatorUser;
			grantToReturn.senderFbUid = senderFbUid;
		} 

		return grantToReturn;
	};

	result.distribute = function (grant, callback){
		try{
		    if(grant.channelType === 0){
				facebook.sendDeal(grant, function (err){
					result.finalizeDealSend(err, grant, callback);
				});
		    } else {
		    	if(grant.previousHolders && grant.previousHolders.length > 0){
					mailComposer.giveDeal(grant, function (err){
						result.finalizeDealSend(err, grant, callback);
					});		    		
		    	} else {
					mailComposer.distributeDeal(grant, function (err){
						result.finalizeDealSend(err, grant, callback);
					});		    		
		    	}
		    }
		}
		catch(ex){
			joblogs.handleJobError('Distribute deals', 'Exception while distributing deal', ex);
			callback(ex);
		}
	};

	result.getToDistribute = function(done){
		var now = bizTime.now();
		models.deal.find({ expiration: { $gt: now } }, { _id: 1 }, function (err, deals){
			if(err) return done(err);

			models.grant.find({ $or: [ { sent : { $exists: false } }, { sent: null } ] , deal: { $in: deals } } )
				.populate('deal', { teaser: 1, benefit: 1, emitter: 1 })
				.exec(function (err, grants){
				if(err) return done(err);

				//Gather the emitter of each deal
				var emitterIds = _.map(grants, function (grant){ return grant.deal.emitter });
				models.emitter.find({ _id: { $in: emitterIds } }, { name: 1, language: 1, identifiers: 1 }, function (err, emitters){
					if(err) return done(err);

					var emittersById = _.indexBy(emitters, '_id');
					_.each(grants, function (grant){
						grant.emitter = emittersById[grant.deal.emitter];
					});

					done(null, grants);
				});
			});
		});
	};

	result.finalizeDealSend = finalizeDealSend;

	return result;
}();