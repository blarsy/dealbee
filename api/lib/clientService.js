module.exports = function (){
	var prices = require(process.cwd() + '/api/lib/prices.json'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		models = require(process.cwd() + '/api/models'),
		uuid = require('node-uuid'),
		_ = require('underscore'),
		settings = require(process.cwd() + '/settings'),
		prices = require(process.cwd() + '/api/lib/prices.json'),
		session = require(process.cwd() + '/api/configuredsession'),
		result = {};

	result.setupSubscription = function(subscription, paymentId, client){
		var subscriptionData = _.find(prices.subscriptions, function (subscriptionData){
				return subscriptionData.type === subscription.type;
			}),
			amount,
			now = bizTime.now(),
			latestRenewal = now;

		if(subscriptionData){
			if(subscriptionData.price === 0){
				amount = 0;

				//Check if no still active pack with same subscription type
				var previousPackForSubscriptionType = _.find(client.packs, function (pack){
					return pack.subscriptionKind === subscriptionData.type && pack.begin <= now && pack.end > now;
				});
				if(!previousPackForSubscriptionType){
					//Create 1 monthly pack starting from now, with no payment attached
					result.createPacksForSubscription(client, now, 1, subscriptionData);
				} else {
					latestRenewal = previousPackForSubscriptionType.begin;
				}
			} else {
				if(subscription.paymentFrequency === 1){
					amount = 2 * subscriptionData.price;
					//Create 2 successive monthly packs
					result.createPacksForSubscription(client, now, 2, subscriptionData, paymentId);
				} else {
					amount = subscription.paymentFrequency * subscriptionData.price;
					//Create as many successive monthly packs as paymentFrequency
					result.createPacksForSubscription(client, now, subscription.paymentFrequency, subscriptionData, paymentId);
				}
			}
		} else {
			throw new Error('Unexpected_Subscription_Name');
		}

		client.subscription = {
			kind: subscription.type,
			latestRenewal: latestRenewal,
			price: subscriptionData.price,
			paymentFrequency: subscription.paymentFrequency
		}

		return amount;
	};

	result.createPacksForSubscription = function (client, begin, paymentFrequency, subscriptionData, paymentId){
		var now = bizTime.now();
		if(!client.packs) client.packs = [];

		for(var i = 0; i < paymentFrequency; i ++){
			var pack = {
				begin: bizTime.addMonths(begin, i),
				end: bizTime.addMonths(begin, i + 1),
				created: now,
				origin: 'renewal',
				subscriptionKind: subscriptionData.type,
				amount: subscriptionData.coupons,
				remaining: subscriptionData.coupons,
				price: subscriptionData.price,
				payment: paymentId
			}
			client.packs.push(pack);
		}
	};

	result.recordDistribution = function (clientId, amount, operationDate, done){
		models.client.findOne({ _id: clientId }, function (err, client){
			if(err) return done(err);

			try{
				result.subtractFromBalance(client, amount);
				if(!client.distributions){
					client.distributions = [];
				}
				client.distributions.push({ date: operationDate, amount: amount });
				done(null, client);
			}
			catch(ex){
				if(ex.message === 'Insufficient_Funds'){
					return done('Insufficient_Credits');
				} else {
					return done(ex);
				}
			}
		});	
	};

	result.getBalance = function(client){
		var now = bizTime.now(),
			balance = 0;

		_.each(client.packs, function (pack){
			if(pack.begin <= now && pack.end > now){
				balance += pack.remaining;
			}
		});

		return balance;
	};

	result.subtractFromBalance = function(client, amount){
		if(result.getBalance(client) < amount){
			throw new Error('Insufficient_Funds');
		}

		var now = bizTime.now(),
			activePacks = _.filter(client.packs, function (pack){
				return pack.begin <= now && pack.end >= now;
			}),
			sortedActivePacks = _.sortBy(activePacks, 'end'),
			i = 0;

		while(amount !== 0){
			if(sortedActivePacks[i].remaining < amount){
				amount -= sortedActivePacks[i].remaining;
				sortedActivePacks[i].remaining = 0;
				i ++;
			} else  {
				sortedActivePacks[i].remaining -= amount;
				amount = 0;
			}
		};
	};

	result.registerPayment = function (client, amount, description, paymentId, done){
		var payment = { uuid: paymentId, description: description },
			vatRate = 1;

		if(!client.payments) client.payments = [];
		if(client.invoicingData && 
			client.invoicingData.VATCountryCode && 
			client.invoicingData.VATCountryCode.toLowerCase() === 'be'){
			vatRate = settings.VATMULTIPLIER;
		}
		payment.amount = amount;
		payment.paid = amount * vatRate;
		payment.vatRate = vatRate;

		//Charge customer for the amount (+ VAT if applicable)
		result.charge(payment.paid, payment.uuid, client, function (err){
			if(err) return done(err);
			payment.date = bizTime.now();
			client.payments.push(payment);

			done(null);
		});
	};

	result.registerOrder = function (subscription, packs, client, done){
		var now = bizTime.now(),
			paymentId = uuid.v4(),
			amount = 0;

		//Create a payment, packs and/or subscription linked to the payment
		if(subscription){
			amount += result.setupSubscription(subscription, paymentId, client);
		}
		if(packs && packs instanceof Array && packs.length > 0){
			amount += result.createPacks(client, now, paymentId, packs);
		}

		if(amount > 0){
			result.registerPayment(client, amount, 'online payment', paymentId, done);
		} else {
			done(null);
		}
	};

	result.createPacks = function (client, begin, paymentId, packs){
		var amount = 0,
			now = bizTime.now();

		for(var i = 0; i < packs.length; i++){
			for(var j = 0; j < packs[i].quantity; j ++){
				var price = result.createPack(packs[i].type, begin, client, paymentId);
				amount += price;
			}
		}

		return amount;
	};

	result.createPack = function(packName, begin, client, paymentId){
		var packData = _.find(prices.packs , function (pack){
				return pack.type === packName;
			}),
			now = bizTime.now();
		if(!packData) throw new Error('Unexpected_Pack_Name');

		if(!client.packs) client.packs = [];

		client.packs.push({
			begin: begin,
			end: bizTime.addDays(begin, packData.daysValid),
			amount: packData.coupons,
			remaining: packData.coupons,
			created: now,
			origin: 'online',
			price: packData.price,
			payment: paymentId
		});

		return packData.price;
	};

	result.charge = function (amount, paymentId, client, done){
		var stripe = require('stripe')(settings.STRIPEAPIKEY);
		stripe.charges.create({
			amount: Math.round(amount * 100),
			currency: 'eur',
			customer: client.stripeCustomerId,
			description: paymentId
		}, function(err, charge) {
			if(err) return done(err);

			done();
		});
	};

	result.makeDto = function (client){
		return {
			_id: client._id,
			balance: result.getBalance(client),
			subscription: client.subscription,
			invoicingData: client.invoicingData,
			hasCard: client.stripeCustomerId ? true : false
		};
	};

	result.saveLoggedInClient = function(client, req, done){
		client.save(function (err, clientSaved){
			if(err) return done(err);

			req.session.loggedInClient = clientSaved;
			session.save(req.session.sid, req.session, function (err, status){
				if(err) return done(err);

				done(null, clientSaved);
			})
		});
	};

	return result;
}();