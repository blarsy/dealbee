module.exports = function (){
	var fs = require('fs'),
		mongoDump = require('mongo-dump-stream'),
		settings = require(process.cwd() + '/settings'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		models = require(process.cwd() + '/api/models');

	return {
		backup: function (done){
			try{
				var fileName = bizTime.getDateTimeHash(bizTime.now()),
					out = fs.createWriteStream(settings.BACKUPSPATH + fileName);
				
				mongoDump.dump(models.connectionInfo.connection.db, out, function (err){
					if(err) return done(err);
					try{
						out.end();
					}
					catch(streamErr){
						return done(streamErr);
					}
					done(null, settings.BACKUPSPATH + fileName, fileName);
				});
			}
			catch(ex){
				done(ex);
			}
		}
	};
}();