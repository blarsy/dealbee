module.exports = function (){
	return {
		price: function (number, lang){
			var numberString = number.toFixed(2).toString();
			if(lang.toLowerCase() === 'fr'){
				numberString = numberString.replace('.', ',');
			}
			return numberString + '€';
		}
	}
}();