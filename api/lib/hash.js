var async = require('async'),
	crypto = require('crypto');
module.exports = function (password, email, callback){
	async.waterfall([
		function (callback){
			var keySize = 512;
			crypto.randomBytes(keySize, function(ex, buf){
				if(ex) return callback(ex);
				callback(null, buf, keySize, 12800, password);
			});
		},
		function (salt, keySize, iterations, password, callback){
			crypto.pbkdf2(password,salt,iterations,keySize,function(err, derivedKey){
				if(err) return callback(err);
				callback(null, { type: 1, email: email, hashInfo: { value: derivedKey, keySize: keySize, algorithm: 'pbkdf2', iterations: iterations, salt: salt } });
			});
		}
	], callback);
};