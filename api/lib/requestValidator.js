module.exports = function (){
	var result = {},
		_ = require('underscore');

	result.orderPost = function(req){
		var body = req.body;

		if(body.packs && !(body.packs instanceof Array)) return 'Expected_Array_Of_Packs';

		if(!body.subscription && (!body.packs || body.packs.length == 0)) return 'Subscription_Or_Pack_Required';

		if(body.subscription){
			if(!body.subscription.type) return 'Subscription_Type_Required';
			if(!body.subscription.paymentFrequency) return 'Subscription_PaymentFrequency_Required';
			if(body.subscription.paymentFrequency !== 1 && body.subscription.paymentFrequency !== 6 && body.subscription.paymentFrequency !== 12)
				return 'Invalid_PaymentFrequency';
		}

		if(body.packs){
			for(var i = 0; i < body.packs.length; i ++){
				if(!body.packs[i].type) return 'Pack_Type_Required';
				if(!body.packs[i].quantity) return 'Pack_Quantity_Required';
			}
		}

		return null;
	};

	result.emitterPost = function(req){
		var emitter = req.body;

		if(!emitter.name){
			return 'Name_required';
		} else {
			if(emitter.name.length < 2) {
				return 'Name_policy_failed';
			}
		}
		if(emitter.address){
			if(!emitter.address.street) {
				return 'Street_required';
			}
			if(!emitter.address.postcode) {
				return 'Postcode_required';
			}
			if(!emitter.address.city) {
				return 'City_required';
			}
			if(emitter.address.useMap) {
				if(!emitter.address.coords || emitter.address.coords.length < 1 || !emitter.address.coords[0]){
					return 'Lng_required';
				} else {
					if(isNaN(Number(emitter.address.coords[0]))) {
						return 'Lng_Invalid';
					}
					//Todo: check against possible longitude range
				}
				if(!emitter.address.coords || emitter.address.coords.length < 2 || !emitter.address.coords[1]){
					return 'Lat_required';
				} else {
					if(isNaN(Number(emitter.address.coords[1]))) {
						return 'Lat_Invalid';
					}
					//Todo: check against possible latitude range
				}
			}
		}
		return null;
	};

	result.identifierPost = function(req){
		var identifier = req.body;

		if(!identifier.email){
			return 'Email_required';
		}
		if(!identifier.fbToken){
			if(!identifier.password){
				return 'Password_required';
			} else {
				if(identifier.password.length < 10){
					return 'Password_policy_failed';
				}
			}
		}
	};

	result.invoicingDataPost = function (invoicingData){
		if(!invoicingData.address) return 'InvoicingAddress_Required';
		if(!invoicingData.companyName) return 'CompanyName_required';
		if(!invoicingData.language) return 'InvoicingLanguage_required';
		if(!invoicingData.hasNoVATNumber){
			if(!/^[a-zA-Z]{2,3}$/.test(invoicingData.VATCountryCode || '')) return 'VATCountryCode_Required';
			if(!/^\w{5,11}$/.test(invoicingData.VATNumber || '')) return 'VATNumber_Required';
		}
		if(invoicingData.address && invoicingData.address.created){
			if(!invoicingData.address.street) {
				return 'InvoicingAddress_Street_required';
			}
			if(!invoicingData.address.postcode) {
				return 'InvoicingAddress_Postcode_required';
			}
			if(!invoicingData.address.city) {
				return 'InvoicingAddress_City_required';
			}
		}
		return null;
	};

	result.dealMetadataPost = function (req){
		if(!req.body.dealId) return 'DealId_Required';
		if(req.body.private === undefined) return 'Private_Required';
		if(!req.body.keywords) return 'Keywords_Required';

		if(req.body.keywords.constructor !== Array) return 'Keywords_MustBeAnArray';

		for(var i = 0; i > req.body.keywords.length; i++){
			var keyword = req.body.keywords[i];
			if(keyword.length < 2) return 'Keyword_MustBeLongerThan1Char';
			if(keyword.length > 25) return 'Keyword_MustBeMaximum25Char';
		}

		if(_.uniq(req.body.keywords).length !== req.body.keywords.length) return 'Duplicate_Keywords_Detected';

		return null;
	};

	return result;
}();