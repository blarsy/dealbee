module.exports = function (){
	var facebook = require(process.cwd() + '/api/lib/facebook');
		seo = require(process.cwd() + '/api/lib/seo');
	return {

		getScrapeInfo: function (req){
			if(!req.headers['user-agent']) return null;
			var scrapeInfo = {},
				onRequestIdMatch = req.url.toLowerCase().match(/^\/viewdeal\/onrequest\/([^\/]*).*$/),
				dealIdMatch = req.url.toLowerCase().match(/^\/viewdeal\/((?!onrequest)[^\/^]*).*$/);

			scrapeInfo.onRequest = false;			
			if(onRequestIdMatch && onRequestIdMatch.length > 0 && onRequestIdMatch[1]){
				scrapeInfo.dealId = onRequestIdMatch[1];
				scrapeInfo.onRequest = true;
			} else if(dealIdMatch && dealIdMatch.length > 0 && dealIdMatch[1]){
				scrapeInfo.dealId = dealIdMatch[1];
			}

			if(req.headers['user-agent'].toLowerCase().indexOf('facebookexternalhit') !== -1){
				scrapeInfo.type = 'facebook';
				return scrapeInfo;
			} else if (req.headers['user-agent'].toLowerCase().indexOf('google') !== -1 ||
				req.headers['user-agent'].toLowerCase().indexOf('bing') !== -1){
				scrapeInfo.type = 'searchengine';
				return scrapeInfo;
			}

			return null;
		},
		scrape: function (scrapeInfo, req, done){
			switch(scrapeInfo.type){
				case 'facebook':
					if(scrapeInfo.dealId && scrapeInfo.onRequest){
						facebook.scrapeDealOnRequest(req, scrapeInfo.dealId, done);
					} else if(scrapeInfo.dealId && ! scrapeInfo.onRequest){
						facebook.scrapeDeal(req, scrapeInfo.dealId, done);						
					} else {
						facebook.scrapeHomePage(req, done);
					}
					break;
				case 'searchengine':
					if(scrapeInfo.dealId){
						seo.scrapeDeal(req, scrapeInfo.dealId, done);
					} else {
						seo.scrapeHomePage(req, done);
					}
					break;
				default:
					return done(new Error('Unexpected scrape type "' + scrapeInfo.type + '"'));
			}
		}
	}
}();