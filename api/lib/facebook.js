var fb = require('fb'),
	powerUsers,
	settings = require(process.cwd() + '/settings'),
	_ = require('underscore'),
	resources = require(process.cwd() + '/api/res.json'),
	models = require(process.cwd() + '/api/models'),
	templateApplier = require(process.cwd() + '/api/lib/templateApplier'),
	restify = require('restify'),
	dealService = require(process.cwd() + '/api/lib/dealService'),
	htmlencode = require('htmlencode').htmlEncode,
	imageRepository = require(process.cwd() + '/api/lib/imageRepository');

module.exports = function (){
	var result = {},
		populatePowerUsers = function(token, done){
			fb.api('oauth/access_token', {
				client_id: settings.FBAPPID,
				client_secret: settings.FBAPPSECRET,
				grant_type: 'client_credentials',
				fb_exchange_token: token }, function (oauthRes){
					if(!oauthRes || oauthRes.error) return done(oauthRes);

					fb.api('app/roles', { access_token: oauthRes.access_token }, function (res){
						if(!res || res.error) return done(res);
						powerUsers = res.data;
						done(null, powerUsers);
				});
			});
		},
		self = this;

	result.makeIdentifier = function (token, email, callback){
		result.exchangeToken(token, function (err, tokenData){
			if(err) return callback(err);

			callback(null, { type: 0, fbUid: tokenData.uid, email: email });
		});
	};

	result.exchangeToken = function(token, callback){
		fb.api('oauth/access_token', {
			client_id: settings.FBAPPID,
			client_secret: settings.FBAPPSECRET,
			grant_type: 'fb_exchange_token',
			fb_exchange_token: token }, function (oauthRes) {
				if(!oauthRes || oauthRes.error) return callback('NotLoggedIn');

				fb.api('me',{fields:['id'], access_token: oauthRes.access_token }, function(fbIdRes){
					if(!fbIdRes || fbIdRes.error) return callback('ErrorContactingFacebook ' + JSON.stringify(fbIdRes.error));
					
					//todo check uniqueness of fb uid
					callback(null, { token: oauthRes.access_token, uid: fbIdRes.id });
				});
		});
	};

	result.sendDeal = function (grant, done){
		try{
			models.fbAccessInfo.findOne({ uid: grant.senderFbUid }, function (err, fbAccessInfo){
				try{
					if(err) return done(err);
					if(!fbAccessInfo) return done('No fbAccessInfo found for uid ' + grant.senderFbUid);
					var link = settings.WEBSITEURL + 'viewdeal/' + grant.uuid.toString(),
						message = grant.fbDestinatorUser.taggedMessage.replace('@tag', '@[' + grant.fbDestinatorUser.tagId + ']');

					fb.api('me/' + settings.FBNAMESPACE + ':give', 'post', {
						access_token: fbAccessInfo.accessToken, 
						deal: link,
						message: message,
						privacy: { value: 'CUSTOM', allow: grant.senderFbUid }
					}, function (res) {
						if(!res || res.error) return done(res.error);
						done(null, res.id);
					});	
				}
				catch(ex){
					return done(ex);
				}
			});
		}
		catch(ex){
			return done(ex);
		}
	};

	result.getPowerUsers = function (token, done){
		if(!powerUsers){
			populatePowerUsers(token, function (err, powerUsersFromFacebook){
				if(err) return done(err);
				powerUsers = powerUsersFromFacebook;
				done(null, powerUsersFromFacebook);
			});
		} else {
			done(null, powerUsers);
		}
	};

	result.invalidatePowerUser = function(){
		powerUsers = null;
	}

	result.scrapeDealOnRequest = function (req, dealId, done){
		dealService.getDealForDisplay(dealId, function (err, deal){
			if(err) return done(err);

			var dealData = {
					fbAppId: settings.FBAPPID,
					type: settings.FBNAMESPACE + ':deal',
					url: settings.WEBSITEURL + 'viewdeal/onrequest/' + deal._id.toString(),
					title: htmlencode(deal.benefit + ' - ' + deal.teaser), 
					image: imageRepository.url(deal.imageRef.public_id + '.' + deal.imageRef.format, { crop: 'crop', gravity: 'custom' }),
					description: htmlencode(resources.scrapedContent.offers[deal.emitter.language] + ' ' + deal.emitter.name) + resources.scrapedContent.fbInstructionsClickToTake[deal.emitter.language]
				};
			
			templateApplier.createFromTemplate('/api/lib/scraperTemplates/scrape-fb.html', '', dealData, {}, '', done);
		});
	};

	result.scrapeDeal = function (req, grantId, done){
		dealService.getGrantForDisplay(grantId, function (err, grant){
			if(err) return done(err);

			var dealData = {
					fbAppId: settings.FBAPPID,
					type: settings.FBNAMESPACE + ':deal',
					url: settings.WEBSITEURL + 'viewdeal/' + grantId,
					title: htmlencode(grant.deal.benefit + ' - ' + grant.deal.teaser), 
					image: imageRepository.url(grant.deal.imageRef.public_id + '.' + grant.deal.imageRef.format, { crop: 'crop', gravity: 'custom' }),
					description: htmlencode(resources.scrapedContent.offers[grant.deal.emitter.language] + ' ' + grant.deal.emitter.name) + resources.scrapedContent.fbInstructionsClickToSee[grant.deal.emitter.language]
				};
			
			templateApplier.createFromTemplate('/api/lib/scraperTemplates/scrape-fb.html', '', dealData, {}, '', done);
		});
	};

	result.scrapeHomePage = function (req, done){
		var scrapeData = {
			fbAppId: settings.FBAPPID,
			type: 'website',
			url: settings.WEBSITEURL,
			title: 'Dealbee: les deals faciles',
			description: htmlencode('Créez des deals pour vos clients actuels et futurs ! Dealbee est votre partenaire de confiance pour gérer vos opérations de promotion et fidélisation envers vos clients. Grâce à notre site web spécialisé, rapide, simple à utiliser et efficace, vous pouvez créer des bons de réduction et des codes de promotion, appelés Deals, pour de nombreuses occasions.'),
			image: 'http://res.cloudinary.com/dealbee/image/upload/w_300,h_300/v1435053227/prod/icon.png'
		};
		templateApplier.createFromTemplate('/api/lib/scraperTemplates/scrape-fb.html', '', scrapeData, {}, '', done);
	};

	result.checkPublishPermission = function (uid, callback){
		models.fbAccessInfo.findOne({ uid: uid }, function (err, fbAccessInfo){
			if(err) return callback(err);
			if(!fbAccessInfo) return callback('No fbAccessInfo found for uid ' + uid);

			fb.api(settings.FBAPPID + '/permissions', 'get',{ access_token: fbAccessInfo.accessToken }, function (res){
				if(!res || res.error) return callback(res.error);

				var canPublish = _.find(res.data, function (permission){ return permission.permission === 'publish_actions' });
				if(!canPublish){
					callback('No publish permission for uid ' + uid)
				}
				callback(null);
			});
		});
	}

	result.createOrRefreshPublishInfo = function(token, done){
		result.exchangeToken(token, function (err, tokenData){
			if(err) return done(err);

			models.fbAccessInfo.findOne({ uid: tokenData.uid }, function (err, fbAccessInfo){
				if(err) return done(err);
				if(!fbAccessInfo){
					var newFbUser = {
						uid: tokenData.uid,
						accessToken: tokenData.token,
						dateIssued: new Date(Date.now())
					};
					models.fbAccessInfo.create(newFbUser, function (err, saved){
						if(err) return done(err);
						done();
					});
				} else {
					fbAccessInfo.accessToken = tokenData.token;
					fbAccessInfo.dateIssued = new Date();

					fbAccessInfo.save(function (err, saved){
						if(err) return done(err);
						done(null, tokenData);
					});
				}

			});
		});		
	};

	result.getIdAndName = function(fbToken, done){
		fb.api('me', {fields:['id', 'name'], access_token: fbToken }, function(fbIdRes){
			if(!fbIdRes || fbIdRes.error){
				var message;
				if(!fbIdRes){
					message = 'No response from Facebook';
				} else {
					message = 'ErrorContactingFacebook ' + JSON.stringify(fbIdRes.error);
				}
				return done(message);
			}

			done(null, { id: fbIdRes.id, name: fbIdRes.name });
		});
	};

	return result;
}();