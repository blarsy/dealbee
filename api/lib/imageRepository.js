module.exports = function (){
	var cloudinary = require('cloudinary'),
		settings = require(process.cwd() + '/settings');

	cloudinary.config({ 
		cloud_name: 'dealbee', 
		api_key: settings.CLOUDINARYAPIKEY, 
		api_secret: settings.CLOUDINARYAPISECRET
	});

	return cloudinary;
}();