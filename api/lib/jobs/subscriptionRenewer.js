module.exports = function (){
	var result = {},
		_ = require('underscore'),
		joblogs = require(process.cwd() + '/api/lib/jobs/joblogs'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		models = require(process.cwd() + '/api/models'),
		clientService = require(process.cwd() + '/api/lib/clientService'),
		uuid = require('node-uuid');

	result.execute = function (done){
		result.findEndingSubscriptions(function (err, endingSubscriptions){
			if(err) return done(err);

			result.chargeAndRenewSubscriptions(endingSubscriptions, function (err, chargingResult){
				if(err) return done(err);

				done(null, chargingResult.nbRenewed, chargingResult.nbChargeFailed);
			});
		});
	};

	result.findLatestPack = function (emitter){
		var subscriptionKind = emitter.subscription.kind,
			packsOfRightSubscription = _.filter(emitter.packs, function (pack){ return pack.subscriptionKind === subscriptionKind }),
			orderedPacks = _.sortBy(packsOfRightSubscription, 'end');

		if(orderedPacks.length > 0){
			return orderedPacks[orderedPacks.length - 1];
		} else {
			return null;
		}
	};

	result.findEndingSubscriptions = function (done){
		models.client.find({ subscription: { $exists: true } }, function (err, clients){
			if(err) return done(err);
			var clientsWithEndingSubscriptions = [],
				now = bizTime.now(),
				maxPackEndDate = bizTime.addDays(now, 3);

			_.each(clients, function (client){
				try{
					var latestPack = result.findLatestPack(client);

					//if this client failed to be charged in the last 24 hours, skip it
					if(client.subscription.latestChargeFailure && bizTime.addDays(client.subscription.latestChargeFailure, 1) > now){
						return;
					}
					if(latestPack && latestPack.end < maxPackEndDate ){
						clientsWithEndingSubscriptions.push(client);
					}
				}
				catch(ex){
					joblogs.handleJobError('Renew subscriptions', 'Find ending subscriptions', ex);
				}
			});

			done(null, clientsWithEndingSubscriptions);
		});
	};

	result.chargeAndRenewSubscriptions = function (clientsToRenew, done){
		var chargeAndRenewResults = { nbRenewed: 0, nbChargeFailed: 0 };
		_.each(clientsToRenew, function (clientToRenew){
			//find subscription info
			try{
				var subscriptionInfos = require(process.cwd() + '/api/lib/prices.json'),
					subscriptionInfo = _.find(subscriptionInfos.subscriptions, function (info){ return info.type === clientToRenew.subscription.kind }),
					latestPack = result.findLatestPack(clientToRenew),
					numberOfPacks = clientToRenew.subscription.paymentFrequency || 1;

				//Create packs
				clientService.createPacksForSubscription(clientToRenew, latestPack.end, numberOfPacks, subscriptionInfo);
				
				//if subscription is not free, attempt charge
				if(subscriptionInfo.price > 0){
					//if charge successful, create payment & renewal pack
					var paymentId = uuid.v4();

					clientService.registerPayment(clientToRenew, subscriptionInfo.price * numberOfPacks, 'automatic renewal', paymentId, function (paymentErr){
						if(paymentErr){
							//if charge failed, log failure and set failure time
							clientToRenew.subscription.latestChargeFailure = bizTime.now();
							chargeAndRenewResults.nbChargeFailed ++;
							joblogs.handleJobError('Renew subscriptions', 'Charge and renew subscriptions', paymentErr);
						}

						clientToRenew.save(function (err, saved){
							if(err){
								joblogs.handleJobError('Renew subscriptions', 'Charge and renew subscriptions - saving charged client', err);
							} else {
								if(!paymentErr)	chargeAndRenewResults.nbRenewed ++;
							}
						});
					});
				} else {
					clientToRenew.save(function (err, saved){
						if(err){
							joblogs.handleJobError('Renew subscriptions', 'Charge and renew subscriptions - saving charged client', err);
						} else {
							chargeAndRenewResults.nbRenewed ++;
						}
					});
				}
			}
			catch(ex){
				joblogs.handleJobError('Renew subscriptions', 'Charge and renew subscriptions', ex);
			}

		});

		//return result object
		done(null, chargeAndRenewResults)
	};

	return result;
}();