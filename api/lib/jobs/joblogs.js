module.exports = function (){
	var bunyan = require('bunyan'),
		settings = require(process.cwd() + '/settings'),
		errorStreams = [{
			level: 'info',
			stream: process.stdout
		}],
		auditStreams = [],
		errorLog,
		auditLog;

	if(!settings.testing){
		errorStreams.push({
			type: 'rotating-file',
			period: '1d',
			count: 3,
			level: 'info',
			path: process.cwd() + settings.LOGPATH + 'joberror.log'
		});
		auditStreams.push({
			type: 'rotating-file',
			period: '1d',
			count: 3,
			level: 'info',
			path: process.cwd() + settings.LOGPATH + 'jobaudit.log'
		});
	}
	errorLog = bunyan.createLogger({
		name: 'error', 
		streams: errorStreams
	});
	auditLog = bunyan.createLogger({
		name: 'audit',
		streams: auditStreams
	});

	return {
		handleJobError: function (jobName, operation, err){
			if(err){
				errorLog.error({ task: jobName, message: operation, err: err });
				return true;
			} else {
				return false;
			}
		},
		info: function (jobName, message){
			auditLog.info({ task: jobName, message: message });
		},
		errorLog: errorLog,
		auditLog: auditLog
	};

}();