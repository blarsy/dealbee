module.exports = function (){
	var remoteFolderProxy = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolderProxy'),
		localBackupFolder = require(process.cwd() + '/api/lib/jobs/backupAgent/localBackupFolder'),
		joblogs = require(process.cwd() + '/api/lib/jobs/joblogs');

	return {
		execute: function (backupFilePath, backupFileName, done){
			remoteFolderProxy.connectBackupFolder(function (err, folder){
				if(!joblogs.handleJobError('Backup database', 'connect remote backup folder', err)){
					//On remote folder: Copy backup if needed, remove outdated backups if needed
					folder.copyBackupIfNeeded(backupFilePath, backupFileName, function (err){
						joblogs.handleJobError('Backup database', 'copy backup to remote folder', err);

						//On local folder: Remove outdated backups
						folder.cleanupOutdatedBackups(function (err){
							joblogs.handleJobError('Backup database', 'cleanup outdated backups', err);
						});
					});

				}

				localBackupFolder.deleteOldBackups(function (err, numberOfFilesDeleted){
					if(!joblogs.handleJobError('Backup database', 'delete old backups from local backup folder', err)){
						joblogs.info('backup database', numberOfFilesDeleted + ' files cleaned up on local backup folder.');
					}
					done();
				});
			});	
		}
	};
}();