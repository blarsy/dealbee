module.exports = function (){
	var backupDatabase = require(process.cwd() + '/api/lib/backupDatabase'),
		joblogs = require(process.cwd() + '/api/lib/jobs/joblogs'),
		automatedTasks = require(process.cwd() + '/api/lib/jobs/backupAgent/automatedTasks');

	return {
		backup: function (agentDone){
			backupDatabase.backup(function (err, backupFilePath, backupFileName){
				if(joblogs.handleJobError('backup database', 'Backup failed.', err)){
					return agentDone();
				}

				automatedTasks.execute(backupFilePath, backupFileName, function (err){
					if(!joblogs.handleJobError('backup database', 'Post backup automated tasks failed.', err)){
						joblogs.info('backup database', 'Backup job finished. See log entries above to determine success or failure.');
					}
					agentDone();
				});
			});
		}
	};
}();