module.exports = function (){
	var fs = require('fs'),
		settings = require(process.cwd() + '/settings'),
		_ = require('underscore'),
		async = require('async');
	return {
		deleteOldBackups: function (done){
			fs.readdir(settings.BACKUPSPATH, function (err, names){
				if(err) return done(err);

				var collectFilesDataOperations = [],
					filesAndData = [],
					fileIndex = 0,
					fileIndexStat = 0;

				_.each(names, function (file){
					collectFilesDataOperations.push(function (callback){
						var fileName = settings.BACKUPSPATH + names[fileIndex];
						fileIndex++;
						fs.stat(fileName, function (err, stat){
							if(err) return callback(err);
							var fileNameForStat = settings.BACKUPSPATH + names[fileIndexStat];
							fileIndexStat++;
							filesAndData.push({ name: fileNameForStat, stat: stat });
							callback();
						});
					});
				});
				async.parallel(collectFilesDataOperations, function (err, results){
					if(err) return done(err);

					var oldestFiles = _.sortBy(filesAndData, function (fileAndData){ return fileAndData.stat.ctime }),
						backupRemoveOperations = [],
						numberOfFiles = oldestFiles.length,
						numberOfFilesToRetain = settings.NUMBEROFLOCALBACKUPSTOSTORE,
						deletedFiles = 0;

					//Remove all files except the n latest
					if(numberOfFiles > numberOfFilesToRetain){
						for(var i = 0; i < numberOfFilesToRetain; i++){
							oldestFiles.pop();
						}

						for(var j = 0; j < oldestFiles.length; j ++){
							backupRemoveOperations.push(function (callback){
								var fileToDelete = oldestFiles.pop().name;
								fs.unlink(fileToDelete, function (err){
									if(err) return callback(err);
									deletedFiles++;
									callback();
								});
							});													
						}
					}

					async.parallel(backupRemoveOperations,
						function (err, results){
							if(err) return done(err);
							done(null, deletedFiles);
						});
				});
			});
		}
	}
}();