module.exports = function (){
	var settings = require(process.cwd() + '/settings'),
		kloudless = require('kloudless')(settings.KLOUDLESSAPIKEY),
		remoteFolder = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder'),
		_ = require('underscore'),
		result = {},
		dropboxAccountId,
		backupRemoteFolder,
		invoiceRemoteFolder;

	result.getDropboxAccountId = function(done){
		if(!dropboxAccountId){
			kloudless.accounts.base({}, function (err, res){
				if(err) return done(err);

				var account = _.find(res.objects, function (account){ return account.service === 'dropbox'; });
				if(!account) return done('No dropbox account found on kloudless.');

				dropboxAccountId = account.id;
				done(null, account.id);
			});
		} else {
			done(null, dropboxAccountId);
		}
	};

	result.connectBackupFolder = function (done){
		if(backupRemoteFolder) return done(null, backupRemoteFolder);

		result.getDropboxAccountId(function (err, accountId){
			if(err) return done(err);

			kloudless.folders.create({ account_id: accountId, parent_id: 'root', name: 'Dealbee' }, function (err, rootFolder){
				if(err) return done(err);
				
				kloudless.folders.create({ account_id: accountId, parent_id: rootFolder.id, name: 'Backups' }, function (err, backupsFolder){
					if(err) return done(err);
					
					kloudless.folders.create({ account_id: accountId, parent_id: backupsFolder.id, name: settings.ENVIRONMENT || 'prod' }, function (err, envFolder){
						if(err) return done(err);
						
						backupRemoteFolder = remoteFolder.create(accountId, envFolder.id);
						done(null, backupRemoteFolder);
					});
					
				});

			});
		});
	};

	result.connectInvoiceFolder = function (done){
		if(invoiceRemoteFolder) return done(null, invoiceRemoteFolder);

		result.getDropboxAccountId(function (err, accountId){
			if(err) return done(err);

			kloudless.folders.create({ account_id: accountId, parent_id: 'root', name: 'Dealbee' }, function (err, rootFolder){
				if(err) return done(err);
				
				kloudless.folders.create({ account_id: accountId, parent_id: rootFolder.id, name: 'Invoices' }, function (err, invoicesFolder){
					if(err) return done(err);
					
					kloudless.folders.create({ account_id: accountId, parent_id: invoicesFolder.id, name: settings.ENVIRONMENT || 'prod' }, function (err, envFolder){
						if(err) return done(err);
						invoiceRemoteFolder = remoteFolder.create(accountId, envFolder.id);
						
						done(null, invoiceRemoteFolder);
					});
				});
			});			
		});
	}

	return result;
}();