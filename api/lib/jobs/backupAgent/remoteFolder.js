module.exports = function (){
	var settings = require(process.cwd() + '/settings'),
		kloudless = require('kloudless')(settings.KLOUDLESSAPIKEY),
		fs = require('fs'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		_ = require('underscore'),
		async = require('async');

	return {
		create: function (accountId, folderId){
			return {
				copyBackupIfNeeded: function (backupFilePath, backupFileName, done){
					kloudless.folders.contents({ account_id: accountId, folder_id: folderId }, function (err, res){
						if(err) return done(err);

						var latestBackup = _.max(res.objects, function (obj){ return new Date(obj.modified); }),
							tooMuchAgo = new Date(bizTime.now().valueOf() - (1000 * 60 * 60 * settings.REMOTEBACKUPUPDATEFREQUENCY)),
							//backup must be copied if either no backup exists on the remote drive, or if the latest backup is too old
							backupMustBeCopied = _.isEmpty(latestBackup) ? true : (new Date(latestBackup.modified) < tooMuchAgo);						
						
						if(backupMustBeCopied){
							fs.readFile(backupFilePath, function (err, data){
								if(err) return done(err);

								kloudless.files.upload({ account_id: accountId, parent_id: folderId, file: data, name: backupFileName }, function (err){
									if(err) return done(err);

									done();
								});
							});							
						} else {
							done();
						}
					});
				},
				cleanupOutdatedBackups: function (done){
					kloudless.folders.contents({ account_id: accountId, folder_id: folderId }, function (err, res){
						if(err) return done(err);
						
						var tooMuchAgo = new Date(bizTime.now().valueOf() - (1000 * 60 * 60 * 24 * settings.REMOTEBACKUPSTORAGEDURATION)),
							backupDeleteOperations = [];
						
						_.each(res.objects, function (obj){
							if(new Date(obj.modified) < tooMuchAgo){
								backupDeleteOperations.push(function (operationDone){
									kloudless.files.delete({ account_id: accountId, file_id: obj.id }, function (err){
										if(err) return operationDone(err);

										operationDone();
									});
								});
							}
						});

						async.parallel(backupDeleteOperations, function (err){
							if(err) return done(err);

							done();
						});
					});
				},
				upload: function (data, fileName, done){
					kloudless.files.upload({ account_id: accountId, parent_id: folderId, file: data, name: fileName, queryParams: {overwrite: true } }, function (err){
						if(err) return done(err);

						done();
					});
				},
				getInvoice: function (paymentUuid, done){
					kloudless.folders.contents({ account_id: accountId, folder_id: folderId }, function (err, res){
						if(err) return done(err);

						var invoiceFile = _.find(res.objects, function (obj){
							return obj.name === paymentUuid + '.pdf';
						});
						if(!invoiceFile) return done('NotFound');

						kloudless.files.contents({ account_id: accountId, file_id: invoiceFile.id }, done);
					});
				}
			}
		}
	}; 
}();