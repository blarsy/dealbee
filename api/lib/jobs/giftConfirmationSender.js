module.exports = function (){
		var mailComposer = require(process.cwd() + '/api/lib/mailassets/mailComposer'),
		async = require('async'),
		models = require(process.cwd() + '/api/models'),
		_ = require('underscore'),
		jobLogs = require(process.cwd() + '/api/lib/jobs/joblogs'),
		result = {},
		confirmationsToSend = [],
		SEND_QUEUE_CONCCURENCY = 100,
		sendErrors,
		processed,
		drained,
		finalizeConfirmationSend = function (err, giftConfirmationRequest, callback){
			var success = true,
				errorMessage;
			if(err){
				if(!giftConfirmationRequest.sendErrors) giftConfirmationRequest.sendErrors = [];
				if(!giftConfirmationRequest.consecutiveSendErrors){
					giftConfirmationRequest.consecutiveSendErrors = 1;
				} else {
					giftConfirmationRequest.consecutiveSendErrors++;
				}
				if(typeof err === 'object'){
					errorMessage = JSON.stringify(err);
				} else {
					errorMessage = err.toString();
				}
				giftConfirmationRequest.sendErrors.push({ date: new Date(), error: errorMessage });
				success = false;
			} else {
				giftConfirmationRequest.consecutiveSendErrors = 0;
				giftConfirmationRequest.sent = new Date();
			}
			giftConfirmationRequest.save(function (err, giftConfirmationRequestSaved){
				if(err){
					jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'Confirmation request sent, but saving the status of the confirmation request failed', err: err, giftConfirmationRequest: giftConfirmationRequest });
					return callback(err);
				}
				callback(null, success);
			});
		},
		queue = async.queue(function (giftConfirmationRequest, callback){
			if(!giftConfirmationRequest.consecutiveSendErrors || giftConfirmationRequest.consecutiveSendErrors < 5){
				result.send(giftConfirmationRequest, function (err, success){
					if(err) return callback(err);
					processed ++;
					if(!success){
						sendErrors ++;
					}
					callback();
				});		
			} else {
				callback();
			}
		}, SEND_QUEUE_CONCCURENCY);

	queue.drain = function (){
		if(sendErrors > 0){
			jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'giftConfirmationSender ended with ' + sendErrors + ' errors out of ' + processed + ' sendings.' });
		} else {
			jobLogs.auditLog.info({ task: 'send gift confirmation request', message:'giftConfirmationSender successfully sent ' + processed + ' gift confirmations.' });
		}
		drained();
	};

	result.queueAndSend = function (done){
		try{
			var now = new Date();
			sendErrors = 0;
			processed = 0;
			drained = done;

			models.giftEmailConfirmation.find({ sent : { $exists: false }, expires: { $gt: now } }).populate('grant').exec(function (err, giftEmailConfirmations){
				if(err) {
					jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'giftConfirmationSender could not gather gift confirmation requests to be sent because of an error during the database request.', err : err });
					return done();
				}
				try{
					if(giftEmailConfirmations.length == 0){
						jobLogs.auditLog.info({ task: 'send gift confirmation request', message:'giftConfirmationSender skipped sending: no gift confirmation request to be sent.' });
						return done();
					}

					models.grant.populate(giftEmailConfirmations, { path: 'grant.deal', model: models.deal }, function (err, giftEmailConfirmationsPopulated){
						try{
							if(err) {
								jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'giftConfirmationSender could not gather gift confirmation requests to be sent because of an error while populating deals during the database request.', err : err });
								return done();
							}

							var emitterIds = _.map(giftEmailConfirmations, function (giftEmailConfirmation){ return giftEmailConfirmation.grant.deal.emitter; })
							models.emitter.find({ _id: { $in: emitterIds } }, function (err, emitters){
								if(err) {
									jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'giftConfirmationSender could not gather gift confirmation requests to be sent because of an error when querying for emitters in the database request.', err : err });
									return done();
								}
								try{
									var emittersById = _.indexBy(emitters, '_id');

									_.each(giftEmailConfirmationsPopulated, function(giftEmailConfirmation){
										giftEmailConfirmation.emitter = emittersById[giftEmailConfirmation.grant.deal.emitter];
									});
									
									jobLogs.auditLog.info({ task: 'send gift confirmation request', message:'giftConfirmationSender starting to send ' + giftEmailConfirmations.length + ' gift confirmation requests.' });
									queue.push(giftEmailConfirmationsPopulated);
								}
								catch(ex){
									jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'Unexpected error.', err : ex });
									return done();
								}
							});							
						}
						catch(ex){
							jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'Unexpected error.', err : ex });
							return done();
						}

					});
				}
				catch(ex){
					jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'Unexpected error.', err : ex });
					return done();
				}
			});
		}
		catch(ex){
			jobLogs.errorLog.error({ task: 'send gift confirmation request', message:'Unexpected error.', err : ex });
			return done();
		}

	};

	result.send = function (giftConfirmationRequest, callback){
		try{
			mailComposer.confirmDealGift(giftConfirmationRequest, function (err){
				if(err) return finalizeConfirmationSend(err, giftConfirmationRequest, callback);
				finalizeConfirmationSend(null, giftConfirmationRequest, callback);
			});
		}
		catch(ex){
			jobLogs.errorLog.error(ex);
			callback(ex);
		}
	};

	return result;
}();