module.exports = function (){
	var async = require('async'),
		grantService = require(process.cwd() + '/api/lib/grantService');
	return {
		create: function (concurrency, distributionStatus){
			var distributeNonFailedGrant = function (grant, callback){
					if(!grant.consecutiveSendErrors || grant.consecutiveSendErrors < 5){
						distributionStatus.processed++;
						grantService.distribute(grant, function (err){
							if(err) {
								distributionStatus.errors++;
							}
							callback();
						});
					} else {
						callback();
					}
				},
				queue = async.queue(distributeNonFailedGrant, concurrency);

			queue.drain = distributionStatus.processFeedback;
			queue.distributeNonFailedGrant = distributeNonFailedGrant;
			return queue;
		}
	}
}();