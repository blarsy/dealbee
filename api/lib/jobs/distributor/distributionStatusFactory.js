module.exports = function(){
	var joblogs = require(process.cwd() + '/api/lib/jobs/joblogs');

	return {
		create: function (callback){
			return function(){
				var taskName = 'Distribute deals',
					result = {};

				result.processed = 0,
				result.errors = 0,
				result.processFeedback = function(){
					if(result.errors > 0){
						joblogs.errorLog.error({ task: taskName, message:'Distribution ended with ' + result.errors + ' errors out of ' + result.processed + ' grants.' });
					} else {
						joblogs.info(taskName, 'Distribution successfully sent ' + result.processed + ' grants.');
					}
					callback();
				}

				return result;
			}();
		}
	}
}();