module.exports = function(){
	var grantService = require(process.cwd() + '/api/lib/grantService'),
		distributionStatusFactory = require(process.cwd() + '/api/lib/jobs/distributor/distributionStatusFactory'),
		queueFactory = require(process.cwd() + '/api/lib/jobs/distributor/queueFactory'),
		joblogs = require(process.cwd() + '/api/lib/jobs/joblogs'),
		taskName = 'Distribute deals',
		QUEUE_CONCURRENCY = 100;

	return {
		execute: function (done){
			grantService.getToDistribute(function (err, grantsToDistribute){
				if(err){
					joblogs.handleJobError(taskName, 'Get grants to distribute', err);
					return done();
				}
				if(grantsToDistribute.length === 0){
					joblogs.info(taskName, 'No grant found to be distributed. Stopping.');
					return done();
				}
				var distributionStatus = distributionStatusFactory.create(done),
					queue = queueFactory.create(QUEUE_CONCURRENCY, distributionStatus);

				queue.push(grantsToDistribute);
			});
		}
	};
}();