module.exports = function(){
	var facebook = require(process.cwd() + '/api/lib/facebook'),
		Agenda = require('agenda'),
		settings = require(process.cwd() + '/settings'),
	    joblogs = require(process.cwd() + '/api/lib/jobs/joblogs'),
	    jobAgent = {},
		//dealSender = require(process.cwd() + '/api/lib/jobs/dealSender'),
		distributor = require(process.cwd() + '/api/lib/jobs/distributor/distributor'),
		subscriptionRenewer = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer'),
		giftConfirmationSender = require(process.cwd() + '/api/lib/jobs/giftConfirmationSender'),
		sessionLogCleaner = require(process.cwd() + '/api/lib/jobs/sessionLogCleaner'),
		agenda,
		backupAgent = require(process.cwd() + '/api/lib/jobs/backupAgent/backupAgent'),
		invoiceGenerator = require(process.cwd() + '/api/lib/jobs/invoiceGenerator');
	
	jobAgent.running = false;

	agenda = new Agenda()
        .database(settings.JOBDB)
        .processEvery(settings.JOBPOLLINTERVAL);

	agenda.define('send deals', { concurrency: 1 }, function (job, done){
		//dealSender.queueAndSend(done);
		joblogs.info('Distribute deals', 'Beginning...');
		distributor.execute(done);
	});

	agenda.define('send gift confirmation requests', { concurrency: 1 }, function (job, done){
		giftConfirmationSender.queueAndSend(done);
	});

	agenda.define('gather power users', { concurrency: 1 }, function (job, done){
		facebook.invalidatePowerUser();
		joblogs.auditLog.info({ task: 'invalidated power users', message: 'invalidated power users' });
		done();
	});

	agenda.define('cleanup old session logs', { concurrency: 1 }, function (job, done){
		joblogs.auditLog.info({ task: 'Cleanup old session logs', message:'Beginning...' });
		sessionLogCleaner.cleanup(done);
	});

	agenda.define('backup database', { concurrency: 1 }, function (job, done){
		joblogs.auditLog.info({ task: 'Backup database', message: 'Beginning...' });
		backupAgent.backup(done);
	});

	agenda.define('renew subscriptions', { concurrency: 1 }, function (job, done){
		joblogs.auditLog.info({ task: 'Renew subscriptions', message: 'Beginning...' });
		subscriptionRenewer.execute(function (err, nbRenewed, nbChargeFailed){
			if(err){
				joblogs.errorLog.error({ task: 'Renew subscriptions', message: 'Failed !', err: err });
			} else {
				joblogs.auditLog.info({ task: 'Renew subscriptions', message: 'Succeeded ! ' + nbRenewed + ' renewed, ' + nbChargeFailed + ' charge failed.' });
			}
			done();
		})
	});

	agenda.define('generate invoices', { concurrency: 1 }, function (job, done){
		joblogs.info({ task: 'Generate invoices', message: 'Beginning...' });
		invoiceGenerator.execute(function (err, nbInvoiceGenerated, nbGenerationsFailed){
			if(err){
				joblogs.errorLog.error({ task: 'Generate invoices', message: 'Failed !', err: err });
			} else {
				joblogs.info({ task: 'Generate invoices', message: 'Succeeded ! ' + nbInvoiceGenerated + ' invoices generated, ' + nbGenerationsFailed + ' invoice generations failed.' });
			}
			done();
		});
	});

	agenda.every('10 seconds', 'send deals');
	agenda.every('10 seconds', 'send gift confirmation requests');
	agenda.every('5 minutes', 'gather power users');
	agenda.every('5 minutes', 'generate invoices');
	agenda.every('10 minutes', 'cleanup old session logs');
	agenda.every('30 minutes', 'backup database');
	agenda.every('30 minutes', 'renew subscriptions');

	jobAgent.start = function (){
		agenda.start();
		jobAgent.running = true;
	};

	jobAgent.stop = function (done){
        try{
            agenda.stop(function (err){
                if(err){
					joblogs.errorLog.error({ task: 'Stop jobAgent', message:'Error while stopping agenda', err: err });
                    return done();
                }
				joblogs.auditLog.info({ task: 'Stop jobAgent', message:'Agenda successfully stopped.' });
				jobAgent.running = false;
                done();
            });
        }
        catch(ex){
            joblogs.errorLog.error({ task: 'Stop jobAgent', message:'Exception while stopping Scheduler: ', err: err });
            done();
        }
   	};

	return jobAgent;
}();