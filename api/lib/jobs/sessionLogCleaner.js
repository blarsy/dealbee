module.exports = function (){
	var models = require(process.cwd() + '/api/models'),
		jobLogs = require(process.cwd() + '/api/lib/jobs/joblogs'),
		result = {};

	result.cleanup = function (callback){
		var soonestCreatedTime = new Date(new Date().valueOf() - ( 1000 * 60 * 60 * 24 * 3 /*3 days ago*/ ));
		models.sessionLog.remove({ created: { $lt: soonestCreatedTime }}, function (err){
			if(err){
				jobLogs.errorLog.error({ task: 'Cleanup old session logs', message:'Error during the removal of the records on the database.', err: err });
			} else {
				jobLogs.auditLog.info({ task: 'Cleanup old session logs', message:'Done.' });
			}
			callback();
		});
	};
	return result;
}();