var facebook = require(process.cwd() + '/api/lib/facebook'),
	mailComposer = require(process.cwd() + '/api/lib/mailassets/mailComposer'),
	async = require('async'),
	models = require(process.cwd() + '/api/models'),
	_ = require('underscore'),
	jobLogs = require(process.cwd() + '/api/lib/jobs/joblogs');

module.exports = function (){
	var dealSender = {},
		grantsToSend = [],
		SEND_QUEUE_CONCCURENCY = 100,
		sendErrors,
		processed,
		drained,
		finalizeDealSend = function (err, grant, callback){
			var success = true,
				errorMessage;

			try{
				if(err){
					if(!grant.sendErrors) grant.sendErrors = [];
					if(!grant.consecutiveSendErrors){
						grant.consecutiveSendErrors = 1;
					} else {
						grant.consecutiveSendErrors++;
					}
					if(typeof err === 'object'){
						errorMessage = JSON.stringify(err);
					} else {
						errorMessage = err.toString();
					}
					grant.sendErrors.push({ date: new Date(), error: errorMessage });
					success = false;
				} else {
					grant.consecutiveSendErrors = 0;
					grant.sent = new Date();
				}
				grant.save(function (err, grantSaved){
					try{
						if(err){
							jobLogs.errorLog.error({ task: 'send deal', message:'Deal sent, but saving the status of the grant failed', err: err, grant: grant });
							return callback(err);
						}
						callback(null, success);
					}
					catch(ex){
						callback(ex);
					}
				});
			}
			catch(ex){
				callback(ex);
			}
		},
		queue = async.queue(function (grant, callback){
			if(!grant.consecutiveSendErrors || grant.consecutiveSendErrors < 5){
				dealSender.send(grant, function (err, success){
					if(err) return callback(err);
					processed++;
					if(!success){
						sendErrors++;
					}
					callback();
				});		
			} else {
				callback();
			}
		}, SEND_QUEUE_CONCCURENCY);

	queue.drain = function (){
		if(sendErrors > 0){
			jobLogs.errorLog.error({ task: 'send deal', message:'dealSender ended with ' + sendErrors + ' errors out of ' + processed + ' sendings.' });
		} else {
			jobLogs.auditLog.info({ task: 'send deal', message:'dealSender successfully sent ' + processed + ' deals.' });
		}
		drained();
	};

	dealSender.send = function(grant, callback){
		try{
		    if(grant.channelType === 0){
		    	//compose variables for Facebook link
				facebook.sendDeal(grant, function (err){
					if(err) return finalizeDealSend(err, grant, callback);
					finalizeDealSend(null, grant, callback);
				});
		    } else {
		    	if(grant.previousHolders && grant.previousHolders.length > 0){
					mailComposer.giveDeal(grant, function (err){
						if(err) return finalizeDealSend(err, grant, callback);
						finalizeDealSend(null, grant, callback);
					});		    		
		    	} else {
					mailComposer.distributeDeal(grant, function (err){
						if(err) return finalizeDealSend(err, grant, callback);
						finalizeDealSend(null, grant, callback);
					});		    		
		    	}
		    }
		}
		catch(ex){
			jobLogs.errorLog.error(ex);
			callback(ex);
		}
	};

	dealSender.queueAndSend = function (done){
		var today = new Date();
		sendErrors = 0;
		processed = 0;
		drained = done;
			
		models.deal.find({ expiration: { $gt: today } }, { _id: 1 }, function (err, deals){
			try{
				if(err){
					jobLogs.errorLog.error({ task: 'send deal', message:'dealSender could not gather deals to be considered for grant distribution because of an error during the database request.', err : err });
					return done();
				}
				models.grant.find({ $or: [ { sent : { $exists: false } }, { sent: null } ] , deal: { $in: deals } } ).populate('deal', { teaser: 1, benefit: 1, emitter: 1 }).exec(function (err, grants){
					if(err) {
						jobLogs.errorLog.error({ task: 'send deal', message:'dealSender could not gather grants to be sent because of an error during the database request.', err : err });
						return done();
					}
					try{
						if(grants.length == 0){
							jobLogs.auditLog.info({ task: 'send deal', message:'dealSender skipped sending: no grant to be sent.' });
							return done();
						}

						//Gather the emitter of each deal
						var emitterIds = _.map(grants, function (grant){ return grant.deal.emitter });
						models.emitter.find({ _id: { $in: emitterIds } }, { name: 1, language: 1, identifiers: 1 }, function (err, emitters){
							try{
								if(err) {
									jobLogs.errorLog.error({ task: 'send deal', message:'dealSender could not gather emitters for the deals to distribute because of an error during the database request.', err : err });
									return done(err);
								}

								var emittersById = _.indexBy(emitters, '_id');
								_.each(grants, function (grant){
									grant.emitter = emittersById[grant.deal.emitter];
								});

								jobLogs.auditLog.info({ task: 'send deal', message:'dealSender starting to send ' + grants.length + ' deals.' });
								queue.push(grants);	
							}
							catch(ex){
								jobLogs.errorLog.error({ task: 'send deal', message:'Unexpected error.', err : ex });
								return done();
							}
						});
					}
					catch(ex){
						jobLogs.errorLog.error({ task: 'send deal', message:'Unexpected error.', err : ex });
						return done();
					}
				});
			}
			catch(ex){
				jobLogs.errorLog.error({ task: 'send deal', message:'Unexpected error.', err : ex });
				return done();
			}
		});
	};

	return dealSender;
}();