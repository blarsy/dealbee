module.exports = function (){
	var models = require(process.cwd() + '/api/models'),
		invoicer = require(process.cwd() + '/api/lib/invoicer'),
		jobLogs = require(process.cwd() + '/api/lib/jobs/joblogs'),
		_ = require('underscore');

	return{
		execute: function (done){
			//Gather clients with uninvoiced payments
			models.client.find({ payments: { $elemMatch: { 'invoice.lastDate': { $exists: false } } } }, 
				{ payments: 1, invoicingData: 1, packs: 1 }, function (err, clients){
				if(err) return done(err);

				jobLogs.info('Generate invoices', clients.length + ' clients with uninvoiced payments found.');

				//generate invoices
				invoicer.invoiceClients(clients, function (err, invoicingErrors, nbInvoicesGenerated){
					if(err) return done(err);

					_.each(invoicingErrors, function (invoicingError){
						jobLogs.handleJobError('Generate invoices', 'Invoice creation', invoicingError);
					});

					done(null, nbInvoicesGenerated, invoicingErrors.length);
				});
			});

		}
	}
}();