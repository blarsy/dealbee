module.exports = function(){
	var fb = require('fb'),
		models = require(process.cwd() + '/api/models'),
		hasher = require(process.cwd() + '/api/lib/hasher'),
		crypto = require('crypto'),
		_ = require('underscore'),
		facebook = require(process.cwd() + '/api/lib/facebook'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		session = require(process.cwd() + '/api/configuredsession'),
		identifierService = require(process.cwd() + '/api/lib/identifierService'),
		clientService = require(process.cwd() + '/api/lib/clientService'),
		result = {};

	result.makeDto = function (emitter){
		var dto;

		dto = { name: emitter.name, email: emitter.email, _id: emitter._id, 
			website: emitter.website, language: emitter.language,
			imageRef: emitter.imageRef };

		if(emitter.address && emitter.address.created){
			dto.address = emitter.address;
		}

		return dto;
	};

	result.checkPassword = function(password, emitter, callback){
		var identifier = _.find(emitter.identifiers, function (identifier){ return identifier.type === 1 });
		if(!identifier) return callback('LoginFailed');

		crypto.pbkdf2(password, identifier.hashInfo.salt, identifier.hashInfo.iterations, identifier.hashInfo.keySize, function(err, derivedKey){
			if(err) return callback(err);

			if(derivedKey.toString() == identifier.hashInfo.value){
				callback();
			} else {
				return callback('LoginFailed');
			}
		});
	};

	result.changePassword = function(emitter, newPassword, callback){
		hasher.hash(newPassword, function(err, identifier){
			if(err) return callback(err);
			
			var oldIdentifier = _.find(emitter.identifiers, function (identifier){ return identifier.type === 1 });
			emitter.identifiers.splice(emitter.identifiers.indexOf(oldIdentifier), 1);
			emitter.identifiers.push(identifier);

			emitter.save(function(err, emitterSaved){
				if(err) return callback(err);
				callback();
			});
		});
	};

	result.makeClientObject = function (emitter, identifier, client){
		return {
			emitter: result.makeDto(emitter),
			identifier: identifierService.makeDto(identifier),
			client: clientService.makeDto(client)
		}
	};

	return result;
}();