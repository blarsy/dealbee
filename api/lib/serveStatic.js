var fs = require('fs'),
    path = require('path'),
    mime = require('mime'),
    restify = require('restify');

module.exports = function (){
	var result = {};
	
	result.serveFileFromStats = function(file, err, stats, isGzip, opts, req, res, next) {
        if (err || !stats.isFile()) {
        	if(opts.fileNotFoundHandler){
        		if(stats && !stats.isFile()){
        			err = file + ' is not a file.'
        		}
        		return opts.fileNotFoundHandler(file, err, req, res, next);
        	} else {
        		return next(new restify.ResourceNotFoundError(file));
        	}
        }

        if (res.handledGzip && isGzip) {
            res.handledGzip();
        }

        var fstream = fs.createReadStream(file + (isGzip ? '.gz' : ''));
        var maxAge = opts.maxAge === undefined ? 3600 : opts.maxAge;
        fstream.once('open', function (fd) {
            res.cache({maxAge: maxAge});
            res.set('Content-Length', stats.size);
            res.set('Content-Type', mime.lookup(file));
            res.set('Last-Modified', stats.mtime);
            if (opts.charSet) {
                var type = res.getHeader('Content-Type') +
                    '; charset=' + opts.charSet;
                res.setHeader('Content-Type', type);
            }
            if (opts.etag) {
                res.set('ETag', opts.etag(stats, opts));
            }
            res.writeHead(200);
            fstream.pipe(res);
            fstream.once('end', function () {
                next(false);
            });
        });
    }

	result.serveNormal = function(file, opts, req, res, next) {
        fs.stat(file, function (err, stats) {
            if (!err && stats.isDirectory() && opts.default) {
                // Serve an index.html page or similar
                file = path.join(file, opts.default);
                fs.stat(file, function (dirErr, dirStats) {
                    result.serveFileFromStats(file,
                        dirErr,
                        dirStats,
                        false,
                        opts,
                        req,
                        res,
                        next);
                });
            } else {
                result.serveFileFromStats(file,
                    err,
                    stats,
                    false,
                    opts,
                    req,
                    res,
                    next);
            }
        });
    };

    return result;
}();