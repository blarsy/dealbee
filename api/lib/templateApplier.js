var fs = require('fs');

module.exports = {
	createFromTemplate: function (fileName, langCode, data, res, resourceSetName, done){
		fs.readFile(process.cwd() + fileName, 'utf8', function (err, template){
			if(err) return done(err);

			try{
				var regex = /_([^_ ]*)_/g,
					result = template,
					resObj = res[resourceSetName];
			
				while(match = regex.exec(template)){
					var replacement,
						replaceToken = match[1];
					if(data[replaceToken]){
						replacement = data[replaceToken];
					} else if(resObj && resObj[replaceToken]){
						replacement = resObj[replaceToken][langCode];
					} else if(res[replaceToken]) {
						replacement = res[replaceToken][langCode];
					} else {
						return done('Missing value for token \'' + replaceToken + '\'');
					}
					result = result.replace(match[0], replacement);
				};

				done(null, result);
			}
			catch(ex){
				done(ex);
			}
		});
	}
};