module.exports = function(){
	var pdfDocument = require('pdfkit'),
		models = require(process.cwd() + '/api/models'),
		_ = require('underscore'),
		resource = require(process.cwd() + '/api/res.json').invoice,
		streamBuffers = require("stream-buffers"),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		settings = require(process.cwd() + '/settings'),
		formatter = require(process.cwd() + '/api/lib/formatter'),
		remoteFolderProxy = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolderProxy'),
		async = require('async');

	function textIt(pdfDoc, text, font, fontSize, options){
		pdfDoc.fontSize(fontSize || 12);
		return pdfDoc.font(font || 'Times-Roman')
			.text(text, options);
	};

	function getSettings (done){
		models.settings.findOne({}, function (err, settings){
			if(err) return done(err);
			if(!settings) return done('Settings_Not_Found');

			return done(null, settings);
		});
	}

	function invoiceOnePayment(client, paymentUuid, done){
		var writableStream = new streamBuffers.WritableStreamBuffer({
			initialSize: (100 * 1024),
			incrementAmount: (10 * 1024)
		});

		makeForPayment(client, paymentUuid, writableStream, function (err){
			if(err) return done(err);

			client.save(function (err){
				if(err) return done(err);

				remoteFolderProxy.connectInvoiceFolder(function (err, invoiceFolder){
					if(err) return done(err);

					invoiceFolder.upload(writableStream.getContents(), paymentUuid + '.pdf', function(err){
						if(err) return done(err);

						done();
					});
				});
			});
		});
	}

	function makeForPayment (client, paymentUuid, stream, done){
		getSettings(function (err, dbSettings){
			if(err) return done(err);

			var pdfDoc = new pdfDocument(),
				payment = _.find(client.payments, function (payment){ return payment.uuid == paymentUuid; }),
				subscriptions = _.filter(client.packs, function (pack){ return pack.subscriptionKind && pack.payment === paymentUuid; }),
				packs = _.filter(client.packs, function (pack){ return !pack.subscriptionKind && pack.payment === paymentUuid }),
				invoicingAddress = client.invoicingData.address || client.address,
				invoicingAdress1 = invoicingAddress.street + (invoicingAddress.number ? ' ' + invoicingAddress.number : '') +
					(invoicingAddress.box ? ' - ' + invoicingAddress.box : ''),
				invoicingAdress2 = invoicingAddress.postcode + ' ' + invoicingAddress.city,
				mustSaveSettings = false,
				invoiceDetailX = 350,
				invoiceDetailLineInterval = 18,
				invoiceDetailCol1Y = 50,
				invoiceDetailCol2Y = 280,
				invoiceDetailCol3Y = 380,
				invoiceDetailCol4Y = 480,
				lang = client.invoicingData.language;
			
			if(!payment.invoice.number){
				payment.invoice.number = dbSettings.nextInvoiceNumber;
				dbSettings.nextInvoiceNumber ++;
				mustSaveSettings = true;
			}

			pdfDoc.pipe(stream);

			pdfDoc.image(process.cwd() + '/api/lib/logo.png', 50, 50, { height: 50 });
			pdfDoc.text(resource.aBrandOf[lang], 50, 110)
			textIt(pdfDoc, resource.motherCompanyName[lang], 'Helvetica-Bold', 14);

			textIt(pdfDoc, resource.address1[lang]);
			textIt(pdfDoc, resource.address2[lang]);
			textIt(pdfDoc, resource.country[lang]);
			textIt(pdfDoc, resource.vat[lang]).moveDown(0.5);

			textIt(pdfDoc, resource.clientTitle[lang], 'Helvetica-Bold', 16);
			textIt(pdfDoc, client.invoicingData.companyName + (client.invoicingData.juridicalForm ? ' ' + client.invoicingData.juridicalForm : ''));
			textIt(pdfDoc, invoicingAdress1);
			textIt(pdfDoc, invoicingAdress2);
			if(invoicingAddress.country){
				textIt(pdfDoc, invoicingAddress.country);
			}
			if(!client.invoicingData.hasNoVATNumber){
				textIt(pdfDoc, resource.vatLabel[lang] + (client.invoicingData.VATCountryCode ? client.invoicingData.VATCountryCode.toUpperCase() + '-' : '') + client.invoicingData.VATNumber);
			}

			pdfDoc.text(resource.dateLabel[lang], 350, 150).moveDown(0.5);
			textIt(pdfDoc, resource.invoiceNumberLabel[lang]);
			pdfDoc.text(bizTime.getDateString(payment.date, lang), 450, 150).moveDown(0.5);
			pdfDoc.text(payment.invoice.number);


			pdfDoc.moveTo(invoiceDetailCol1Y, invoiceDetailX - 5).lineTo(600, invoiceDetailX - 5).stroke();
			pdfDoc.font('Times-Bold').text(resource.descriptionColumnLabel[lang], invoiceDetailCol1Y, invoiceDetailX);
			pdfDoc.text(resource.quantityColumnLabel[lang], invoiceDetailCol2Y, invoiceDetailX, { width: invoiceDetailCol3Y - invoiceDetailCol2Y, align: 'center'});
			pdfDoc.text(resource.unitPriceColumnLabel[lang], invoiceDetailCol3Y, invoiceDetailX, { width: invoiceDetailCol4Y - invoiceDetailCol3Y, align: 'right'});
			pdfDoc.text(resource.amountColumnLabel[lang], invoiceDetailCol4Y, invoiceDetailX, { width: 600 - invoiceDetailCol4Y, align: 'right'});
			invoiceDetailX += invoiceDetailLineInterval;
			pdfDoc.moveTo(invoiceDetailCol1Y, invoiceDetailX - 5).lineTo(600, invoiceDetailX - 5).stroke();
			
			if(subscriptions.length > 0){
				pdfDoc.font('Times-Roman').text(resource.subscriptionLabel[lang] + subscriptions[0].subscriptionKind, invoiceDetailCol1Y, invoiceDetailX);
				pdfDoc.text(subscriptions.length, invoiceDetailCol2Y, invoiceDetailX, { width: invoiceDetailCol3Y - invoiceDetailCol2Y, align: 'center'});
				pdfDoc.text(formatter.price(subscriptions[0].price, lang), invoiceDetailCol3Y, invoiceDetailX, { width: invoiceDetailCol4Y - invoiceDetailCol3Y, align: 'right'});
				pdfDoc.text(formatter.price(subscriptions.length * subscriptions[0].price, lang), invoiceDetailCol4Y, invoiceDetailX, { width: 600 - invoiceDetailCol4Y, align: 'right'});
				invoiceDetailX += invoiceDetailLineInterval;
			}

			var packsPerAmountCoupons = _.groupBy(packs, function (pack){ return pack.amount; });
			_.each(packsPerAmountCoupons, function (packs){ 
				pdfDoc.font('Times-Roman').text(resource.packLabel[lang] + packs[0].amount + resource.coupons[lang], invoiceDetailCol1Y, invoiceDetailX);
				pdfDoc.text(packs.length, invoiceDetailCol2Y, invoiceDetailX, { width: invoiceDetailCol3Y - invoiceDetailCol2Y, align: 'center'});
				pdfDoc.text(formatter.price(packs[0].price, lang), invoiceDetailCol3Y, invoiceDetailX, { width: invoiceDetailCol4Y - invoiceDetailCol3Y, align: 'right'});
				pdfDoc.text(formatter.price(packs.length * packs[0].price, lang), invoiceDetailCol4Y, invoiceDetailX, { width: 600 - invoiceDetailCol4Y, align: 'right'});
				invoiceDetailX += invoiceDetailLineInterval;
			 });
			pdfDoc.moveTo(invoiceDetailCol1Y, invoiceDetailX - 5).lineTo(600, invoiceDetailX - 5).stroke();

			if(payment.vatRate > 1){
				pdfDoc.font('Times-Bold').text(resource.subTotalLabel[lang], invoiceDetailCol3Y, invoiceDetailX);
				pdfDoc.font('Times-Roman').text(formatter.price(payment.amount, lang), invoiceDetailCol4Y, invoiceDetailX, { width: 600 - invoiceDetailCol4Y, align: 'right'});
				invoiceDetailX += invoiceDetailLineInterval;
				pdfDoc.font('Times-Bold').text(resource.vatRateLabel[lang], invoiceDetailCol3Y, invoiceDetailX);
				pdfDoc.font('Times-Roman').text(((payment.vatRate - 1) * 100).toFixed(0) + '%', invoiceDetailCol4Y, invoiceDetailX, { width: 600 - invoiceDetailCol4Y, align: 'right'});
				invoiceDetailX += invoiceDetailLineInterval;
				pdfDoc.font('Times-Bold').text(resource.vatAmountLabel[lang], invoiceDetailCol3Y, invoiceDetailX);
				pdfDoc.font('Times-Roman').text(formatter.price(payment.amount * (payment.vatRate - 1), lang), invoiceDetailCol4Y, invoiceDetailX, { width: 600 - invoiceDetailCol4Y, align: 'right'});
				invoiceDetailX += invoiceDetailLineInterval;
			} else {
				pdfDoc.font('Times-Bold').text(resource.vatRateLabel[lang], invoiceDetailCol3Y, invoiceDetailX);
				pdfDoc.font('Times-Roman').text(resource.noVATOnInvoiceLabel[lang], invoiceDetailCol4Y, invoiceDetailX, { width: 600 - invoiceDetailCol4Y, align: 'right'});
				invoiceDetailX += invoiceDetailLineInterval;					
			}

			pdfDoc.font('Times-Bold').text(resource.totalLabel[lang], invoiceDetailCol3Y, invoiceDetailX);
			pdfDoc.font('Times-Bold').text(formatter.price(payment.paid, lang), invoiceDetailCol4Y, invoiceDetailX, { width: 600 - invoiceDetailCol4Y, align: 'right'});

			invoiceDetailX += 30;
			pdfDoc.fontSize(16);
			pdfDoc.font('Helvetica-Bold').text(resource.receivedLabel[lang], invoiceDetailCol1Y, invoiceDetailX);

			payment.invoice.lastDate = bizTime.now();

			if(mustSaveSettings){
				dbSettings.save(function (err){
					if(err) return done(err);
					stream.on('finish', done);
					pdfDoc.end();
				});
			} else {
				stream.on('finish', done);		
				pdfDoc.end();
			}
		});
	};

	return {
		invoiceOnePayment: invoiceOnePayment,
		invoiceClients: function (clients, done){
			var invoicingOperations = [],
				invoicingErrors = [],
				nbInvoiceGenerated = 0;
			_.each(clients, function (client){
				var uninvoicedPayments = _.filter(client.payments, function (payment){ return !payment.invoice.lastDate; });

				_.each(uninvoicedPayments, function (payment){
					invoicingOperations.push(function (callback){
						invoiceOnePayment(client, payment.uuid, function (err){
							if(err){
								invoicingErrors.push('Invoicing for payment with uuid "' + payment.uuid + '" failed with error :' + err);
							} else {
								nbInvoiceGenerated ++;
							}

							callback();
						});
					});					
				});

			});
			
			async.series(invoicingOperations, function (err){
				return done(err, invoicingErrors, nbInvoiceGenerated);
			})
		},
		makeForPayment: makeForPayment
	};
}();