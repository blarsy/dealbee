module.exports = function(){
	return {
		now: function(){
			return new Date();
		},
		getDateHash: function(date){
			var month = '0' + date.getMonth(),
				day = '0' + date.getDate();

			return date.getFullYear() + month.substring(month.length - 2) + day.substring(day.length - 2);
		},
		getDateTimeHash: function(date){
			var month = '0' + date.getMonth(),
				day = '0' + date.getDate(),
				hour = '0' + date.getHours(),
				minute = '0' + date.getMinutes(),
				second = '0' + date.getSeconds(),
				mSecond = '00' + date.getMilliseconds();

			return date.getFullYear() + month.substring(month.length - 2) + day.substring(day.length - 2) + '-' +
				hour.substring(hour.length - 2) + minute.substring(minute.length - 2) + 
				second.substring(second.length - 2) + mSecond.substring(mSecond.length - 3);
		},
		addMonths: function(date, nbMonths){
			return new Date(date.getFullYear(), date.getMonth() + nbMonths, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
		},
		addDays: function(date, nbDays){
			return new Date(date.getFullYear(), date.getMonth(), date.getDate() + nbDays, date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
		},
		getDateString: function(date, lang){
			var month = '0' + (date.getMonth() + 1),
				day = '0' + date.getDate();
			if(lang === 'en'){
				return month.substring(month.length - 2) + '-' + day.substring(day.length - 2) + '-' + date.getFullYear();
			} else {
				return day.substring(day.length - 2) + '/' + month.substring(month.length - 2) + '/' + date.getFullYear();
			}
		}
	}
}();