module.exports = function (){
	var result = {},
		hasher = require(process.cwd() + '/api/lib/hasher'),
		facebook = require(process.cwd() + '/api/lib/facebook'),
		mailComposer = require(process.cwd() + '/api/lib/mailassets/mailComposer'),
		models = require(process.cwd() + '/api/models'),
		_ = require('underscore'),
		uuid = require('node-uuid'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		clientService = require(process.cwd() + '/api/lib/clientService');

	result.createIdentifierAndClient = function (identifier, done){
		models.identifier.create(identifier, function (err, identifierSaved){
			if(err) return done(err);

			var client = { created: bizTime.now() };
			clientService.setupSubscription({ type: 'XS', paymentFrequency: 1 }, null, client);

			models.client.create(client, function (err, clientSaved){
				if(err) return done(err);
				
				identifierSaved.client = clientSaved;
				identifierSaved.save(function (err, identifierLinkedToClient){
					if(err) return done(err);

					done(null, identifierLinkedToClient);
				});
			});
		});
	};

	result.create = function (identifierInfo, done){
		if(identifierInfo.fbToken){
			//Check if identifier already exists
			facebook.makeIdentifier(identifierInfo.fbToken, identifierInfo.email, function (err, identifier){
				if(err) return done(err);

				models.identifier.findOne({ type: 0, fbUid: identifier.fbUid }, function (err, identifierFound){
					if(err) return done(err);
					if(identifierFound) return done(null, identifierFound);

					result.createIdentifierAndClient(identifier, done);
				});					
			});
		} else {
			models.identifier.findOne({ email: identifierInfo.email }, function (err, identifierFound){
				if(err) return done(err);
				if(identifierFound) return done('Email_Already_Used');

				hasher.hash(identifierInfo.password, identifierInfo.email, function(err, identifier){
					if(err) return done(err);

					result.createIdentifierAndClient(identifier, done);
				});
			});
		}
	};

	result.ensureHasDefaultEmitter = function (identifierId, done){
		models.identifier.findOne({ _id: identifierId }, function (err, identifier){
			if(err) return done(err);

			if(!identifier.defaultEmitter){
				models.emitter.findOne({ identifiers: identifierId }, function (err, emitter){
					if(err) return done(err);

					identifier.defaultEmitter = emitter._id;
					identifier.save(function (err){
						if(err) return done(err);
						done(null, identifier);
					});
				});
			} else {
				done(null, identifier);
			}			
		});
	};

	result.linkEmitters = function (identifier, done){
		models.emitter.find({ identifiers: identifier._id }).lean().exec(function (err, emitters){
			if(err) return done(err);
			
			identifier.emitters = emitters;

			done(null, identifier);
		});
	};

	result.makeDto = function (identifier){
		var dto = {
			_id: identifier._id,
			email: identifier.email,
			type: identifier.type,
			emitters: []
		};
		if(identifier.name){
			dto.name = identifier.name;
		}
		_.each(identifier.emitters, function (emitter){
			dto.emitters.push({ _id: emitter._id, name: emitter.name });
		});
		return dto;
	};

	result.inviteManager = function (emitter, requestor, newManagerEmail, done){
		var invitationData = {
			emitter: emitter,
			uuid: uuid.v4(),
			requestor: requestor,
			newManagerEmail: newManagerEmail,
			created: bizTime.now()
		};

		models.managerInvitation.create(invitationData, function(err, invitationSaved){
			if(err) return done(err);

			mailComposer.inviteManager(invitationSaved, done);
		});
	};

	return result;
}();