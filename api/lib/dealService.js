var models = require(process.cwd() + '/api/models'),
	_ = require('underscore');

function getDealForDisplay(dealId, done){
	models.deal.findOne({ _id: dealId }, { teaser:1, benefit: 1, termsOfUse: 1, expiration: 1, beginDate: 1, imageRef: 1, description: 1, emitter: 1, visibility: 1, nbOnRequest: 1 }).lean().exec(function (err, deal){
		if(err) return done(err);

		models.emitter.findOne({ _id: deal.emitter }, { address: 1, name: 1, website: 1, imageRef: 1, language: 1 }).lean().exec(function (err, emitter){
			if(err) return done(err);

			deal.emitter = emitter;
			done(null, deal);
		});
	});
};

module.exports = {
	getDealWithGrantsPerEmitter: function (emitterId, done){
		models.deal.find({ 'emitter': emitterId }).lean().exec(function (err, deals){
			if(err) return done(err);

			var dealIds = _.map(deals, function (deal){ return deal._id; });

			models.grant.find({ deal: { $in: dealIds } }, function (err, grants){
				if(err) return done(err);

				var grantsPerDeal = _.groupBy(grants, function (grant){ return grant.deal; });
				_.each(deals, function (deal){
					deal.grants = grantsPerDeal[deal._id];
				});

				done(null, deals);
			});
		});
	},
	getDealWithGrants: function (dealId, emitterId, done){
		var query = { _id: dealId };
		if(emitterId){
			query.emitter = emitterId;
		}
		models.deal.findOne(query).lean().exec(function (err, deal){
			if(err) return done(err);
			if(!deal) return done(null, null);

			models.grant.find({ deal: dealId }, function (err, grants){
				if(err) return done(err);

				deal.grants = grants;

				done(null, deal);
			});
		});
	},
	getGrantForDisplay: function (grantId, done){
		models.grant.findOne({ uuid: grantId }).lean().exec(function (err, grant){
			if(err)	return done(err);
			if(!grant) return done(null, null);

			getDealForDisplay(grant.deal, function (err, deal){
				if(err) return done(err);

				grant.deal = deal;
				done(null, grant);
			});
		});		
	},
	getDealForDisplay: getDealForDisplay,
	getDealMetadata: function (dealId, done){
		models.deal.findOne({ _id: dealId }, { keywords: 1, private: 1 }, function (err, dealFound){
			if(err) return done(err);

			done(null, dealFound);
		});
	}
};