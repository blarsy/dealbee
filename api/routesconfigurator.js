module.exports = function (server){
	var restify = require('restify'),
		session = require(process.cwd() + '/api/configuredsession'),
		emitterRoutes = require(process.cwd() + '/api/routes/emitter'),
		sessionRoutes = require(process.cwd() + '/api/routes/session'),
		dealRoutes = require(process.cwd() + '/api/routes/deal'),
		distributeRoutes = require(process.cwd() + '/api/routes/distribute'),
		dealHolderRoutes = require(process.cwd() + '/api/routes/dealholder'),
		resourceRoutes = require(process.cwd() + '/api/routes/resource'),
		fbUserRoutes = require(process.cwd() + '/api/routes/fbUser'),
		grantRoutes = require(process.cwd() + '/api/routes/grant'),
		appRoutes = require(process.cwd() + '/api/routes/app'),
		prefRoutes = require(process.cwd() + '/api/routes/pref'),
		messageRoutes = require(process.cwd() + '/api/routes/message'),
		maillistRoutes = require(process.cwd() + '/api/routes/maillist'),
		jobRoutes = require(process.cwd() + '/api/routes/job'),
		errorRoutes = require(process.cwd() + '/api/routes/error'),
		sessionLogRoutes = require(process.cwd() + '/api/routes/sessionLog'),
		backupRoutes = require(process.cwd() + '/api/routes/backup'),
		priceRoutes = require(process.cwd() + '/api/routes/price'),
		packRoutes = require(process.cwd() + '/api/routes/pack'),
		subscriptionRoutes = require(process.cwd() + '/api/routes/subscription'),
		creditCardRoutes = require(process.cwd() + '/api/routes/creditcard'),
		orderRoutes = require(process.cwd() + '/api/routes/order'),
		qrRoutes = require(process.cwd() + '/api/routes/qr'),
		invoiceRoutes = require(process.cwd() + '/api/routes/invoice'),
		settingsRoutes = require(process.cwd() + '/api/routes/settings'),
		managerRoutes = require(process.cwd() + '/api/routes/manager'),
		clientRoutes = require(process.cwd() + '/api/routes/client'),
		identifierRoutes = require(process.cwd() + '/api/routes/identifier');

	server.use(restify.bodyParser());
	server.use(restify.queryParser());
	server.use(session.sessionManager);

	//routes go here
	server.post('/api/emitter', emitterRoutes.post);
	server.get('/api/session', sessionRoutes.get);
	server.post('/api/session', sessionRoutes.post);
	server.post('/api/session/switchEmitter', sessionRoutes.switchEmitter);

	server.get('/api/deal', dealRoutes.get);
	server.post('/api/deal', dealRoutes.post);
	server.del('/api/deal', dealRoutes.del);
	server.get('/api/grant', grantRoutes.get);
	server.post('/api/deal/metadata', dealRoutes.postMetadata)
	server.get('/api/deal/publicInfo/:id', dealRoutes.publicInfo);
	server.post('/api/grant/giveByEmail', grantRoutes.giveByEmail);
	server.post('/api/grant/giveByFacebook', grantRoutes.giveByFacebook);
	server.post('/api/grant/confirmGift', grantRoutes.confirmGift);
	server.post('/api/grant/scan', grantRoutes.scan);
	server.post('/api/grant', grantRoutes.post);
	server.post('/api/deal/consume', dealRoutes.consume);
	server.post('/api/fbUser', fbUserRoutes.post);
	server.get('/api/invoice/:uuid', invoiceRoutes.get);
	server.post('/api/invoice', invoiceRoutes.post);

	server.post('/api/distribute', distributeRoutes.post);

	server.get('/api/dealholder', dealHolderRoutes.get);

	server.get('/api/preference', prefRoutes.get);
	server.post('/api/preference', prefRoutes.post);

	server.post('/api/message', messageRoutes.post);

	server.post('/api/maillist', maillistRoutes.post);
	server.post('/api/emitter/changepassword', emitterRoutes.changePassword);
	server.post('/api/emitter/forgotpassword', emitterRoutes.forgotPassword);
	server.post('/api/emitter/recoverpassword', emitterRoutes.recoverPassword);

	server.post('/api/error', errorRoutes.post);
	server.get('/api/price', priceRoutes.get);
	server.post('/api/pack', packRoutes.post);
	server.post('/api/subscription', subscriptionRoutes.post);
	server.get('/api/creditcard', creditCardRoutes.get);
	server.post('/api/creditcard', creditCardRoutes.post);
	server.post('/api/order', orderRoutes.post);
	server.get('/api/qr', qrRoutes.get);
	server.get('/api/manager', managerRoutes.get);
	server.post('/api/manager', managerRoutes.post);
	server.del('/api/manager', managerRoutes.del);
	server.get('/api/client/payments', clientRoutes.getPayments);
	server.post('/api/client/invoicingData', clientRoutes.postInvoicingData);

	//power user routes
	server.post('/api/resource', resourceRoutes.post);
 
	//Admin routes
	server.post('/api/session/admin', sessionRoutes.admin);
	server.get('/api/emitter', emitterRoutes.get);
	server.del('/api/emitter', emitterRoutes.del);
	server.post('/api/grant/resend', grantRoutes.resend);
	server.post('/api/grant/resendGiftConfirmation', grantRoutes.resendGiftConfirmationEmail);
	server.get('/api/app', appRoutes.get);
	server.post('/api/app', appRoutes.post);
	server.get('/api/job', jobRoutes.get);
	server.get('/api/sessionLog', sessionLogRoutes.get);
	server.get('/api/backup', backupRoutes.get);
	server.del('/api/backup', backupRoutes.del);
	server.post('/api/backup', backupRoutes.post);
	server.post('/api/backup/full', backupRoutes.full);
	server.post('/api/deal/togglePrivate', dealRoutes.togglePrivate);
	server.post('/api/job/renewSubscriptions', jobRoutes.renewSubscriptions);
	server.get('/api/settings', settingsRoutes.get);
	server.post('/api/settings', settingsRoutes.post);
	server.get('/api/identifier', identifierRoutes.get);
	server.get('/api/client', clientRoutes.get);
};