var restify = require('restify'),
	settings = require(process.cwd() + '/settings'),
	serveStaticPlugin = require(process.cwd() + '/api/lib/serveStaticPlugin'),
    serveStatic = require(process.cwd() + '/api/lib/serveStatic'),
    routesConfigurator = require(process.cwd() + '/api/routesconfigurator'),
    path = require('path'),
    templateApplier = require(process.cwd() + '/api/lib/templateApplier'),
    facebook = require(process.cwd() + '/api/lib/facebook'),
    scraper = require(process.cwd() + '/api/lib/scraper'),
    toDefaultPage = function (file, err, req, res, next){
        serveStatic.serveNormal(path.join(settings.WEBAPPPATH, 'index.html'), {}, req, res, next);
    },
	forceSsl = function (req, res, next){
		if (req.headers['x-forwarded-proto'] !== 'https') {
			res.header('Location', ['https://', req.header('Host'), req.url].join(''));
			return res.send(302);
		}
		return next();
	},
	logConfigurator = require(process.cwd() + '/api/logConfigurator'),
	app,
	server = {},
	prerender = require(process.cwd() + '/api/lib/prerender-restify/'),
	prerenderPipeline = prerender.set('prerenderToken', 'yx1dL1yjsG7Q69trBrua').blacklisted(['^/viewdeal', '^/$', '^$']),
	dependenciesOverrider = require(process.cwd() + '/api/lib/dependenciesOverrider');

server.inMaintenance = false;
server.configure = function(){
	app = restify.createServer({
		name: 'Dealbee'
	});
	if(settings.FORCESSL) 
	{ 
		app.use(forceSsl); 
	}
	app.use(prerenderPipeline);

	//This static file handler matches any 'admin/' route
	app.get(/^\/?admin(\/(.*))?$/, serveStaticPlugin({
		directory: settings.ROOTPATH,
		default: 'index.html'
	}));

	//This static file handler matches any route except those starting with 'api/' or 'admin/'
	app.get(/^(?!\/(api|admin)\/)(.*)$/, function(req, res, next){
		var scrapeType = scraper.getScrapeInfo(req);
		if(scrapeType){
			scraper.scrape(scrapeType, req, function (err, content){
				if(err) return(next(err));

				res.setHeader("Content-Type", "text/html");
				res.writeHead(200);
				res.write(content);
				res.end();
				next();
			});
		} else {
			serveStaticPlugin({
				directory: settings.WEBAPPPATH,
				default: 'index.html',
				fileNotFoundHandler: toDefaultPage
			})(req, res, next);
		}
	});

	//Configure REST routes
	routesConfigurator(app);
	logConfigurator(app);
};
server.start = function(port, ip, done){
	app.listen(port, ip, done);
}

module.exports = server;