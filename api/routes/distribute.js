module.exports = function (){
	var restify = require('restify'),
		_ = require('underscore'),
		models = require(process.cwd() + '/api/models'),
		uuid = require('node-uuid'),
		facebook = require(process.cwd() + '/api/lib/facebook'),
		async = require('async'),
		emitterService = require(process.cwd() + '/api/lib/emitterService'),
		clientService = require(process.cwd() + '/api/lib/clientService'),
		authorize = require(process.cwd() + '/api/lib/authorize'),
		resources = require(process.cwd() + '/api/res.json'),
		grantService = require(process.cwd() + '/api/lib/grantService'),
		validate = function(req, next){
			var messages = [],
				hasFbGrants = false,
				senderFbUids = [];

			if(!req.body.dealId){
				message.push('DealId_Required');
			}

			if(req.body.amountOnRequest !== undefined){
				if(new Number(req.body.amountOnRequest) === NaN){
					messages.push('Invalid_Amount_On_Request');
				} else if(new Number(req.body.amountOnRequest) < 1){
					messages.push('Amount_On_Request_Must_Be_Greater_Than_Zero');
				}
			} else {
				if(!req.body.grants || !req.body.grants instanceof Array){
					messages.push('Array_Of_Grants_Required');
				}
			}
			
			if(req.body.grants && req.body.grants instanceof Array){
				if(req.body.grants.length === 0){
					messages.push('At_Least_One_Grant_Required');
				} else {
					_.each(req.body.grants, function (grant){
						if(grant.channelType == null || grant.channelType == undefined){
							messages.push('Grant_ChannelType_Required');
						} else {
							if(grant.channelType !== 0 && grant.channelType !== 1){
								messages.push('Grant_ChannelType_Invalid');
							} else {
								if(grant.channelType === 0){

									hasFbGrants = true;

									if(!grant.fbDestinatorUser){
										messages.push('Grant_FbUser_Required');
									} else {
										if(!grant.fbDestinatorUser.tagId){
											messages.push('Grant_FbUser_tagId_Required');
										}
										if(!grant.fbDestinatorUser.name){
											messages.push('Grant_FbUser_Name_Required');
										}
										if(!grant.fbDestinatorUser.picUrl){
											messages.push('Grant_FbUser_PicUrl_Required');
										}
										if(!grant.fbDestinatorUser.taggedMessage){
											messages.push('Grant_FbUser_TaggedMessage_Required')
										}
									}
									if(!grant.senderFbUid){
										senderFbUids.push(grant.senderFbUid);
										messages.push('Grant_SendFbUid_Required')
									}
								} else {
									if(!grant.email) {
										messages.push('Grant_Email_Required');
									}
								}
							}
						}
					});
				}

				if(req.body.grants.length > req.session.loggedInEmitter.balance) return next(new restify.InternalError('Insufficient_Credits'));

				if(hasFbGrants){
					var checks = [];
					_.each(senderFbUids.uniq, function (senderFbUid){
						checks.push(function (callback){
							facebook.checkPublishPermission(senderFbUid, callback);
						});
					});
					async.parallel(checks, function (err, results){
						if(err) return next(err);

						return next();
					});
				} else {
					return next();
				}
			} else {
				if(messages.length > 0) return next(new restify.InternalError(messages.join(', ')));
				return next();			
			}
		},
		result = {};

	result.post = function (req, res, next){
		authorize.ensureAuthenticated(req, res, function (err){
			if(err) return next(new restify.InternalError(err));

			validate(req, function(err){
				if(err) return next(err);

				models.deal.findOne({ _id: req.body.dealId }, function (err, deal){
					if(err) return next(err);
					//Check that the deal is one from the currently logged in emitter
					if(deal.emitter.toString() !== req.session.loggedInEmitter._id) return next(new restify.InternalError('Deal_Not_Found'));
					//The deal should not be expired
					if(deal.expiration < new Date()) return next(new restify.InternalError('Deal_Expired'));

					var operations = [],
						creditsToRemoveFromBalance = 0;
					if(deal.nbOnRequest === undefined){
						deal.nbOnRequest = 0;
					}

					if(req.body.grants){
						operations.push(function (callback){
							grantService.createMany(req.body.dealId, req.body.grants, req.session.loggedInEmitter.language, function (err, amountCreated){
								if(err) return callback(err);

								creditsToRemoveFromBalance += amountCreated;
								callback();
							});
						});
					}

					if(req.body.amountOnRequest !== undefined){
						operations.push(function (callback){
							creditsToRemoveFromBalance += req.body.amountOnRequest;
							deal.nbOnRequest += req.body.amountOnRequest;
							
							deal.save(function (err, dealSaved){
								if(err) return callback(err);

								callback();
							});
						});
					}

					operations.push(function (callback){
						clientService.recordDistribution(req.session.loggedInIdentifier.client, creditsToRemoveFromBalance, new Date(), function (err, modifiedClient){
							if(err) return callback(err);

							callback(null, modifiedClient);
						});
					});

					async.waterfall(operations, function (err, modifiedClient){
						if(err) return next(err);

						clientService.saveLoggedInClient(modifiedClient, req, function (err, clientSaved){
							if(err) return next(err);

							res.send(emitterService.makeClientObject(req.session.loggedInEmitter, req.session.loggedInIdentifier, clientSaved));
							next();
						})
					});
				})
			});
		});
	};

	return result;
}();
