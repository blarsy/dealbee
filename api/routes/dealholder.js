module.exports = function (){
	var models = require(process.cwd() + '/api/models'),
		_ = require('underscore'),
		authorize = require(process.cwd() + '/api/lib/authorize');

	return {
		get: function (req, res, next){
			authorize.ensureAuthenticated(req, res, function(err){
				if(err) return next(new restify.InternalError(err));

				models.deal.find({ emitter: req.session.loggedInEmitter._id, expiration: { $gt: new Date() } }, { _id: 1 }, function (err, deals){
					if(err) return next(err);

					var dealIds = _.pluck(deals, '_id');

					models.grant.find({ deal: { $in: dealIds }, sent : { $exists: true }, consumed: { $exists: false } }).populate('deal').exec(function (err, grants){
						if(err) return next(err);

						var grantByDestinator = _.groupBy(grants, function (grant){ 
								if(grant.channelType === 0){
									return grant.fbDestinatorUser.name;
								} else {
									return grant.email;
								}
							}),
							pairs = _.pairs(grantByDestinator),
							result = _.map(pairs, function (pair){
								var dealHolder = { channelType: pair[1][0].channelType, deals: pair[1] };
								if(dealHolder.channelType === 0){
									dealHolder.holder = dealHolder.deals[0].fbDestinatorUser.name;
									dealHolder.picUrl = dealHolder.deals[0].fbDestinatorUser.picUrl;
								} else {
									dealHolder.holder = dealHolder.deals[0].email;
								}
								return dealHolder;
							});

						res.send(result);
						next();
					});
				});
			});
		}
	};
}();