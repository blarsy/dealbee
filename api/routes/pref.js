var restify = require('restify'),
	models = require(process.cwd() + '/api/models'),
	_ = require('underscore'),
	authorize = require(process.cwd() + '/api/lib/authorize');

module.exports = {
	get: function (req, res, next){
		if(req.params.email){
			authorize.ensureAdmin(req, res, function (err, sessionData){
				if(err) return next(new restify.InternalError(err));

				models.preference.find({ email: { $regex: '.*' + req.params.email + '.*', $options: 'i' } }).lean().exec(function (err, preferencesFound){
					if(err) return next(err);
					if(!preferencesFound){
						res.send([]);
					} else {
						var emitters = [];
						_.each(preferencesFound, function (preference){
							if(preference.blockedEmitters){
								emitters.push.apply(emitters, preference.blockedEmitters);
							}
						});

						models.emitter.find({ _id: { $in: _.uniq(emitters) } }, { name: 1 }, function (err, emittersFound){
							if(err) return next(err);

							emitters = _.indexBy(emittersFound, '_id');
							_.each(preferencesFound, function (preference){
								if(preference.blockedEmitters){
									preference.blockedEmittersWithName = [];
									_.each(preference.blockedEmitters, function (blockedEmitter){
										var emitterWithName = emitters[blockedEmitter.toString()];
										preference.blockedEmittersWithName.push({ _id: emitterWithName._id, name: emitterWithName.name });
									})
								}
							});

							res.send(preferencesFound);
						});
					}
					next();
				});
			})
		} else {
			if(!req.params.grantId) return next(new restify.InternalError('GrantId_Required'));

			models.grant.findOne({ uuid: req.params.grantId }, { email: 1, deal: 1, channelType: 1 }).populate('deal').exec(function (err, grant){
				if(err) return next(err);
				if(!grant) return next();
				if(grant.channelType !== 1) return next(new restify.InternalError('WrongGrantType'));

				models.preference.findOne({ email: grant.email }).lean().exec(function (err, preferenceFound){
					if(err) return next(err);

					if(preferenceFound){
						if(_.find(preferenceFound.blockedEmitters, function (blockedEmitter){ return blockedEmitter.id === grant.deal.emitter.id })){
							preferenceFound.unsubscribedFromEmitter = true;
						} else {
							preferenceFound.unsubscribedFromEmitter = false;
						}
						res.send(preferenceFound);
					} else {
						res.send(200, {});
					}
					next()
				});
			});
		}
	},
	post: function (req, res, next){
		if(!req.body.grantId) return next(new restify.InternalError('GrantId_Required'));
		if(req.body.unsubscribed === undefined) return next(new restify.InternalError('Unsubscribed_Required'));
		if(req.body.blockEmitter === undefined) return next(new restify.InternalError('BlockEmitter_Required'));

		models.grant.findOne({ uuid: req.body.grantId }, { email: 1, channelType: 1, deal: 1 }).populate('deal').exec(function (err, grant){
			if(err) return next(err);
			if(grant.channelType !== 1) return next(new restify.InternalError('WrongGrantType'));

			models.preference.findOne({ email: grant.email }, function (err, preferenceFound){
				if(err) return next(err);
				var preferenceToSave,
					mustSave, 
					emitterBlocked;

				if(!preferenceFound){
					mustSave = true;
					preferenceToSave = { 
						email: grant.email, 
						unsubscribedDate: req.body.unsubscribed ? new Date() : undefined, 
						blockedEmitters: req.body.blockEmitter ? [ grant.deal.emitter ] : undefined
					};
				} else {
					preferenceToSave = preferenceFound;
					if(req.body.unsubscribed && !preferenceFound.unsubscribedDate){
						preferenceFound.unsubscribedDate = new Date();
						mustSave = true;
					} else if(!req.body.unsubscribed && preferenceFound.unsubscribedDate){
						preferenceFound.unsubscribedDate = undefined;
						mustSave = true;
					}
					emitterBlocked = _.find(preferenceFound.blockedEmitters, function (blockedEmitter){
						return blockedEmitter.id === grant.deal.emitter.id;
					});
					if(emitterBlocked && !req.body.blockEmitter){
						preferenceFound.blockedEmitters = _.without(preferenceFound.blockedEmitters, emitterBlocked);
						mustSave = true;
					} else if(!emitterBlocked && req.body.blockEmitter){
						preferenceFound.blockedEmitters.push(grant.deal.emitter);
						mustSave = true;
					}
				}
				if(mustSave){
					models.preference.create(preferenceToSave, function (err, savedPreference){
						if(err) return next(err);

						res.send(savedPreference);
						next();
					})					
				} else {
					res.send(200, 'NothingChanged');
					next();
				}
			});

			next();
		});
	}
};