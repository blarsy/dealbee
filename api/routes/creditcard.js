module.exports = function (){
	var result = {},
		authorize = require(process.cwd() + '/api/lib/authorize'),
		restify = require('restify'),
		models = require(process.cwd() + '/api/models'),
		settings = require(process.cwd() + '/settings');

	function cardFromStripeCustomer(stripeCustomer){
		var cardData = stripeCustomer.sources.data[0];
		return {
			cardNumber: cardData.last4,
			expirationMonth: cardData.exp_month,
			expirationYear: cardData.exp_year		
		};
	}

	result.post = function (req, res, next){
		authorize.ensureAuthenticated(req, res, function (err){
			if(err) return next(new restify.InternalError(err));

			if(!req.body.cardNumber) return next(new restify.InternalError('CardNumber_Required'));
			if(!req.body.expirationYear) return next(new restify.InternalError('ExpirationYear_Required'));
			if(!req.body.expirationMonth) return next(new restify.InternalError('ExpirationMonth_Required'));
			if(!req.body.cvc) return next(new restify.InternalError('Cvc_Required'));

			models.client.findOne({ _id: req.session.loggedInClient._id }, { stripeCustomerId: 1 }, function (err, clientFound){
				if(err) return next(err);

				var stripe = require("stripe")(settings.STRIPEAPIKEY),
					customerFn,
					customer = {
						description: req.session.loggedInClient._id,
						source: {
							object: 'card',
							number: req.body.cardNumber,
							exp_month: req.body.expirationMonth,
							exp_year: req.body.expirationYear,
							cvc: req.body.cvc
						}
					};

				if(clientFound.stripeCustomerId){
					customerFn = function (callback){
						stripe.customers.update(clientFound.stripeCustomerId, customer, callback);
					};
				} else {
					customerFn = function (callback){
						stripe.customers.create(customer, callback);
					};
				}

				customerFn(function(err, customer) {
					if(err) return next(err);

					if(!clientFound.stripeCustomerId){
						clientFound.stripeCustomerId = customer.id;
						clientFound.save(function (err, saved){
							if(err) return next(err);

							res.send(cardFromStripeCustomer(customer));
							next();
						});
					} else {
						res.send(cardFromStripeCustomer(customer));
						next();
					}
				});
			});
		});
	};

	result.get = function (req, res, next){
		authorize.ensureAuthenticated(req, res, function (err){
			if(err) return next(new restify.InternalError(err));

			models.client.findOne({ _id: req.session.loggedInClient._id }, { stripeCustomerId: 1 }, function (err, clientFound){
				if(err) return next(err);
				if(!clientFound.stripeCustomerId){
					res.send({});
					return next();
				}
				var stripe = require("stripe")(settings.STRIPEAPIKEY);

				stripe.customers.retrieve(
					clientFound.stripeCustomerId,
					function (err, customer){
						if(err) return next(err);
						var creditCard;

						if(customer.sources.data.length > 0){
							res.send(cardFromStripeCustomer(customer));
						} else {
							res.send({});
						}

						next();
					});
			});
		});
	};

	return result;
}();