module.exports = function (){
	var route = require(process.cwd() + '/api/routes/routeBase'),
		self = this,
		requestValidator = require(process.cwd() + '/api/lib/requestValidator'),
		emitterService = require(process.cwd() + '/api/lib/emitterService'),
		clientService = require(process.cwd() + '/api/lib/clientService');

	route.mixWith(this);


	return {
		getPayments: function (req, res, next){
			self.authorize.ensureAuthenticated(req, res, function(err){
				if(!req.params.id) self.makeServerError(next, err);
					
				self.models.client.findOne({ _id: req.params.id }, { payments: 1 }, function (err, clientWithPayments){
					if(err) self.makeServerError(next, err);

					res.send(clientWithPayments);
					next();
				});
			});
		},
		get: function (req, res, next){
			self.authorize.ensureAdmin(req, res, function(err){
				if(!req.params.id) self.makeServerError(next, err);

				self.models.client.findOne({ _id: req.params.id }).lean().exec(function (err, client){
					if(err) self.makeServerError(next, err);

					client.balance = clientService.getBalance(client);

					res.send(client);
					next();
				});
			});			
		},
		postInvoicingData: function (req, res, next){
			self.authorize.ensureAuthenticated(req, res, function(err){
				if(err) return self.makeServerError(next, err);

				var msg = requestValidator.invoicingDataPost(req.body);

				if(msg) return self.makeServerError(next, msg);

				self.models.client.findOne({ _id: req.session.loggedInClient._id }, { invoicingData: 1 }, function (err, client){
					if(err) return self.makeServerError(next, err);
					var postData = req.body;

					client.invoicingData = {
						address: postData.address,
						hasNoVATNumber: postData.hasNoVATNumber,
						VATCountryCode: postData.VATCountryCode,
						VATNumber: postData.VATNumber,
						companyName: postData.companyName,
						juridicalForm: postData.juridicalForm,
						language: postData.language
					};

					client.save(function (err){
						if(err) return self.makeServerError(next, err);

						self.authorize.createSessionForEmitter(req, req.session.loggedInEmitter, req.session.loggedInIdentifier, function (err){
							if(err) return self.makeServerError(next, err);

							res.send(emitterService.makeClientObject(req.session.loggedInEmitter, req.session.loggedInIdentifier, req.session.loggedInClient));
							next();
						});
					});
				});
			});
		}
	};
}();