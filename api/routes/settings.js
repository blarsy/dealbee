module.exports = function (){
	var restify = require('restify'),
		authorize = require(process.cwd() + '/api/lib/authorize'),
		models = require(process.cwd() + '/api/models');

	return {
		post: function (req, res, next){
			if(!req.body.nextInvoiceNumber) return next(new restify.InternalError('Missing_NewInvoiceNumber'));
			var newNumber = new Number(req.body.nextInvoiceNumber);
			if(!newNumber) return next(new restify.InternalError('Invalid_NewInvoiceNumber'));
			
			authorize.ensureAdmin(req, res, function (err){
				if(err) return next(new restify.InternalError(err));

				models.settings.findOne({}, function (err, settings){
					if(err) return next(new restify.InternalError(err));
					var settingsFound = settings;

					if(!settingsFound){
						//start numbering at 59 (last invoice number for expansive passion is currently 58)
						settingsFound = { nextInvoiceNumber: 59 };
					}

					if(newNumber < settingsFound.nextInvoiceNumber + 1) return next(new restify.InternalError('NewInvoiceNumber_MustBe_AtLeast_CurrentPlusOne'));

					settingsFound.nextInvoiceNumber = req.body.nextInvoiceNumber;
					models.settings.create(settingsFound, function (err){
						if(err) return next(new restify.InternalError(err));

						res.send('Ok');
						next();
					});
				});
			});
		},
		get: function (req, res, next){
			authorize.ensureAdmin(req, res, function (err){
				if(err) return next(new restify.InternalError(err));

				models.settings.findOne({}, function (err, settings){
					if(err) return next(new restify.InternalError(err));
					var settingsFound = settings;

					if(!settingsFound){
						//start numbering at 59 (last invoice number for expansive passion is currently 58)
						models.settings.create({ nextInvoiceNumber: 59 }, function (err, savedSettings){
							if(err) return next(new restify.InternalError(err));

							res.send(savedSettings);
							next();
						});
					} else {
						res.send(settingsFound);
						next();
					}
				});
			});
		}
	};
}();