var restify = require('restify'),
	models = require(process.cwd() + '/api/models'),
	facebook = require(process.cwd() + '/api/lib/facebook');

module.exports = {
	post: function(req, res, next){
		if(!req.body.token) return next(new restify.InternalError('Token_Required'));

		facebook.createOrRefreshPublishInfo(req.body.token, function (err){
			if(err) return next(err);

			res.send('Ok');
			next();
		});
	}
};