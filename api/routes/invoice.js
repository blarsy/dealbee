module.exports = function (){
	var invoicer = require(process.cwd() + '/api/lib/invoicer'),
		route = require(process.cwd() + '/api/routes/routeBase'),
		remoteFolderProxy = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolderProxy'),
		self = this;

	route.mixWith(this);

	return {
		post: function (req, res, next){
			if(!req.body.uuid) return self.makeServerError(next, 'PaymentId_Required');

			self.models.client.findOne({ payments: { $elemMatch: { uuid: req.body.uuid } }}, 
					{ payments: 1, invoicingData: 1, packs: 1 }, function (err, clientFound){
				if(err) return self.makeServerError(next, err);
				if(!clientFound) return self.makeServerError(next, 'Client_Payment_NotFound');

				invoicer.invoiceOnePayment(clientFound, req.body.uuid, function(err){
					if(err) return self.makeServerError(next, err);

					res.send('Ok');
					next();
				});
			});
		},
		get: function (req, res, next){
			self.authorize.ensureAuthenticated(req, res, function (err){
				if(err) return self.makeServerError(next, err);
				if(!req.params.uuid) return self.makeServerError(next, 'Missing_Uuid');

				self.models.client.findOne({ payments: { $elemMatch: { uuid: req.params.uuid } }}, { payments: 1 }, function (err, clientFound){
					if(err) return self.makeServerError(next, err);
					if(!req.session.loggedInIdentifier.god && req.session.loggedInClient._id !== clientFound._id.toString()) return self.makeServerError(next, 'Unauthorized');

					remoteFolderProxy.connectInvoiceFolder(function (err, invoiceFolder){
						if(err) return self.makeServerError(next, err);

						invoiceFolder.getInvoice(req.params.uuid, function (err, fileStream){
							if(err){
								if(err === 'NotFound') return next(new self.restify.NotFoundError());
								return self.makeServerError(next, err);
							}
							res.header('Content-disposition', 'inline; filename=' + req.params.uuid + '.pdf');
							res.header('Content-type', 'application/pdf');

							//Black magic: workaround Kloudless policy of setting a 'binary' encoding to pdf downloads
							fileStream._readableState.decoder = null;
							fileStream._readableState.encoding = null;

							fileStream.pipe(res);
							fileStream.on('end',next);
						});
					});
				});
			});
		}
	};
}();