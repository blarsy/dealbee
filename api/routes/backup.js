module.exports = function (){
	var mongoDump = require('mongo-dump-stream'),
		authorize = require(process.cwd() + '/api/lib/authorize'),
		fs = require('fs'),
		settings = require(process.cwd() + '/settings'),
		_ = require('underscore'),
		restify = require('restify'),
		backupDatabase = require(process.cwd() + '/api/lib/backupDatabase'),
		backupAgent = require(process.cwd() + '/api/lib/jobs/backupAgent/backupAgent');

	return {
		get: function (req, res, next){
			authorize.ensureAdmin(req, res, function(err){
				if(err) return next(new restify.InternalError(err));

				fs.readdir(settings.BACKUPSPATH, function (err, files){
					if(err) return next(err);

					files.filter(function (file) {
						return fs.statSync(settings.BACKUPSPATH + file).isFile();
					});

					res.send(_.map(files, function (file){ return { fileName: file } }));
					next();
				});
			})
		},
		del: function (req, res, next){
			authorize.ensureAdmin(req, res, function(err){
				if(err) return next(new restify.InternalError(err));
				if(!req.params.fileName) return next(new restify.InternalError('FileName_Required'));

				fs.unlink(settings.BACKUPSPATH + req.params.fileName, function (err){
					if(err) return next(err);

					res.send('Ok');
					next();
				});
			});
		},
		post: function (req, res, next){
			authorize.ensureAdmin(req, res, function (err){
				if(err) return next(new restify.InternalError(err));

				backupDatabase.backup(function (err){
					if(err) return next(err);

					res.send('Ok');
					next();
				});
			});
		},
		full: function (req, res, next){
			authorize.ensureAdmin(req, res, function (err){
				if(err) return next(new restify.InternalError(err));

				backupAgent.backup(function (){
					res.send('Ok');
					next();
				});
			});
		}
	}
}();