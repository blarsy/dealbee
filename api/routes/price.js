module.exports = function (){
	var result = {},
		prices = require(process.cwd() + '/api/lib/prices.json');

	result.get = function (req, res, next){
		res.send(prices);
		next();
	};

	return result;
}();