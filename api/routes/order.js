module.exports = function (){
	var result = {},
		route = require(process.cwd() + '/api/routes/routeBase'),
		self = this,
		requestValidator = require(process.cwd() + '/api/lib/requestValidator'),
		emitterService = require(process.cwd() + '/api/lib/emitterService'),
		clientService = require(process.cwd() + '/api/lib/clientService');

	route.mixWith(this);

	return {
		post: function(req, res, next){
			self.authorize.ensureAuthenticated(req, res, function(err){
				if(err) return self.makeServerError(next, err);

				//Check args: emitterId, at least 1 pack or 1 subscription
				var msg = requestValidator.orderPost(req);
				if(msg) return self.makeServerError(next, msg);

				self.models.client.findOne({ _id: req.session.loggedInIdentifier.client }, function (err, clientFound){
					if(err) return self.makeServerError(next, err);

					//Check: if subscription, must be different than current
					if(req.body.subscription && req.body.subscription.type === clientFound.subscription.type) return self.makeServerError(next, 'Subscription_Not_Changed');

					//Check: logged in emitter has opted out VAT appliance, or has a VAT number
					var invoicingDataMsg = requestValidator.invoicingDataPost(clientFound.invoicingData);
					if(invoicingDataMsg) return self.makeServerError(next, invoicingDataMsg);

					//Check: credit card defined for emitter
					if(!clientFound.stripeCustomerId) return self.makeServerError(next, 'Credit_Card_Must_Be_Defined');
					
					//register order
					clientService.registerOrder(req.body.subscription, req.body.packs, clientFound, function (err) {
						if(err) return self.makeServerError(next, err);

						//Save emitter & refresh logged in emitter
						clientService.saveLoggedInClient(clientFound, req, function (err, clientSaved){
							if(err) return self.makeServerError(next, err);

							//TODO: Attempt to send invoice via email
							//Return emitter dto (balance updated)
							res.send(emitterService.makeClientObject(req.session.loggedInEmitter, req.session.loggedInIdentifier, clientSaved));
							next();
						});
					});
				});
			});
		}
	};
}();