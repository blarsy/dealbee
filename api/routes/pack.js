module.exports = function (){
	var result = {},
		route = require(process.cwd() + '/api/routes/routeBase'),
		authorize = require(process.cwd() + '/api/lib/authorize'),
		restify = require('restify'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		clientService = require(process.cwd() + '/api/lib/clientService');

	route.mixWith(this);

	result.post = function (req, res, next){
		self.authorize.ensureAdmin(req, res, function (err){
			if(err) return self.makeServerError(next, err);
			if(!req.body.clientId) return self.makeServerError(next, 'ClientId_Required');
			if(!req.body.packName) return self.makeServerError(next, 'PackName_Required');

			self.models.client.findOne({ _id: req.body.clientId }, function (err, clientFound){
				if(err) return self.makeServerError(next, err);
				if(!clientFound) return self.makeServerError(next, 'Client_Not_Found');
				var beginDate = req.body.beginDate ? new Date(req.body.beginDate) : bizTime.now();

				try{
					clientService.createPack(req.body.packName, beginDate, clientFound);
				}
				catch(ex){
					return self.makeServerError(next, ex);
				}

				clientFound.save(function (err){
					if(err) return self.makeServerError(next, 'err');

					res.send('Ok');
					next();
				});
			});
		});
	};

	return result;
}();