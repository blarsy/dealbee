module.exports = function(){
	var restify = require('restify'),
		models = require(process.cwd() + '/api/models'),
		authorize = require(process.cwd() + '/api/lib/authorize'),
		_ = require('underscore');
	return {
		get: function (req, res, next){
			authorize.ensureAdmin(req, res, function (err){
				if(err) return next(new restify.InternalError(err));

				if(req.params.sessionLogId){
					models.sessionLog.findOne({ id: req.params.sessionLogId }, function (err, sessionLog){
						if(err) return next(err);
						if(!sessionLog) return next(new restify.InternalError('SessionLog_NotFound'));

						res.send(sessionLog);
						next();
					});
				} else {
					models.sessionLog.find({ emitter: { $exists : true } }).populate('emitter').sort('-created').limit(20).lean().exec(function (err, sessionLogs){
						if(err) return next(err);

						_.each(sessionLogs, function (sessionLog){
							sessionLog.activities = sessionLog.activity ? sessionLog.activity.length : 0;
							delete sessionLog.activity;
						});

						res.send(sessionLogs);
						next();
					});
				}
			});
		}
	}
}();