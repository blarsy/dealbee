module.exports = function(){
	var result = {},
		settings = require(process.cwd() + '/settings'),
		Agenda = require('agenda'),
		agenda = new Agenda().database(settings.JOBDB),
		authorize = require(process.cwd() + '/api/lib/authorize'),
		subscriptionRenewer = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer'),
		jobLogs = require(process.cwd() + '/api/lib/jobs/joblogs');
	
	result.get = function (req, res, next){
		authorize.ensureAdmin(req, res, function (err){
			if(err) return next(new restify.InternalError(err));
			agenda.jobs({}, function(err, jobs) {
				if(err) return next(err);

				next();
				res.send(jobs);
			});
		});
	};
	result.renewSubscriptions = function (req, res, next){
		authorize.ensureAdmin(req, res, function (err){
			if(err) return next(new restify.InternalError(err));

			jobLogs.auditLog.info({ task: 'Renew subscriptions', message: 'Beginning (triggered from admin website) ...' });
			subscriptionRenewer.execute(function (err, nbRenewed, nbChargeFailed){
				if(err){
					jobLogs.errorLog.error({ task: 'Renew subscriptions', message: 'Failed !', err: err });
				} else {
					jobLogs.auditLog.info({ task: 'Renew subscriptions', message: 'Succeeded ! ' + nbRenewed + ' renewed, ' + nbChargeFailed + ' charge failed.' });
				}
				res.send('Ok');
				next();
			})
		});
	};
	return result;
}();