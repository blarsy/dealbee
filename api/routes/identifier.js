module.exports = function (){
	var route = require(process.cwd() + '/api/routes/routeBase'),
		self = this;

	route.mixWith(this);

	return {
		get: function (req, res, next){
			self.authorize.ensureAdmin(req, res, function (err){
				if(err) return self.makeServerError(next, err);

				if(req.params.query){
					self.models.identifier.find({ email: { $regex: '.*' + req.params.query + '.*', $options: 'i' } }, 
						{ email: 1, type: 1, lastLoggedInEmitter: 1, defaultEmitter: 1, client: 1 })
						.limit(30)
						.exec(function (err, identifiers){
							if(err) return self.makeServerError(next, err);

							res.send(identifiers);
							next();
						});
				} else if(req.params.id){
					self.models.identifier.findOne({ _id: req.params.id }, { hashInfo: 0 }, function (err, identifier){
						if(err) return self.makeServerError(next, err);
						
						res.send(identifier);
						next();
					});
				} else if(req.params.clientId) {
					self.models.identifier.find({ client: req.params.clientId }, { hashInfo: 0 }, function (err, identifiers){
						if(err) return self.makeServerError(next, err);
						
						res.send(identifiers);
						next();
					});
				} else {
					return self.makeServerError(next, 'Missing_Argument');
				}
			});
		}
	};
}();
