module.exports = function (){
	var route = require(process.cwd() + '/api/routes/routeBase'),
		result = {},
		self = this,
		session = require(process.cwd() + '/api/configuredsession'),
		identifierService = require(process.cwd() + '/api/lib/identifierService');

	route.mixWith(this);

	this.createManager = function (managerInvitation, identifier, done){
		models.emitter.findOne({ _id: managerInvitation.emitter }, { identifiers: 1 }, function (err, emitter){
			if(err) return done(err);
			if(!emitter) return done('Emitter_NotFound');
			if(_.find(emitter.identifiers, function (existingIdentifier){ 
				return existingIdentifier.toString() == identifier._id
			})){
				return done('Duplicate_Manager');
			}

			emitter.identifiers.push(identifier);

			managerInvitation.accepted = self.bizTime.now();
			emitter.save(function (err){
				if(err) return done(err);

				managerInvitation.save(function (err){
					if(err) return done(err);

					done();
				});
			});
		});
	};

	result = {
		get: function (req, res, next){
			self.authorize.ensureAuthenticated(req, res, function (err){
				if(err) return self.makeServerError(next, err);

				self.models.emitter.findOne({ _id: req.session.loggedInEmitter._id }, { identifiers: 1 })
					.populate('identifiers', 'email')
					.exec(function (err, emitterWithManagers){

					if(err) return self.makeServerError(next, err);

					res.send(emitterWithManagers.identifiers);
					next();
				});
			});
		},
		post: function (req, res, next){
			if(req.body.invitationId){
				models.managerInvitation.findOne({ uuid: req.body.invitationId }).populate('emitter').exec(function (err, managerInvitation){
					if(err) return self.makeServerError(next, err);
					if(!managerInvitation) return self.makeServerError(next, 'Invitation_NotFound');
					if(managerInvitation.accepted) return self.makeServerError(next, 'Invitation_Already_Accepted');
					if(self.bizTime.now() > new Date(managerInvitation.created.valueOf() + (1000 * 60 * 60 * 24 * 7))) return self.makeServerError(next, 'Invitation_Expired');

					if(req.body.fbToken || req.body.email){
						//Create new identity
						identifierService.create(req.body, function (err, identifier){
							if(err) return self.makeServerError(next, err);

							self.createManager(managerInvitation, identifier, function (err){
								if(err) return self.makeServerError(next, err);

								res.send('Ok');
								next();
							});
						})
					} else {
						//Link existing identity
						self.authorize.ensureAuthenticated(req, res, function (err){
							if(err) return self.makeServerError(next, err);

							models.identifier.findOne({ _id: req.session.loggedInIdentifier._id }, function (err, identifier){
								if(err) return self.makeServerError(next, err);
								if(!identifier) return self.makeServerError(next, 'Identifier_NotFound');

								self.createManager(managerInvitation, identifier, function (err){
									if(err) return self.makeServerError(next, err);

									//add emitter to identity
									req.session.loggedInIdentifier.emitters.push(managerInvitation.emitter);
									session.save(req.session.sid, req.session, function (err, status){
										if(err) return done(err);

										res.send(req.session.loggedInIdentifier);
										next();
									})
								});
							});

						});
					}					
				});
			} else {
				self.authorize.ensureAuthenticated(req, res, function (err){
					if(err) return self.makeServerError(next, err);
					if(!req.body.email) return self.makeServerError(next, 'Email_Required');

					self.models.identifier.findOne({ email: req.body.email }, function (err, identifierFound){
						if(err) return self.makeServerError(next, err);

						if(identifierFound){
							//add the identifier to the emitter's list
							self.models.emitter.findOne({ _id: req.session.loggedInEmitter._id }, 
								{ identifiers: 1 }, function (err, emitterWithManagers){

								if(err) return self.makeServerError(next, err);
								var identifierExists = _.find(emitterWithManagers.identifiers, function (identifier){ return identifier._id === identifierFound._id });
								if(identifierExists) return self.makeServerError(next, 'Already_Manager');

								emitterWithManagers.identifiers.push(identifierFound);
								emitterWithManagers.save(function (err){
									if(err) return self.makeServerError(next, err);

									res.send({ result: 'Ok' });
									next();
								});
							});
						} else {
							//send a manager invitation mail
							identifierService.inviteManager(req.session.loggedInEmitter._id, req.session.loggedInIdentifier._id, req.body.email, function (err){
								if(err) return self.makeServerError(next, err);

								res.send({ result: 'Mail_Sent' });
								next();
							});
						}
					});
				});
			}
		},
		del: function (req, res, next){
			self.authorize.ensureAuthenticated(req, res, function (err){
				if(err) return self.makeServerError(next, err);
				if(!req.params.id) return self.makeServerError(next, 'Id_Required');
				if(req.params.id === req.session.loggedInEmitter._id.toString()) return self.makeServerError(next, 'Cannot_Delete_Thyself');

				self.models.emitter.findOne({ _id: req.session.loggedInEmitter._id }, 
					{ identifiers: 1 }, function (err, emitterWithManagers){

					if(err) return self.makeServerError(next, err);
					if(!_.find(emitterWithManagers.identifiers, function (identifier){ return identifier.toString() == req.session.loggedInIdentifier._id.toString() })){
						//Logged in identity is no manager of the target emitter
						return self.makeServerError(next, 'Unauthorized');
					}
					var identifierFound = _.find(emitterWithManagers.identifiers, function (identifier){ return identifier.toString() == req.params.id });
					if(identifierFound){
						for(var i = 0; i < emitterWithManagers.identifiers.length; i ++){
							if(emitterWithManagers.identifiers[i].toString() == req.params.id){
								emitterWithManagers.identifiers.splice(i, 1);
								break;
							}
						}
						emitterWithManagers.save(function (err){
							if(err) return self.makeServerError(next, err);

							res.send('Ok');
							next();
						})
					} else {
						return self.makeServerError(next, 'Manager_Not_Found');
					}
				});
			});			
		}
	};

	return result;
}();