module.exports = function(){
	var result = {};

	result.restify = require('restify');
	result.authorize = require(process.cwd() + '/api/lib/authorize');
	result.models = require(process.cwd() + '/api/models');
	result.settings = require(process.cwd() + '/settings');
	result.fs = require('fs');
	result.async = require('async');
	result._ = require('underscore');
	result.bizTime = require(process.cwd() + '/api/lib/bizTime');
	result.uuid = require('node-uuid');

	result.mixWith = function (obj){
		var prop = {};

		for (prop in result) {
			if (result.hasOwnProperty(prop)) {
				obj[prop] = result[prop];
			}
		}
	};

	result.makeServerError = function (fn, err){
		var errorMsg;

		if(typeof err === 'string'){
			errorMsg = err;
		} else {
			try{
				errorMsg = err.message || JSON.stringify(err);
			} catch(ex){
				errorMsg = err.toString();
			}			
		}

		return fn(new restify.InternalError(errorMsg));
	};

	return result;
}();