
module.exports = function (){
	var result = {},
		route = require(process.cwd() + '/api/routes/routeBase'),
		self = this,
		dealService = require(process.cwd() + '/api/lib/dealService'),
		geolib = require('geolib'),
		requestValidator = require(process.cwd() + '/api/lib/requestValidator');

	route.mixWith(this);

	function validate(req, callback){
		var messages = [],
			dealFormData = req.body,
			now = new Date();

		if(typeof dealFormData.expiration === 'string') dealFormData.expiration = new Date(dealFormData.expiration);
		if(typeof dealFormData.beginDate === 'string') dealFormData.beginDate = new Date(dealFormData.beginDate);

		if(!dealFormData.teaser){
			messages.push('Teaser_Required');
		} else if(dealFormData.teaser.length > 200){
			messages.push('Teaser_Too_Long')
		}
		if(!dealFormData.benefit){
			messages.push('Benefit_Required');
		} else if(dealFormData.benefit.length > 20){
			messages.push('Benefit_Too_Long');
		}

		if(!dealFormData.expiration){
			messages.push('Expiration_Required');
		} else {
			if(dealFormData.expiration < now){
				messages.push('Expiration_Invalid');
			}
		}
		if(!dealFormData.visibility){
			messages.push('Visibility_Required');
		} else {
			if(dealFormData.visibility != 'limited' && dealFormData.visibility != 'public'){
				messages.push('Invalid_visibility');
			}
		}
		if(!dealFormData.beginDate){
			messages.push('BeginDate_Required');
		}
		if(dealFormData.beginDate >= dealFormData.expiration){
			messages.push('Invalid_BeginDate_Expiration');
		}
		if(dealFormData.expiration > dealFormData.beginDate.clone().clearTime().addYears(1).addDays(1)){
			messages.push('Deal_Max_Duration_Exceeded');
		}

		if(messages.length > 0){
			return callback(messages.join(', '));
		};
		if(dealFormData._id){
			self.models.grant.findOne({ deal: dealFormData._id, sent: { $exists: true } }, function (err, grantFound){
				if(err) return callback(err);
				if(grantFound) return callback('Cant_Modify_Deal_With_Sent_Grants');

				callback(null);
			})
		} else {
			callback(null);
		}
	};

	function queryFromReq(req, done){

		if(req.params.mode === 'nearest'){
			if(!req.params.lng) return done('Lng_Required');
			if(!req.params.lat) return done('Lat_Required');

			self.models.emitter.where('address.coords').near({
				center:{
					type: 'Point',
					coordinates: [ req.params.lng, req.params.lat ]
				},
				$maxdistance: 50000,
				spherical: true
			}).select('_id').exec(function (err, emitters){
				if(err) return done(err);

				queryForEmitters(emitters, req.params.keywords, req.params.lat, req.params.lng, done);
			});
		} else {
			queryForEmitters(null, req.params.keywords, req.params.lat, req.params.lng, done);
		}
	};

	function queryForEmitters(emitters, keywords, latitude, longitude, done){
		var now = self.bizTime.now(),
			query = {
				nbOnRequest: { $gt: 0 },
				expiration: { $gt: now },
				beginDate: { $lt: now },
				private: false
			},
			keywordsList = [];

		if(emitters){
			query.emitter = { $in: self._.pluck(emitters, '_id') };
		}
		if(keywords){
			keywordsList = keywords.split(/[\s,]+/);
			query.keywords = { $all: keywordsList };
		}

		self.models.deal.find(query, { created: 1, expiration: 1, beginDate: 1, teaser: 1, benefit: 1, nbOnRequest: 1, emitter: 1, imageRef: 1 })
			.populate('emitter', 'address name imageRef language website')
			.lean()
			.exec(function (err, deals){
				if(err) return done(err);

				if(latitude){
					self._.each(deals, function (deal){
						deal.distance = geolib.getDistance(
							{ latitude: latitude, longitude: longitude },
							{ latitude: deal.emitter.address.coords[1], longitude: deal.emitter.address.coords[0] }
						);
					});
				}

				done(null, deals);
			});
	};

	result.get = function (req, res, next){
		if(req.params.emitterId){
			self.authorize.ensureAdmin(req, res, function(err){
				if(err) return self.makeServerError(next, err);

				dealService.getDealWithGrantsPerEmitter(req.params.emitterId, function (err, deals){
					if(err) return next(err);

					res.send(deals);
					next();
				});		
			});
		} else if(req.params.dealId){
			self.authorize.ensureAuthenticated(req, res, function (err){
				if(err) return self.makeServerError(next, err);

				dealService.getDealMetadata(req.params.dealId, function (err, deal){
					if(err) return self.makeServerError(next, err);

					res.send(deal);
					next();
				});
			});
		} else if(req.params.mode){
			queryFromReq(req, function (err, deals){
				if(err) return self.makeServerError(next, err);

				res.send(deals);
				next();
			});
		} else {	
			self.authorize.ensureAuthenticated(req, res, function(err){
				if(err) return self.makeServerError(next, err);
				var dealId = req.params.id;

				if(dealId){
					var query = { _id: dealId };
					if(!req.session.loggedInIdentifier.god){
						query.emitter = req.session.loggedInEmitter._id;
					}
					dealService.getDealWithGrants(dealId, req.session.loggedInIdentifier.god ? null : req.session.loggedInEmitter._id, function (err, deal){
						if(err) return next(err);

						res.send(deal);
						next();
					});
				} else {
					dealService.getDealWithGrantsPerEmitter(req.session.loggedInEmitter._id, function (err, deals){
						if(err) return next(err);

						res.send(deals);
						next();
					});		
				}
			});

		}
	};
	result.publicInfo = function (req, res, next){
		var dealId = req.params.id;
		if(!dealId) return self.makeServerError(next, 'Id_Required');

		dealService.getDealForDisplay(dealId, function (err, deal){
			if(err) return self.makeServerError(next, err);

			res.send(deal);
			next();
		});
	};
	result.post = function (req, res, next){
		self.authorize.ensureAuthenticated(req, res, function(err){
			if(err) return self.makeServerError(next, err);
			var dealFormData = req.body,
				dealToSave,
				dealCreationTasks = [],
				termsOfUse = [];

			validate(req, function(err){
				if(err) return self.makeServerError(next, err);

				if(dealFormData.termsOfUse){
					if(typeof dealFormData.termsOfUse === 'string'){
						dealFormData.termsOfUse = JSON.parse(dealFormData.termsOfUse);
					}
					//remove empty terms of use
					for(var i = dealFormData.termsOfUse.length - 1; i >= 0; i--){
						if(dealFormData.termsOfUse[i].text && typeof dealFormData.termsOfUse[i].text === 'string' && dealFormData.termsOfUse[i].text.trim() !== ''){
							termsOfUse.push(dealFormData.termsOfUse[i].text);
						}
					}
				}
				if(!dealFormData._id){
					dealToSave = {
						teaser: dealFormData.teaser,
						benefit: dealFormData.benefit,
						description: dealFormData.description,
						expiration: dealFormData.expiration,
						beginDate: dealFormData.beginDate,
						termsOfUse: termsOfUse,
						emitter: req.session.loggedInEmitter._id,
						imageRef: dealFormData.imageRef,
						nbOnRequest: 0,
						private: dealFormData.visibility == 'limited',
						created: new Date()
					};
					dealCreationTasks.push(function (callback){
						self.models.deal.create(dealToSave, function (err, dealSaved){
							if(err) return callback(err);

							callback(null, dealSaved);				
						});
					});
				} else {
					dealCreationTasks.push(function (callback){
						self.models.deal.findOne({ _id: dealFormData._id }, function (err, dealFound){
							if(err) return callback(err);

							dealToSave = dealFound;
							dealToSave.teaser = dealFormData.teaser;
							dealToSave.benefit = dealFormData.benefit;
							dealToSave.description = dealFormData.description;
							dealToSave.expiration = dealFormData.expiration;
							dealToSave.beginDate = dealFormData.beginDate;
							dealToSave.termsOfUse = termsOfUse;
							dealToSave.imageRef = dealFormData.imageRef;
							dealToSave.visibility = dealFormData.visibility == 'limited';


							dealToSave.save(function (err, dealSaved){
								if(err) return callback(err);

								callback(null, dealSaved);				
							});
						});
					});
				}

				self.async.waterfall(dealCreationTasks, function (err, result){
					if(err) return next(err);

					res.send(result);
					next();
				});
			});
		});
	};
	result.consume = function (req, res, next){
		self.authorize.ensureAuthenticated(req, res, function(err){
			if(err) return self.makeServerError(next, err);
			if(!req.body.number){
				return self.makeServerError(next, 'Grant_Number_Required');
			} else {
				if(! /^[0-9]{1,10}$/.test(req.body.number)){
					return self.makeServerError(next, 'Grant_Number_Must_Be_Integer');
				}
			}

			self.models.deal.find({ emitter: req.session.loggedInEmitter._id }, { _id:1 }, function (err, deals){
				if(err) return next(err);

				var dealIds = self._.map(deals, function (deal){ return deal._id; })
				self.models.grant.findOne({ deal: { $in: dealIds }, number: req.body.number }, function(err, grant){
					if(err) return next(err);
					if(!grant) return self.makeServerError(next, 'Grant_Not_Found');

					grant.consumed = new Date();
					grant.save(function (err, grantSaved){
						if(err) return next(err);
						res.send('Ok');
						next();
					});
				});
			});
		});
	};
	result.del = function (req, res, next){
		self.authorize.ensureAuthenticated(req, res, function(err){
			if(err) return self.makeServerError(next, err);

			if(!req.params.id) return self.makeServerError(next, 'Id_Required');

			self.models.deal.findOne({ _id: req.params.id }, function (err, dealFound){
				if(err) return self.makeServerError(next, err);

				self.models.grant.count({ deal: dealFound._id }, function (err, grantCount){
					if(err) return self.makeServerError(next, err);
					if(dealFound.nbOnRequest > 0 || grantCount > 0) return self.makeServerError(next, 'Cannot_Delete_Active_Deal');

					self.models.deal.remove({ _id: req.params.id }, function (err){
						if(err) return self.makeServerError(next, err);

						res.send('Ok');
						next();
					});

				});

			});
		});
	};

	result.togglePrivate = function (req, res, next){
		self.authorize.ensureAdmin(req, res, function(err){
			if(err) return self.makeServerError(next, err);
			if(!req.body.id) return self.makeServerError(next, 'Id_Required');

			self.models.deal.findOne({ _id: req.body.id }, function (err, deal){
				if(err) return next(err);

				deal.private = !deal.private;
				deal.save(function (err, dealSaved){
					if(err) return next(err);

					res.send(dealSaved);
					next();
				});
			});
		});
	};

	result.postMetadata = function (req, res, next){
		self.authorize.ensureAuthenticated(req, res, function (err){
			var validationMsg = requestValidator.dealMetadataPost(req);
			if(validationMsg) return self.makeServerError(next, validationMsg);

			self.models.deal.findOne({ _id: req.body.dealId }, function (err, deal){
				if(err) return self.makeServerError(next, err);

				deal.private = req.body.private;
				deal.keywords = req.body.keywords;

				deal.save(function (err){
					if(err) return self.makeServerError(next, err);

					res.send('Ok');
					next();
				});
			});
		})
	}

	return result;
}();