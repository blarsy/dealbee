var restify = require('restify'),
	_ = require('underscore'),
	authorize = require(process.cwd() + '/api/lib/authorize'),
	models = require(process.cwd() + '/api/models');

module.exports = {
	post: function (req, res, next){
		authorize.ensureAuthenticated(req, res, function(err){
			if(err) return next(new restify.InternalError(err));
			if(!req.body.list) return next(new restify.InternalError('List_Required'));

			var list = [];
			try{
				_.each(req.body.list.match(/([^\n\@\t ]+)\@([\w\d\-]+)\.([\w\d]{2,8})/gm), function (email){
					list.push(email);
				});

				models.preference.find({ email: { $in: list } }, function (err, emailPrefsBlocked){
					if(err) return next(err);

					if(emailPrefsBlocked.length > 0){
						var blockedEmailPrefs =	_.filter(emailPrefsBlocked, function (emailPrefBlocked){
								//emails that blocked Dealbee altogether are returned systematically
								if(emailPrefBlocked.unsubscribedDate ) return true;
								else {
									//Check whether the logged in emitter has been explicitly blocked by the currently analyzed email
									if(_.find(emailPrefBlocked.blockedEmitters, function (blockedEmitter){
										return blockedEmitter.toString() === req.session.loggedInEmitter._id;
									})){
										return true;
									}
								}
								return false;
							}),
							blockedEmails = _.map(blockedEmailPrefs, function (blockedEmailPref){
								return { email: blockedEmailPref.email, unsubscribedFromEmitter: !blockedEmailPref.unsubscribedDate };
							}),
							nonBlocked = _.difference(list, _.pluck(blockedEmails, 'email'));

						res.send({ emails: nonBlocked, blocked: blockedEmails });
					} else {
						res.send({ emails: list, blocked: [] });
					}
					next();
				});
			}
			catch(ex){
				return next(ex);
			}
		});
	}
};