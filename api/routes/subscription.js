module.exports = function (){
	var route = require(process.cwd() + '/api/routes/routeBase'),
		result = {},
		self = this,
		clientService = require(process.cwd() + '/api/lib/clientService');

	route.mixWith(this);

	return {
		post: function (req, res, next){
			self.authorize.ensureAdmin(req, res, function (err){
				if(err) return self.makeServerError(next, err);
				if(!req.body.clientId) return self.makeServerError(next, 'ClientId_Required');
				if(!req.body.subscriptionName) return self.makeServerError(next, 'SubscriptionName_Required');

				self.models.client.findOne({ _id: req.body.clientId }, function (err, clientFound){
					if(err) return next(err);
					if(!clientFound) return self.makeServerError(next, 'Client_Not_Found');

					try{
						clientService.setupSubscription({ type: req.body.subscriptionName, paymentFrequency: 1 }, null, clientFound);
					}
					catch(ex){
						return self.makeServerError(next, ex);
					}
					clientFound.save(function (err){
						if(err) return self.makeServerError(next, err);

						res.send('Ok');
						next();
					});
				});
			});
		}
	};
}();