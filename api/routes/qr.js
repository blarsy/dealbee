module.exports = function (){
	var result = {},
		qr = require('qr-image'),
		settings = require(process.cwd() + '/settings'),
		restify = require('restify');

	result.get = function (req, res, next){
		if(!req.params.grantUuid) return next(new restify.InternalError('GrantUuid_Required'));

		var code = qr.image(settings.WEBSITEURL + 'consume/' + req.params.grantUuid, { type: 'png' });

		code.pipe(res);
		next();
	};

	return result;
}();