module.exports = function (){
	var route = require(process.cwd() + '/api/routes/routeBase'),
		self = this,
		requestValidator = require(process.cwd() + '/api/lib/requestValidator'),
		emitterService = require(process.cwd() + '/api/lib/emitterService'),
		identifierService = require(process.cwd() + '/api/lib/identifierService'),
		mailComposer = require(process.cwd() + '/api/lib/mailassets/mailComposer'),
		cleaner = require(process.cwd() + '/api/lib/cleaner');

	route.mixWith(this);

	return {
		post: function (req, res, next){
			//Restore session if applicable
			self.authorize.tryRestoreSession(req, res, function (err){
				if(err) self.makeServerError(next, err);
				var msg,
					emitterToSave,
					emitterCreationTasks = [],
					emitterFormData = req.body;

				if(!req.session.loggedInIdentifier){
					//Validate identifier
					msg = requestValidator.identifierPost(req);
					if(msg) return self.makeServerError(next, msg);
				}

				//Validate emitter
				msg = requestValidator.emitterPost(req);
				if(msg){
					return self.makeServerError(next, msg);
				}

				if(!req.session.loggedInEmitter){
					emitterCreationTasks.push(function (callback){
						identifierService.create(emitterFormData, callback);
					});
				} else {
					emitterCreationTasks.push(function (callback){
						identifierService.ensureHasDefaultEmitter(req.session.loggedInIdentifier._id, function (err, identifier){
							if(err) return callback(err);

							//When the name has been gathered during login (fb login), perpetuate it
							if(req.session.loggedInIdentifier.name){
								identifier.name = req.session.loggedInIdentifier.name;
							}
							callback(null, identifier);
						});
					});
				}

				if(emitterFormData.id){
					//load emitter to modify
					emitterCreationTasks.push(function (identifier, callback){
						if(req.session.loggedInEmitter._id === emitterFormData.id){
							self.models.emitter.findOne({ _id: emitterFormData.id }, function (err, emitterFound){
								if(err) return callback(err);
								if(!emitterFound) return callback('EmitterNotFound');

								callback(null, emitterFound, identifier);
							});
						} else {
							return callback('Unauthorized');
						}
					});
				} else {
					emitterCreationTasks.push(function (identifier, callback){
						//link identifiers to its emitters
						callback(null, new self.models.emitter(), identifier);
					});
				}

				emitterCreationTasks.push(function (emitterToSave, identifier, callback){
					emitterToSave.name = emitterFormData.name;
					emitterToSave.website = emitterFormData.website;
					emitterToSave.language = emitterFormData.language;
					emitterToSave.imageRef = emitterFormData.imageRef;
					if(emitterFormData.invoicingData){
						emitterToSave.invoicingData = emitterFormData.invoicingData;
					}
					if(emitterFormData.address){
						emitterToSave.address = {
							street: emitterFormData.address.street,
							number: emitterFormData.address.number,
							box: emitterFormData.address.box,
							postcode: emitterFormData.address.postcode,
							city: emitterFormData.address.city,
							country: emitterFormData.address.country,
							useMap: emitterFormData.address.useMap,
							coords: emitterFormData.address.coords
						};
						if(!emitterToSave.address.created) emitterToSave.address.created = new Date();
					} else {
						emitterToSave.address = undefined;
					}
					if(identifier){
						emitterToSave.identifiers = [ identifier ];
					}
					if(!emitterFormData.id){
						emitterToSave.created = self.bizTime.now();
					}

					emitterToSave.save(function (err, emitterSaved){
						if(err) return self.makeServerError(next, err);

						identifierService.ensureHasDefaultEmitter(identifier._id, function (err, updatedIdentifier){
							if(err) return callback(err);

							identifierService.linkEmitters(updatedIdentifier, function (err, linkedIdentifier){
								if(err) return callback(err);

								return callback(null, { emitter: emitterSaved, identifier: linkedIdentifier });
							});
						});
					});
				});

				self.async.waterfall(emitterCreationTasks, function (err, result){
					if(err) return self.makeServerError(next, err);

					self.authorize.createSessionForEmitter(req, result.emitter, result.identifier, function (err){
						if(err) return self.makeServerError(next, err);

						res.send(emitterService.makeClientObject(result.emitter, result.identifier, req.session.loggedInClient));
						next();
					});
				});
			});
		},
		get: function (req, res, next){
			if(req.params.grantId){
				self.models.grant.findOne({ uuid: req.params.grantId }, { deal: 1 }).populate('deal').exec(function (err, grant){
					if(err) return self.makeServerError(next, err);

					self.models.emitter.findOne({ _id: grant.deal.emitter }, { name: 1, address: 1, website: 1, imageRef: 1 }).exec(function (err, emitter){
						if(err) return self.makeServerError(next, err);
						
						res.send(emitter);
						next();
					});
				});
			} else if(req.params.managerInvitationId){
				self.models.managerInvitation.findOne({ uuid: req.params.managerInvitationId }, { emitter: 1, accepted: 1, created: 1 }).populate('emitter', 'name address website imageRef').exec(function (err, managerInvitation){
					if(err) return self.makeServerError(next, err);
					if(!managerInvitation) return self.makeServerError(next, 'Invitation_NotFound');
					if(managerInvitation.accepted) return self.makeServerError(next, 'Invitation_Already_Accepted');
					if(self.bizTime.now() > new Date(managerInvitation.created.valueOf() + (1000 * 60 * 60 * 24 * 7))) return self.makeServerError(next, 'Invitation_Expired');

					res.send(managerInvitation.emitter);
					next();
				});				
			} else {
				self.authorize.ensureAuthenticated(req, res, function (err){
					if(err) self.makeServerError(next, err);

					if(req.session.loggedInEmitter && req.session.loggedInIdentifier.god){
						if(req.params.query === undefined && !req.params.id) return self.makeServerError(next, 'Missing_Argument');

						if(req.params.id){
							self.models.emitter.findOne({ _id: req.params.id }).exec(function (err, emitter){
								if(err) self.makeServerError(next, err);

								res.send(emitter);
								next();
							});
						} else {
							self.models.emitter.find({ name: { $regex: '.*' + req.params.query + '.*', $options: 'i' } }, 
								{ name: 1, created: 1, email: 1, language: 1, packs: 1, identifiers: 1 })
								.limit(30)
								.populate('identifiers', 'email type')
								.exec(function (err, emitters){
									if(err) self.makeServerError(next, err);

									res.send(emitters);
									next();
								});				
						}						
					} else {
						return self.makeServerError(next, 'Unexpected_Parameters');
					}
				});			
			}
		},
		changePassword: function (req, res, next){
			self.authorize.ensureAuthenticated(req, res, function(err){
				if(err) self.makeServerError(next, err);

				if(!req.body.currentPassword) return self.makeServerError(next, 'Missing_Argument');
				if(!req.body.password) return self.makeServerError(next, 'Missing_Argument');

				self.models.emitter.findOne({ _id: req.session.loggedInEmitter._id }).populate('identifiers').exec(function (err, emitterFound){
					if(err) return self.makeServerError(next, err);

					emitterService.checkPassword(req.body.currentPassword, emitterFound, function (err){
						if(err) return self.makeServerError(next, err);

						emitterService.changePassword(emitterFound, req.body.password, function (err){
							if(err) return self.makeServerError(next, err);

							res.send('Ok');
							next();						
						});
					});
				});
			});
		},
		forgotPassword: function (req, res, next){
			if(!req.body.email) return self.makeServerError(next, 'Missing_Argument');

			self.models.emitter.findOne({ email: req.body.email }, function (err, emitterFound){
				if(err) return self.makeServerError(next, err);
				if(!emitterFound) return self.makeServerError(next, 'EmitterNotFound');

				var request = new self.models.passwordRecoveryRequest({
					email: req.body.email,
					emitter: emitterFound,
					//expires in 2 hours
					expiration: new Date(new Date().valueOf() + (1000 * 60 * 60 * 120)),
					uuid: self.uuid.v4()
				});

				request.save(function (err, requestSaved){
					if(err) return self.makeServerError(next, err);

					mailComposer.recoverPassword(request, function (err){
						if(err) return self.makeServerError(next, err);

						res.send('Ok');
						next();
					});

				});

			});
		},
		recoverPassword: function (req, res, next){
			if(!req.body.requestId) return self.makeServerError(next, 'Missing_Argument');
			if(!req.body.password) return self.makeServerError(next, 'Missing_Argument');

			self.models.passwordRecoveryRequest.findOne({ uuid: req.body.requestId, expiration: { $gte: new Date() } }, function (err, passwordRecoveryRequest){
				if(err) return self.makeServerError(next, err);
				if(!passwordRecoveryRequest) return self.makeServerError(next, 'RequestNotFoundOrExpired');

				self.models.emitter.findOne({ _id: passwordRecoveryRequest.emitter }).populate('identifiers').exec(function (err, emitterFound){
					if(err) return self.makeServerError(next, err);

					emitterService.changePassword(emitterFound, req.body.password, function (err){
						if(err) return self.makeServerError(next, err);

						passwordRecoveryRequest.recovered = new Date();
						passwordRecoveryRequest.save(function (err, passwordRecoveryRequestSaved){
							if(err) return self.makeServerError(next, err);

							res.send('Ok');
							next();
						});
					});

				});
			});
		},
		del: function (req, res, next){
			self.authorize.ensureAdmin(req, res, function (err){
				if(err) self.makeServerError(next, err);
				if(!req.params.emitterId) return self.makeServerError(next, 'Missing_Argument');

				cleaner.deleteEmitter({ _id: req.params.emitterId }, function (err){
					if(err) return self.makeServerError(next, err);

					res.send('Ok');
					next();
				});
			});
		}
	};
}();
