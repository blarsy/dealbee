var restify = require('restify'),
	settings = require(process.cwd() + '/settings'),
	fs = require('fs'),
	authorize = require(process.cwd() + '/api/lib/authorize');
module.exports = {
	post: function(req, res, next){
		authorize.ensureAuthenticated(req, res, function(err, session){
			var resourceFilePath = process.cwd() + '/' + settings.WEBAPPPATH + 'res.json';
				resourceFile = require(resourceFilePath);

			if(err) return next(new restify.InternalError(err));
			if(!session.loggedInIdentifier.powerUser) return next(new restify.NotAuthorizedError());
			if(!req.body.resources) return next(new restify.InternalError('Expecting_Array_Of_Resources'));

			resourceFile.resources = req.body.resources;

			fs.writeFile(resourceFilePath, JSON.stringify(resourceFile), function (err){
				if(err) return next(err);

				res.send('Ok');
				next();
			});
		});
	}
}