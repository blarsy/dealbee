
module.exports = function(){
		route = require(process.cwd() + '/api/routes/routeBase'),
		self = this;

	route.mixWith(this);
	
	return {
		get: function (req, res, next){
			self.authorize.ensureAdmin(req, res, function(err, sessionData){
				if(err) return self.makeServerError(next, err);
				var restApiServer = require(process.cwd() + '/api/restapiserver'),
					jobAgent = require(process.cwd() + '/api/lib/jobs/jobAgent');

				res.send({ 
					inMaintenance: restApiServer.inMaintenance,
					jobsRunning: jobAgent.running
				});
				next();
			});
		},
		post: function (req, res, next){
			self.authorize.ensureAdmin(req, res, function(err, sessionData){
				if(err) return self.makeServerError(next, err);

				if(typeof req.body.inMaintenance === 'undefined') return self.makeServerError(next, 'InMaintenance_Required');
				if(typeof req.body.jobsRunning === 'undefined') return self.makeServerError(next, 'JobsRunning_Required');

				var restApiServer = require(process.cwd() + '/api/restapiserver'),
					jobAgent = require(process.cwd() + '/api/lib/jobs/jobAgent');
				restApiServer.inMaintenance = req.body.inMaintenance;
				if(jobAgent.running !== req.body.jobsRunning){
					if(jobAgent.running){
						jobAgent.stop(function (err){
							if(err) return self.makeServerError(next, err);

							res.send({ inMaintenance: restApiServer.inMaintenance, jobsRunning: jobAgent.running });
							return next();
						});

					} else {
						jobAgent.start();

						res.send({ inMaintenance: restApiServer.inMaintenance, jobsRunning: jobAgent.running });
						return next();
					}
				} else {
					res.send({ inMaintenance: restApiServer.inMaintenance, jobsRunning: jobAgent.running });
					return next();
				}
			});
		}
	};
}();