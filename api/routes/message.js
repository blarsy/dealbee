var restify = require('restify'),
	_ = require('underscore'),
	authorize = require(process.cwd() + '/api/lib/authorize'),
	mailSender = require(process.cwd() + '/api/lib/mailassets/mailSender'),
	settings = require(process.cwd() + '/settings');

module.exports = {
	post: function (req, res, next){
		if(!req.body.subject || req.body.subject.toString().trim() === '') return next(new restify.InternalError('Subject_Required'));
		if(!req.body.body || req.body.body.toString().trim() === '') return next(new restify.InternalError('Body_Required'));

		if(!req.body.replyTo){
			authorize.ensureAuthenticated(req, res, function (err){
				if(err) return next(new restify.InternalError(err));

				mailSender.send({ from: req.session.loggedInEmitter.email, to: settings.SUPPORTMAIL, subject: req.body.subject, text: req.body.body, html: req.body.body }, function (err){
					if(err) return next(err);

					res.send(200);
					next();
				});
			})
		} else {
			mailSender.send({ from: req.body.replyTo, to: settings.SUPPORTMAIL, subject: req.body.subject, text: req.body.body, html: req.body.body }, function (err){
				if(err) return next(err);

				res.send(200);
				next();
			});
		}
	}
};