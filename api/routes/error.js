module.exports = function (){
	var result = {},
		authorize = require(process.cwd() + '/api/lib/authorize');
	
	result.post = function(req, res, next){
		if(req.params.sessionId || req.body.sessionId){
			authorize.ensureAuthenticated(req, res, function (err, sessionData){
				if(err) return next(new restify.InternalError(err));
				res.send('Ok');
				next();
			});
		} else {
			//Do nothing, the sole purpose of this route is to be logged
			res.send('Ok');
			next();
		}
	};

	return result;
}()