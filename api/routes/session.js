module.exports = function(){
	var restify = require('restify'),
		_ = require('underscore'),
		models = require(process.cwd() + '/api/models'),
		emitterService = require(process.cwd() + '/api/lib/emitterService'),
		authorize = require(process.cwd() + '/api/lib/authorize');

	return {
		post: function(req, res, next){
			if(!req.body.fbToken && (!req.body.password || !req.body.email)) return next(new restify.InternalError('MissingArgument'));

			authorize.login(req, req.body.fbToken, req.body.email, req.body.password, function (err){
				if(err) return next(new restify.InternalError(err));

				res.send(emitterService.makeClientObject(req.session.loggedInEmitter, req.session.loggedInIdentifier, req.session.loggedInClient));
				next();
			});
		},
		admin: function (req, res, next){
			if(!req.body.fbToken) return next(new restify.InternalError('MissingArgument'));

			authorize.loginFb(req, req.body.fbToken, function (err){
				if(err) return next(err);
				if(!req.session.loggedInIdentifier) return next('LoginFailed');

				res.send(emitterService.makeClientObject(req.session.loggedInEmitter, req.session.loggedInIdentifier, req.session.loggedInClient));
				next();
			});
		},
		get: function(req, res, next){
			var sessionId = req.params.id,
				emitterId = req.params.emitterId;
			if(!sessionId && !emitterId) return next(new restify.InternalError('MissingArgument'));

			if(sessionId){
				authorize.restoreSession(sessionId, req, res, function (){
					if(!req.session.loggedInEmitter) return next(new restify.InternalError('SessionExpired'));
					res.send(emitterService.makeClientObject(req.session.loggedInEmitter, req.session.loggedInIdentifier, req.session.loggedInClient));
					next();
				});
			} else {
				authorize.ensureAdmin(req, res, function (err){
					if(err) return next(new restify.InternalError(err));

					models.sessionLog.find({ emitter: emitterId }).lean().exec(function (err, sessionLogs){
						if(err) return next(err);

						_.each(sessionLogs, function (sessionLog){
							sessionLog.activities = sessionLog.activity ? sessionLog.activity.length : 0;
							delete sessionLog.activity;
						});

						res.send(sessionLogs);
						next();
					});
				});
			}
		},
		switchEmitter: function (req, res, next){
			authorize.ensureAuthenticated(req, res, function (err){
				if(err) return next(new restify.InternalError(err));

				if(!req.body.emitterId) return next(new restify.InternalError('Missing_Argument'));
				authorize.switchEmitter(req, req.body.emitterId, function (err){
					if(err) return next(new restify.InternalError(err));

					res.send(emitterService.makeClientObject(req.session.loggedInEmitter, req.session.loggedInIdentifier, req.session.loggedInClient));
					next();
				});
			});
		}
	};
}(); 