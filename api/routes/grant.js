module.exports = function(){
	var restify = require('restify'),
		models = require(process.cwd() + '/api/models'),
		_ = require('underscore'),
		uuid = require('node-uuid'),
		settings = require(process.cwd() + '/settings'),
		facebook = require(process.cwd() + '/api/lib/facebook'),
		dealSender = require(process.cwd() + '/api/lib/jobs/dealSender'),
		giftConfirmationSender = require(process.cwd() + '/api/lib/jobs/giftConfirmationSender'),
		authorize = require(process.cwd() + '/api/lib/authorize'),
		resources = require(process.cwd() + '/api/res.json'),
		dealService = require(process.cwd() + '/api/lib/dealService'),
		bizTime = require(process.cwd() + '/api/lib/bizTime'),
		grantService = require(process.cwd() + '/api/lib/grantService')
		getGrantToGive = function(dealId, done){
			if(!dealId) return done(new restify.InternalError('DealId_Required'));

			models.grant.findOne({ uuid: dealId }).populate('deal').exec(function (err, grant){
				if(err) return done(err);

				if(!grant.consumed){
					var now = new Date();
					if(grant.deal.expiration > now){
						models.emitter.findOne({ _id: grant.deal.emitter }).lean().exec(function (err, emitter){
							if(err) return done(err);
							grant.emitter = emitter;

							return done(null, grant);
						});
					} else {
						return done(new restify.InternalError('DealExpired'));
					}
				} else {
					return done(new restify.InternalError('DealAlreadyConsumed'));
				}
			});
		},
		setDealToBeGiven = function(grant, senderFbUid, fbDestinatorUser, email, senderName, done){
			//If authenticated by Facebook, directly proceed to the gift
			var now = new Date();

			grant.previousHolders.push({
				uuid: grant.uuid,
				email: grant.email,
				fbDestinatorUser: grant.fbDestinatorUser,
				received: grant.sent,
				given: now
			});
			grant.channelType = email ? 1 : 0;
			grant.email = email;
			grant.senderName = senderName;
			grant.senderFbUid = senderFbUid;
			grant.fbDestinatorUser = fbDestinatorUser;
			grant.sent = null;
			grant.uuid = uuid.v4();

			grant.save(function (err, savedGrant){
				if(err) return done(err);
				done();
			});
		},
		createGiftEmailConfirmation = function(grant, destinatorEmail, senderName, destinatorFbUid, fbDestinatorUser, done){
			var now = new Date(),
				giftEmailConfirmation = {
					created: now,
					grant: grant,
					destinatorEmail: destinatorEmail,
					senderName: senderName,
					senderFbUid: destinatorFbUid,
					fbDestinatorUser: fbDestinatorUser,
					expires: new Date(now.getTime() + (settings.MAILGIFTREQUESTTTLMINUTES * 60 * 1000)), 
					uuid: uuid.v4()
				};

			models.giftEmailConfirmation.create(giftEmailConfirmation, function (err, giftSaved){
				if(err) return done(err);
				done();
			});
		};

	return {
		get: function (req, res, next){
			if(!req.params.dealId && !req.params.grantId){
				if(!req.params.id) return next(new restify.InternalError('Id_Required'));

				dealService.getGrantForDisplay(req.params.id, function(err, grant){
					if(err) return next(err);
					if(!grant){
						res.send({});
						return next();
					}

					res.send(grant);
					next();
				});
			} else {
				authorize.ensureAdmin(req, res, function (err){
					if(err) return next(new restify.InternalError(err));

					if(req.params.dealId){
						models.grant.find({ deal: req.params.dealId }, function (err, grants){
							if(err) return next(err);

							res.send(grants);
							next();
						});
					} else {
						models.grant.findOne({ _id: req.params.grantId }).lean().exec(function (err, grant){
							if(err) return next(err);

							models.giftEmailConfirmation.find({ grant: grant._id }, function (err, giftEmailConfirmations){
								if(err) return next(err);

								grant.giftEmailConfirmations = giftEmailConfirmations;

								res.send(grant);
								next();
							});

						});
					}
				});
			}
		},
		resend: function (req, res, next){
			authorize.ensureAdmin(req, res, function (err){
				if(err) return next(err);
				if(!req.body.id) return new restify.InternalError('Id_Required');

				models.grant.findOne({ _id: req.body.id }).populate('deal').exec(function (err, grant){
					if(err) return next(err);

					models.emitter.findOne({ _id: grant.deal.emitter }, { name: 1, language: 1, identifiers: 1 }, function (err, emitter){
						if(err) return next(err);
						grant.emitter = emitter;

						dealSender.send(grant, function (err, success){
							if(err){
								//the error has already been logged by the dealSender
								res.send(500, err);
								return next();
							}
							if(!success){
								res.send(500, 'failed');
								return next();
							}

							res.send(grant);
							next();
						});					
					})
				});
			});
		},
		giveByEmail: function (req, res, next){
			if(!req.body.email) return next(new restify.InternalError('Email_Required'));

			getGrantToGive(req.body.dealId, function (err, grant){
				if(err) return next(err);

				if(req.body.email){
					createGiftEmailConfirmation(grant, req.body.email, req.body.senderName, null, null, function(err){
						if(err) return next(err);

						res.send('Ok');
						next();
					});
				} else {
					facebook.createOrRefreshPublishInfo(req.body.senderFbToken, function (err, tokenData){
						if(err) return next(err);

						setDealToBeGiven(grant, tokenData.uid, null, req.body.email, req.body.senderName, function (err){
							if(err) return next(err);

							res.send('Ok');
							next();
						});
					});
				}
			});
		},
		giveByFacebook: function (req, res, next){
			if(!req.body.senderFbToken) return next(new restify.InternalError('SenderFacebookId_Required'));
			if(!req.body.destinatorFbUser) return next(new restify.InternalError('DestinatorFbUser_Required'));
			if(!req.body.destinatorFbUser.tagId) return next(new restify.InternalError('DestinatorFbUser_tagId_Required'));
			if(!req.body.destinatorFbUser.name) return next(new restify.InternalError('DestinatorFbUser_Name_Required'));
			if(!req.body.destinatorFbUser.picUrl) return next(new restify.InternalError('DestinatorFbUser_PicUrl_Required'));
			if(!req.body.destinatorFbUser.taggedMessage) return next(new restify.InternalError('DestinatorFbUser_TaggedMessage_Required'));
			
			getGrantToGive(req.body.dealId, function (err, grant){
				if(err) return next(err);

				req.body.destinatorFbUser.taggedMessage += resources.tagWith[grant.emitter.language] + '@tag';
				facebook.createOrRefreshPublishInfo(req.body.senderFbToken, function (err, tokenData){
					if(err) return next(err);

					if(grant.email){
						//We have the email address of the current owner, so we first request email confirmation of the gift
						createGiftEmailConfirmation(grant, null, tokenData.uid, req.body.destinatorFbUser, function(err){
							if(err) return next(err);

							res.send('Ok');
							next();
						})
					} else {
						//We can't check the identity of the owner, we assume we can give the deal right away
						setDealToBeGiven(grant, tokenData.uid, req.body.destinatorFbUser, null, null, function (err){
							if(err) return next(err);
							res.send('Ok');
							next();
						});
					}
				});
			});
		},
		resendGiftConfirmationEmail: function (req, res, next){
			authorize.ensureAdmin(req, res, function (err){
				if(err) return next(err);
				if(!req.body.giftConfirmationId) return next(new restify.InternalError('GiftConfirmationId_Required'));

				models.giftEmailConfirmation.findOne({ _id: req.body.giftConfirmationId }).populate('grant').exec(function (err, giftConfirmation){
					if(err) return next(err);

					models.deal.findOne({ _id: giftConfirmation.grant.deal }).populate('emitter').exec(function (err, deal){
						if(err) return next(err);

						giftConfirmation.grant.deal = deal;
						giftConfirmation.emitter = deal.emitter;
						
						giftConfirmationSender.send(giftConfirmation, function (err, success){
							if(err) return next(err);
							if(success){
								res.send('Ok');
							} else {
								res.send('Failed');
							}
							next();
						});
					});
				});
			});
		},
		confirmGift: function(req, res, next){
			if(!req.body.id) return next(new restify.InternalError('GiftConfirmationId_Required'));

			models.giftEmailConfirmation.findOne({ uuid: req.body.id }).populate('grant').exec(function (err, giftConfirmation){
				if(err) return next(err);
				if(!giftConfirmation) return next(new restify.InternalError('GiftConfirmation_NotFound'));

				if(giftConfirmation.confirmed) return next(new restify.InternalError('WasAlreadyConfirmed'));
				if(giftConfirmation.expires < new Date()) return next(new restify.InternalError('ConfirmationRequestExpired'));
				if(giftConfirmation.grant.consumed) return next(new restify.InternalError('DealAlreadyConsumed'));

				setDealToBeGiven(giftConfirmation.grant, giftConfirmation.senderFbUid, giftConfirmation.fbDestinatorUser, giftConfirmation.destinatorEmail, giftConfirmation.senderName, function (err){
					if(err) return next(err);

					giftConfirmation.confirmed = new Date();
					giftConfirmation.save(function (err, giftConfirmationSaved){
						if(err) return next(err);

						res.send('Ok');
						next();
					});

				});
			});
		},
		post: function (req, res, next){
			if(!req.body.email) return next(new restify.InternalError('Email_Required'));
			if(!req.body.dealId) return next(new restify.InternalError('DealId_Required'));

			grantService.createOnRequest(req.body.dealId, req.body.email, function (err){
				if(err) return next(new restify.InternalError(err));

				res.send('Ok');
				next();
			});
		},
		scan: function (req, res, next){
			authorize.ensureAuthenticated(req, res, function(err){
				if(err) return next(new restify.InternalError(err));
				if(!req.body.uuid) return next(new restify.InternalError('Uuid_Required'));

				models.grant.findOne({ uuid: req.body.uuid }, { number:1, email: 1, fbDestinatorUser: 1, consumed: 1, deal: 1 }, function (err, grantFound){
					if(err) return next(err);

					var scanResponse = {
						grantNumber: grantFound.number,
						dealHolder:{
							name: grantFound.email ? grantFound.email : grantFound.fbDestinatorUser.name,
							picUrl: grantFound.fbDestinatorUser.picUrl || null
						}
					}


					models.deal.findOne({ _id: grantFound.deal }, { benefit: 1, teaser: 1 }, function (err, dealFound){
						if(err) return next(err);

						scanResponse.teaser = dealFound.teaser;
						scanResponse.benefit = dealFound.benefit;

						if(grantFound.consumed){
							scanResponse.scanDate = grantFound.consumed;

							res.send(scanResponse);
							next();
						} else {
							grantFound.consumed = bizTime.now();

							grantFound.save(function (err){
								if(err) return next(err);

								res.send(scanResponse);
								next();
							});
						}
					});
				});
			});
		}
	};
}();