module.exports = function(){
	var mongoose = require('mongoose'),
		settings = require(process.cwd() + '/settings'),
		fbAccessInfoSchema = new mongoose.Schema({
			uid: { type: String, unique: true },
			accessToken: String,
			dateIssued: Date
		}),
		clientSchema = new mongoose.Schema({
			invoicingData: {
				address: {
					street: String,
					number: String,
					box: String,
					postcode: String,
					city: String,
					country: String,
					created: Date
				},
				hasNoVATNumber: Boolean,
				VATCountryCode: String,
				VATNumber: String,
				companyName: String,
				juridicalForm: String,
				language: String
			},
			payments:[{
				date: Date,
				amount: Number,
				vatRate: Number,
				paid: Number,
				description: String,
				uuid: String,
				invoice: {
					lastDate: Date,
					number: Number
				}
			}],
			packs:[{
				begin: Date,
				end: Date,
				amount: Number,
				remaining: Number,
				subscriptionKind: String,
				created: Date,
				origin: String,
				price: Number,
				payment: String
			}],
			distributions:[{
				date: Date,
				amount: Number
			}],
			subscription: {
				latestRenewal: Date,
				kind: String,
				paymentFrequency: Number,
				price: Number,
				latestChargeFailure: Date
			},
			stripeCustomerId: String,
			created: Date
		}, { safe: { j: 1,  wtimeout: 5000 } }),
		identifierSchema = new mongoose.Schema({
			email: String,
			type: Number,
			lastLoggedInEmitter: { type: mongoose.Schema.Types.ObjectId, ref: 'emitter' },
			defaultEmitter: { type: mongoose.Schema.Types.ObjectId, ref: 'emitter' },
			hashInfo: {
				value: Buffer,
				keySize: Number,
				algorithm: String,
				iterations: Number,
				salt: Buffer
			},
			fbUid: { type: String, index: true },
			client: { type: mongoose.Schema.Types.ObjectId, ref: 'client' }
		}, { safe: { j: 1,  wtimeout: 5000 } }),
		emitterSchema = new mongoose.Schema ({
			name: String,
			website: String,
			imageRef: {
				public_id: String,
				format: String
			},
			language: String,
			address: {
				street: String,
				number: String,
				box: String,
				postcode: String,
				city: String,
				country: String,
				useMap: Boolean,
				coords: {
					index: '2dsphere',
					type: [Number]
				},
				created: Date
			},
			identifiers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'identifier' }],
			created: Date
		}, { safe: { j: 1,  wtimeout: 5000 } }),
		dealSchema = new mongoose.Schema ({
			teaser: String,
			benefit: String,
			description: String,
			beginDate: Date,
			expiration: Date,
			reimbursed: Date,
			imageRef: {
				public_id: String,
				format: String
			},
			emitter: { type: mongoose.Schema.Types.ObjectId, ref: 'emitter' },
			termsOfUse: [String],
			nbOnRequest: Number,
			created: Date,
			private: Boolean,
			keywords: [String]
		}, { safe: { j: 1,  wtimeout: 5000 } }),
		giftEmailConfirmationSchema = new mongoose.Schema({
			created: Date,
			sent: Date,
			grant: { type: mongoose.Schema.Types.ObjectId, ref: 'grant', index: true },
			senderFbUid: String,
			senderName: String,
			fbDestinatorUser: {
				tagId: String,
				name: String,
				picUrl: String,
				taggedMessage: String
			},
			destinatorEmail: String,
			expires: Date,
			confirmed: Date,
			uuid: String,
			consecutiveSendErrors: Number,
			sendErrors: [{
				date: Date,
				error: String
			}]
		}),
		grantSchema = new mongoose.Schema ({
			deal: { type: mongoose.Schema.Types.ObjectId, ref: 'deal', index: true },
			channelType: Number,
			email: String,
			senderName: String,
			senderFbUid: String,
			fbDestinatorUser: {
				tagId: String,
				name: String,
				picUrl: String,
				taggedMessage: String
			},
			previousHolders: [{
				uuid: String,
				email: String,
				fbDestinatorUser: {
					tagId: String,
					name: String,
					picUrl: String
				},
				received: Date,
				given: Date
			}],
			uuid: { type: String, index: true },
			created: Date,
			sent: Date,
			consumed: Date,
			number: Number,
			consecutiveSendErrors: Number,
			sendErrors: [{
				date: Date,
				error: String
			}]
		}),
		preferenceSchema = new mongoose.Schema({
			email: { type: String, index: true },
			unsubscribedDate: Date,
			blockedEmitters: [{ type: mongoose.Schema.Types.ObjectId, ref: 'emitter' }]
		}),
		passwordRecoveryRequestSchema = new mongoose.Schema({
			email: { type: String, index: true },
			expiration: Date,
			emitter: { type: mongoose.Schema.Types.ObjectId, ref: 'emitter' },
			recovered: Date,
			uuid: { type: String, index: true }
		}),
		sessionLogSchema = new mongoose.Schema({
			identifier: { type: mongoose.Schema.Types.ObjectId, ref: 'identifier', index: true },
			emitter: { type: mongoose.Schema.Types.ObjectId, ref: 'emitter', index: true },
			id: { type: String, index: true },
			created: Date,
			identifierType: Number,
			activity: [{
				time: Date,
				route: String,
				req: String,
				err: String,
				res: String
			}]
		}),
		settingsSchema = new mongoose.Schema({
			nextInvoiceNumber: Number
		}, { safe: { j: 1,  wtimeout: 5000 } }),
		managerInvitationSchema = new mongoose.Schema({
			emitter: { type: mongoose.Schema.Types.ObjectId, ref: 'emitter', index: true },
			uuid: { type: String, index: true },
			requestor: { type: mongoose.Schema.Types.ObjectId, ref: 'identifier', index: true },
			newManagerEmail: String,
			created: Date,
			accepted: Date
		}),
		connection_string;

	return {
		identifier: mongoose.model('identifier', identifierSchema),
		emitter: mongoose.model('emitter', emitterSchema),
		client: mongoose.model('client', clientSchema),
		fbAccessInfo: mongoose.model('fbAccessInfo', fbAccessInfoSchema),
		deal: mongoose.model('deal', dealSchema),
		grant: mongoose.model('grant', grantSchema),
		giftEmailConfirmation: mongoose.model('giftEmailConfirmation', giftEmailConfirmationSchema),
		preference: mongoose.model('preference', preferenceSchema),
		passwordRecoveryRequest: mongoose.model('passwordRecoveryRequest', passwordRecoveryRequestSchema),
		sessionLog: mongoose.model('sessionLog', sessionLogSchema),
		settings: mongoose.model('settings', settingsSchema),
		managerInvitation: mongoose.model('managerInvitation', managerInvitationSchema),
		connectionInfo: mongoose.connect(settings.DB)
	};
}();