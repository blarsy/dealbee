module.exports = function(){
	var settings = require(process.cwd() + '/settings'),
		redisConnection = process.env.OPENSHIFT_REDIS_HOST ? { 
			port: process.env.OPENSHIFT_REDIS_PORT, 
			host: process.env.OPENSHIFT_REDIS_HOST, 
			pass: process.env.REDIS_PASSWORD || settings.REDISPASSWORD
		} : {},
		session = require('restify-session')({ connection: redisConnection, ttl: settings.SESSIONTTL });

	return session;
}();