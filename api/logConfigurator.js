module.exports = function(server){
	var	_ = require('underscore'),
		bunyan = require('bunyan'),
		settings = require(process.cwd() + '/settings')
		restify = require('restify'),
		auditHandler = restify.auditLogger({ 
			log: bunyan.createLogger({
				name: 'audit', streams:[
					{
						type: 'rotating-file',
						period: '1d',
		        		count: 3,
						level: 'info',
						path: process.cwd() + settings.LOGPATH + 'webaudit.log'
					}
				]
			})
		}),
		errorHandler = restify.auditLogger({
			log: bunyan.createLogger({
				name: 'error', streams:[
					{
						level: 'info',
						stream: process.stdout
					},
					{
						type: 'rotating-file',
						period: '1d',
		        		count: 3,
						level: 'info',
						path: process.cwd() + settings.LOGPATH + 'weberror.log'
					}
				]
			}),
			body: true
		}),
		localHandler = restify.auditLogger({
			log: bunyan.createLogger({
				name: 'error', streams:[
					{
						level: 'info',
						stream: process.stdout
					},
					{
						type: 'rotating-file',
						period: '1d',
		        		count: 3,
						level: 'info',
						path: process.cwd() + settings.LOGPATH + 'local.log'
					}
				]
			}),
			body: true
		}),
		nonLoggedRoutes = [{ method: 'POST', url: '/api/session'}, 
							{ method: 'POST', url: '/api/giveByEmail'}, 
							{ method: 'POST', url: '/api/giveByFacebook'}, 
							{ method: 'POST', url: '/api/confirmGift'},
							{ method: 'POST', url: '/api/preference'}, 
							{ method: 'GET', url: '/api/preference'}, 
							{ method: 'POST', url: '/api/message'}, 
							{ method: 'POST', url: '/api/changepassword'}, 
							{ method: 'POST', url: '/api/forgotpassword'}, 
							{ method: 'POST', url: '/api/recoverpassword'}, 
							{ method: 'POST', url: '/api/session/admin'} ],
		models = require(process.cwd() + '/api/models');
		function filterOutGiganticKloudlessErr(req, res, route, reqErr, logHandler){
			if(reqErr && reqErr.err && reqErr.err.type && reqErr.err.type.toString() === 'KloudlessError'){
				var smallerReqErr = { type: reqErr.err.type };
				if(reqErr.err.message) smallerReqErr.message = reqErr.err.message;
				if(reqErr.err.exception) smallerReqErr.exception = reqErr.err.exception;
				if(reqErr.err.errorData) smallerReqErr.errorData = reqErr.err.errorData;
				if(reqErr.err.status) smallerReqErr.status = reqErr.err.status;
				reqErr = smallerReqErr;
			}
			logHandler(req, res, route, reqErr);
		};
		function logApiRequest(req, res, route, reqErr){
			if(req.url.indexOf('/api/')!== -1 && !_.find(nonLoggedRoutes, function(nonLoggedRoute){ return nonLoggedRoute.method === req.method && req.url.indexOf(nonLoggedRoute.url) !== -1 })){
				if(!req.session) return filterOutGiganticKloudlessErr(req, res, route, new restify.InternalError('Session_NotFound'), errorHandler);
				models.sessionLog.findOne({ id: req.session.sid }, function (err, sessionLog){
					if(err) return filterOutGiganticKloudlessErr(req, res, route, err, errorHandler);

					var thisActivity;
					if(!sessionLog){
						if(reqErr || req.url.indexOf('/api/error') !== -1){
							return filterOutGiganticKloudlessErr(req, res, route, reqErr, errorHandler);
						} else {
							return filterOutGiganticKloudlessErr(req, res, route, reqErr, auditHandler);
						}
					};

					if(!sessionLog.activity) sessionLog.activity = [];

					thisActivity = {
						time: req.time(),
						req: JSON.stringify({ 
							params: JSON.stringify(req.params), 
							body: JSON.stringify(req.body), 
							method: res.method,
							headers: JSON.stringify(req.headers)
						}),
						res: JSON.stringify({ 
							data: JSON.stringify(res.data), 
							statusCode: res.statusCode, 
							contentLength: res['content-length'] 
						}),
						route: req.url
					};

					if(reqErr){
						thisActivity.err = JSON.stringify({ 
							name: reqErr.name, 
							message: reqErr.message, 
							lineNumber: reqErr.lineNumber, 
							fileName: reqErr.fileName, 
							columnNumber: reqErr.columnNumber, 
							stack: reqErr.stack, 
						});
					}

					sessionLog.activity.push(thisActivity);

					sessionLog.save(function (err, sessionLogSaved){
						if(err) return filterOutGiganticKloudlessErr(req, res, route, err, errorHandler);

						if(settings.ENVIRONMENT === 'local'){
							if(reqErr){
								return filterOutGiganticKloudlessErr(req, res, route, reqErr, errorHandler);
							} else {
								return filterOutGiganticKloudlessErr(req, res, route, reqErr, localHandler);
							}
						}
					});
				});
			} else {
				if(reqErr){
					return filterOutGiganticKloudlessErr(req, res, route, reqErr, errorHandler);
				} else {
					return filterOutGiganticKloudlessErr(req, res, route, reqErr, auditHandler);
				}
			}
		};

	server.on('after', function (req, res, route, err){
		logApiRequest(req, res, route, err);
	});
	server.on('uncaughtException', function (req, res, route, err){
		if(err.message === 'maintenance') return;
		logApiRequest(req, res, route, err);
		res.send(new restify.InternalError('InternalError'));
	});
};