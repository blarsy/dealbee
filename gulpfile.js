var gulp = require('gulp'),
	cliConfig = require(process.cwd() + '/deploy/cliConfig'),
	util = require('gulp-util'),
	fs = require('fs'),
	serversLauncher = require(process.cwd() + '/deploy/serversLauncher');

require(process.cwd() + '/deploy/dev')();
require(process.cwd() + '/deploy/test')();
require(process.cwd() + '/deploy/deploy')();
require(process.cwd() + '/deploy/minify')();

gulp.task('default', function(taskCallback) {
	var redisProc;
	if(!cliConfig.run){
		if(!cliConfig.deploy){
			if(!cliConfig.test){
				gulp.start('dev');
			} else {
				gulp.start('test');
			}
		} else {
			if(cliConfig.copyOnly){
				gulp.start('deploy-copyOnly');
			} else {
				gulp.start('deploy-full');
			}
		}
	} else {
		gulp.start(cliConfig.run);
	}
	taskCallback();
});

gulp.task('launchServers', function (taskCallback){
	serversLauncher.launch();
	taskCallback();	
});