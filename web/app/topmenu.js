'use strict';

angular.module('Dealbee.topmenu', [])
.controller('TopMenuCtrl', function ($scope, l8n, authentication, settings, $location, $rootScope){
	$scope.disconnect = function (){
		authentication.killSession();
	};
	$scope.navigateToLogin = function (){
		$location.path('/login');
	};
	$scope.navigateToSubscribe = function (){
		$location.path('/subscribe');
	};
	$scope.switchToEmitter = function (emitter){
		authentication.switchToEmitter(emitter._id).then(function (){
			$location.path('/home');
		});
	};
	$scope.environment = settings.environment;
});