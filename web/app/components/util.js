var app = {};

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
};

app.formatting = {};
app.formatting.pad = function (s) { 
  return (s < 10) ? '0' + s : s;
};
app.formatting.toDate = function(date, langCode){
  var pad = app.formatting.pad;
  if(langCode === 'en'){
    return [pad(date.getMonth()+1), pad(date.getDate()), date.getFullYear()].join('-');
  } else {
    return [pad(date.getDate()), pad(date.getMonth()+1), date.getFullYear()].join('/');
  }
};
app.formatting.toDateTime = function(date, langCode){
  var pad = app.formatting.pad;
  if(langCode === 'en'){
    return [pad(date.getMonth()+1), pad(date.getDate()), date.getFullYear()].join('-') + ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes());
  } else {
    return [pad(date.getDate()), pad(date.getMonth()+1), date.getFullYear()].join('/') + ' ' + pad(date.getHours()) + ':' + pad(date.getMinutes());
  }
};
app.formatting.toMonth = function(date, langCode){
  var pad = app.formatting.pad;
  if(langCode === 'en'){
    return [pad(date.getMonth()+1), date.getFullYear()].join('-');
  } else {
    return [pad(date.getMonth()+1), date.getFullYear()].join('/');
  }
};