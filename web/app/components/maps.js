angular.module('Dealbee.services.maps', [])
	.factory('maps', function($injector){
		var marker = null,
			map = null,
			self = this;

	this.initialize = function(lat, lng, elementId, positionChanged){
		try{
			var myLatlng = new google.maps.LatLng(lat, lng),
				mapOptions = {
					zoom: 14,
					center: myLatlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
			map = new google.maps.Map(document.getElementById(elementId), mapOptions);

			marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				draggable: true
			});

			google.maps.event.addListener(marker, 'dragend', function() {
				positionChanged(marker.getPosition());
			});
		}
		catch(ex){
			//initialization failed, make the whole object do nothing
			self.initLocationDisplay = function (){};
			self.geocode = function (){};
			var rootScope = $injector.get('$rootScope');
			rootScope.displayError('errorLoadingGoogleMaps', 'errorLoadingGoogleMaps');
		}
	};

	this.initLocationDisplay = function(lat, lng, elementId){
		var myLatlng = new google.maps.LatLng(lat, lng),
			mapOptions = {
				zoom: 14,
				center: myLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
		map = new google.maps.Map(document.getElementById(elementId), mapOptions);

		marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			draggable: false
		});
	};

	this.geocode = function(address, positionChanged){
		geocoder = new google.maps.Geocoder();
		geocoder.geocode({ 'address': address }, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				marker.setMap(null);
				map.setCenter(results[0].geometry.location);
				marker = new google.maps.Marker({
					position: results[0].geometry.location,
					map: map,
					draggable: true
				});
				google.maps.event.addListener(marker, 'dragend', function() {
					positionChanged(marker.getPosition());
				});

				positionChanged(results[0].geometry.location);
			}
		});
	};

	this.getDistance = function (lat1, lng1, lat2, lng2){
		return google.maps.geometry.computeDistanceBetween(new google.maps.LatLng(lat1, lng1), new google.maps.LatLng(lat2, lng2));
	}

	return this;
});
