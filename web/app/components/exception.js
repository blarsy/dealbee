angular.module('Dealbee.exception', [])
	.factory('$exceptionHandler', function($log, $injector) {
		return function(exception, cause) {
			//Call rest api log method, fail-safe
			try{
				var restApi = $injector.get('restApi');
				if(restApi){
					var error = new restApi.error({ message: exception.stack });
					error.$save();
				}
			}
			catch(ex){}
			$log.error(exception);
		};
	});