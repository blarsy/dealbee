angular.module('Dealbee.interceptors', [])
.factory('sessionInjector', function(globalState, $q, $rootScope){
	var sessionInjector = {
		request: function(config) {
			if (config.url.indexOf('/api/') !== -1 && globalState.sessionId){
				if(config.method.toLowerCase() !== 'post'){
					config.params = config.params || {};
					config.params.sessionId = globalState.sessionId;
				} else {
					config.data = config.data || {};
					config.data.sessionId = globalState.sessionId;
				}
			}
			return config;
		},
		responseError: function(res) {
			if($rootScope.handleExpiredSession(res)){
				return $q.reject('Reconnecting');
			} else {
				return $q.reject(res);
			}
		}
	};
	return sessionInjector;
})
.config(function($httpProvider) {
    $httpProvider.interceptors.push('sessionInjector');
});
