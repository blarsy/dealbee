angular.module('Dealbee.services.facebook', [])
	.factory('facebook', function($q, $injector, settings, restApi, l8n, fbWrapper){
		var self = this,
			FB = fbWrapper.FB,
			loadFbAccount = function (response, callback){
				var permissions = response.authResponse.grantedScopes ? 
						response.authResponse.grantedScopes.split(',') :
						[],
						canPublish,
						publishPerm,
						rootScope = $injector.get('$rootScope');

				publishPerm = _.find(permissions, function (permission){ return permission === 'publish_actions'; }),
				canPublish = publishPerm ? true : false;

				if(!rootScope.fbAccount){
					FB.api('/me', function (apiResponse){
						rootScope.fbAccount = {
							loggedIn: true, 
							token: response.authResponse.accessToken,
							email: apiResponse.email,
							name: apiResponse.name,
							id: apiResponse.id,
							canPublish: canPublish };
						getPic(30).then(function (url){
							rootScope.fbAccount.pic = url;
						});					
						rootScope.$broadcast('fbConnected');

						if(callback) callback(null, rootScope.fbAccount, canPublish);
					});
				} else {
					rootScope.fbAccount.canPublish = canPublish;
					if(callback) callback(null, rootScope.fbAccount, canPublish);
				}

			},
			getPic = function(height){
				var result = $q.defer();

				FB.api('/me/picture?height=' + height + '&type=normal&redirect=false', function(response){
					if(response.error) result.reject(response.error);
					result.resolve(response.data.url);
				});

				return result.promise;
			},
			getFriendsPage = function(pageSize, token, afterCursor, pageReadyCallback){
				var url = '/me/taggable_friends?limit=' + pageSize + '&fields=name,picture,id&access_token=' + token;

				if(afterCursor) url += '&after=' + afterCursor;

				FB.api(url, function (response){
					if(response.error) pageReadyCallback(response.error);

					if(response.data.length === pageSize && response.paging && response.paging.cursors && response.paging.cursors.after){
						getFriendsPage(pageSize, token, response.paging.cursors.after, pageReadyCallback);
					}

					pageReadyCallback(null, response.data);
				});
			};

		this.getFriends = function(pageReady){
			var rootScope = $injector.get('$rootScope'),
				pageSize = 500;

			getFriendsPage(pageSize, rootScope.fbAccount.token, '', pageReady);
		};
		
		this.processLoginResponseWithPublishPerm = function (loginResponse){
			var fbUser = new restApi.fbUser(),
				result = $q.defer();

			fbUser.token = loginResponse.authResponse.accessToken;
			fbUser.$save(function (){
				result.resolve();
				}, function(response){
					result.reject(response);
			});

			return result.promise;
		};

		this.processLoginResponse = function (loginResponse){
			var result = $q.defer();

			if(loginResponse.authResponse){
				loadFbAccount(loginResponse, function(){ 
					result.resolve(); 
				});
			} else {
				result.reject('failed');
			}

			return result.promise;
		};

		this.disconnect = function (){
			FB.logout(function (response){
				var rootScope = $injector.get('$rootScope');
				rootScope.fbAccount = undefined;
				rootScope.$apply();
			});
		}

		try{
			FB.init({
				appId: settings.fbAppId,
				xfbml: true,
				version: 'v2.1'
			});
			FB.getLoginStatus(function (response){
				if (response.status === 'connected'){
					loadFbAccount(response);
				}
			});
		}
		catch(ex){
			var rootScope = $injector.get('$rootScope');
			console.log('Facebook load error : ' + ex.toString());
			rootScope.displayError('errorLoadingFacebook', 'errorLoadingFacebook');
		}

		return this;
	})
	.factory('fbWrapper', function(){
		try{
			this.FB = FB;
		}
		catch(ex){
			this.FB = {};
		}
		return this;
	});