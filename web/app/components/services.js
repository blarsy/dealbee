angular.module('Dealbee.services', ['Dealbee.services.facebook'])
	.factory('l8n', function($http, $locale, $cookieStore, $injector, tmhDynamicLocale, $q, $location){
		var self = this,
			getLangFromLocation = function(){
				var currentPath = $location.path(),
					segments = currentPath.split('/'),
					urlLang = segments[segments.length - 1],
					lang = _.find(self.rootScope.languages, function (language){
						return language.code.toLowerCase() === segments[1].toLowerCase();
					});

				return lang ? lang.code : null;
			};
		this.getCurrentLangIsoCode = function(){var currentLang = _.find(self.rootScope.languages, function (language){
				return language.code.toLowerCase() === self.getLang().toLowerCase();
			});
			return currentLang.isoCode;
		};
		this.getLang = function(){
			return $cookieStore.get('lang') || 'fr';
		};
		this.setLang = function (langCode) {
			self.rootScope.currentLang = langCode;
			return tmhDynamicLocale.set(langCode).then(function(){
				$cookieStore.put('lang', langCode);
			});
		};
		this.getRes = function (resCode){
			if(resCode){
	            var resNs = resCode.split('.'),
	            	resource = self.allRes ? self.allRes.resources : {},
	                langCode = self.getLang();

	            for(var i = 0; i < resNs.length; i++){
	            	if(!resource[resNs[i]]){
	            		//happens when resource file is not loaded yet
	            		return '';
	            	}
	                resource = resource[resNs[i]];
	            }

	            if(!_.find(self.rootScope.displayedResources, function (resource){ return resource.code === resCode; })){
	                self.rootScope.displayedResources.push({ code: resCode, translations: resource });
	            }

	            return resource[langCode];				
			}
			return '';
  		};
  		this.switchLang = function(langCode){
			self.resourcesPromise.then(function (){
				self.setLang(langCode).then(function (){
					var currentPath = $location.path(),
						newPath = currentPath.substring(0, currentPath.length - 2) + langCode;
					$location.path(newPath);
					//self.rootScope.$broadcast('langChanged');
				});
			});
		};
        this.rootScope = $injector.get('$rootScope');
        this.rootScope.getRes = this.getRes;
		this.allRes = null;
		this.resourcesPromise = $http.get('res.json').then(function (response){
			self.rootScope.languages = response.data.languages;
			self.allRes = response.data;
			var langFromLocation = getLangFromLocation();
			if(langFromLocation && langFromLocation !== self.getLang()){
				return self.setLang(langFromLocation);
			} else {
				return self.setLang(self.getLang());
			}
		});
        this.rootScope.$on('$locationChangeStart', function (event, next){
        	var path = $location.path(),
        		segments = path.split('/'),
        		langInRoute = segments.length > 1 ? segments[segments.length - 1] : null;

			if(!langInRoute || !/^\w\w$/.test(langInRoute)){
				$location.path($location.path() + '/' + self.getLang());
			} 
		});
   		return this;
	})
	.factory('settings', function (){
		return {
			webApi: 'http://localhost:8081/api/',
			fbAppId: '1523173214607128',
			environment: 'Local',
			stripePublishableKey: 'pk_test_NYE7FIl5PpaUjqLSLeDe3lDr',
			uploadImagePreset: 'dng61d4o'
		};
	})
	.factory('restApi', function ($resource, settings){
		this.emitter = $resource(settings.webApi + 'emitter', {},{
			'changePassword': { method: 'POST', url: settings.webApi + 'emitter/changepassword' },
			'forgotPassword': { method: 'POST', url: settings.webApi + 'emitter/forgotpassword' },
			'recoverPassword': { method: 'POST', url: settings.webApi + 'emitter/recoverpassword' }
		});
		this.session = $resource(settings.webApi + 'session', {}, {
			'switchEmitter': { method: 'POST', url: settings.webApi + 'session/switchEmitter' }
		});
		this.deal = $resource(settings.webApi + 'deal', {}, {
			'consume': { method: 'POST', url: settings.webApi + 'deal/consume' },
			'publicInfo': { method: 'GET', url: settings.webApi + 'deal/publicInfo/:id' },
			'request': { method: 'POST', url: settings.webApi + 'deal/request' }
		});
		this.grant = $resource(settings.webApi + 'grant', {}, {
			'giveByEmail': { method: 'POST', url: settings.webApi + 'grant/giveByEmail'},
			'giveByFacebook': { method: 'POST', url: settings.webApi + 'grant/giveByFacebook'},
			'confirmGift': { method: 'POST', url: settings.webApi + 'grant/confirmGift'},
			'scan': { method: 'POST', url: settings.webApi + 'grant/scan' }
		});
		this.distribute = $resource(settings.webApi + 'distribute', {}, {
			'batchCreate': { method: 'POST', isArray: false, url: settings.webApi + 'distribute' }
		});
		this.dealHolders = $resource(settings.webApi + 'dealholder');
		this.resources = $resource(settings.webApi + 'resource');
		this.fbUser = $resource(settings.webApi + 'fbUser');
		this.preference = $resource(settings.webApi + 'preference');
		this.message = $resource(settings.webApi + 'message');
		this.maillist = $resource(settings.webApi + 'maillist');
		this.error = $resource(settings.webApi + 'error');
		this.price = $resource(settings.webApi + 'price');
		this.creditcard = $resource(settings.webApi + 'creditcard');
		this.order = $resource(settings.webApi + 'order');
		this.dealMetadata = $resource(settings.webApi + 'deal/metadata');
		this.manager = $resource(settings.webApi + 'manager');
		this.client = $resource(settings.webApi + 'client', {}, {
			'payments': { method: 'GET', isArray: false, url: settings.webApi + 'client/payments' }
		});
		this.invoicingData = $resource(settings.webApi + 'client/invoicingData');
		return this;
	})
	.factory('globalState', function(){
		this.sessionId = null;
		return this;
	})
	.factory('authentication', function ($rootScope, $q, $location, $cookieStore, restApi, globalState){
		var self = this,
			restoreSessionDeferred;

		function restoreSession(sessionIdToRestore){
			var session = new restApi.session();
			restoreSessionDeferred = $q.defer();

			session.$get({ id: sessionIdToRestore }, function(res, headers){
				self.createSession(res, headers);
				restoreSessionDeferred.resolve(headers('Session-Id'));
				restoreSessionDeferred = null;
			}, function(res){
				self.killSession();
				restoreSessionDeferred.reject();
				restoreSessionDeferred = null;
			});
		};

		this.isLoggedIn = function(){
			var deferred = $q.defer();
			if($rootScope.account){
				deferred.resolve(globalState.sessionId);
			} else {
				if(restoreSessionDeferred){
					restoreSessionDeferred.promise.then(function (sessionId){
						deferred.resolve(sessionId);
					}, function (){
						deferred.reject();
					});
				} else {
					deferred.reject();
				}
			}
			return deferred.promise;
		};

		this.login = function(fbToken, email, password){
			var session = new restApi.session(),
				deferred = $q.defer();
			if(fbToken){
				session.fbToken = fbToken;
			} else {
				session.email = email;
				session.password = password;
			}

			session.$save(function (res, headers){
				self.createSession(res, headers);
				deferred.resolve();
			}, function(err){
				deferred.reject(err);
			});

			return deferred.promise;
		};

		this.createSession = function(sessionData, headers){
			$rootScope.account = sessionData;
			if($rootScope.account.identifier.powerUser){
				$rootScope.localizeMode = true;
			}
			globalState.sessionId = headers('Session-Id');
			$cookieStore.put('sessionId', headers('Session-Id'));

			$rootScope.breadCrumb[0] = {name: $rootScope.getLevel1BreadcrumbNodeText(), url: '/home' };
			$rootScope.safeApply();
		};

		this.killSession = function(){
			$rootScope.account = undefined;
			globalState.sessionId = null;
			$cookieStore.remove('sessionId');
			$location.path('/login')
		};

		this.getRestoreSessionPromise = function(){
			if(restoreSessionDeferred){
				return restoreSessionDeferred.promise;
			} else {
				var rejected = $q.defer();
				rejected.reject('notloggedin');
				return rejected.promise;
			}
		};

		this.switchToEmitter = function (emitterId){
			var deffered = $q.defer();

			restApi.session.switchEmitter({ emitterId: emitterId }, function (res, httpHeaders){
				self.createSession(res, httpHeaders);
				$rootScope.$broadcast('switchedEmitter');
				deffered.resolve();
			}, function (res){
				deffered.reject(res);
			});

			return deffered.promise;
		};

		if(!$rootScope.account && $cookieStore.get('sessionId')){
			restoreSession($cookieStore.get('sessionId'));
		}
		return this;
	})
	.factory('emitter', function ($rootScope, restApi, $q, $cookieStore, globalState){
		var self = this,
			createEmitterToSave = function (){
				var emitterToSave = new restApi.emitter({
					id: self.id,
					name: self.name,
					email: self.email,
					imageRef: self.imageRef,
					language: self.language,
					website: self.website,
					address: self.address,
				});
				if(self.invoicingData){
					emitterToSave.invoicingData = self.invoicingData;
				}
				if(self.fbToken){
					emitterToSave.fbToken = self.fbToken;
				} else {
					emitterToSave.password = self.password;
				}

				return emitterToSave;
			},
			restoreIfStored = function (){
				var stored = $cookieStore.get('emitterToSave');
				if(stored){
					self.name = stored.name;
					self.email=  stored.email;
					self.language = stored.language;
					self.imageRef = stored.imageRef;
					self.website=  stored.website;
					self.address = stored.address;
					if(stored.fbToken) self.fbToken = stored.fbToken;
					if(stored.password) self.password = stored.password;
				}
			};

		this.store = function (){
			$cookieStore.put('emitterToSave', createEmitterToSave());
		};

		this.save = function (){
			var deffered = $q.defer(),
				emitterToSave = createEmitterToSave();

			emitterToSave.$save(function (res, headers){
				$cookieStore.remove('emitterToSave');
				self.clear();
				deffered.resolve({ res: res, headers: headers });
			}, function (res){
				deffered.reject(res);
			});

			return deffered.promise;
		};

		this.clear = function (){
			delete self.name;
			delete self.email;
			delete self.language;
			delete self.imageRef;
			delete self.website;
			delete self.address;
			delete self.id;
			delete self.invoicingData;
			if(self.fbToken) delete self.fbToken;
			if(self.password) delete self.password;
		};

		this.loadLoggedInEmitter = function (){
			$cookieStore.remove('emitterToSave');
			self.name = $rootScope.account.emitter.name;
			self.email=  $rootScope.account.emitter.email;
			self.imageRef = $rootScope.account.emitter.imageRef;
			self.language = $rootScope.account.emitter.language;
			self.website=  $rootScope.account.emitter.website;
			self.address = $rootScope.account.emitter.address;
			self.invoicingData = $rootScope.account.emitter.invoicingData;
			self.id = $rootScope.account.emitter._id;
			delete self.password;
			delete self.fbToken;
			delete self.deleteImage;
		};

		restoreIfStored();

		return this;
	})
	.factory('deal', function ($rootScope, restApi, $q, $cookieStore, globalState){
		var self = this,
			createDealToSave = function (){
				var dealToSave = new restApi.deal({
					_id: self._id,
					benefit: self.benefit,
					teaser: self.teaser,
					expiration: self.expiration,
					beginDate: self.beginDate,
					description: self.description,
					imageRef: self.imageRef,
					visibility: self.visibility || 'limited'
				});
				if(self.termsOfUse){
					dealToSave.termsOfUse = self.termsOfUse;
				}
				return dealToSave;
			},
			loadFromObject = function (dealToLoad){
				self.init();
				self._id = dealToLoad._id;
				self.benefit =  dealToLoad.benefit;
				self.teaser = dealToLoad.teaser;
				self.expiration = dealToLoad.expiration;
				self.beginDate =  dealToLoad.beginDate;
				self.description = dealToLoad.description;
				self.visibility = dealToLoad.private ? 'limited' : 'public';
				self.imageRef = dealToLoad.imageRef;
				if(dealToLoad.termsOfUse){
					if(dealToLoad.termsOfUse.length > 0 && dealToLoad.termsOfUse[0].text){
						self.termsOfUse = dealToLoad.termsOfUse;	
					} else {
						var displayableTermsOfUse = [];
						_.each(dealToLoad.termsOfUse, function (termOfUse){
							displayableTermsOfUse.push({ text: termOfUse });
						});
						self.termsOfUse = displayableTermsOfUse;
					}
				}
			},
			restoreIfStored = function (){
				var stored = $cookieStore.get('dealToSave');
				if(stored){
					loadFromObject(stored);
				}
			};

		this.store = function (){
			$cookieStore.put('dealToSave', createDealToSave());
		};

		this.init = function (){
			$cookieStore.remove('dealToSave');
			delete self._id;
			delete self.benefit;
			delete self.teaser;
			delete self.expiration;
			delete self.beginDate;
			delete self.description;
			delete self.imageRef;
			delete self.termsOfUse;
			delete self.visibility;
		}

		this.getDealObject = function (){
			return createDealToSave();
		};

		this.loadFrom = function (dealToLoad){
			loadFromObject(dealToLoad);
		};

		this.save = function (){
			var deffered = $q.defer(),
				dealToSave = createDealToSave();

			dealToSave.$save(function (){
				self.init();
				deffered.resolve();
			}, function (res){
				deffered.reject(res);
			});

			return deffered.promise;
		};

		restoreIfStored();

		return this;
	})
	.factory('distribute', function(restApi, $q, $rootScope){
		var self = this,
			makeGrants = function (){
				var grants = [];
				_.each(self.emails, function (email){
					grants.push({ channelType: 1, email: email.email });
				});
				_.each(self.facebookDistribution.friends, function (facebookFriend){
					grants.push({ 
						channelType: 0,
						senderFbUid: facebookFriend.senderUid,
						fbDestinatorUser: {
							tagId: facebookFriend.friend.id,
							picUrl: facebookFriend.friend.picture.data.url,
							name: facebookFriend.friend.name,
							taggedMessage: self.facebookDistribution.taggedMessage
						}
					});
				});
				if(grants.length > 0){
					return grants;
				} else {
					return undefined;
				}
			};

		this.onRequestDeals = {};

		this.init = function (dealId){
			var deffered = $q.defer();

			self.dealId = dealId;
			self.emails = [];
			self.facebookDistribution = { friends: [], taggedMessage: '' };
			restApi.deal.publicInfo({ id: dealId }, function (deal){
				var nbDealsOnRequest = deal.nbOnRequest || 0;
				self.onRequestDeals.amount = 0;
				self.onRequestDeals.initialAmount = nbDealsOnRequest;
				deffered.resolve(deal);
			}, function (err){
				deffered.reject(err);
			});
			return deffered.promise;
		};

		this.addEmail = function (emailToAdd){
			for(var i = 0; i < self.emails.length; i ++){
				if(self.emails[i].email.trim().toLowerCase() === emailToAdd.trim().toLowerCase()) return false;
			};

			self.emails.push({ email: emailToAdd });
			self.emails = self.emails.sort(function (a, b) { 
				if ( a.email < b.email )
					return 1;
				if ( a.email > b.email )
					return -1;
				return 0;
			});
			return true;
		};

		this.addEmails = function (emailsToAdd){
			_.each(emailsToAdd, function (emailToAdd){
				var cancel = false;
				for(var i = 0; i < self.emails.length; i ++){
					if(self.emails[i].email.trim().toLowerCase() === emailToAdd.trim().toLowerCase()){
						cancel = true;
						break;
					}
				};
				if(!cancel){
					self.emails.push({ email: emailToAdd });
				}
			});
		};

		this.removeEmail = function (emailToRemove){
			for(var i = 0; i < self.emails.length ; i++){
				if(self.emails[i].email === emailToRemove){
					self.emails.splice(i, 1);
					break;
				}
			}
		};

		this.addFacebookFriend = function (friend, senderUid){
			self.facebookDistribution.friends.push({ friend: friend, senderUid: senderUid });
		};

		this.removeFacebookFriend = function (friend){
			for (var i = 0; i < self.facebookDistribution.friends.length; i ++){
				if(self.facebookDistribution.friends[i].friend.id === friend.id){
					self.facebookDistribution.friends.splice(i, 1);
					return;
				}
			}
		};

		this.save = function (){
			var distributionToSave = new restApi.distribute(),
				deffered = $q.defer();

			distributionToSave.dealId = self.dealId;
			distributionToSave.grants = makeGrants();
			distributionToSave.amountOnRequest = self.onRequestDeals.amount;

			distributionToSave.$batchCreate(function (res){
				self.dealId = null;
				deffered.resolve(res);
			}, function(res){
				deffered.reject(res);
			});	

			return deffered.promise;
		};

		this.getSummary = function (){
			var summary = {};

			summary.nbEmailDistribution = self.emails.length;
			summary.nbFacebookDistribution = self.facebookDistribution.friends.length;
			summary.nbOnRequestDistribution = self.onRequestDeals.amount;

			summary.creditsUsed = summary.nbEmailDistribution + summary.nbFacebookDistribution + summary.nbOnRequestDistribution;

			if(summary.creditsUsed > $rootScope.account.client.balance){
				summary.missingFund =  summary.creditsUsed - $rootScope.account.client.balance;
			}
			if(summary.nbFacebookDistribution > 0 && !$rootScope.fbAccount.canPublish){
				summary.publishRightsRequired = true;
			}
			return summary;
		};

		return this;
	})
	.factory('errorHandling', function(){
		var self = this;
		this.getErrorMessage = function(res){
			if(res.status && res.status === 500){
				if(res.responseText){
					try{
						var errorInfo = JSON.parse(res.responseText);
						if(errorInfo.message){
							return errorInfo.message;
						}
					}
					catch(err){}
				} else if(res.data.message){
					return res.data.message;
				}
			}
		};
		return this;
	})
	.factory('order', function (restApi, $rootScope, $q){
		var self = this;
		this.subscription = {};
		this.packs = [];
		this.pricesDeferred = $q.defer();
		this.pricesPromise = this.pricesDeferred.promise;

		var priceRequest = new restApi.price();
		priceRequest.$get(function (res){
			self.pricesDeferred.resolve(res);
		}, function (err){
			$rootScope.handleRestApiError('loadPriceError', err);
			self.pricesDeferred.reject(err);
		});

		this.addPack = function (type){
			return self.pricesPromise.then(function (prices){
				var packData = _.find(prices.packs, function (packData){
					return type === packData.type;
				});
				if(packData){
					var packInBasket = _.find(self.packs, function (pack){ return pack.type === type; });
					if(!packInBasket){
						self.packs.push({
							type: type,
							quantity: 1,
							data: packData
						});
					} else {
						packInBasket.quantity ++;
					}
				}
			});
		};

		this.removePack = function (type){
			self.pricesPromise.then(function (prices){
				var packData = _.find(prices.packs, function (packData){
					return type === packData.type;
				});
				if(packData){
					var packInBasket = _.find(self.packs, function (pack){ return pack.type === type; });
					if(packInBasket){
						packInBasket.quantity --;
						if(packInBasket.quantity === 0){
							self.packs.splice(self.packs.indexOf(packInBasket), 1);
						}
					}
				}
			});		
		}

		this.setSubscription = function (type, nbMonthPaid){
			return self.pricesPromise.then(function (prices){
				var subscriptionData = _.find(prices.subscriptions, function (subscriptionData){
					return type === subscriptionData.type;
				});
				if(subscriptionData){
					self.subscription.type = type;
					self.subscription.nbMonthPaid = nbMonthPaid;
					self.subscription.data = subscriptionData;
					if(nbMonthPaid === 1){
						self.subscription.price = subscriptionData.price * 2;
					} else {
						var discountData = _.find(prices.discounts, function (discount){
							return discount.months === nbMonthPaid;
						});
						if(discountData){
							self.subscription.price = subscriptionData.price * nbMonthPaid * ( 1 - (discountData.percentage / 100));
						}
					}
				}
			});
		};

		this.getTotalPrice = function (){
			var result = 0;

			if(self.subscription && self.subscription.data){
				result += self.subscription.price;
			}
			_.each(self.packs, function (pack){
				result += pack.data.price * pack.quantity;
			});
			return result;
		};

		this.getAmountNewCoupons = function (){
			var result = 0;
			if(self.subscription.data){
				result += self.subscription.data.coupons;
			}
			_.each(self.packs, function (pack){
				result += pack.data.coupons * pack.quantity;
			});

			return result;
		};

		this.save = function (){
			var result = $q.defer(),
				orderRequest = new restApi.order({
					packs: _.map(self.packs, function (pack){
						return { type: pack.type, quantity: pack.quantity };
					}),
				});

			if(self.subscription.type){
				orderRequest.subscription = {
					type: self.subscription.type,
					paymentFrequency: self.subscription.nbMonthPaid
				}
			}

			orderRequest.$save(function (res){
				self.packs.length = 0;
				self.clearSubscription();
				result.resolve(res);
			}, function (err){
				result.reject(err);
			});

			return result.promise;
		};

		this.clearSubscription = function (){
			if(self.subscription.data) delete self.subscription.data;
			if(self.subscription.type) delete self.subscription.type;
		};

		return this;
	})
	.factory('identity', function(restApi){
		var self = this;

		this.linkToController = function (createIdentityController){
			self.controllerValid = createIdentityController.isValid;
		};

		this.createAsManager = function (managerInvitationId){

		};

		this.isValid = function (){
			return self.controllerValid();
		}

		this.clear = function (){
			delete self.email;
			if(self.fbToken) delete self.fbToken;
			if(self.password) delete self.password;
		};

		return this;
	});