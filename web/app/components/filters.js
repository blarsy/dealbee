angular.module('Dealbee.filters', [])
	.filter('dlbdate', function (l8n){
		return function(input) {
			var date = new Date(Date.parse(input)),
				lang = l8n.getLang();

			return app.formatting.toDate(date, lang);
		};
	})	
	.filter('dlbdatetime', function (l8n){
		return function(input) {
			var date = new Date(Date.parse(input)),
				lang = l8n.getLang();

			return app.formatting.toDateTime(date, lang);
		};
	})
	.filter('dlbPrice', function($rootScope){
		return function(input){
			if(input === undefined) return 'err';
			return input.toFixed(2).toLocaleString($rootScope.currentLang);
		}
	})
	.filter('trailzeroes', function (){
		return function (input, numberOfZeroes){
			var fullString = '';
			for(var i = 0; i < numberOfZeroes; i++){
				fullString += '0';
			}
			fullString += input;
			return fullString.substr(fullString.length - numberOfZeroes);
		};
	})
	.filter('dblMonth', function (l8n){
		return function (input){
			var date = new Date(Date.parse(input)),
				lang = l8n.getLang();

			return app.formatting.toMonth(date, lang);
		};
	})
	.filter('dlbemitterwebsite', function (){
		return function (input){
			if(!/^http(s)?:\/\//.test(input)) {
				return 'http://' + input;
			} else {
				return input;
			}
		};
	})
	.filter('dlbescape', function($sanitize) {
		return function (input){
			return $sanitize(input);
		}
	})
	.filter('dlbdistance', function(){
		return function (input){
			var meters = new Number(input);
			if(meters){
				if(meters < 5000){
					return (meters / 1000).toFixed(1) + ' km';
				} else {
					return (meters / 1000).toFixed(0) + ' km';
				}
			}
		}
	});