'use strict';

// Declare app level module which depends on views, and components
angular.module('Dealbee', [
  'Dealbee.services',
  'Dealbee.filters',
  'Dealbee.directives',
  'Dealbee.splash',
  'Dealbee.login',
  'Dealbee.subscribe',
  'Dealbee.home',
  'Dealbee.deal',
  'Dealbee.deal.distribute',
  'Dealbee.deal.give',
  'Dealbee.deal.giftconfirmed',
  'Dealbee.deal.dealgiven',
  'Dealbee.deal.request',
  'Dealbee.deal.dealrequested',
  'Dealbee.deal.grantsStatus',
  'Dealbee.interceptors',
  'Dealbee.viewDeal',
  'Dealbee.consume',
  'Dealbee.scan',
  'Dealbee.localizer',
  'Dealbee.topmenu',
  'Dealbee.parameters',
  'Dealbee.load',
  'Dealbee.pref',
  'Dealbee.contact',
  'Dealbee.account',
  'Dealbee.find',
  'Dealbee.emitter.name',
  'Dealbee.emitter.location',
  'Dealbee.emitter.identity',
  'Dealbee.emitter.login',
  'Dealbee.emitter.creditcard',
  'Dealbee.emitter.invoicing',
  'Dealbee.emitter.createIdentity',
  'Dealbee.deal.title',
  'Dealbee.deal.image',
  'Dealbee.deal.validity',
  'Dealbee.deal.content',
  'Dealbee.deal.followup',
  'Dealbee.deal.metadata',
  'Dealbee.pricing',
  'Dealbee.order',
  'Dealbee.basket',
  'Dealbee.viewdeal.title',
  'Dealbee.distribute.emailList',
  'Dealbee.distribute.friendList',
  'Dealbee.distribute.summary',
  'Dealbee.forgotPassword',
  'Dealbee.recoverPassword',
  'Dealbee.consumptions',
  'Dealbee.exception',
  'Dealbee.managers',
  'Dealbee.managers.accept',
  'ngRoute',
  'ngCookies',
  'ngResource',
  'ngAnimate',
  'ngSanitize',
  'tmh.dynamicLocale',
  'cloudinary',
  'ui.bootstrap.datepicker',
  'ui.bootstrap.timepicker',
  'ui.bootstrap.typeahead',
  'ui.bootstrap.collapse',
  'ui.bootstrap.modal',
  'ui.tinymce',
  'ui.bootstrap.tabs',
  'ui.bootstrap.buttons',
  'ui.bootstrap.tooltip',
  'ui.bootstrap.accordion',
  'uiGmapgoogle-maps',
  'ngGeolocation'
])
.config(['$routeProvider', '$locationProvider', 'uiGmapGoogleMapApiProvider', function($routeProvider, $locationProvider, uiGmapGoogleMapApiProvider) {
	$routeProvider.otherwise({redirectTo: '/find'});
	$routeProvider.when('/privacy/:lang', {
		templateUrl: 'routes/privacy/privacy.html',
		controller: function ($scope){
			$scope.registerInBreadcrumb(2, '/privacy', 'privacy.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
			$scope.registerStandardMeta('privacy');
		}
  	});
	$routeProvider.when('/faq/:lang', {
	    templateUrl: 'routes/faq/faq.html',
		controller: function ($scope){
			$scope.registerInBreadcrumb(2, '/faq', 'faq.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
			$scope.registerStandardMeta('faq');
		}
  	});
	$routeProvider.when('/our-commitment/:lang', {
	    templateUrl: 'routes/our-commitment/our-commitment.html',
		controller: function ($scope){
			$scope.registerInBreadcrumb(2, '/our-commitment', 'ourCommitment.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
			$scope.registerStandardMeta('ourCommitment');
		}
  	});
	$routeProvider.when('/about/:lang', {
	    templateUrl: 'routes/about/about.html',
		controller: function ($scope){
			$scope.registerInBreadcrumb(2, '/about', 'about.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
			$scope.registerStandardMeta('about');
		}
  	});
	$routeProvider.when('/how-does-it-work/:lang', {
	    templateUrl: 'routes/how-does-it-work/how-does-it-work.html',
		controller: function ($scope){
			$scope.registerInBreadcrumb(2, '/how-does-it-work', 'howItWorks.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
			$scope.registerStandardMeta('howItWorks');
		}
  	});
	$routeProvider.when('/underconstruction/:lang', {
	    templateUrl: 'routes/underconstruction.html',
		controller: function ($scope){
			$scope.registerInBreadcrumb(2, '/underconstruction', 'underConstruction.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
		}
  	});
	$locationProvider.html5Mode(true).hashPrefix('!');
	uiGmapGoogleMapApiProvider.configure({
		//    key: 'your api key',
		v: '3.17',
		libraries: 'geometry'
	});
}])
.run(function($rootScope, $location, $timeout, l8n, globalState, settings, $modal, errorHandling, $anchorScroll){
	var loginModal;
	$rootScope.alerts = [];
	$rootScope.breadCrumb = [];
	$rootScope.mode = '';
	$rootScope.now = new Date();
	$rootScope.launchDate = settings.launchDate;

	$rootScope.switchLang = function (langCode){
		l8n.switchLang(langCode);
	};

	$rootScope.dismissAlert = function (id){
		var keyToRemove = -1;
		angular.forEach($rootScope.alerts, function (value, key){
			if(value.id === id) keyToRemove = key;
		});
		$rootScope.alerts.splice(keyToRemove, 1);
	};
	$rootScope.displayGenericError = function (errorId){
		$rootScope.alerts.push({ id: errorId, type: 'danger', message: 'technicalError' });
		$anchorScroll('alerts');
	};
	$rootScope.handleRestApiError = function (errorId, res){
		if(!$rootScope.handleExpiredSession(res) && res !== 'Reconnecting'){
			$rootScope.displayGenericError(errorId);
		}
	};
	$rootScope.displayError = function (errorId, errorMessage){
		$rootScope.alerts.push({ id: errorId, type: 'danger', message: errorMessage });
		$anchorScroll('alerts');
	};
	$rootScope.displaySuccess = function (msgId, msg){
		$rootScope.alerts.push({ id: msgId, type: 'success', message: msg });
		$anchorScroll('alerts');
	};
	$rootScope.handleExpiredSession = function (res){
		var errorMessage = errorHandling.getErrorMessage(res);
		if(errorMessage === 'SessionExpired'){
			$rootScope.loginAgain();
			return true;
		}
		return false;
	};
	$rootScope.loginAgain = function (){
		if(!loginModal){
			loginModal = $modal.open({
				templateUrl: 'loginModalContent',
				backdrop: 'static',
				size: 'lg'});
			loginModal.result.then(function (){
				loginModal = undefined;
			}, function (){
				loginModal = undefined;
			});
		}
	};
	$rootScope.$on('loggedIn', function (){
		if(loginModal){
			$rootScope.displaySuccess('reauthorized', 'reauthorized');
			loginModal.close();
		}
	})
	$rootScope.$on('$routeChangeError', function (){
		$location.path('/');
	});

	$rootScope.$on('$routeChangeSuccess', function (){
		for(var i = $rootScope.alerts.length - 1; i > -1; i--){
			if($rootScope.alerts[i].type === 'danger'){
				$rootScope.alerts.splice(i, 1);
			}
		}
		$rootScope.metaTitle = "defaultMetaTitle";
		globalState.previousView = globalState.currentView;
		globalState.currentView = $location.path();
		$rootScope.mode = null;
		$rootScope.showBreadCrumb = true;
		$timeout(function (){
			try{
				FB.XFBML.parse(); 
			}catch(ex){}		
		});
	});
	$rootScope.safeApply = function (){
		$timeout(function (){
			$rootScope.$apply();
		})
	};
	$rootScope.registerInBreadcrumb = function (level, url, name, defaultParentLevels){
		var breadCrumb = $rootScope.breadCrumb;
		if(defaultParentLevels){
			_.each(defaultParentLevels, function (parentLevel){
				if(breadCrumb.length <= parentLevel.level){
					$rootScope.registerInBreadcrumb(parentLevel.level, parentLevel.url, parentLevel.name);
				}
			});
		}
		if(breadCrumb.length < level){
			breadCrumb.push({ name: name, url: url });
		} else {
			breadCrumb[level - 1] = { name: name, url: url };
			while(breadCrumb.length > level){
				breadCrumb.pop();
			}
		}
	};
	$rootScope.getLevel1BreadcrumbNodeText = function(){
		return $rootScope.account ? 'home.title' : 'splash.title';
	};
	$rootScope.getLevel1BreadcrumbPath = function(){
		return $rootScope.account ? '/home' : '/splash';
	};
	$rootScope.registerStandardMeta = function (pageName){
		$rootScope.metaTitle = pageName + '.metaTitle';
		$rootScope.metaDescription = pageName + '.metaDescription';
		$rootScope.metaKeywords = pageName + '.metaKeywords';
	};
});
