'use strict';

angular.module('Dealbee.load', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/load/:lang', {
		templateUrl: 'routes/load/load.html',
		controller: 'LoadCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('LoadCtrl', function ($scope, restApi, globalState) {
	$scope.$on('InvoicingDataSaved', function (){
		$scope.displaySuccess('invoiceDataSaved', 'InvoicingCtrl.invoiceDataSaved');
	});
	$scope.loadingPayments = true;
	restApi.client.payments({ id: $scope.account.client._id }, function (client){
		$scope.payments = client.payments;
		$scope.loadingPayments = false;
	}, function (err){
		$scope.loadingPayments = false;
		$scope.handleRestApiError('LoadPaymentsFailed', err);
	});
	$scope.sessionId = globalState.sessionId;
	$scope.registerInBreadcrumb(2, '/load', 'load.title');
});