'use strict';

angular.module('Dealbee.deal.dealgiven', ['ngRoute'])

.config(function ($routeProvider){
	$routeProvider.when('/dealgiven/:lang', {
		templateUrl: 'routes/deal/dealgiven.html',
		controller: 'DealGivenCtrl'
	});
})
.controller('DealGivenCtrl', function ($scope, restApi, $rootScope){
	$rootScope.showBreadCrumb = false;
	$rootScope.mode = 'lean';
});