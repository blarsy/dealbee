'use strict';

angular.module('Dealbee.deal.give', ['ngRoute'])

.config(function ($routeProvider){
	$routeProvider.when('/deal/give/:id/:lang', {
		templateUrl: 'routes/deal/give.html',
		controller: 'GiveCtrl'
	});
})
.controller('GiveCtrl', function ($scope, restApi, $location, $routeParams, facebook, $rootScope){
	var self = this;
	this.callRestApiToGiveByEmail = function (){
		var givenDeal = new restApi.grant();
		$scope.givingByEmail = true;

		if($scope.fbAccount){
			givenDeal.senderFbToken = $scope.fbAccount.token;
		}
		givenDeal.dealId = $routeParams.id;
		givenDeal.email = $scope.friendEmail;
		givenDeal.senderName = $scope.senderName;
		givenDeal.$giveByEmail(function(res){
			$location.path('/dealgiven');
			$scope.givingByEmail = false;
		}, function(res){
			$scope.givingByEmail = false;
			$scope.handleRestApiError('errorGivingByEmail', res);
		});
	};
	$scope.selectEmail = function (){
		$scope.byEmail = true;
		$scope.byFacebook = false;
	};
	$scope.selectFacebook = function (){
		$scope.byFacebook = true;
		$scope.byEmail = false;
	};

	this.loadFriends = function (done){
		facebook.getFriends().then(function (friends){
			$scope.friends = friends;
			done();
		});
	};

	$scope.fbConnect = function (){
		$scope.loggingInFacebook = true;
		if(!$scope.fbAccount){
			FB.login(function (loginResponse){
				facebook.processLoginResponse(loginResponse).then(
					function(){
						self.loadFriends(function (){
							$scope.connectingFacebook = false;
						});
					},
					function (err){
						$scope.connectingFacebook = false;
						$scope.displayGenericError('errorLogginInForPublish');
						$scope.safeApply();
					}
				), { scope: 'email, user_friends', return_scopes: true }
			});
		}
	};

	$scope.giveByEmail = function (){
		$scope.submittedOnce = true;
		if($scope.giveByEmailForm.$valid){
			if(!$scope.grant.email){
				FB.login(function (loginResponse){
					facebook.processLoginResponse(loginResponse).then(function (){
						self.callRestApiToGiveByEmail();
					});
				}, { scope: 'email, user_friends', return_scopes: true });
			} else {
				self.callRestApiToGiveByEmail();
			}
		}
	};
	$scope.giveByFacebook = function (){
		$scope.mustSelectFacebookAccount = false;
		$scope.fbSubmittedOnce = true;
		$scope.mustGrantPublishRights = false;
		if(!$scope.selectedFacebookAccount || !$scope.selectedFacebookAccount.id){
			$scope.mustSelectFacebookAccount = true;
		} else {
			$scope.givingByFacebook = true;

			FB.login(function (loginResponse){
				facebook.processLoginResponse(loginResponse).then(
					function (){
						if($scope.fbAccount.canPublish){
							var givenDeal = new restApi.grant();
							givenDeal.dealId = $routeParams.id;
							givenDeal.senderFbToken = $scope.fbAccount.token;
							givenDeal.destinatorFbUser = {
								tagId: $scope.selectedFacebookAccount.id,
								name: $scope.selectedFacebookAccount.name,
								picUrl: $scope.selectedFacebookAccount.picture.data.url,
								taggedMessage: $scope.taggedMessage
							};
							givenDeal.$giveByFacebook(function(res){
								$scope.givingByFacebook = false;
								$location.path('/dealgiven');
							}, function(res){
								$scope.givingByFacebook = false;
								$scope.handleRestApiError('errorGivingByEmail', res);
							});								
						} else {
							$scope.givingByFacebook = false;
							$scope.mustGrantPublishRights = true;
						}
					},
					function (err){
						$scope.givingByFacebook = false;
						$scope.displayGenericError('errorLogginInForPublish');
						$scope.safeApply();
					}
				);
			}, { scope: 'email, user_friends, publish_actions', return_scopes: true });
		}
	};
	$scope.disconnectFb = function (){
		facebook.disconnect();
	};

	$scope.selectFacebookAccount = function (account){
		$scope.selectedFacebookAccount = account;
	};

	$rootScope.mode = 'lean';
	$rootScope.showBreadCrumb = true;
	$scope.grant = restApi.grant.get({ id: $routeParams.id });
	$scope.id = $routeParams.id ;

	if($scope.fbAccount){
		self.loadFriends(function (){});
	}

	$scope.$on('fbConnected', function (){
		self.loadFriends(function (){});
	});
});