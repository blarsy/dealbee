'use strict';

angular.module('Dealbee.deal.followup', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/deal/:dealId/followup/:lang', {
		templateUrl: 'routes/deal/followup.html',
		controller: 'FollowupCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('FollowupCtrl', function ($scope, $routeParams, restApi, $location, deal){
	$scope.loading = true;
	$scope.now = new Date();
	var dealDto = new restApi.deal();
	dealDto.$get({ id: $routeParams.dealId }, function (dealFound){
		var port = $location.port();
		$scope.deal = dealFound;
		$scope.deal.expiration = new Date($scope.deal.expiration);

		$scope.deal.grants.toSend = _.filter(dealFound.grants, function (grant){ return !grant.sent; });
		$scope.deal.grants.error = _.filter(dealFound.grants, function (grant){ return !grant.sent && grant.consecutiveSendErrors > 0; });
		$scope.deal.grants.sent = _.filter(dealFound.grants, function (grant){ return grant.sent && !grant.consumed; });
		$scope.deal.grants.consumed = _.filter(dealFound.grants, function (grant){ return grant.consumed; });
		
		$scope.toSendAccordionOpen = $scope.deal.grants.toSend.length > 0;
		$scope.onRequestAccordionOpen = $scope.deal.nbOnRequest > 0;
		$scope.sentAccordionOpen = $scope.deal.grants.sent.length > 0;
		$scope.errorAccordionOpen = $scope.deal.grants.error.length > 0;
		$scope.consumedAccordionOpen = $scope.deal.grants.consumed.length > 0;

		$scope.shareDealPageUrl = 'http://www.facebook.com/sharer/sharer.php?u=' + window.encodeURIComponent($location.protocol() + '://' + $location.host() +
			(port !== '80' ? ':' + port : '') + '/viewdeal/onrequest/' + dealFound._id);

		$scope.loading = false;
	}, function (res){
		$scope.handleRestApiError('loadDealFailed', res);
		$scope.loading = false;
	});
	$scope.preview = function(dealToPreview){
		deal.loadFrom(dealToPreview);
		$location.path('/viewdeal/preview');
	};
	$scope.navigateToGrantsStatus = function(){
		$location.path('/deal/' + $routeParams.dealId + '/grants-status')
	};
	$scope.registerInBreadcrumb(2, '/deal/' + $routeParams.dealId + '/followup', 'followup.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
});