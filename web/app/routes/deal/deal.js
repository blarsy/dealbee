'use strict';

angular.module('Dealbee.deal', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/deal/:step/:lang', {
		templateUrl: 'routes/deal/deal.html',
		controller: 'DealCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
	$routeProvider.when('/deal/:lang', {
		redirectTo: '/deal/1/:lang'
	});
})
.controller('DealCtrl', function($scope, $rootScope, $routeParams, deal, $location, restApi) {
	var self = this;

	$scope.next = function (){
		if($scope.saveStep()){
			deal.store();
			$location.path('/deal/' + ($scope.step + 1));
		}
	};

	$scope.previous = function (){
		$location.path('/deal/' + ($scope.step - 1));
	};

	$scope.finish = function (){
		var modifying;
		if($scope.saveStep()){
			$scope.creating = true;
			deal.store();

			modifying = deal._id;

			deal.save().then(function (){
				$scope.creating = false;
				if(modifying){
					$scope.displaySuccess('dealModified', 'deal.dealModified');
				} else {
					$scope.displaySuccess('dealCreated', 'deal.dealCreated');
				}
				$location.path('home');
				$scope.safeApply();
			}, function (res){
				$scope.creating = false;
				$scope.handleRestApiError('dealCreationFailed', res);
				$scope.safeApply();
			});
		}
	};
	$scope.preview = function (){
		$location.path('/viewdeal/preview');
	};

	$scope.step = Number($routeParams.step) || 1;
	$rootScope.mode = 'createDeal';
	$scope.registerInBreadcrumb(2, '/deal', 'deal.title', [{ level: 1, url: '/home',name: $scope.getLevel1BreadcrumbNodeText() }]);
});