'use strict';

angular.module('Dealbee.deal.metadata', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/deal/:dealId/metadata/:lang', {
		templateUrl: 'routes/deal/metadata.html',
		controller: 'MetadataCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('MetadataCtrl', function ($scope, $routeParams, restApi){
	$scope.addKeyword = function (){
		$scope.submittedOnce = true;
		$scope.duplicateKeyword = false;
		if($scope.newKeywordForm.$valid){
			if(_.find($scope.deal.keywords, function (keyword){ return keyword.value === $scope.newKeyword })){
				$scope.duplicateKeyword = true;
				return;
			}
			$scope.deal.keywords.push({ value: $scope.newKeyword });
			$scope.newKeyword  = '';
			$scope.submittedOnce = false;
		}
	};
	$scope.save = function(){
		var metadata = new restApi.dealMetadata({
			dealId: $routeParams.dealId,
			private: $scope.deal.private,
			keywords: _.pluck($scope.deal.keywords, 'value')
		});
		$scope.savingMetadata = true;
		metadata.$save(function(){
			$scope.savingMetadata = false;
			$scope.displaySuccess('saveMetadataSuccess', 'metadata.saveMetadataSuccess');
		}, function(err){
			$scope.savingMetadata = false;
			$scope.handleRestApiError('saveDealFailed', err);
		});
	};
	$scope.removeKeyword = function (keyword){
		var index = $scope.deal.keywords.indexOf(keyword);
		$scope.deal.keywords.splice(index, 1);
	}

	restApi.deal.get({ dealId: $routeParams.dealId }, function (deal){
		deal.keywords = _.map(deal.keywords, function (keyword){ return { value: keyword} });
		$scope.deal = deal;
	}, function (err){
		$scope.handleRestApiError('dealLoadFailed', err);
	});

	$scope.registerInBreadcrumb(2, '/deal/' + $routeParams.dealId + '/metadata', 'metadata.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
});