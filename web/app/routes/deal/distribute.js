'use strict';

angular.module('Dealbee.deal.distribute', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/deal/:dealId/distribute/:step/:lang', {
		templateUrl: 'routes/deal/distribute.html',
		controller: 'DistributeCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
	$routeProvider.when('/deal/:dealId/distribute/:lang', {
		templateUrl: 'routes/deal/distribute.html',
		controller: 'DistributeCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('DistributeCtrl', function ($scope, $routeParams, distribute, $location, facebook){
	if($routeParams.step === 'confirm' && !distribute.dealId){
		$location.path('/deal/' + $routeParams.dealId + '/distribute/' + $routeParams.lang);
		return;
	}
	if($routeParams.step === 'confirm' && distribute.dealId){
		$scope.summary = distribute.getSummary();
		$scope.confirming = true;
	}

	$scope.confirmDistribute = function (){
		$scope.mustProvideTaggedMessage = false;
		$scope.pleaseSelectSomeContactToSendTo = false;

		if((distribute.onRequestDeals.amount || 0) === 0){
			if(distribute.facebookDistribution.friends.length === 0 && distribute.emails.length === 0){
				$scope.pleaseSelectSomeContactToSendTo = true;
				return;
			}
		}
		if(distribute.facebookDistribution.friends.length > 0){
			if(!distribute.facebookDistribution.taggedMessage){
				$scope.mustProvideTaggedMessage = true;
				return;
			}
		}
		$location.path('/deal/' + $routeParams.dealId + '/distribute/confirm/' + $routeParams.lang);
	};

	$scope.distribute = function (){
		$scope.invalid = $scope.summary.publishRightsRequired || $scope.summary.missingFund > 0;
		if(!$scope.invalid){
			$scope.distributing = true;
			distribute.save().then(function (res){
				$scope.distributing = false;
				$scope.account.client.balance = res.client.balance;
				$scope.displaySuccess('DistributeSucessful', 'distribute.distributeSucceeded');
				$location.path('home');
			}, function (res){
				$scope.distributing = false;
				$scope.handleRestApiError('DistributeFailed', res);
			});
		}
	};

	$scope.previous = function (){
		$location.path('/deal/' + $routeParams.dealId + '/distribute/' + $routeParams.lang);
	};

	if(!$scope.confirming && $routeParams.dealId !== distribute.dealId){
		distribute.init($routeParams.dealId).then(function (){
			$scope.emailsToInvite = distribute.emails;
			$scope.friends = distribute.facebookDistribution.friends;
			$scope.onRequest = distribute.onRequestDeals;
		}, function (res){
			$scope.handleRestApiError('LoadDealFailed', res);
		});
	} else if (!$scope.confirming){
		$scope.emailsToInvite = distribute.emails;
		$scope.friends = distribute.facebookDistribution.friends;
		$scope.onRequest = distribute.onRequestDeals;
	}

	$scope.registerInBreadcrumb(2, '/deal/' + $routeParams.dealId + '/distribute', 'distribute.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
});