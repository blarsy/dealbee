'use strict';

angular.module('Dealbee.deal.request', ['ngRoute'])

.config(function ($routeProvider){
	$routeProvider.when('/deal/request/:id/:lang', {
		templateUrl: 'routes/deal/request.html',
		controller: 'RequestCtrl'
	});
})
.controller('RequestCtrl', function ($scope, restApi, $rootScope, $routeParams, $location){
	$rootScope.showBreadCrumb = false;
	$rootScope.mode = 'lean';

	$scope.request = function (){
		$scope.submittedOnce = true;
		if($scope.requestByEmailForm.$valid){
			var grant = new restApi.grant({ dealId: $routeParams.id, email: $scope.email });
			$scope.requestingByEmail = true;
			grant.$save({ id: $routeParams.id }, function (){
				$scope.requestingByEmail = false;
				$location.path('/dealrequested')
			}, function (res){
				$scope.requestingByEmail = false;
				if(res.status && res.status === 500){
					if(res.data.message === 'No_More_Deals_Available'){
						$scope.displayError('noMoreDealsAvailable', 'request.noMoreDealsAvailable');
					}
					if(res.data.message === 'Only_One_Deal_Per_Email'){
						$scope.displayError('oneDealPerEmail', 'request.oneDealPerEmail');
					}
					if(res.data.message === 'EMail_Blocked'){
						$scope.displayError('emailBlocked', 'request.emailBlocked');
					}
					if(res.data.message === 'Emitter_Blocked'){
						$scope.displayError('emitterBlocked', 'request.emitterBlocked');
					}

				}
			});
		}
	};
});