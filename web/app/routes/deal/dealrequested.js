'use strict';

angular.module('Dealbee.deal.dealrequested', ['ngRoute'])

.config(function ($routeProvider){
	$routeProvider.when('/dealrequested/:lang', {
		templateUrl: 'routes/deal/dealrequested.html',
		controller: 'DealRequestedCtrl'
	});
})
.controller('DealRequestedCtrl', function ($rootScope){
	$rootScope.showBreadCrumb = false;
	$rootScope.mode = 'lean';
});