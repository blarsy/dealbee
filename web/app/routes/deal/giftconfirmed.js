'use strict';

angular.module('Dealbee.deal.giftconfirmed', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/deal/giftconfirmed/:id/:lang', {
		templateUrl: 'routes/deal/giftconfirmed.html',
		controller: 'GiftconfirmedCtrl'
	});
})
.controller('GiftconfirmedCtrl', function($scope, restApi, $routeParams, $rootScope) {
	var self = this;

	this.handleGiftError = function (res){
		if(res.status && res.status === 500){
			if(res.data.message){
				if(res.data.message === 'WasAlreadyConfirmed'){
					$scope.errorMessage = 'giftConfirmed.alreadyConfirmed';
					return true;
				}
				if(res.data.message === 'ConfirmationRequestExpired'){
					$scope.errorMessage = 'giftConfirmed.expired';
					return true;
				}
				if(res.data.message === 'DealAlreadyConsumed'){
					$scope.errorMessage = 'giftConfirmed.alreadyConsumed';
					return true;
				}
			}
		}
		return false;
	};

	$scope.confirmGift = function(){
		$scope.status = 'processing';
		if($routeParams.id){
			restApi.grant.confirmGift({ id: $routeParams.id }, function(){
				$scope.status = 'success';
			}, function(res){
				$scope.status = 'failed';
				if(!self.handleGiftError(res)){
					$scope.handleRestApiError('errorConfirmingGift', res);		
				}
			})
		}
	};

	$rootScope.showBreadCrumb = false;
	$scope.confirmGift();
	$rootScope.mode = 'lean';
});