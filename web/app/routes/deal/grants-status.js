'use strict';

angular.module('Dealbee.deal.grantsStatus', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/deal/:dealId/grants-status/:lang', {
		templateUrl: 'routes/deal/grants-status.html',
		controller: 'GrantsStatusCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('GrantsStatusCtrl', function ($scope, $routeParams, restApi){
	new restApi.deal().$get({ id: $routeParams.dealId }, function (dealFound){
		$scope.deal = dealFound;
		$scope.generationTime = new Date();
		$scope.toBeConsumed = _.filter($scope.deal.grants, function (grant){ return grant.sent && !grant.consumed; });
		$scope.consumed = _.filter($scope.deal.grants, function (grant){ return grant.sent && grant.consumed; });
	}, function (res){
		$scope.handleRestApiError('dealLoadError', res);
	});

});