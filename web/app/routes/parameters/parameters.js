'use strict';

angular.module('Dealbee.parameters', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/parameters/:lang', {
	    templateUrl: 'routes/parameters/parameters.html',
	    controller: 'ParametersCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
  	});
}])
.controller('ParametersCtrl', function($scope, facebook, restApi) {
	$scope.reauthorizeFb = function (){
		$scope.reauthorizing = true;
		$scope.must
		FB.login(function (loginResponse){
			facebook.processLoginResponse(loginResponse).then(
				function(){
					if($scope.fbAccount.canPublish){
						facebook.processLoginResponseWithPublishPerm(loginResponse).then(
							function(){
								$scope.reauthorizing = false;
								$scope.displaySuccess('reauthorized', 'parameters.reauthorized');		
							},
							function (err){
								$scope.handleRestApiError('fbUserPublishInfoRegistrationFailed', err);
								$scope.connectingFacebook = false;
							}
						);
					}
				},
				function (err){
					$scope.reauthorizing = false;
					$scope.displayGenericError('reauthorizedFailed');		
				});
		}, { scope: 'email, user_friends, publish_actions', return_scopes: true });
	};
	$scope.fbConnect = function (){
		$scope.connectingFacebook = true;
		FB.login(function (loginResponse){
			facebook.processLoginResponse(loginResponse).then(
				function(){
					$scope.connectingFacebook = false;
				}
			);
		}, { scope: 'email, user_friends', return_scopes: true });
	};
	$scope.disconnectFb = function (){
		facebook.disconnect();
	};

	$scope.resetPassword = function (){
		$scope.submittedOnce = true;
		if($scope.resetPasswordForm.$valid){
			$scope.resettingPassword = true;
			restApi.emitter.changePassword({ 
				currentPassword: $scope.currentPassword,
				password: $scope.password 
				}, function(res){
					$scope.resettingPassword = false;
					$scope.displaySuccess('resetPasswordSuccess', 'parameters.resetPasswordSuccess')
				}, function(res){
					$scope.resettingPassword = false;
					$scope.handleRestApiError('resetPasswordFailed', res);
			});
		}
	};

	$scope.registerInBreadcrumb(2, '/parameters', 'parameters.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
});