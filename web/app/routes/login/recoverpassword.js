'use strict';

angular.module('Dealbee.recoverPassword', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/recoverpassword/:passwordRecoveryRequestId/:lang', {
	    templateUrl: 'routes/login/recoverpassword.html',
	    controller: 'RecoverPasswordCtrl'
  	});
}])
.controller('RecoverPasswordCtrl', function($scope, restApi, $routeParams) {
	$scope.recover = function (){
		$scope.submittedOnce = true;
		if($scope.recoverPasswordForm.$valid){
			$scope.recovering = true;
			restApi.emitter.recoverPassword({ 
					requestId: $routeParams.passwordRecoveryRequestId,
					password: $scope.password 
				}, function (){
				$scope.recovering = false;
				$scope.displaySuccess('changePasswordSuccess', 'recoverPassword.changePasswordSuccess');
			}, function (res){
				$scope.recovering = false;
				if(res.code === 'RequestNotFoundOrExpired'){
					$scope.displayError('changePasswordFailed', 'recoverPassword.requestMayBeExpired');
				} else {
					$scope.handleRestApiError('changePasswordFailed', res);
				}
			});
		}
	};
	$scope.registerInBreadcrumb(2, '/login', 'login.loginButton', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
});