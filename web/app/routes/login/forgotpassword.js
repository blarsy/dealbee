'use strict';

angular.module('Dealbee.forgotPassword', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/forgotpassword/:lang', {
	    templateUrl: 'routes/login/forgotpassword.html',
	    controller: 'ForgotPasswordCtrl'
  	});
}])
.controller('ForgotPasswordCtrl', function($scope, restApi) {
	$scope.recover = function (){
		$scope.submittedOnce = true;
		if($scope.forgotPasswordForm.$valid){
			$scope.recovering = true;
			restApi.emitter.forgotPassword({ email: $scope.email }, function (){
				$scope.recovering = false;
				$scope.displaySuccess('recoveryMessageSuccess', 'forgotPassword.recoveryMessageSuccess');
			}, function (res){
				$scope.recovering = false;
				$scope.handleRestApiError('recoveryMessageFailed', res);
			});
		}
	};
	$scope.registerInBreadcrumb(2, '/login', 'login.loginButton', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
});