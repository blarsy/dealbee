'use strict';

angular.module('Dealbee.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/login/:lang', {
	    templateUrl: 'routes/login/login.html',
	    controller: 'LoginCtrl'
  	});
}])
.controller('LoginCtrl', function($scope, facebook, $location, restApi, authentication) {
	$scope.$on('loggedIn', function(){
		$location.path('/home');
	});

	$scope.registerInBreadcrumb(2, '/login', 'login.loginButton', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
	$scope.registerStandardMeta('login');
});