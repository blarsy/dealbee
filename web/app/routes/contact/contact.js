'use strict';

angular.module('Dealbee.contact', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/contact/:lang', {
		templateUrl: 'routes/contact/contact.html',
		controller: 'ContactCtrl'
	});
})
.controller('ContactCtrl', function($scope, restApi) {
	$scope.registerInBreadcrumb(2, '/contact', 'contact.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
	$scope.registerStandardMeta('contact');

	$scope.sendMessage = function (){
		if($scope.messageForm.$valid){
			var message = new restApi.message({ subject: $scope.subject, body: $scope.body });

			if(!$scope.account){
				message.replyTo = $scope.email;
			}

			message.$save(function (){
				$scope.displaySuccess('messageSentSuccessfully', 'contact.messageSentSuccessfully')
			}, function (res){
				$scope.handleRestApiError('messageSendFailed', res);
			});			
		}
	};
});