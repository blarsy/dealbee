'use strict';

angular.module('Dealbee.pricing', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/pricing/:lang', {
	    templateUrl: 'routes/pricing/pricing.html',
	    controller: 'PricingCtrl'
  	});
}])
.controller('PricingCtrl', function($scope) {
	$scope.registerInBreadcrumb(2, '/login', 'pricing.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(), name: $scope.getLevel1BreadcrumbNodeText() }]);
	$scope.registerStandardMeta('pricing');
});