'use strict';

angular.module('Dealbee.consume', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/consume/:lang', {
		templateUrl: 'routes/consume/consume.html',
		controller: 'ConsumeCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
	$routeProvider.when('/consume/:uuid/:lang', {
		templateUrl: 'routes/consume/scan.html',
		controller: 'ScanCtrl'
	});
})
.controller('ConsumeCtrl', function($scope, restApi, $location, $timeout, $q) {
	var self = this;

	this.consume = function (dealNumber){
		var deffered = $q.defer();

		restApi.deal.consume({ number: dealNumber }, function(){
			$scope.displaySuccess('consumeSucess', 'consume.consumeSuccess');
			$location.path('/home');
			deffered.resolve();
		}, function(res){
			$scope.handleRestApiError('consumeError', res);
			deffered.resolve();
		});
		return deffered.promise;
	};
		
	$scope.consumeByNumber = function (){
		$scope.numberSubmittedOnce = true;
		if($scope.consumeByNumberForm.$valid){
			$scope.consuming = true;
			self.consume($scope.dealNumber).then(function (){
				$scope.consuming = false;
			});
		}
	};

	$scope.consumeSelectedDeal = function (deal){
		deal.consuming = true;
		self.consume(deal.number).then(function (){
			deal.consuming = false;
		});
	};

	$scope.filterDealHolders = function (){
		$scope.noDealHolderFound = false;
		$timeout(function (){
			if($scope.dealHolderFilterCriteria.length > 0){
				$scope.dealHoldersDisplayed = _.filter($scope.allDealHolders, function (dealHolder){
					if(dealHolder.deals[0].email){
						return dealHolder.holder.toLowerCase().indexOf($scope.dealHolderFilterCriteria.toLowerCase()) !== -1;
					} else {
						return dealHolder.deals[0].fbDestinatorUser.name.toLowerCase().indexOf($scope.dealHolderFilterCriteria.toLowerCase()) !== -1;
					}
				});
				if($scope.dealHoldersDisplayed.length === 0){
					$scope.noDealHolderFound = true;
				}
			} else {
				$scope.dealHoldersDisplayed = [];
			}
		});
	};
	$scope.dealHoldersDisplayed = [];
	$scope.loadingDealHolders = true;
	restApi.dealHolders.query({}, function (res){
		$scope.allDealHolders = res;
		$scope.dealHolders = $scope.allDealHolders.slice();
		$scope.loadingDealHolders = false;
	});
	$scope.registerInBreadcrumb(2, '/consume', 'consume.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
});