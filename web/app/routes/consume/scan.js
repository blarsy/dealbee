'use strict';

angular.module('Dealbee.scan', [])

.controller('ScanCtrl', function($scope, $routeParams, restApi, $location, $rootScope, authentication){
	var self = this;

	this.load = function (){
		$scope.scanning = true;
		restApi.grant.scan({ uuid: $routeParams.uuid }, function (res){
			$scope.scanResult = res;
			$scope.scanning = false;
		}, function (err){
			$scope.scanning = false;
			$scope.error = true;
		});
	};

	authentication.getRestoreSessionPromise().then(function (){
		self.load();
	});

	$scope.registerInBreadcrumb(2, $location.path(), 'scan.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
});