'use strict';

angular.module('Dealbee.pref', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/pref/:grantId/:lang', {
		templateUrl: 'routes/pref/pref.html',
		controller: 'PrefCtrl'
	});
})
.controller('PrefCtrl', function($scope, $routeParams, restApi) {
	$scope.registerInBreadcrumb(2, '/pref', 'pref.title', [{ level: 1, url: '/splash',name: $scope.getLevel1BreadcrumbNodeText() }]);

	$scope.loadingPref = true;
	restApi.preference.get({ grantId: $routeParams.grantId }, function (res){
		$scope.unsubscribedFromDealbee = res.unsubscribedDate ? true : false;
		$scope.unsubscribedFromEmitter = res.unsubscribedFromEmitter;
		restApi.emitter.get({ grantId: $routeParams.grantId }, function (res){
			$scope.loadingPref = false;
			$scope.emitter = res;
		}, function (res){
			$scope.loadingPref = false;
			$scope.handleRestApiError('loadEmitterFailed', res);
		});
	}, function (res){
		$scope.loadingPref = false;
		$scope.handleRestApiError('loadPreferenceFailed', res);
	});

	$scope.savePreferences = function (){
		var pref = new restApi.preference({
			grantId: $routeParams.grantId,
			unsubscribed: $scope.unsubscribedFromDealbee || false,
			blockEmitter: $scope.unsubscribedFromEmitter || false
		});

		$scope.savingPref = true;

		pref.$save(function (){
			$scope.savingPref = false;
			$scope.displaySuccess('PrefSaved', 'pref.preferencesSavedSuccessfully');
		 }, function(res){
			$scope.savingPref = false;
		 	$scope.handleRestApiError('PrefSaveFailed', res);
		 });
	};
});