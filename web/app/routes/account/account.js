'use strict';

angular.module('Dealbee.account', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/account/:lang', {
		templateUrl: 'routes/account/account.html',
		controller: 'AccountCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('AccountCtrl', function($scope, emitter, $rootScope) {
	$scope.registerInBreadcrumb(2, '/account', 'account.title', [{ level: 1, url: '/home',name: $scope.getLevel1BreadcrumbNodeText() }]);
	emitter.loadLoggedInEmitter();

	$scope.saveAccount = function (){
		var allValid = true;
		_.each($scope.saveSteps, function (saveStep){
			if(!saveStep.func()) allValid = false;
		});
		if(allValid){
			$scope.savingAccount = true;
			emitter.save().then(function (response){
				$rootScope.account = response.res;
				$scope.savingAccount = false;
				$scope.displaySuccess('accountSavedSuccessfully', 'account.accountSavedSuccessfully')
			}, function (res){
				$scope.savingAccount = false;
				$scope.handleRestApiError('accountSaveFailed', res);
			});
		}
	};

	$scope.subscribeStep = function(id, func){
		var step,
			existingSaveStep = _.find($scope.saveSteps, function (saveStep){
				return saveStep.id === id;
			});

		if(existingSaveStep){
			$scope.saveSteps.splice($scope.saveSteps.indexOf(existingSaveStep), 1);
		};

		step = { id: id, func: func };
		$scope.saveSteps.push(step);
		$scope.lastLoadedStep = step;
	};

	$scope.saveSteps = [];
});