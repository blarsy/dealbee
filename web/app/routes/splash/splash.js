'use strict';

angular.module('Dealbee.splash', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/splash/:lang', {
    templateUrl: 'routes/splash/splash.html',
    controller: 'SplashCtrl'
  });
}])
.controller('SplashCtrl', function($scope, $rootScope, settings, facebook, authentication, $location, restApi) {
	authentication.getRestoreSessionPromise().then(function (){
		$location.path('home');
	});

	$rootScope.showBreadCrumb = false;
	$scope.registerInBreadcrumb(1, '/splash', $scope.getLevel1BreadcrumbNodeText());
	$scope.registerStandardMeta('splash');
});