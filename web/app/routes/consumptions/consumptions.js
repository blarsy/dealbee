'use strict';

angular.module('Dealbee.consumptions', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/consumptions/:lang', {
		templateUrl: 'routes/consumptions/consumptions.html',
		controller: 'ConsumptionsCtrl'
	});
})
.controller('ConsumptionsCtrl', function($scope, restApi) {
	$scope.queryConsumptions = function (month){
		
	};
	var thisYear = new Date().getFullYear(),
		thisMonth = new Date().getMonth();
	$scope.monthes = [];
	$scope.consumptions = [];
	for(var i = -5; i < 1; i ++){
		$scope.monthes.push(new Date(thisYear, thisMonth + i));
	}
});