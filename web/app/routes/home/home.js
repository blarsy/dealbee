'use strict';

angular.module('Dealbee.home', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/home/:lang', {
		templateUrl: 'routes/home/home.html',
		controller: 'HomeCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('HomeCtrl', function ($scope, restApi, $location, globalState, deal, distribute, $rootScope, $interval) {
	var self = this,
		autoRefreshPromise;

	$scope.$on('switchedEmitter', function (){
		$scope.deals = [];
		$scope.dealsToDistribute = [];
		$scope.dealsRunning = [];
		self.loadDeals();
	});

	this.sortByDate = function (a, b){
		var timestampA = new Date(a.created).valueOf(),
			timestampB = new Date(b.created).valueOf();
		
		if(timestampA > timestampB) return -1;
		else if(timestampA === timestampB) return 0;
		else return 1;
	};

	this.refreshDeals = function(displayedDeals, newerDeals){
		_.each(newerDeals, function (newerDeal){
			var displayedDeal = _.find(displayedDeals, function (scannedDeal){ return scannedDeal._id === newerDeal._id });
			if(displayedDeal){
				displayedDeal.sent = newerDeal.sent;
				displayedDeal.notSent = newerDeal.notSent;
				displayedDeal.consumed = newerDeal.consumed;
				displayedDeal.expired = newerDeal.expired;
			} else {
				displayedDeals.push(newerDeal);
			}
		});
		_.each(displayedDeals, function (displayedDeal){
			if(!_.find(newerDeals, function (newerDeal){
				return newerDeal._id === displayedDeal._id;
			})){
				displayedDeals.splice(displayedDeals.indexOf(displayedDeal), 1);
			}
		});
	}

	this.loadDeals = function(justOnce){
		restApi.deal.query(function (res){
			$scope.noDealYet = res.length === 0;
			if(!$scope.noDealYet){
				var deals = _.map(res, function (deal){
						var dealExpired = Date.parse(deal.expiration) < new Date().valueOf(),
							counts = _.countBy(deal.grants, function (grant){
							if(!grant.sent){
								return dealExpired ? 'expired' : 'notSent';
							} else if (!grant.consumed){
								return dealExpired ? 'expired' : 'sent';
							} else {
								return 'consumed';	
							}
						});
						deal.expiration = new Date(Date.parse(deal.expiration));
						deal.beginDate = new Date(Date.parse(deal.beginDate));
						deal.sent = counts.sent || 0;
						deal.notSent = counts.notSent || 0;
						deal.consumed = counts.consumed || 0;
						deal.expired = counts.expired || 0;
						return deal;
					}),
					dealsToDistribute = _.filter(deals, function(deal){ return $scope.isNew(deal); }).sort(self.sortByDate),
					dealsRunning = _.filter(deals, function(deal){ return !$scope.isNew(deal); }).sort(self.sortByDate);

				if(!$scope.dealsToDistribute){
					$scope.dealsToDistribute = dealsToDistribute;
				} else {
					self.refreshDeals($scope.dealsToDistribute, dealsToDistribute);
				}
				if(!$scope.dealsRunning){
					$scope.dealsRunning = dealsRunning;
				} else {
					self.refreshDeals($scope.dealsRunning, dealsRunning);
				}

			}
		});
		if(!justOnce){
			if(autoRefreshPromise) $interval.cancel(autoRefreshPromise);
			autoRefreshPromise = $interval(function (){
				self.loadDeals();
			}, 10000, 1);
		}
	};
	$scope.isNew = function (deal){
		return deal.notSent === 0 && 
			deal.sent === 0 && 
			deal.consumed === 0 && 
			deal.expired === 0 && 
			deal.nbOnRequest === 0;
	}
	$scope.navigateToDistributeDeal = function (deal){
		$location.path('deal/' + deal._id + '/distribute');
	};
	$scope.preview = function(dealToPreview){
		deal.loadFrom(dealToPreview);
		$location.path('/viewdeal/preview');
	};
	$scope.deleteDeal = function (deal){
		deal.deleting = true;
		restApi.deal.delete({ id: deal._id }, function (value, headers){
			$scope.displaySuccess('dealDeleted', 'home.dealDeleted');
			deal.deleting = false;
			self.loadDeals(true);
		}, function (res){
			deal.deleting = false;
			$scope.handleRestApiError('dealNotDeleted', res)
		});
	};
	$scope.createDeal = function (){
		deal.init();
		$location.path('/deal');
	};
	$scope.modifyDeal = function (dealToModify){
		deal.loadFrom(dealToModify);
		$location.path('/deal');		
	};
	self.loadDeals();

	$scope.$on('$destroy', function (){
		if(autoRefreshPromise) $interval.cancel(autoRefreshPromise);
	});

	$rootScope.mode = 'home';
	$scope.registerInBreadcrumb(1, '/home', $scope.getLevel1BreadcrumbNodeText());
	$scope.now = new Date();
});