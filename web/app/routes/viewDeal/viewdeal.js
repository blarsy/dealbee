'use strict';

angular.module('Dealbee.viewDeal', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/viewdeal/:hash/:lang', {
		templateUrl: 'routes/viewdeal/viewdeal.html',
		controller: 'ViewDealCtrl'
	});
	$routeProvider.when('/viewdeal/:context/:hash/:lang', {
		templateUrl: 'routes/viewdeal/viewdeal.html',
		controller: 'ViewDealCtrl'
	});
})
.controller('ViewDealCtrl', function($scope, $rootScope, $routeParams, $timeout, $interval, restApi, settings, $location, deal, globalState, $q){
	var timeToExpiration,
		self = this,
		termsOfUse = [],
		intervalId;

	this.calculateRemaining = function (){
		var timeToExpiration = new Date($scope.deal.expiration).getTime() - new Date().getTime();
		if(timeToExpiration < 0){
			$scope.expired = true;
		}
		$scope.remainingDays = Math.floor(timeToExpiration / (1000 * 60 * 60 * 24));
		$scope.remainingHours = Math.floor((timeToExpiration - ($scope.remainingDays * 1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		$scope.remainingMinutes = Math.floor((timeToExpiration - ($scope.remainingDays * 1000 * 60 * 60 * 24) - ($scope.remainingHours * 1000 * 60 * 60)) / (1000 * 60));
		$scope.remainingSeconds = Math.floor((timeToExpiration - ($scope.remainingDays * 1000 * 60 * 60 * 24) - ($scope.remainingHours * 1000 * 60 * 60) - ($scope.remainingMinutes * 1000 * 60)) / 1000);
	};

	this.initScope = function (){
		try{
			var shareDealPageUrl = 'http://www.facebook.com/sharer/sharer.php?u=' + window.encodeURIComponent($location.absUrl());
			$scope.shareDealPageUrl = shareDealPageUrl.substring(0, shareDealPageUrl.lastIndexOf('%2F'));


			if($scope.deal.termsOfUse.length > 0 && !$scope.deal.termsOfUse[0].text){
				_.each($scope.deal.termsOfUse, function (termOfUse){
					termsOfUse.push({ text: termOfUse });
				});
				$scope.deal.termsOfUse = termsOfUse;
			}
			if($scope.deal.expiration){
				$scope.now = new Date();
				$scope.expiration = new Date($scope.expiration);
				$scope.beginDate = new Date($scope.deal.beginDate);
				self.calculateRemaining();
				intervalId = $interval(self.calculateRemaining, 1000);
			} else {
				$scope.remainingDays = 'XX';
				$scope.remainingHours = 'XX';
				$scope.remainingMinutes = 'XX';
				$scope.remainingSeconds = 'XX';
			}
			if($scope.emitter.address && $scope.emitter.address.coords){
				var thePoint = { latitude: new Number($scope.emitter.address.coords[1]), longitude: new Number($scope.emitter.address.coords[0]) };

				$scope.map = { 
					marker: { location: thePoint, options: { draggable: false }, id:1 },
					center: { latitude: $scope.emitter.address.coords[1], longitude: $scope.emitter.address.coords[0] }, 
					zoom: 14 };
			}
			$timeout(function (){
				$('#expiration').bigtext();	
				$('#expiration').trigger('resize');
			});	
		}
		finally{
			$scope.loading = false;
		}
	};

	this.loadDealPublicInfo = function (){
		var deffered = $q.defer();

		restApi.deal.publicInfo({ id: $routeParams.hash }, function (deal){
			$scope.deal = deal;
			$scope.emitter = deal.emitter;
			self.initScope();
			$scope.loading = false;
			deffered.resolve();
		}, function (err){
			$scope.loading = false;
			$scope.handleRestApiError('loadDealFailed', err);
			deffered.reject(err);
		});

		return deffered.promise;
	}

	$scope.$on('$destroy', function (){
		if(intervalId) $interval.cancel(intervalId);
	});

	$scope.returnToPreviousView = function (){
		$location.path(globalState.previousView);
	};

	$scope.give = function(){
		if($scope.displayMode !== 'preview'){
			$location.path('/deal/give/' + $routeParams.hash)
		}
	}

	$scope.request = function (){
		$scope.requestSubmittedOnce = true;
		if($scope.deal.nbOnRequest > 0){
			$location.path('/deal/request/' + $routeParams.hash)
		}
	};
		
	$rootScope.mode = 'leaner';
	$rootScope.showBreadCrumb = true;
	$scope.loading = true;
	$scope.thisPageUrl = $location.absUrl();

	if($routeParams.hash === 'preview'){
		$scope.deal = deal.getDealObject();
		$scope.emitter = $scope.account.emitter;
		$scope.number = 1;
		$scope.displayMode = 'preview';
		self.initScope();
	} else if($routeParams.context === 'onrequest'){
		self.loadDealPublicInfo().then(
			function (){
				$scope.displayMode = 'onrequest';
			}
		);
	} else {
		$scope.hash = $routeParams.hash;
		restApi.grant.get({ id: $routeParams.hash }, function (res){
			$scope.deal = res.deal;
			if(res.deal){
				$scope.emitter = res.deal ? res.deal.emitter : null;
				$scope.number = res.number;
				$scope.channelType = res.channelType;
				if(res.channelType === 0){
					$scope.dealHolder = res.fbDestinatorUser.name;
				} else {
					$scope.dealHolder = res.email;
				}
				self.initScope();				
			} else {
				$scope.loading = false;
			}
		});
	}
});