'use strict';

angular.module('Dealbee.find', ['ngRoute'])

.config(function ($routeProvider){
	$routeProvider.when('/find/:lang', {
		templateUrl: 'routes/find/find.html',
		controller: 'FindCtrl'
	});
})
.controller('FindCtrl', function ($scope, restApi, $rootScope, $interval, $geolocation, $routeParams, $location){
	var self = this,
		intervalId;

	$scope.loading = true;

	this.calculateExpiration = function(){
		$scope.now = new Date();
		_.each($scope.myDeals, function (deal){
			deal.beginDate = new Date(deal.beginDate);
			deal.expiration = new Date(deal.expiration);
			var timeToExpiration = new Date(deal.expiration).getTime() - new Date().getTime();
			deal.remainingDays = Math.floor(timeToExpiration / (1000 * 60 * 60 * 24));
			deal.remainingHours = Math.floor((timeToExpiration - (deal.remainingDays * 1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			deal.remainingMinutes = Math.floor((timeToExpiration - (deal.remainingDays * 1000 * 60 * 60 * 24) - (deal.remainingHours * 1000 * 60 * 60)) / (1000 * 60));
			deal.remainingSeconds = Math.floor((timeToExpiration - (deal.remainingDays * 1000 * 60 * 60 * 24) - (deal.remainingHours * 1000 * 60 * 60) - (deal.remainingMinutes * 1000 * 60)) / 1000);
		});
	};

	$scope.search = function (queryMode){
		if(queryMode){
			$scope.queryMode = queryMode;
		}
		$location.search({ search: $scope.keywords, queryMode: $scope.queryMode });
		//$scope.queryDeals($scope.queryMode, $scope.keywords);
	};

	$scope.queryDeals = function (queryMode, keywords){
		var previousQueryMode = $scope.queryMode;

		$scope.loading = true;
		$scope.pleaseEnableGeolocation = false;
		if($scope.queryMode === 'nearest'){
			$geolocation.getCurrentPosition({
					timeout: 60000
				}).then(function (position){
					$scope.position = position.coords;
					restApi.deal.query({ mode: $scope.queryMode, keywords: keywords, lat: $scope.position.latitude, lng: $scope.position.longitude }, function (deals){
						$scope.myDeals = _.sortBy(deals, 'distance');
						$scope.loading = false;
						if(!intervalId){
							intervalId = $interval(self.calculateExpiration, 1000);
						}
					}, function(res){
						$scope.loading = false;
						$scope.handleRestApiError('errorLoadingDeals', res);
					});
				}, function (err){
					if(err.error.message === 'User denied Geolocation'){
						$scope.pleaseEnableGeolocation = true;
					}
					$scope.queryMode = previousQueryMode;
					$scope.loading = false;
				});
		} else {
			restApi.deal.query({ mode: $scope.queryMode, keywords: keywords }, function (deals){
				$scope.myDeals = _.sortBy(deals, function(deal){
					if($scope.queryMode === 'expiresoon'){
						return new Date(deal.expiration).valueOf();
					} else if ($scope.queryMode === 'latestcreated'){
						return -(new Date(deal.created).valueOf());
					}
				});
				$scope.loading = false;
				if(!intervalId){
					intervalId = $interval(self.calculateExpiration, 1000);
				}
			}, function(res){
				$scope.loading = false;
				$scope.handleRestApiError('errorLoadingDeals', res);
			});		
		}
	};

	$scope.$on('$destroy', function (){
		if(intervalId) $interval.cancel(intervalId);
	});

	$scope.keywords = $routeParams.search;
	$scope.queryMode = $routeParams.queryMode || 'latestcreated';
	$scope.registerInBreadcrumb(2, '/find', 'find.title', [{ level: 1, url: '/splash',name: $scope.getLevel1BreadcrumbNodeText() }]);
	$scope.registerStandardMeta('find');
	$scope.queryDeals($scope.queryMode, $scope.keywords);
	$rootScope.mode = 'lean';
});