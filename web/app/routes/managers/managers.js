'use strict';

angular.module('Dealbee.managers', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/managers/:lang', {
	    templateUrl: 'routes/managers/managers.html',
	    controller: 'ManagersCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
  	});
}])
.controller('ManagersCtrl', function($scope, restApi) {
	var self = this;

	this.load = function(){
		$scope.loadingManagers = true;
		restApi.manager.query(function(res){
			$scope.managers = res;
			$scope.loadingManagers = false;
		}, function(err){
			$scope.loadingManagers = false;
			$scope.handleRestApiError('loadManagerFailed', err);
		});
	};

	$scope.removeManager = function (manager){
		$scope.removingManager = true;
		restApi.manager.delete({ id: manager._id }, function (){
			$scope.removingManager = false;
			self.load();
		}, function (err){
			$scope.removingManager = false;
			$scope.handleRestApiError('deleteManagerFailed', err);
		});
	};

	$scope.addManager = function (){
		$scope.submittedOnce = true;
		$scope.addingManager = true;
		if($scope.addManagerForm.$valid){
			var newManager = new restApi.manager({ email: $scope.emailToAdd });
			newManager.$save(function (res){
				$scope.addingManager = false;
				$scope.emailToAdd = '';
				if(res.result === 'Mail_Sent'){
					$scope.displaySuccess('managerMailed', 'managers.mailSentToNewManager');
				} else {
					self.load();
				}
			}, function (err){
				$scope.addingManager = false;
				$scope.handleRestApiError('addManagerFailed', err);
			});
			$scope.submittedOnce = false;
		}
	};

	this.load();
	$scope.registerInBreadcrumb(2, '/managers', 'managers.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
});