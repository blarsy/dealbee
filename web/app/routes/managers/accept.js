'use strict';

angular.module('Dealbee.managers.accept', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/managers/accept/:invitationId/:lang', {
	    templateUrl: 'routes/managers/accept.html',
	    controller: 'AcceptCtrl'
  	});
}])
.controller('AcceptCtrl', function($scope, restApi, $routeParams, identity, authentication, $modal, $location, $q) {
	var self = this,
		loginModal;

	restApi.emitter.get({ managerInvitationId: $routeParams.invitationId }, function (emitter){
		$scope.emitter = emitter;
		$scope.loadingEmitter = false;
	}, function (err){
		$scope.loadingEmitter = false;
		switch(err.data.message){
			case 'Invitation_Already_Accepted':
				$scope.displayError('invitationAlreadyAccepted', 'accept.invitationAlreadyAccepted');
				break;
			case 'Invitation_Expired':
				$scope.displayError('invitationExpired', 'accept.invitationExpired');
				break;
			case 'Email_Already_Used':
				$scope.displayError('emailAlreadyUsed', 'accept.emailAlreadyUsed');
				break;
			default:
				$scope.handleRestApiError('managerPromotionFailed', err);
		}
	});

	$scope.login = function (){
		if(!loginModal){
			loginModal = $modal.open({
				templateUrl: 'managerLoginModalContent',
				backdrop: 'static',
				size: 'lg',
				scope: $scope });
			loginModal.result.then(function (){
				loginModal = undefined;
			}, function (){
				loginModal = undefined;
			});
		}
	};

	this.saveManager = function (manager){
		var result = $q.defer();
		manager.$save(function (){
			$scope.displaySuccess = ('managerCreated', 'accept.managerCreated');
			result.resolve();
		}, function (err){
			status = false;
			switch(err.data.message){
				case 'Invitation_Already_Accepted':
					result.reject('accept.invitationAlreadyAccepted');
					break;
				case 'Invitation_Expired':
					result.reject('accept.invitationExpired');
					break;
				case 'Duplicate_Manager':
					result.reject('accept.alreadyManager');
					break;
				case 'Email_Already_Used':
					result.reject('accept.emailAlreadyUsed');
					break;
				default:
					$scope.handleRestApiError('managerPromotionFailed', err);
					result.reject();
			}
		});

		return result.promise;
	};

	// this.saveManagerAndLogin = function (manager){
	// 	var result = $q.defer();
	// 	manager.$save(function (){
	// 		$scope.displaySuccess = ('managerCreated', 'accept.managerCreated');
	// 		authentication.login(identity.fbToken, identity.email, identity.password).then(function (){
	// 			authentication.switchToEmitter($scope.emitter._id).then(function (){
	// 				$location.path('/home');
	// 				result.resolve();
	// 			}, function (err){
	// 				$scope.handleRestApiError('switchFailed', err);
	// 				result.reject();
	// 			});
	// 		}, function (err){
	// 			$scope.handleRestApiError('loginFailed', err);
	// 			result.reject();
	// 		});
	// 	}, function (err){
	// 		status = false;
	// 		switch(err.data.message){
	// 			case 'Invitation_Already_Accepted':
	// 				result.reject('accept.invitationAlreadyAccepted');
	// 				break;
	// 			case 'Invitation_Expired':
	// 				result.reject('accept.invitationExpired');
	// 				break;
	// 			case 'Duplicate_Manager':
	// 				result.reject('accept.alreadyManager');
	// 				break;
	// 			case 'Email_Already_Used':
	// 				result.reject('accept.emailAlreadyUsed');
	// 				break;
	// 			default:
	// 				$scope.handleRestApiError('managerPromotionFailed', err);
	// 				result.reject();
	// 		}
	// 	});

	// 	return result.promise;
	// };

	$scope.$on('loggedIn', function(){
		var manager = new restApi.manager();
		manager.invitationId = $routeParams.invitationId;
		manager.identifierId = $scope.account.identifier._id;
		$scope.promotingManager = true;
		$scope.managerCreateError = '';

		self.saveManager(manager).then(function (identifier){
			$scope.account.identifier = identifier;
			authentication.switchToEmitter($scope.emitter._id).then(function (){
				$scope.promotingManager = false;
				loginModal.close();
				$location.path('/home');
			}, function (err){
				$scope.promotingManager = false;
				$scope.handleRestApiError('switchFailed', err);
			});
		}, function (reason){
			$scope.promotingManager = false;
			if(!reason){
				loginModal.close();
			} else {
				$scope.managerCreateError = reason;
			}
		});

		// self.saveManagerAndLogin(manager, $scope.promotingManager).then(function (){
		// 	$scope.managerCreateError = '';
		// 	$scope.promotingManager = false;
		// 	loginModal.close();
		// }, function (reason){
		// 	$scope.promotingManager = false;
		// 	if(!reason){
		// 		loginModal.close();
		// 	} else {
		// 		$scope.managerCreateError = reason;
		// 	}
		// });
	});

	$scope.createProfile = function (){
		if(identity.isValid()){
			var manager = new restApi.manager();
			manager.invitationId = $routeParams.invitationId;
			manager.fbToken = identity.fbToken;
			manager.email = identity.email;
			manager.password = identity.password;
			$scope.createProfileError = '';
			$scope.creatingManager = true;

			self.saveManager(manager).then(function (){
				authentication.login(identity.fbToken, identity.email, identity.password).then(function (){
					authentication.switchToEmitter($scope.emitter._id).then(function (){
						$scope.creatingManager = false;
						$location.path('/home');
					}, function (err){
						$scope.creatingManager = false;
						$scope.handleRestApiError('switchFailed', err);
					});
				}, function (err){
					$scope.handleRestApiError('loginFailed', err);
					$scope.creatingManager = false;
				});
			}, function (reason){
				$scope.creatingManager = false;
				if(reason){
					$scope.createProfileError = reason;
				}
			});


			// self.saveManagerAndLogin(manager, $scope.creatingManager).then(function (){
			// 	$scope.creatingManager = false;
			// }, function (reason){
			// 	$scope.creatingManager = false;
			// 	if(reason){
			// 		$scope.createProfileError = reason;
			// 	}
			// });
		}
	};

	$scope.loadingEmitter = true;
	$scope.registerInBreadcrumb(2, '/accept', 'accept.title', [{ level: 1, url: $scope.getLevel1BreadcrumbPath(),name: $scope.getLevel1BreadcrumbNodeText() }]);
});