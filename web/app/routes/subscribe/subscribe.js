'use strict';

angular.module('Dealbee.subscribe', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/subscribe/:step/:lang', {
    templateUrl: 'routes/subscribe/subscribe.html',
    controller: 'SubscribeCtrl'
  });
  $routeProvider.when('/subscribe/:lang', {
    redirectTo: '/subscribe/1/:lang'
  });
}])
.controller('SubscribeCtrl', function($scope, $rootScope, restApi, $location, authentication, emitter, identity, $routeParams, $q) {
	var self = this,
		handleSubscriptionError = function (res){
			if(res.status && res.status === 500){
				if(res.responseText){
					try{
						var errorInfo = JSON.parse(res.responseText);
						if(errorInfo.message){
							if(errorInfo.message === 'File_Exceeded_Max_Size'){
								return "imageTooBig";
							}
							if(errorInfo.message === 'Expected_Image_File'){
								return "notAnImage";
							}
						}
					}
					catch(err){}
				} else if(res.data.message){
					if(res.data.message === 'Email_Already_Used'){
						return "subscribe.duplicateEmail";
					}
				}
			}
			return null;
		};

	$scope.finish = function (){
		$scope.subscribing = true;
		var deferred = $q.defer();

		try
		{
			if($scope.lastLoadedStep.func()){
				emitter.email = identity.email;
				emitter.password = identity.password;
				emitter.fbToken = identity.fbToken;
				emitter.store();
				emitter.save().then(
					function (response){
						if(!$scope.account){
							authentication.login(identity.fbToken, identity.email, identity.password).then(
								function (){
									if(identity.fbToken){
										authentication.switchToEmitter(response.res.emitter._id).then(function (){
											$scope.subscribing = false;
											$location.path('/home');
											emitter.clear();
											identity.clear();
											deferred.resolve();
										}, function (res){
											$scope.subscribing = false;
											deferred.reject(res);
										});
									} else {
										$scope.subscribing = false;
										$location.path('/home');
										emitter.clear();
										identity.clear();
										deferred.resolve();
									}
								},
								function (err){
									$scope.subscribing = false;
									$scope.handleRestApiError('loginFailed', response.res);
									deferred.reject(response.res);
								}
							);
						} else {
							$scope.subscribing = false;
							emitter.clear();
							authentication.switchToEmitter(response.res.emitter._id).then(function (){
								$location.path('/home');
								deferred.resolve();
							}, function (res){
								$scope.subscribing = false;
								deferred.reject(res);
							});
						}
					},
					function (res){
						$scope.subscribing = false;
						var errorMessage = handleSubscriptionError(res);
						if(errorMessage){
							$scope.displayError('subscribeFailed', errorMessage);
						} else {
							$scope.handleRestApiError('subscribeFailed', res);
						}
						$scope.safeApply();
						deferred.reject(res);
					}
				);
			} else {
				$scope.subscribing = false;
				deferred.reject('invalid');
			}
		}
		catch(ex){
			deferred.reject(ex);
		}

		return deferred.promise;
	};

	$scope.next = function (){
		if($scope.lastLoadedStep.func()){
			emitter.store();
			$location.path('/subscribe/' + ($scope.step + 1));
		}
	};

	$scope.previous = function (){
		$location.path('/subscribe/' + ($scope.step - 1));
	}
	
	$scope.skip = function (){
		$location.path('/subscribe/' + ($scope.step + 1));
	};

	$scope.subscribeStep = function(id, func){
		var step,
			existingSaveStep = _.find($scope.saveSteps, function (saveStep){
				return saveStep.id === id;
			});

		if(existingSaveStep){
			$scope.saveSteps.splice($scope.saveSteps.indexOf(existingSaveStep), 1);
		};

		step = { id: id, func: func };
		$scope.saveSteps.push(step);
		$scope.lastLoadedStep = step;
	};

	$scope.getLastStep = function(){
		return $scope.account ? 2 : 3;
	};

	$scope.saveSteps = [];
	$scope.step = Number($routeParams.step) || 1;
	$rootScope.mode = 'subscribe'
	$scope.registerInBreadcrumb(2, '/subscribe', 'subscribe.title', [{ level: 1, url: '/splash',name: $scope.getLevel1BreadcrumbNodeText() }]);
	$scope.registerStandardMeta('subscribe');
});