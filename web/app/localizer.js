'use strict';

angular.module('Dealbee.localizer', [])
.controller('LocalizerCtrl', function($scope, restApi, l8n) {
	this.endPicking = function (){
		$scope.picking = false;
		if(self.latestHoveredElement){
			$(self.latestHoveredElement).css('border', '');
		}
		$(document.documentElement).off('mousemove');
		$(document.documentElement).off('click');
	};
	this.clickWhilePicking = function (event){
		var clickedElement = document.elementFromPoint(event.clientX, event.clientY);
		if(clickedElement.attributes['data-dlb-localize']){
			self.endPicking();

			$scope.selectResource(
				_.find($scope.displayedResources, function (resource){ return resource.code === clickedElement.attributes['data-dlb-localize'].value; })
			);
			$scope.safeApply();
		}
		$(self.latestHoveredElement).css('border', '');
	};
	this.mousemoveWhilePicking = function (event){
		var hoveredElement = document.elementFromPoint(event.clientX, event.clientY);
		if(hoveredElement.attributes['data-dlb-localize'] && hoveredElement !== self.latestHoveredElement){
			$(hoveredElement).css('border', '4px dashed red');
			$scope.hoveredResource = _.find($scope.displayedResources, function (resource){ return resource.code === hoveredElement.attributes['data-dlb-localize'].value; });
			$(self.latestHoveredElement).css('border', '');
			self.latestHoveredElement = hoveredElement;
			$scope.safeApply();
		}
	};
	$scope.selectResource = function (resource){
		if($scope.selectedResource){
			$('*[data-dlb-localize=\'' + $scope.selectedResource.code + '\']').css('border','');
		}
		$scope.selectedResource = resource;
		$('*[data-dlb-localize=\'' + resource.code + '\']').css('border','4px dashed red');
		self.endPicking();
	};
	$scope.togglePickFromScreen = function (){
		$scope.picking = !$scope.picking;
		if($scope.picking){
			$(document.documentElement).on('click', self.clickWhilePicking);	
			$(document.documentElement).on('mousemove', self.mousemoveWhilePicking);
		} else {
			self.endPicking();
		}
	};
	$scope.saveResources = function (){
		var resources = new restApi.resources();
		resources.resources = l8n.allRes.resources;
		if(!$scope.account){
			$scope.saveStatus = 'FAILED to save: please login again with an authorized account, and try again.';
			return;
		}		
		$scope.saving = true;
		resources.$save(function (){
			$scope.saving = false;
			$scope.saveStatus = 'Saved succesfully at ' + app.formatting.toDateTime(new Date(), 'en');
		}, function (res){
			$scope.saving = false;
			$scope.saveStatus = 'FAILED to save at ' + app.formatting.toDateTime(new Date(), 'en');
		})
	};
	$scope.collapsePane = function (collapsed){
		$scope.collapsed = collapsed;
	};
	var self = this;
	this.latestHoveredElement = null;
	$scope.collapsed = false;
});