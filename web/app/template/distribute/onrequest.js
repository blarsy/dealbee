'use strict';

angular.module('Dealbee.distribute.onRequest', ['ngRoute'])

.controller('OnRequestCtrl', function($scope, distribute) {
	$scope.onRequest = distribute.onRequestDeals;
});