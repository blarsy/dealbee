'use strict';
angular.module('Dealbee.distribute.summary', ['ngRoute'])

.controller('DistributeSummaryCtrl', function($scope, distribute, facebook) {
	var self = this;

	$scope.ensurePublishRights = function (){
		$scope.connectingFacebook = true;
		FB.login(function (loginResponse){
			facebook.processLoginResponse(loginResponse).then(
				function(){
					if($scope.fbAccount.canPublish){
						facebook.processLoginResponseWithPublishPerm(loginResponse).then(
							function(){
								$scope.summary.publishRightsRequired = false;
								$scope.connectingFacebook = false;
							},
							function (err){
								$scope.handleRestApiError('fbUserPublishInfoRegistrationFailed', err);
								$scope.connectingFacebook = false;
							}
						);
					} else {
						$scope.connectingFacebook = false;
					}
				},
				function (reason){
					$scope.publishPerm = false;
					$scope.connectingFacebook = false;
				}
			);
		}, { scope: 'email, user_friends, publish_actions', return_scopes: true });
	};
	
	$scope.$on('payment', function (newBalance){
		$scope.summary = distribute.getSummary();
	});
});