'use strict';
angular.module('Dealbee.distribute.friendList', ['ngRoute'])

.controller('FriendListCtrl', function($scope, distribute, facebook, $timeout) {
	var self = this;

	this.initFromFacebook = function (){
		$scope.loadingFriends = true;
		$scope.selectedShown = false;
		$scope.allShown = true;
		$scope.notSelectedShown = false;
		facebook.getFriends(function (err, data){
			if(err){
				$scope.loadingFriends = false;
				$scope.facebookError = true;
				return;
			}

			$scope.allFriends = $scope.allFriends.concat(data);
			_.each($scope.facebookDistribution.friends, function (selectedFriend) {
				var match = _.find($scope.allFriends, function (newFriend){ return !newFriend.selected && newFriend.name === selectedFriend.friend.name });
				if(match) match.selected = true;
			});
			
			if(!$scope.friendsShown){
				$scope.friendsShown = [];
			} else {
				$scope.friendsShown.length = 0;
			}

			$scope.friendsShown = $scope.friendsShown.concat($scope.allFriends.slice());
			$scope.loadingFriends = false;
		});
	};

	$scope.disconnectFb = function (){
		facebook.disconnect();
		$scope.allFriends = [];
	};

	$scope.fbConnect = function (){
		$scope.connectingFacebook = true;
		FB.login(function (loginResponse){
			facebook.processLoginResponse(loginResponse).then(
				function(){
					$scope.connectingFacebook = false;
					self.initFromFacebook();
				},
				function (err){
					$scope.connectingFacebook = false;
					$scope.facebookError = true;
				}
			);
		}, { scope: 'email, user_friends', return_scopes: true });
	};

	$scope.toggleFriend = function (friend){
		friend.selected = !friend.selected;
		if(friend.selected){
			distribute.addFacebookFriend(friend, $scope.fbAccount.id);
		} else {
			distribute.removeFacebookFriend(friend);
		}
	};

	$scope.showAllFriends = function (){
		$scope.friendsShown = $scope.allFriends.slice();
		$scope.selectedShown = false;
		$scope.allShown = true;
		$scope.notSelectedShown = false;
	};

	$scope.showSelectedFriends = function (){
		$scope.friendsShown.length = 0;
		angular.forEach($scope.allFriends, function (friend, key){
			if(friend.selected){
				$scope.friendsShown.push(friend);
			}
		});
		$scope.selectedShown = true;
		$scope.allShown = false;
		$scope.notSelectedShown = false;
	};

	$scope.showNotSelectedFriends = function (){
		$scope.friendsShown.length = 0;
		angular.forEach($scope.allFriends, function (friend, key){
			if(!friend.selected){
				$scope.friendsShown.push(friend);
			}
		});
		$scope.selectedShown = false;
		$scope.allShown = false;
		$scope.notSelectedShown = true;
	};

	$scope.filterFriends = function (){
		$timeout(function (){
			$scope.friendsShown.length = 0;
			angular.forEach($scope.allFriends, function (friend, key){
				if(friend.name.toLowerCase().indexOf($scope.friendFilter.toLowerCase()) != -1){
					$scope.friendsShown.push(friend);
				}
			});
			$scope.selectedShown = false;
			$scope.allShown = false;
			$scope.notSelectedShown = false;
		});
	};

	$scope.facebookDistribution = distribute.facebookDistribution;
	$scope.allFriends = [];

	if($scope.fbAccount){
		self.initFromFacebook();
	}

	$scope.$on('fbConnected',function (){
		self.initFromFacebook();
	});
	
});