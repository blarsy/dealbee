'use strict';

angular.module('Dealbee.distribute.emailList', ['ngRoute'])

.controller('EmailListCtrl', function($scope, distribute, $timeout, restApi, $modal) {
	var self = this,
		addressListModal;

	this.addEmailIfNotDuplicate = function (email){
		return distribute.addEmail(email);
	}

	this.sort = function(a,b){
		if ( a.email.toLowerCase() < b.email.toLowerCase() )
			return -1;
		if ( a.email.toLowerCase() > b.email.toLowerCase() )
			return 1;
		return 0;
	};

	this.setEmailsToInvite = function (emailsList){
		while($scope.emailsToInvite.length > 0) {
			$scope.emailsToInvite.pop();
		}
		$scope.emailsToInvite.push.apply($scope.emailsToInvite, emailsList);
	};

	this.addBlockedEmails = function (blockedEmails){
		if(!$scope.blockedEmails) $scope.blockedEmails = [];
		if(!$scope.blockedEmitterEmails) $scope.blockedEmitterEmails = [];

		_.each(blockedEmails, function (blockedEmail){
			if(blockedEmail.unsubscribedFromEmitter){
				if(!_.find($scope.blockedEmitterEmails, function (blockedEmailSearched){ return blockedEmailSearched.email === blockedEmail.email })){
					$scope.blockedEmitterEmails.push(blockedEmail);
				}
			} else {
				if(!_.find($scope.blockedEmails, function (blockedEmailSearched){ return blockedEmailSearched.email === blockedEmail.email })){
					$scope.blockedEmails.push(blockedEmail);
				}
			}
		});

		$scope.blockedEmails.sort(self.sort);
		$scope.blockedEmitterEmails.sort(self.sort);
	};

	$scope.addEmail = function (){
		$scope.emailSubmittedOnce = true;
		if($scope.emailInviteForm.$valid){
			if(distribute.addEmail($scope.emailToAdd)){
				$scope.emailToAdd = '';
				$scope.emailSubmittedOnce = false;
				self.setEmailsToInvite(distribute.emails.sort(self.sort));
			}
		}
	};

	$scope.removeEmail = function (email){
		distribute.removeEmail(email);
		self.setEmailsToInvite(distribute.emails);
	};

	$scope.pasteAddressList = function (){
		addressListModal = $modal.open({
			templateUrl: 'uploadAddressesModalContent',
			size: 'lg',
			scope: $scope });
	};

	$scope.findEmailAddressesInContent = function (event){
		var pastedContent = event.originalEvent.clipboardData.getData("text/plain");
		if(pastedContent && pastedContent.trim() !== ''){
			$scope.parsingEmails = true;
			$timeout(function (){
				var maillist = new restApi.maillist({
					list: pastedContent
				});

				maillist.$save(function (res){
					$scope.parsingEmails = false;
					$scope.pastedEmails = '';
					distribute.addEmails(res.emails);
					self.setEmailsToInvite(distribute.emails.sort(self.sort));
					self.addBlockedEmails(res.blocked);
					addressListModal.close();
				}, function (res){
					$scope.parsingEmails = false;
					$scope.pastedEmails = '';
					$scope.handleRestApiError('MailListParsingFailed', res);	
					addressListModal.close();
				});
			});
		}
	};

	$scope.emailsToInvite = [];
	if(distribute.emails.length > 0){
		self.setEmailsToInvite(distribute.emails);
	}
});