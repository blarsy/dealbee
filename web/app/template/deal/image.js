'use strict';

angular.module('Dealbee.deal.image', ['ngRoute'])

.controller('DealImageCtrl', function($scope, deal, settings) {
	var self = this,
		parentScope = $scope.$parent.$parent.$parent,
		OOB_PICS_FOLDER = 'oob/';

	parentScope.saveStep = function (){
		$scope.submittedOnce = true;
		$scope.mustProvideImg = false;
		if(!$scope.imageRef){
			$scope.mustProvideImg = true;
			return false;
		}
		deal.imageRef = $scope.imageRef;
		return true;
	};

	$scope.outOfTheBoxImages = [
			{ label: 'dealImageControl.giftImg', public_id: OOB_PICS_FOLDER + 'gift', format: 'jpg' },
			{ label: 'dealImageControl.openingImg', public_id: OOB_PICS_FOLDER + 'opening', format: 'jpg' },
			{ label: 'dealImageControl.discountImg', public_id: OOB_PICS_FOLDER + 'discount', format: 'jpg' },
			{ label: 'dealImageControl.limitedTimeImg', public_id: OOB_PICS_FOLDER + 'limitedtime', format: 'jpg' },
			{ label: 'dealImageControl.loveImg', public_id: OOB_PICS_FOLDER + 'love', format: 'jpg' },
			{ label: 'dealImageControl.xmasImg', public_id: OOB_PICS_FOLDER + 'xmas', format: 'jpg' },
		];

	$scope.openUploadBox = function (){
		cloudinary.openUploadWidget({ 
				upload_preset: settings.uploadImagePreset,
				theme: 'white',
				sources: ['local', 'url'],
				max_file_size: 2000000,
				thumbnail_transformation: { height: 300, crop: 'fit' },
				cropping: 'server',
				cropping_aspect_ratio: 1.91
			}, 
			function(error, result) {
				if(!error){
					$scope.setImage({ public_id: result[0].public_id, format: result[0].format });
					$scope.safeApply();
				}
			}
		);
	};

	$scope.toggleOobImages = function (){
		if(!$scope.bundledImagesCollapsed && $scope.imageRef && /^oob\/.*$/.test($scope.imageRef.public_id)){
			return;
		}
		$scope.bundledImagesCollapsed = !$scope.bundledImagesCollapsed;
	};

	this.unselectOobImage = function (){
		_.each($scope.outOfTheBoxImages, function (image){
			image.active = false;
		})
	};

	$scope.setImage = function (imageRef){
		self.unselectOobImage();
		if(imageRef && /^oob\/.*$/.test(imageRef.public_id)){
			var oobPic = _.find($scope.outOfTheBoxImages, function(pic){ return pic.public_id === imageRef.public_id;  });
			if(oobPic){
				$scope.bundledImagesCollapsed = false;
				oobPic.active = true;
				$scope.oobImageSelected = true;
			} else {
				$scope.bundledImagesCollapsed = true;
				$scope.oobImageSelected = false;
			}
		} else {
			$scope.bundledImagesCollapsed = true;
			$scope.oobImageSelected = false;
		}
		$scope.imageRef = imageRef;
	};

	$scope.setImage(deal.imageRef);
	$scope._id = deal._id;
});