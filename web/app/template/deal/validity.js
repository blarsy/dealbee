'use strict';

angular.module('Dealbee.deal.validity', ['ngRoute'])

.controller('DealValidityCtrl', function($scope, deal, l8n, $timeout) {
	var self = this,
		parentScope = $scope.$parent.$parent.$parent,
		now = new Date();

	parentScope.saveStep = function (){
		$scope.submittedOnce = true;
		if($scope.validityForm.$valid){
			deal.beginDate = $scope.beginDate;
			deal.expiration = $scope.expiration;

			for(var i = $scope.termsOfUse.length - 1; i >= 0 ; i --){
				if($scope.termsOfUse[i].text === ''){
					$scope.termsOfUse.splice(i, 1);
				}
			}


			deal.termsOfUse = $scope.termsOfUse;
			return true;
		}
		return false;
	};

	this.removeEmptyTous = function (){
		var len = $scope.termsOfUse.length;
		for(var i = len-1; i >= 0; i--){
			if(!$scope.termsOfUse[i].text || $scope.termsOfUse[i].text.trim() === ''){
				$scope.termsOfUse.splice(i, 1);
			}
		}
	};
	$scope.openExpirationDtp = function ($event){
	    $event.preventDefault();
	    $event.stopPropagation();

		$scope.expirationDtpOpened = true;
	};

	$scope.openBeginDateDtp = function ($event){
	    $event.preventDefault();
	    $event.stopPropagation();

		$scope.beginDateDtpOpened = true;
	};

	$scope.dateFormat = function(){
		return l8n.allRes.resources.dateFormat[$scope.currentLang];
	};

	$scope.addTermOfUse = function(){
		self.removeEmptyTous();
		$scope.termsOfUse.push({ text: '' });
	};

	$scope.deleteTermOfUse = function(termOfUse){
		var len = $scope.termsOfUse.length;
		for(var i = 0; i < len; i++){
			if($scope.termsOfUse[i].text == termOfUse.text){
				$scope.termsOfUse.splice(i, 1);
				return;
			}
		}
	};

	this.ensureDate = function(dateToEnsure){
		if(!dateToEnsure) return dateToEnsure;
		if(dateToEnsure instanceof Date) return dateToEnsure;
		return new Date(dateToEnsure);
	};

	$scope.calculateOneYearAfter = function (){
		if(!$scope.beginDate){
			$scope.oneYearAfter = now.clone().addYears(1);
		} else {
			$scope.oneYearAfter = $scope.beginDate.clone().addYears(1);
		}
	};

	this.composeDateAndTime = function (date, time){
		if(date){
			return new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes());
		}
	};

	$scope.setBeginTime = function (){
		$scope.beginDate = self.composeDateAndTime($scope.beginDate, $scope.beginTime);
	};

	$scope.setExpirationTime = function (){
		$scope.expiration = self.composeDateAndTime($scope.expiration, $scope.expirationTime);
	};


	$scope.beginDate = self.ensureDate(deal.beginDate) || now.clone().clearTime().addHours(7);
	$scope.expiration = self.ensureDate(deal.expiration) || now.clone().addWeeks(1).clearTime().addHours(20);
	$scope.beginTime = $scope.beginDate.clone();
	$scope.expirationTime = $scope.expiration.clone();
	$scope.today = now.clone().clearTime();

	$timeout(function() {
		$scope.$watch('beginTime', $scope.setBeginTime);
		$scope.$watch('expirationTime', $scope.setExpirationTime);
	});
	$scope.termsOfUse = deal.termsOfUse || [{ text: '' }];
	$scope.calculateOneYearAfter();
});