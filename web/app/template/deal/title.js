'use strict';

angular.module('Dealbee.deal.title', ['ngRoute'])

.controller('DealTitleCtrl', function($scope, deal) {
	var parentScope = $scope.$parent.$parent.$parent;
	parentScope.saveStep = function (){
		$scope.submittedOnce = true;
		if($scope.titleForm.$valid){
			deal.benefit = $scope.benefit;
			deal.teaser = $scope.teaser;
			return true;
		}
		return false;
	};

	$scope.benefit = deal.benefit;
	$scope.teaser = deal.teaser;
	$scope.previewMode = true;
});