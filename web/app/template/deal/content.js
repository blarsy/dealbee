'use strict';

angular.module('Dealbee.deal.content', ['ngRoute'])

.controller('DealContentCtrl', function($scope, deal, l8n, $timeout) {
	var self = this,
		parentScope = $scope.$parent.$parent.$parent,
		currentLangIsoCode = l8n.getCurrentLangIsoCode();

	$scope.mceDescriptionOptions = { menubar : false, statusbar: false, toolbar: 'undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | link', plugins: "link", link_title: false };

	if(currentLangIsoCode !== 'en_US'){
		$scope.mceDescriptionOptions.language = currentLangIsoCode;
	}

	$scope.syncWithDeal = function (){
		$timeout(function (){
			deal.description = $scope.description;
			deal.visibility = $scope.visibility;	
		});
	};

	parentScope.saveStep = function (){
		$scope.submittedOnce = true;
		if($scope.contentForm.$valid){
			deal.description = $scope.description;
			deal.visibility = $scope.visibility;
			return true;
		}
		return false;
	};

	$scope.description = deal.description;
	$scope.visibility = deal.visibility || 'public';
});