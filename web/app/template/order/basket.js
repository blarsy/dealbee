angular.module('Dealbee.basket', ['ngRoute'])
.controller('BasketCtrl', function($scope, order) {
	$scope.packs = order.packs;
	$scope.subscription = order.subscription;

	$scope.getBasketTotal = order.getTotalPrice;
	$scope.removePack = order.removePack;
	$scope.clearSubscription = order.clearSubscription;
});