angular.module('Dealbee.order', ['ngRoute'])
.controller('OrderCtrl', function($scope, restApi, order, $anchorScroll, $location, $timeout, $modal, $rootScope) {
	$scope.loadingPrices = true;
	$scope.paymentFrequency = '1';
	$scope.discount = 1;
	$scope.nbMonthsDiscount = 1;
	var priceRequest = new restApi.price(),
		self = this,
		confirmPaymentModal;
	priceRequest.$get(function (prices){
		$scope.prices = prices;
		$scope.loadingPrices = false;
	});
	
	$scope.addPack = function (type){
		order.addPack(type).then(function (){
			$anchorScroll('basket');
		});
	};
	$scope.setSubscription = function (type){
		order.setSubscription(type, $scope.nbMonthsDiscount).then(function (){
			$anchorScroll('basket');
		});
	};

	$scope.packs = order.packs;
	$scope.subscription = order.subscription;

	$scope.setDiscount = function (months){
		if(months === 6){
			$scope.discount = 1 - ($scope.prices.discounts[0].percentage / 100);
		} else if (months === 12){
			$scope.discount = 1 - ($scope.prices.discounts[1].percentage / 100);
		} else {
			$scope.discount = 1;
		}
		$scope.nbMonthsDiscount = months;
		if(order.subscription.type){
			order.setSubscription(order.subscription.type, months);
		}
	}

	$scope.buy = function (){
		$scope.loadingBuyModal = true;
		$scope.totalPrice = order.getTotalPrice();

		confirmPaymentModal = $modal.open({
			templateUrl: 'confirmPaymentModalContent',
			backdrop: 'static',
			size: 'lg',
			scope: $scope
		});
		$scope.loadingBuyModal = false;
	};

	$scope.confirmPayment = function (){
		$scope.mustProvideInvoicingData = false;
		$scope.mustProvideCreditCardInfo = false;
		$scope.technicalError = false;
		if($scope.account.client.invoicingData && $scope.account.client.invoicingData.companyName){
			if($scope.account.client.hasCard){
				$scope.paying = true;
				order.save().then(function (emitter){
					$rootScope.account = emitter;
					$rootScope.$broadcast('payment');
					$scope.paying = false;
					confirmPaymentModal.close();
					$scope.displaySuccess('paymentSuccessful', 'orderCtrl.paymentSuccessful');	
				}, function (err){
					$scope.paying = false;
					$scope.technicalError = true;
				});
			} else {
				$scope.mustProvideCreditCardInfo = true;
			}
		} else {
			$scope.mustProvideInvoicingData = true;
		}
	};

	$scope.$on('InvoicingDataSaved', function(){
		$scope.mustProvideInvoicingData = false;
	});

	$scope.dismissConfirmPaymentModal = function (){
		confirmPaymentModal.dismiss();
	}
});