'use strict';

angular.module('Dealbee.viewdeal.title', ['ngRoute'])

.controller('ViewdealTitleCtrl', function ($scope, $timeout){
	$timeout(function (){
		$('#benefit').bigtext();
		$('#benefit').trigger('resize');
	});

	$scope.$parent.$parent.$parent.resizeBenefit = function (){
		$timeout(function (){
			$('#benefit').trigger('resize');
		});
	};
});