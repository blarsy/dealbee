'use strict';

angular.module('Dealbee.emitter.login', ['ngRoute'])
.controller('EmitterLoginCtrl', function($scope, facebook, $location, restApi, authentication, $rootScope, errorHandling) {
	var self = this;

	$scope.newSession = new restApi.session();
	$scope.fbAccountUnknown = false;

	this.createSession = function (){
		$scope.newSession.fbToken = $scope.fbAccount.token;
		$scope.newSession.$save(function(httpResponse, headers){
			authentication.createSession(httpResponse, headers);
			$scope.loggingInFacebook = false;
			$rootScope.$broadcast('loggedIn', null);
		}, function(res){
			$scope.loggingInFacebook = false;
			var msg = errorHandling.getErrorMessage(res);
			if(msg === 'Identifier_Not_Found'){
				$scope.fbAccountUnknown = true;
			} else {
				$scope.handleRestApiError('errorLoggingInWithFacebook', res);
			}
		});
	};

	$scope.fbConnect = function (){
		$scope.loggingInFacebook = true;
		if(!$scope.fbAccount){
			FB.login(function (loginResponse){
				facebook.processLoginResponse(loginResponse).then(
					function(){
						self.createSession();
					},
					function (err){
						$scope.loggingInFacebook = false;
					}
				);
			}, { scope: 'email, user_friends', return_scopes: true });
		} else {
			self.createSession();
		}
	};
	$scope.login = function (){
		$scope.loggingInPassword = true;
		$scope.submittedOnce = true;
		$scope.loginFailed = false;
		if($scope.loginForm.$valid){
			authentication.login($scope.newSession.fbToken, $scope.newSession.email, $scope.newSession.password).then(function (){
				$scope.loggingInPassword = false;
				$rootScope.$broadcast('loggedIn', null);
			}, function (res){
				$scope.loggingInPassword = false;
				if(res.status === 500 && res.data && res.data.message && res.data.message === 'LoginFailed'){
					$scope.loginFailed = true;
				} else {
					//log to server log !
					$scope.handleRestApiError('errorLoggingInWithPassword', res);
				}
			});
		} else {
			$scope.loggingInPassword = false;
		}
	};
});