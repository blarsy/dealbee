'use strict';

angular.module('Dealbee.emitter.location', ['ngRoute'])

.controller('EmitterLocationCtrl', function($scope, emitter, $interval, $anchorScroll, $location, uiGmapGoogleMapApi) {
	var self = this,
		parentScope = $scope.$parent.$parent.$parent.subscribeStep ? 
			$scope.$parent.$parent.$parent : $scope.$parent.$parent;

	this.geocoder = {};

	$scope.address = emitter.address || { useMap: true, coords: [ 4.352452, 50.846939 ]  };

	parentScope.subscribeStep('EmitterLocationCtrl', function (){
		$scope.submittedOnce = true;
		if($scope.locationForm.$valid){
			var address = $scope.address;
			if($scope.anyAddressFieldProvided()){
				emitter.address = {
					coords: address.coords,
					useMap: address.useMap,
					country: address.country,
					city: address.city,
					street: address.street,
					number: address.number,
					box: address.box,
					postcode: address.postcode
				}
			} else {
				emitter.address = undefined;
			}
			return true;
		} else {
			$anchorScroll();
		}
		return false;
	});

	this.recordPosition = function(position){
		$scope.address.coords = [ position.lng(), position.lat() ];
		self.displayEmitterAddress();
	};

	this.formatAddressPart = function(addressPart){
		if(addressPart == undefined)
			return '';
		var result = addressPart.trim();
		if(result !== '')
			result += ' ';
		return result;
	};

	this.displayEmitterAddress = function (){
		$scope.map = {
			center: { latitude: $scope.address.coords[1], longitude: $scope.address.coords[0] },
			zoom: 14,
			marker: { 
				location: { latitude: $scope.address.coords[1], longitude: $scope.address.coords[0] }, 
				events: { 
					dragend: function (marker, eventName, args) {
						self.recordPosition(marker.getPosition());
					} 
				},
				options: {
					draggable: true
				},
				id: 1
			}
		};
	};

	$scope.anyAddressFieldProvided = function (){
		if(!$scope.address) return false;
		var address = $scope.address,
			result = ( address.useMap && ( address.coords ) ) || 
			( address.street && address.street.trim() !== '' ) ||
			( address.number && address.number.trim() !== '' ) ||
			( address.box && address.box.trim() !== '' ) ||
			( address.postcode && address.postcode.trim() !== '' ) ||
			( address.city && address.city.trim() !== '' ) ||
			( address.country && address.country.trim() !== '' );
		return result;
	};

	$scope.geocode = function(){
		if($scope.address && $scope.address.useMap &&
			(self.formatAddressPart($scope.address.country) !== '' ||
			self.formatAddressPart($scope.address.city) !== '')){
			var address = self.formatAddressPart($scope.address.street),
				number = self.formatAddressPart($scope.address.number);

			if(number){
				address += ', ' + number;
			}
			address += self.formatAddressPart($scope.address.postcode) +
				self.formatAddressPart($scope.address.city) +
				self.formatAddressPart($scope.address.country);

			if(address !== ''){
				uiGmapGoogleMapApi.then(function (){
					self.geocoder.geocode({ 'address': address }, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							self.recordPosition(results[0].geometry.location);
						}
					});
					$scope.queriedMapsOnce = true;
				});
			}
		}
	};

	$scope.skip = function (){
		$scope.$parent.$parent.$parent.skip();
	};

	$scope.intervalId = $interval(function(){
		if(!$scope.queriedMapsOnce){
			$scope.geocode();
		}
	},2000);

	$scope.$on('destroy', function (){
		$interval.cancel($scope.intervalId);
	});

	uiGmapGoogleMapApi.then(function (){
		self.geocoder = new google.maps.Geocoder();
	});
	if($scope.address.useMap)
		self.displayEmitterAddress();
});