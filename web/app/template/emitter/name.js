'use strict';

angular.module('Dealbee.emitter.name', [])

.controller('EmitterNameCtrl', function($scope, emitter, settings) {
	var imageUploader,
		previewNodeSelector = '#imagePreview',
		parentScope = $scope.$parent.$parent.$parent.subscribeStep ? 
			$scope.$parent.$parent.$parent : $scope.$parent.$parent;
			
	parentScope.subscribeStep('EmitterNameCtrl', function (){
		$scope.submittedOnce = true;
		if($scope.nameForm.$valid){
			emitter.name = $scope.name;
			emitter.website = $scope.website;
			emitter.language = $scope.emitterLanguage;
			emitter.imageRef = $scope.imageRef;
			return true;
		}
		return false;
	});

	$scope.openUploadBox = function (){
		$scope.loadingImageSelector = true;
		cloudinary.openUploadWidget({ 
				upload_preset: settings.uploadImagePreset,
				theme: 'white',
				sources: ['local', 'url'],
				max_file_size: 2000000,
				thumbnail_transformation: { height: 200, crop: 'fit' }
			}, 
			function(error, result) {
				$scope.loadingImageSelector = false;
				$scope.safeApply();
				if(!error){
					$scope.imageRef = { public_id: result[0].public_id, format: result[0].format };
					$scope.safeApply();
				}
			}
		);
	};

	$scope.removeImage = function (){
		delete $scope.imageRef;
	};

	$scope.name = emitter.name;
	$scope.website = emitter.website;
	$scope.emitterLanguage = emitter.language || $scope.currentLang;
	$scope.imageRef = emitter.imageRef;
});