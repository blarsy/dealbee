'use strict';

angular.module('Dealbee.emitter.creditcard', [])

.controller('CreditCardCtrl', function($scope, restApi) {
	var self = this,
		creditcardRequest = new restApi.creditcard();
	$scope.savingCreditCard = false;
	$scope.loadingCreditCard = true;
	$scope.creditCard = {}

	creditcardRequest.$get(function (creditCard){
		$scope.loadingCreditCard = false;
		$scope.creditCard.cardNumber = creditCard.cardNumber;
		$scope.creditCard.expirationMonth = creditCard.expirationMonth;
		$scope.creditCard.expirationYear = creditCard.expirationYear;
	}, function (err){
		$scope.loadingCreditCard = false;
		$scope.handleRestApiError('loadCreditCardFailed', err);
	});

	$scope.saveCreditCard = function (){
		$scope.creditCardSaveFailed = false;
		$scope.submittedOnce = true;
		if($scope.creditCardForm.$valid){
			var creditcard = new restApi.creditcard({
				cardNumber: $scope.cardNumber,
				expirationMonth: $scope.expirationMonth,
				expirationYear: $scope.expirationYear,
				cvc: $scope.cvc
			});
			$scope.savingCreditCard = true;
			creditcard.$save(function (creditCard){
				$scope.creditCard.cardNumber = creditCard.cardNumber;
				$scope.creditCard.expirationMonth = creditCard.expirationMonth;
				$scope.creditCard.expirationYear = creditCard.expirationYear;
				$scope.account.client.hasCard = true;
				$scope.savingCreditCard = false;
			}, function (err){
				$scope.savingCreditCard = false;
				$scope.handleRestApiError('saveCreditCardFailed', err);
			});			
		}
	};

	$scope.handleRestApiError = function(errorCode, err){
		$scope.creditCardSaveFailed = true;
	}

	$scope.changeCard = function (){
		$scope.previousCardNumber = $scope.creditCard.cardNumber;
		$scope.creditCard.cardNumber = null;
	};

	$scope.resetPreviousCard = function (){
		$scope.creditCard.cardNumber = $scope.previousCardNumber;
	};
});