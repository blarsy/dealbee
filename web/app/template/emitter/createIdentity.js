angular.module('Dealbee.emitter.createIdentity', [])

.controller('CreateIdentityCtrl', function($scope, facebook, identity) {
	this.isValid = function (){
		$scope.submittedOnce = true;
		if($scope.byEmail){
			if($scope.identityByMailForm.$valid){
				identity.email = $scope.email;
				identity.password = $scope.password;
				return true;
			}
		} else {
			if($scope.fbAccount && $scope.identityFbForm.$valid){
				identity.email = $scope.emailFb;
				identity.fbToken = $scope.fbAccount.token;
				return true;
			}
		}
	};

	$scope.selectEmail = function (){
		$scope.byEmail = true;
		$scope.byFacebook = false;
		delete identity.fbToken;
	};

	$scope.selectFacebook = function (){
		$scope.byFacebook = true;
		$scope.byEmail = false;
		delete identity.password;
	};

	$scope.fbConnect = function (){
		FB.login(function (loginResponse){
			facebook.processLoginResponse(loginResponse);
		}, { scope: 'email, user_friends', return_scopes: true });
	};

	$scope.disconnectFb = function (){
		facebook.disconnect();
		$scope.identifyWithPwd = true;
	};

	$scope.$on('fbConnected',function (){
		$scope.emailFb = $scope.fbAccount.email;
	});

	if($scope.fbAccount){
		$scope.emailFb = $scope.fbAccount.email;
	};

	identity.linkToController(this);
	$scope.selectFacebook();
});