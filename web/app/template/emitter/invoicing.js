'use strict';

angular.module('Dealbee.emitter.invoicing', ['ngRoute'])

.controller('InvoicingCtrl', function($scope, restApi, $rootScope, $timeout) {
	var self = this;

	this.setInvoicingAddress = function (address){
		$scope.invoicingData.address = {
			street: address.street,
			number: address.number,
			box: address.box,
			postcode: address.postcode,
			city: address.city,
			country: address.country
		};
	};

	$scope.initInvoicingDataFromSession = function (){
		var invoicingData = $scope.account.client.invoicingData;
		$scope.invoicingData = {
			hasNoVATNumber: invoicingData.hasNoVATNumber,
			VATNumber: invoicingData.VATNumber,
			VATCountryCode: invoicingData.VATCountryCode,
			companyName: invoicingData.companyName,
			juridicalForm: invoicingData.juridicalForm,
			language: $scope.currentLang
		};
		self.setInvoicingAddress($scope.account.client.invoicingData.address);
		$scope.editMode = false;
	};

	this.initFromSession = function (){
		if($scope.account.client.invoicingData && $scope.account.client.invoicingData.companyName){
			$scope.initInvoicingDataFromSession();
			$scope.invoiceAddressIsEmitterAddress = false;
		} else {
			$scope.invoicingData = { 
				address : {},
				language: $scope.currentLang
			};
			$scope.invoicingData.companyName = $scope.account.emitter.name;
			if($scope.account.emitter.address && $scope.account.emitter.address.created) {
				$scope.invoiceAddressIsEmitterAddress = true;
				self.setInvoicingAddress($scope.account.emitter.address);
			}
			$scope.editMode = true;
		}
	};
	this.initFromSession();


	$scope.saveInvoicingData = function (){
		$scope.invoicingDataSumittedOnce = true;
		if($scope.invoicingDataForm.$valid){
			//Remove invoicing address completely if it was not filled in
			if(!$scope.invoicingData.address) delete $scope.invoicingData.address;
			$scope.account.client.invoicingData = $scope.invoicingData;
			var invoicingDataToSave = new restApi.invoicingData($scope.invoicingData);
			$scope.savingInvoicingData = true;
			invoicingDataToSave.$save(function (updatedAccount){
				$scope.account = updatedAccount;
				self.initFromSession();
				$scope.savingInvoicingData = false;
				$rootScope.$broadcast('InvoicingDataSaved');
			}, function (err){
				$scope.savingInvoicingData = false;
				$scope.handleRestApiError('failedSavingInvoicingData', err);
			});
		}
	};

	$scope.setEmitterAddress = function (){
		self.setInvoicingAddress($scope.account.emitter.address);
	};
});