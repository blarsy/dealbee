'use strict';

angular.module('Dealbee.emitter.identity', [])

.controller('EmitterIdentityCtrl', function($scope, emitter, facebook, identity) {
	var parentScope = $scope.$parent.$parent.$parent.subscribeStep ? 
		$scope.$parent.$parent.$parent : $scope.$parent.$parent;

	parentScope.subscribeStep('EmitterIdentityCtrl', identity.isValid);
});