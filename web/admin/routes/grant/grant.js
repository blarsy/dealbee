'use strict';

angular.module('DealbeeAdmin.grant', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/grant/:id', {
		templateUrl: 'admin/routes/grant/grant.html',
		controller: 'GrantCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('GrantCtrl', function($scope, restApi, $routeParams) {
	$scope.resend = function (grant){
		restApi.grant.resend({ id: $routeParams.id }, function (res){
			if(res === 'Failed'){
				console.log('Resent failed, refresh the grant to see why.');
			} else {
				console.log('Resent deal successfuly');
			}
		})
	};
	$scope.load = function (){
		$scope.grant = restApi.grant.get({ grantId: $routeParams.id });
	};
	$scope.resendGiftConfirmationEmail = function (giftRequest){
		restApi.grant.resendGiftConfirmation({ giftConfirmationId: giftRequest._id }, function (res){
			$scope.load();
		}, function (res){});
	};

	$scope.load();
});