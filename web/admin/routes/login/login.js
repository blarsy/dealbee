'use strict';

angular.module('DealbeeAdmin.login', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/login', {
		templateUrl: 'admin/routes/login/login.html',
		controller: 'LoginCtrl'
	});
})
.controller('LoginCtrl', function($scope, facebook, restApi, authentication, $location) {
	$scope.login = function (){
		facebook.login(function (fbToken){
			var session = new restApi.session;
			session.fbToken = fbToken;
			session.$admin(function (res, headers){
				authentication.createSession(res, headers);
				$location.path('/home');
			});
		});
	};
});