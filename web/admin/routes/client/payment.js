'use strict';

angular.module('DealbeeAdmin.payment', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/client/:id/payments', {
		templateUrl: 'admin/routes/client/payment.html',
		controller: 'PaymentCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('PaymentCtrl', function($scope, restApi, $routeParams, globalState) {
	var self = this,
		now = new Date();

	this.reload = function(){
		restApi.client.get({ id: $routeParams.id }, function (res){
			$scope.client = res;
			$scope.client.identifiers = restApi.identifier.query({ clientId: res._id });
		});
	}

	$scope.generateInvoice = function (payment){
		var invoiceReq = new restApi.invoice({
			uuid: payment.uuid
		});
		invoiceReq.$save(function (){
			self.reload();
		},function (err){
			payment.invoiceGenError = JSON.stringify(err);
		});
	};

	this.reload();
	$scope.sessionId = globalState.sessionId;
});