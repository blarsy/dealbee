'use strict';

angular.module('DealbeeAdmin.settings', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/settings', {
		templateUrl: 'admin/routes/settings/settings.html',
		controller: 'SettingsCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('SettingsCtrl', function($scope, restApi) {
	$scope.load = function (){
		new restApi.settings().$get(function (settings){
			$scope.nextInvoiceNumber = settings.nextInvoiceNumber;
		});
	};

	$scope.changeSettings = function (){
		var settingsReq = new restApi.settings({ nextInvoiceNumber: $scope.nextInvoiceNumber });
		settingsReq.$save(function(res){
			$scope.load();
		});
	};

	$scope.load();
});