'use strict';

angular.module('DealbeeAdmin.emitter', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/emitter/:id', {
		templateUrl: 'admin/routes/emitter/emitter.html',
		controller: 'EmitterCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('EmitterCtrl', function($scope, restApi, $routeParams, $location) {
	var self = this;

	this.reload = function (){
		$scope.emitter = restApi.emitter.get({ id: $routeParams.id });
	};
	restApi.deal.query({ emitterId: $routeParams.id}, function (res){
		$scope.deals = res;
		_.each($scope.deals, function (deal){
			var counts = _.countBy(deal.grants, function (grant){
				if(!grant.sent){
					return 'toBeSent';
				} else if (!grant.consumed){
					return 'sent';
				} else {
					return 'consumed';	
				}
			});
			deal.toBeSent = counts.toBeSent || 0;
			deal.sent = counts.sent || 0;
			deal.consumed = counts.consumed || 0;
		});
	});
	restApi.session.query({ emitterId: $routeParams.id }, function (res){
		$scope.sessionLogs = res;
	});
	$scope.delete = function (){
		restApi.emitter.delete({ emitterId: $routeParams.id }, function(res){
			$location.path('home');
		});
	};

	this.reload();
	$scope.canDelete = 'false';
});