'use strict';

angular.module('DealbeeAdmin.pack', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/pack/:id', {
		templateUrl: 'admin/routes/emitter/pack.html',
		controller: 'PackCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('PackCtrl', function($scope, restApi, $routeParams) {
	var self = this,
		now = new Date();

	$scope.prices = restApi.price.get();
	$scope.tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);

	$scope.createPack = function (){
		if($scope.newPack){
			var packRequest = new restApi.pack({ emitterId: $routeParams.id, packName: $scope.newPack });
			if($scope.scheduledBegin) packRequest.beginDate = $scope.beginDate;

			packRequest.$save(function (){
				self.reload();
			});
		}
	}

	$scope.switchSubscription = function (){
		if($scope.newSubscription){
			var subscriptionRequest = new restApi.subscription({ emitterId: $routeParams.id, subscriptionName: $scope.newSubscription });

			subscriptionRequest.$save(function (){
				self.reload();
			});
		}
	};

	this.reload = function (){
		$scope.emitter = restApi.emitter.get({ id: $routeParams.id });
	};
	this.reload();
});