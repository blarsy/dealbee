'use strict';

angular.module('DealbeeAdmin.payment', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/payment/:id', {
		templateUrl: 'admin/routes/emitter/payment.html',
		controller: 'PaymentCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('PaymentCtrl', function($scope, restApi, $routeParams, globalState) {
	var self = this,
		now = new Date();

	this.reload = function(){
		$scope.emitter = restApi.emitter.get({ id: $routeParams.id });
	}

	$scope.generateInvoice = function (payment){
		var invoiceReq = new restApi.invoice({
			uuid: payment.uuid
		});
		invoiceReq.$save(function (){
			self.reload();
		},function (err){
			payment.invoiceGenError = JSON.stringify(err);
		});
	};

	this.reload();
	$scope.sessionId = globalState.sessionId;
});