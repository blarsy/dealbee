'use strict';

angular.module('DealbeeAdmin.home', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl: 'admin/routes/home/home.html',
		controller: 'HomeCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('HomeCtrl', function($scope, restApi) {
	var self = this;
	this.refreshFromApp = function (app){
		$scope.inMaintenance = app.inMaintenance;
		$scope.jobsRunning = app.jobsRunning;
	};
	$scope.queryEmitters = function (){
		$scope.emitters = restApi.emitter.query({ query: $scope.query });
	};
	$scope.refreshStatus = function (){
		$scope.refreshingStatus = true;
		restApi.app.get({}, function (res){
			self.refreshFromApp(res);
			$scope.refreshingStatus = false;
		}, function (res){
			$scope.refreshStatus = false;
		});		
	};
	$scope.toggleMaintenance = function(){
		var app = new restApi.app({ inMaintenance: !$scope.inMaintenance, jobsRunning: $scope.jobsRunning });
		app.$save(function (res){
			self.refreshFromApp(res);
		});
	};
	$scope.toggleJobsRunning = function(){
		var app = new restApi.app({ inMaintenance: $scope.inMaintenance, jobsRunning: !$scope.jobsRunning });
		app.$save(function (res){
			self.refreshFromApp(res);
		});
	};
	$scope.query = '';
	$scope.queryEmitters();
	$scope.refreshStatus();
});