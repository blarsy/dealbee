'use strict';

angular.module('DealbeeAdmin.identifier', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/identifier/:id', {
		templateUrl: 'admin/routes/identifier/identifier.html',
		controller: 'IdentifierCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('IdentifierCtrl', function($scope, restApi, $routeParams, $location) {
	var self = this;

	this.reload = function (){
		restApi.identifier.get({ id: $routeParams.id }, function (res) {
			$scope.identifier = res;
			if(res.defaultEmitter)
				$scope.defaultEmitter = restApi.emitter.get({ id: res.defaultEmitter });
			if(res.lastLoggedInEmitter)
				$scope.lastLoggedInEmitter = restApi.emitter.get({ id: res.lastLoggedInEmitter });

			$scope.client = restApi.client.get({ id: res.client });
		});
	};

	$scope.delete = function (){
	};

	this.reload();
});