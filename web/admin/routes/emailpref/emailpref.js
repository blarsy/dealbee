'use strict';

angular.module('DealbeeAdmin.emailpref', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/emailpref', {
		templateUrl: 'admin/routes/emailpref/emailpref.html',
		controller: 'EmailprefCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('EmailprefCtrl', function($scope, restApi) {
	var self = this;
	$scope.queryEmailPref = function(){
		restApi.preference.query({ email: $scope.email }, function (res){
			$scope.preferences = res;
		});
	};
});