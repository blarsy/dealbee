'use strict';

angular.module('DealbeeAdmin.session', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/session/:id', {
		templateUrl: 'admin/routes/session/session.html',
		controller: 'SessionCtrl'
	});
})
.controller('SessionCtrl', function($scope, $routeParams, restApi) {
	$scope.session = restApi.sessionLog.get({ sessionLogId: $routeParams.id });
});