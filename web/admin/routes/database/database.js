'use strict';

angular.module('DealbeeAdmin.database', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/database', {
		templateUrl: 'admin/routes/database/database.html',
		controller: 'DatabaseCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('DatabaseCtrl', function($scope, restApi) {
	$scope.load = function (){
		restApi.backups.query({}, function (res){
			$scope.backups = res;
		});
	};

	$scope.deleteBackupFile = function (backupFile){
		restApi.backups.delete({ fileName: backupFile.fileName }, function (){
			$scope.load();
		});
	};

	$scope.takeBackup = function (){
		var backup = new restApi.backups();
		backup.$save(function (){
			$scope.load();
		});
	};

	$scope.fullBackup = function (){
		var backups = new restApi.backups();
		backups.$fullBackup({});
	}
	
	$scope.load();
});