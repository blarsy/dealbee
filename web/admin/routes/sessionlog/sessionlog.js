'use strict';

angular.module('DealbeeAdmin.sessionLog', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/sessionlog', {
		templateUrl: 'admin/routes/sessionlog/sessionlog.html',
		controller: 'SessionLogCtrl'
	});
})
.controller('SessionLogCtrl', function($scope, restApi) {
	$scope.sessions = restApi.sessionLog.query({});
});