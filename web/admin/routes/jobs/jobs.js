'use strict';

angular.module('DealbeeAdmin.jobs', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/jobs', {
		templateUrl: 'admin/routes/jobs/jobs.html',
		controller: 'JobsCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('JobsCtrl', function($scope, restApi) {
	$scope.jobs = restApi.jobs.query({});
	$scope.columns = [{name: 'name'}, {name: 'repeatInterval'}, {name: 'nextRunAt'}, {name: 'lockedAt'}, {name: 'lastRunAt'}, {name: 'lastFinishedAt'}];

	$scope.renewSubscriptions = function (){
		restApi.jobs.renewSubscriptions();
	};
});