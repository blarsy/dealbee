'use strict';

angular.module('DealbeeAdmin.deal', ['ngRoute'])

.config(function($routeProvider) {
	$routeProvider.when('/deal/:id', {
		templateUrl: 'admin/routes/deal/deal.html',
		controller: 'DealCtrl',
		resolve: {
			authorize: function(authentication) {
				return authentication.isLoggedIn();
			}
		}
	});
})
.controller('DealCtrl', function($scope, restApi, $routeParams) {
	$scope.deal = restApi.deal.get({ id: $routeParams.id }, function (res){
		$scope.deal.grants = restApi.grant.query({ dealId: $routeParams.id });
	});
	$scope.togglePrivate = function (){
		restApi.deal.togglePrivate({ id: $routeParams.id }, function (deal){
			$scope.deal = deal;
		});
	};
});