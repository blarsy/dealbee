angular.module('DealbeeAdmin.interceptors', [])
.factory('sessionInjector', function(globalState){
	var sessionInjector = {
		request: function(config) {
			if (config.url.indexOf('/api/') !== -1 && globalState.sessionId){
				if(config.method.toLowerCase() !== 'post'){
					config.params = config.params || {};
					config.params.sessionId = globalState.sessionId;
				} else {
					config.data = config.data || {};
					config.data.sessionId = globalState.sessionId;
				}
			}
			return config;
		}
	};
	return sessionInjector;
})
.config(function($httpProvider) {
    $httpProvider.interceptors.push('sessionInjector');
});
