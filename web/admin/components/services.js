angular.module('DealbeeAdmin.services', [])
	.factory('settings', function (){
		return {
			webApi: 'http://localhost:8081/api/',
			fbAppId: '1523173214607128',
			environment: 'Local'
		};
	})
	.factory('restApi', function ($resource, settings){
		this.session = $resource(settings.webApi + 'session', {}, {
			'admin': { method: 'POST', url: settings.webApi + 'session/admin'}
		});
		this.sessionLog = $resource(settings.webApi + 'sessionLog');
		this.emitter = $resource(settings.webApi + 'emitter');
		this.deal = $resource(settings.webApi + 'deal', {}, {
			'togglePrivate': { method: 'POST', url: settings.webApi + 'deal/togglePrivate'}
		});
		this.grant = $resource(settings.webApi + 'grant', {}, {
			'resend': { method: 'POST', url: settings.webApi + 'grant/resend' },
			'resendGiftConfirmation': { method: 'POST', url: settings.webApi + 'grant/resendGiftConfirmation' }
		});
		this.pack = $resource(settings.webApi + 'pack');
		this.subscription = $resource(settings.webApi + 'subscription');
		this.price = $resource(settings.webApi + 'price');
		this.app = $resource(settings.webApi + 'app');
		this.preference = $resource(settings.webApi + 'preference');
		this.jobs = $resource(settings.webApi + 'job', {}, {
			'renewSubscriptions': { method: 'POST', url: settings.webApi + 'job/renewSubscriptions' }
		});
		this.backups = $resource(settings.webApi + 'backup', {}, {
			'fullBackup': { method: 'POST', url: settings.webApi + 'backup/full' }
		});
		this.invoice = $resource(settings.webApi + 'invoice');
		this.settings = $resource(settings.webApi + 'settings');
		this.identifier = $resource(settings.webApi + 'identifier');
		this.client = $resource(settings.webApi + 'client');
		return this;
	})
	.factory('globalState', function(){
		this.sessionId = null;
		return this;
	})
	.factory('authentication', function ($rootScope, $q, $location, $cookieStore, restApi, globalState){
		var self = this;
		this.isLoggedIn = function(){
			var deferred = $q.defer();
			if($rootScope.account){
				deferred.resolve(globalState.sessionId);
			} else {
				var sessionIdToRestore = $cookieStore.get('sessionId');
				if(!sessionIdToRestore){
					deferred.reject();
				} else {
					var session = new restApi.session();
					session.$get({ id: sessionIdToRestore }, function(res, headers){
						self.createSession(res, headers);
						deferred.resolve(headers('Session-Id'));
					}, function(res){
						self.killSession();
						deferred.reject();
					});
				}
			}
			return deferred.promise;
		};
		this.createSession = function(sessionData, headers){
			$rootScope.account = sessionData;
			globalState.sessionId = headers('Session-Id');
			$cookieStore.put('sessionId', headers('Session-Id'));
		};
		this.killSession = function(){
			$rootScope.account = undefined;
			globalState.sessionId = null;
			$cookieStore.remove('sessionId');
			$location.path('/login')
		};
		return this;
	})
	.factory('facebook', function($q, $injector, settings){

		FB.init({
			appId: settings.fbAppId,
			xfbml: true,
			version: 'v2.1'
		});			

		this.login = function (callback){
			FB.getLoginStatus(function (response){
				if (response.status === 'connected'){
					callback(response.authResponse.accessToken);
				} else {
					FB.login(function (loginResponse){
						callback(loginResponse.authResponse.accessToken);
					}, { scope: 'email, user_friends, publish_actions' });
				}
			});
		}

		this.disconnect = function (){
			FB.logout(function (response){
				var rootScope = $injector.get('$rootScope');
				rootScope.fbAccount = undefined;
				rootScope.$apply();
			});
		}

		return this;
	});
;