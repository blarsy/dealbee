'use strict';

// Declare app level module which depends on views, and components
angular.module('DealbeeAdmin', [
  'ngRoute',
  'ngResource',
  'ngCookies',
  'DealbeeAdmin.home',
  'DealbeeAdmin.login',
  'DealbeeAdmin.emitter',
  'DealbeeAdmin.pack',
  'DealbeeAdmin.payment',
  'DealbeeAdmin.deal',
  'DealbeeAdmin.grant',
  'DealbeeAdmin.emailpref',
  'DealbeeAdmin.jobs',
  'DealbeeAdmin.services',
  'DealbeeAdmin.session',
  'DealbeeAdmin.database',
  'DealbeeAdmin.interceptors',
  'DealbeeAdmin.sessionLog',
  'DealbeeAdmin.settings',
  'DealbeeAdmin.identifier',
  'ui.bootstrap.datepicker',
  'cloudinary'
])
.config(['$routeProvider', function($routeProvider) {
	$routeProvider.otherwise({redirectTo: '/login'});
}])
.run(function (){
  $.cloudinary.config('cloud_name', 'dealbee');
});
