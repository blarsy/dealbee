module.exports = function(){
	var smallPackPrice = 14.9,
		models = require(process.cwd() + '/api/models'),
		defaultStripeCustomerId,
		_ = require('underscore');
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^I requested to buy a pack$/, function (done){
		element(by.id('emitterMenuHead')).click();
		element(by.css('[data-dlb-localize="topMenu.credits"]')).click();
		element(by.css('[data-dlb-localize="load.buyButton"]')).click();
		element(by.css('table.price-table td:first-child button[data-dlb-localize="orderCtrl.buyPackButton"]')).click();
		element(by.css('[data-dlb-localize="orderCtrl.buyButton"]')).click();
		done();
	});

	this.Given(/^I have valid invoicing data$/, function (done){
		this.setDefaultInvoicingData(this.minimalisticEmitter.email, done);
	});

	this.Given(/^I have valid credit card info$/, function (done){
		models.identifier.findOne({ email: this.minimalisticEmitter.email }).populate('client').exec(function (err, identifier){
			if(err) return done(err);

			var client = identifier.client;

			client.stripeCustomerId = defaultStripeCustomerId;

			client.save(done);
		});
	});

	this.When(/^I request to buy a pack$/, function (done){
		element(by.id('emitterMenuHead')).click();
		element(by.css('[data-dlb-localize="topMenu.credits"]')).click();
		element(by.css('[data-dlb-localize="load.buyButton"]')).click();
		element(by.css('table.price-table td:first-child button[data-dlb-localize="orderCtrl.buyPackButton"]')).click();
		element(by.css('[data-dlb-localize="orderCtrl.buyButton"]')).click().then(done, done);
	});

	this.When(/^I confirm the payment$/, function (done){
		element(by.css('[data-dlb-localize="orderCtrl.confirmPaymentButton"]')).click().then(done, done);
	});

	this.Then(/^I see the pack price$/, function (done){
		this.expect(element(by.css('[data-ng-bind="totalPrice | dlbPrice"]')).getText())
			.to.eventually.satisfy(function (priceAsText){ return new Number(priceAsText) == smallPackPrice }).and.notify(done);
	});

	this.Then(/^invoicing data is required$/, function (done){
		this.expect(element(by.css('[data-dlb-localize="orderCtrl.mustProvideInvoicingData"]')).isDisplayed())
			.to.eventually.be.true.and.notify(done);
	});

	this.Then(/^credit card info is required$/, function (done){
		this.expect(element(by.css('[data-dlb-localize="orderCtrl.mustProvideCreditCardInfo"]')).isDisplayed())
			.to.eventually.be.true.and.notify(done);
	});

	this.Then(/^I am prompted for my invoicing data$/, function (done){
		this.expect(element(by.id('invoicingData')).isDisplayed())
			.to.eventually.be.true.and.notify(done);
	});

	this.Then(/^I am prompted for my credit card info$/, function (done){
		this.expect(element(by.css('form[name="creditCardForm"]')).isDisplayed())
			.to.eventually.be.true.and.notify(done);
	});
	this.Then(/^I am not prompted for my invoicing data$/, function (done){
		this.expect(element(by.id('invoicingData')).isPresent())
			.to.eventually.be.false.and.notify(done);
	});

	this.Then(/^I am not prompted for my credit card info$/, function (done){
		this.expect(element(by.css('form[name="creditCardForm"]')).isDisplayed())
			.to.eventually.be.false.and.notify(done);
	});

	this.Then(/^the pack is added to my profile$/, function (done){
		var world = this;
		setTimeout(function (){
			models.identifier.findOne({ email: world.minimalisticEmitter.email }).populate('client').exec(function (err, identifier){
				if(err) return done(err);

				try{
					world.expect(_.filter(identifier.client.packs, function (pack){ return pack.amount; }).length).to.equal(1);
					done();
				}
				catch(ex){
					done(ex);
				}
			});
		}, 200);
	});
};