module.exports = function(){
	var async = require('async'),
		companyWithoutVAT = {
			companyName: 'Company without VAT',
			juridicalForm: 'SARL',
			hasNoVATNumber: true,
			address:{
				street: 'rue jolie',
				number: '12',
				box: 'B',
				postcode: '7800',
				city: 'Belleville',
				country: 'Loin'
			}
		},
		companyWithVAT = {
			companyName: 'Company with VAT',
			juridicalForm: 'INC',
			VATCountryCode: 'FR',
			VATNumber: '0830123456'
		},
		models = require(process.cwd() + '/api/models');
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^An emitter with just a name$/, function (done){
		this.createEmitter(this.minimalisticEmitter, done);
	});

	this.Given(/^I am logged in as the emitter with just a name$/, function (done){
		this.login(this.minimalisticEmitter, done);
	});

	this.Given(/^I have an emitter address$/, function (done){
		this.setDefaultAddress(this.minimalisticEmitter, done);
	});

	this.Given(/^I remove the prefilled company name$/, function (done){
		 element(by.id('inputCompanyName')).clear();
		 done();
	});

	this.Given(/^I provide company info and vat info$/, function (done){
		element(by.id('inputCompanyName')).clear();
		element(by.id('inputCompanyName')).sendKeys(companyWithVAT.companyName);
		element(by.id('inputJuridicalForm')).sendKeys(companyWithVAT.juridicalForm);
		element(by.id('inputVATCountry')).sendKeys(companyWithVAT.VATCountryCode);
		element(by.id('inputVATNumber')).sendKeys(companyWithVAT.VATNumber);
		done();
	});

	this.Given(/^I provide my company info, having no vat$/, function (done){
		element(by.id('inputCompanyName')).clear();
		element(by.id('inputCompanyName')).sendKeys(companyWithoutVAT.companyName);
		element(by.id('inputJuridicalForm')).sendKeys(companyWithoutVAT.juridicalForm);
		this.fillInAddress(companyWithoutVAT.address);
		element(by.id('inputHasNoVATNumber')).click();
		done();
	});

	this.When(/^I save the invoicing data$/, function (done){
		element(by.css('[data-dlb-localize="load.saveInvoicingDataButton"]')).click().then(done, done);
	});

	this.Then(/^the invoicing data is saved with the same address as my emitter address$/, function (done){
		var world = this;
		setTimeout(function (){
			models.identifier.findOne({ email: world.minimalisticEmitter.email }, function (err, identifier){
				if(err) return done(err);

				models.client.findOne({ _id: identifier.client }).lean().exec(function (err, client){
					if(err) return done(err);

					try{
						var data = client.invoicingData;
						console.log(JSON.stringify(client));
						world.expect(data.companyName).to.equal(companyWithVAT.companyName);
						world.expect(data.juridicalForm).to.equal(companyWithVAT.juridicalForm);
						world.expect(data.hasNoVATNumber).to.not.be.ok;
						world.expect(data.VATCountryCode).to.equal(companyWithVAT.VATCountryCode);
						world.expect(data.VATNumber).to.equal(companyWithVAT.VATNumber);
						world.expect(data.address.street).to.equal(world.defaultAddress.street);
						world.expect(data.address.number).to.equal(world.defaultAddress.number);
						world.expect(data.address.box).to.equal(world.defaultAddress.box);
						world.expect(data.address.postcode).to.equal(world.defaultAddress.postcode);
						world.expect(data.address.city).to.equal(world.defaultAddress.city);
						world.expect(data.address.country).to.equal(world.defaultAddress.country);
						done();
					}
					catch(ex){
						return done(ex);
					}
				});
			});
		}, 200);
	});

	this.Then(/^I see the invoicing data fields are required$/, function (done){
		var world = this;
		async.parallel([
			function (callback){
				world.expect(element(by.css('[data-dlb-localize="InvoicingCtrl.companyNameRequired"]')).isDisplayed())
					.to.eventually.be.true.and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('[data-dlb-localize="emitterLocationControl.streetRequired"]')).isDisplayed())
					.to.eventually.be.true.and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('[data-dlb-localize="emitterLocationControl.postcodeRequired"]')).isDisplayed())
					.to.eventually.be.true.and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('[data-dlb-localize="emitterLocationControl.cityRequired"]')).isDisplayed())
					.to.eventually.be.true.and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('[data-dlb-localize="InvoicingCtrl.VATCountryRequired"]')).isDisplayed())
					.to.eventually.be.true.and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('[data-dlb-localize="InvoicingCtrl.VATNumberRequired"]')).isDisplayed())
					.to.eventually.be.true.and.notify(callback);
			}
		], done);
	});

	this.Then(/^the invoicing data of the company without VAT is saved$/, function (done){
		var world = this;
		setTimeout(function (){
			models.identifier.findOne({ email: world.minimalisticEmitter.email }, function (err, identifier){
				if(err) return done(err);

				models.client.findOne({ _id: identifier.client }, function (err, client){
					if(err) return done(err);

					try{
						var data = client.invoicingData;
						console.log(JSON.stringify(client));
						world.expect(data.companyName).to.equal(companyWithoutVAT.companyName);
						world.expect(data.juridicalForm).to.equal(companyWithoutVAT.juridicalForm);
						world.expect(data.hasNoVATNumber).to.equal(true);
						world.expect(data.address.street).to.equal(companyWithoutVAT.address.street);
						world.expect(data.address.number).to.equal(companyWithoutVAT.address.number);
						world.expect(data.address.box).to.equal(companyWithoutVAT.address.box);
						world.expect(data.address.postcode).to.equal(companyWithoutVAT.address.postcode);
						world.expect(data.address.city).to.equal(companyWithoutVAT.address.city);
						world.expect(data.address.country).to.equal(companyWithoutVAT.address.country);
						done();
					}
					catch(ex){
						return done(ex);
					}
				});
			});			
		}, 200);
	});

	this.Then(/^the emitter name and emitter address are prefilled as invoicing data$/, function (done){
		var world = this;
		async.parallel([
			function (callback){
				world.expect(element(by.id('inputCompanyName')).getAttribute('value'))
					.to.eventually.equal(world.minimalisticEmitter.name).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputJuridicalForm')).getAttribute('value'))
					.to.eventually.equal('').and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputStreet')).getAttribute('value'))
					.to.eventually.equal(world.defaultAddress.street).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputNumber')).getAttribute('value'))
					.to.eventually.equal(world.defaultAddress.number).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputBox')).getAttribute('value'))
					.to.eventually.equal(world.defaultAddress.box).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputPostcode')).getAttribute('value'))
					.to.eventually.equal(world.defaultAddress.postcode.toString()).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputCity')).getAttribute('value'))
					.to.eventually.equal(world.defaultAddress.city).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputCountry')).getAttribute('value'))
					.to.eventually.equal(world.defaultAddress.country).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputVATCountry')).getAttribute('value'))
					.to.eventually.equal('').and.notify(callback);
			},
			function (callback){
				world.expect(element(by.id('inputVATNumber')).getAttribute('value'))
					.to.eventually.equal('').and.notify(callback);
			},
		], done);
	});

	this.Then(/^I cannot set the invoicing address as the emitter address$/, function (done){
		this.expect(element(by.id('useEmitterAddressButton')).isDisplayed())
			.to.eventually.be.false.and.notify(done);
	});

};