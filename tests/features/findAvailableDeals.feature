@Active
Feature: Find available deals
	As a customer
	I want to view available deals
	So that I claim some deals

Background:
	Given some deals on request

Scenario: View available deals
	When I request the find page
	Then I see the available deals