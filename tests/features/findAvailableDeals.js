module.exports = function(){
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^some deals on request$/, function (done){
		var world = this;
		this.createDefaultEmitterAndSession(function (err, sessionId){
			if(err) return done(err);

			world.createDealWithOnRequest({
				benefit: '20%',
				teaser: 'on your next meal',
				description: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...',		
				beginDate: new Date(),
				expiration: new Date(new Date().valueOf() + (1000 * 60 * 60 * 24 * 3)),
				visibility: 'public',
				imageRef: {}, //just give it a non-null value, we won't test further anyway
				termsOfUse: []
			}, sessionId, function (){
				world.createDealWithOnRequest({
					benefit: '40%',
					teaser: 'on your next meal',
					description: 'quia dolor sit amet, consectetur, adipisci velit...',		
					beginDate: new Date(),
					expiration: new Date(new Date().valueOf() + (1000 * 60 * 60 * 24 * 3)),
					visibility: 'public',
					imageRef: {}, //just give it a non-null value, we won't test further anyway
					termsOfUse: []
				}, sessionId, done);
			});
		});
	});

	this.Then(/^I see the available deals$/, function (done){
		var world = this;
		element.all(by.css('#findDeals > div')).then(
			function (elements){
				try{
					world.expect(elements.length).to.be.above(2);
					done();
				}
				catch(ex){
					return done(ex);
				}
			}, done);
	});
};