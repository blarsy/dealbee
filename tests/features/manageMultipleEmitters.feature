@Active
Feature: Subscribe
	As a deal creation specialist
	I want to subscribe multiple emitters
	So that I can send unique deals to the clients of these emitters

Scenario: Cannot create an emitter with same email as existing profile
	Given I manage an existing emitter
	And I am on the subscription page
	And I enter a valid emitter business name
	And I request the next step
	And I enter a valid emitter postcode
	And I enter a valid emitter street
	And I enter a valid emitter city
	And the address has been geolocated
	And I request the next step
	And I choose to use an email identity
	And I enter my email
	And I enter my password
	And I enter my repeated password
	When I request to finish the operation
	Then I see I must login before creating other companies

Scenario: Create an emitter when logged in
	Given I manage an existing emitter
	And I am logged in on my existing emitter account
	And I request the create emitter page
	When I create a second emitter
	Then I am taken to the new emitter home page
	And the new emitter exists
	And existing emitter is default emitter
	And I can switch between both emitters

Scenario: Switch between managed emitters
	Given I manage 2 emitters
	And I am logged in as emitter 1
	When I switch to the emitter 2
	Then I see the home page of emitter 2