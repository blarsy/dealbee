module.exports = function(){
	var self = this;
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^I am on the (.*) page$/, function (pageName, done){
		var isSimpleUrl = true,
			world = this;
		switch(pageName){
			case 'subscription':
				urlExcerpt = 'subscribe';
				break;
			case 'splash':
				urlExcerpt = 'splash';
				break;
			case 'create deal':
				urlExcerpt = 'deal';
				break;
			case 'deal distribution':
				isSimpleUrl = false;
				browser.get(world.webUrl + 'deal/' + this.lastCreatedDealId.toString() + '/distribute');
				done();
				break;
			case 'credits-payments':
				urlExcerpt = 'load';
				break;
			default:
				throw 'unexpected page name ' + pageName;
		}
		if(isSimpleUrl){
			browser.get(this.webUrl + urlExcerpt);
			done();
		}
	});

	this.When(/^I request the (.*) page$/, function (pageName, done){
		var selector,
			world = this;
		switch(pageName){
			case 'subscription':
				selector = by.id('subscribeButton');
				break;
			case 'create emitter':
				browser.get(world.webUrl + 'subscribe').then(done, done);
				return;
			case 'create deal':
				selector = by.id('createDealButton');
				break;
			case 'deal distribution':
				element(by.css('#dealsToDistribute button.dropdown-toggle')).click();
				selector = by.css('a[data-ng-click="navigateToDistributeDeal(deal)"]');
				break;
			case 'find':
				browser.get(world.webUrl + 'find').then(done, done);
				return;
			case 'credits-payments':
				element(by.css('#emitterMenuHead')).click();
				selector = by.css('a[href="/load"]');
				break;
			default:
				throw 'unexpected page name ' + pageName;
		}
		element(selector).click().then(done, done);
	});

	this.When(/^I request the subscription page from the top menu$/, function (done){
		element(by.id('subscribeButtonTop')).click().then(done, done);
	});

	this.Then(/^I see the (.*) page$/, function (pageName, done){
		var urlExcerpt;
		switch(pageName){
			case 'subscription':
				urlExcerpt = '/subscribe/1';
				break;
			case 'home':
				urlExcerpt = '/home/';
				break;
			case 'create deal':
				urlExcerpt = '/deal/';
				break;
			case 'deal distribution':
				urlExcerpt = '/distribute/';
				break;
			default:
				throw 'unexpected page name ' + pageName;
		}
		this.waitForPage(urlExcerpt, done);
	});

	this.Then(/^I am connected$/, function (done){
		this.expect(element(by.css('#emitterMenuHead span:first-child')).getText()).to.eventually.equal(this.defaultEmitter.email).and.notify(done);
	});

	this.Given(/^I enter a valid emitter (.*)$/, function (fieldName, done){
		this.enterValidEmitterField(fieldName, this.defaultEmitter).then(done, done);
	});

	this.When(/^I enter a valid emitter (.*)$/, function (fieldName, done){
		this.enterValidEmitterField(fieldName, this.defaultEmitter).then(done, done);
	});

	this.Given(/^I enter a valid deal (.*)$/, function (fieldName, done){
		if(fieldName !== 'description'){
			this.enterValidDealField(fieldName, this.defaultDeal, this);
			done();
		} else {
			//I really can't find a way to 'register' anything that comes out of browser.wait :-(
			this.enterValidDealField(fieldName, this.defaultDeal, this).then(done, done);
		}
	});

	this.When(/^I enter a valid deal (.*)$/, function (fieldName, done){
		this.enterValidDealField(fieldName, this.defaultDeal, this).then(done, done);
	});

	this.Then(/^I see that (.*) is required$/, function (fieldRequired, done){
		var validationMsgCode;
		switch(fieldRequired){
			case 'business name':
				validationMsgCode = 'emitterNameControl.nameRequired';
				break;
			case 'a city':
				validationMsgCode = 'emitterLocationControl.cityRequired';
				break;
			case 'a postcode':
				validationMsgCode = 'emitterLocationControl.postcodeRequired';
				break;
			case 'a street':
				validationMsgCode = 'emitterLocationControl.streetRequired';
				break;
			case 'a password':
				validationMsgCode = 'passwordRequired';
				break;
			case 'a repeated password':
				validationMsgCode = 'repeatPasswordRequired';
				break;
			case 'an email':
				validationMsgCode = 'emitterIdentityControl.emailRequired';
				break;
			case 'a benefit':
				validationMsgCode = 'dealTitleControl.benefitRequired';
				break;
			case 'a teaser':
				validationMsgCode = 'dealTitleControl.teaserRequired';
				break;
			case 'an image':
				validationMsgCode = 'dealImageControl.imageRequired'
				break;
			case 'a begin date':
				validationMsgCode = 'dealValidityControl.beginDateRequired'
				break;
			case 'an expiration date':
				validationMsgCode = 'dealValidityControl.expirationRequired'
				break;
			case 'a description':
				validationMsgCode = 'dealContentControl.descriptionRequired'
				break;
			case 'executing all distribution summary actions':
				validationMsgCode = 'DistributeSummaryCtrl.summaryInvalid';
				break;
			default:
				throw 'unexpected field name ' + fieldRequired;
		}
		this.expect(element(by.css('[data-dlb-localize="' + validationMsgCode + '"]')).isDisplayed()).to.eventually.be.true.and.notify(done);
	});

};