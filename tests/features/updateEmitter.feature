@Active
Feature: Update emitter info
	As a merchant
	I want to update my info
	So that my deals show up-to-date info

Scenario: Update emitter having minimal info
	Given An emitter with just a name
	And I am logged in as the emitter with just a name
	When I fill in all emitter info
	Then my emitter data has all emitter info

Scenario: Update emitter having full info
	Given An emitter with full info
	And I am logged in as the emitter having full info
	When I fill in some other full emitter info
	Then my emitter data has all other emitter info

Scenario: Update emitter having full info to minimum emitter info
	Given An emitter with full info
	And I am logged in as the emitter having full info
	When I fill minimalistic emitter info
	Then my emitter data has minimal emitter info