@Active
Feature: Distribute deal
	As a merchant
	I want to distribute a deal
	So that my clients can benefit from it

Background:
	Given a valid deal

Scenario: Navigate to distribute deal
	When I request the deal distribution page
	Then I see the deal distribution page

Scenario: Missing distribution contact
	Given I am on the deal distribution page
	When I request a distribution summary
	Then I see that some contacts are required

Scenario: Ask for facebook login
	Given I am on the deal distribution page
	When I request the Facebook distribution interface
	Then I am requested to connect on Facebook

Scenario: See Facebook friends
	Given I am connected on Facebook
	And I have some Facebook friends
	And I am on the deal distribution page
	When I request the Facebook distribution interface
	Then I see my Facebook friends

Scenario: Distribute summary asks for publish permissions on Facebook
	Given I am connected on Facebook
	And I have some Facebook friends
	And I am on the deal distribution page
	And I request the Facebook distribution interface
	And I selected a Facebook friend
	When I request a distribution summary
	Then I am requested publish permissions on Facebook

Scenario: Distribute summary disallow distributing on Facebook without publish permission
	Given I am connected on Facebook
	And I have some Facebook friends
	And I am on the deal distribution page
	And I request the Facebook distribution interface
	And I selected a Facebook friend
	And I request a distribution summary
	When I request a distribution
	Then I see that executing all distribution summary actions is required

Scenario: Distribute summary warns about insufficient funds
	Given I am connected on Facebook
	And I have some Facebook friends
	And I have 2 credits left
	And I am on the deal distribution page
	And I request the Facebook distribution interface
	And I selected a Facebook friend
	And I provided an email contact
	And I set 1 deal available on request
	When I request a distribution summary
	Then I am requested to buy credits

Scenario: Distribute summary disallow distributing when insufficient funds
	Given I am connected on Facebook
	And I have some Facebook friends
	And I have 2 credits left
	And I am on the deal distribution page
	And I request the Facebook distribution interface
	And I selected a Facebook friend
	And I provided an email contact
	And I set 1 deal available on request
	And I request a distribution summary
	When I request a distribution
	Then I see that executing all distribution summary actions is required

Scenario: Distribute
	Given I am connected on Facebook
	And I have some Facebook friends
	And I granted Dealbee publish rights on Facebook
	And I am on the deal distribution page
	And I provided an email contact
	And I pasted a list of emails
	And I selected some Facebook friends
	And I request a distribution summary
	And I allow publishing on Facebook
	When I request a distribution
	Then the grants were created

Scenario: Distribute deals on request
	Given I am connected on Facebook
	And I am on the deal distribution page
	And I set 10 deal available on request
	And I request a distribution summary
	When I request a distribution
	Then 10 deals on request are available