module.exports = function () {
	var sinon = require('sinon'),
		_ = require('underscore'),
		cleaner = require(process.cwd() + '/api/lib/cleaner'),
		async = require('async'),
		settings = require(process.cwd() + '/settings');
	
    this.Before(function(callback) {
    	var self = this;
		self.fbMockDescriptor = { methods:[] };

		self.mockModuleFacebook = function() {
			var fbApiCalls = arguments[0];
			angular.module('Dealbee.test.facebook', [])
				.factory('fbWrapper', function(){
				var methodsDescriptor = JSON.parse(fbApiCalls),
					self = this,
					uniqueMethods = _.uniq(_.pluck(methodsDescriptor.methods, 'name'));

				console.log('FB mock calls def : ' + fbApiCalls);

				this.FB = { XFBML: { parse: function(){} } };

				_.each(uniqueMethods, function (methodName){
					var method = function(){
						var fbCallArgs = arguments;
						//find the methodDescriptor with the right name that matches the arguments
						matchingMethodDescriptor = _.find(methodsDescriptor.methods, function (methodDescriptor){
							var argsMatch = true;

							if(methodDescriptor.name !== methodName){
								return false;
							}

							if(methodDescriptor.arguments.length !== fbCallArgs.length ){
								return false;
							}

							for(var i = 0; i < fbCallArgs.length; i++){
								if(methodDescriptor.arguments[i] === 'callback'){
									var getType = {};
										if(fbCallArgs[i] && getType.toString.call(fbCallArgs[i]) !== '[object Function]'){
										argsMatch = false;
										break;
										} else {
											methodDescriptor.callbackIndex = i;
										}
								} else if(methodDescriptor.arguments[i] !== null && typeof methodDescriptor.arguments[i] === 'object'){
									if(JSON.stringify(methodDescriptor.arguments[i]) !== JSON.stringify(fbCallArgs[i])){
										argsMatch = false;
										break;
									}
								} else {
									if(methodDescriptor.arguments[i] !== fbCallArgs[i]){
										argsMatch = false;
										break;
									}
								}
							}
							return argsMatch;							
						});

						if(!matchingMethodDescriptor){
							throw new Error('Unexpected call to fbWrapper.' + methodName + ': ' + JSON.stringify(arguments) + '\n\nExpected : '+ fbApiCalls);
						} else {
							if(matchingMethodDescriptor.returnValue){
								console.log('intercepted successfully call to FB.' + matchingMethodDescriptor.name + ' with args ' + JSON.stringify(arguments) + ', yield returning ' + JSON.stringify(matchingMethodDescriptor.returnValue));
								//last argument is expected to be a callback with a single argument
								if(matchingMethodDescriptor.callbackIndex === undefined) throw new Error('Got a returnValue, but no callback argument yiels with : ' + JSON.stringify(matchingMethodDescriptor));
								arguments[matchingMethodDescriptor.callbackIndex](matchingMethodDescriptor.returnValue);								
							} else {
								console.log('intercepted successfully call to FB.' + matchingMethodDescriptor.name + ' with args ' + JSON.stringify(arguments));
							}
						}
					};
					self.FB[methodName] = method;
				});
				FB = self.FB;
				return this;
			});
		};

		self.fbMockDescriptor.methods.push({ 
			name: 'init', 
			arguments: [{
				appId: settings.FBAPPID,
				xfbml: true,
				version: 'v2.1'
			}]
		});
		self.fbMockDescriptor.methods.push({ 
			name: 'getLoginStatus', 
			arguments: ['callback'], 
			returnValue: {}
		});

		browser.addMockModule('Dealbee.test.facebook', self.mockModuleFacebook, JSON.stringify(self.fbMockDescriptor));
		browser.manage().deleteAllCookies();
		callback();
    });
    this.After(function(callback) {
        var deleteOperations = [];
        _.each(this.emittersToDelete, function (emitterToDelete){
        	deleteOperations.push(function (callback){
        		cleaner.deleteEmitter(emitterToDelete, callback);
        	});
        });
        _.each(this.identifiersToDelete, function (identifierToDelete){
        	deleteOperations.push(function (callback){
        		cleaner.deleteIdentifier(identifierToDelete, callback);
        	});
        });
        _.each(this.managerInvitationsToDelete, function (managerInvitationToDelete){
        	deleteOperations.push(function (callback){
        		cleaner.deleteManagerRequest(managerInvitationToDelete, callback);
        	});
        });
        async.parallel(deleteOperations, callback);
    });
};