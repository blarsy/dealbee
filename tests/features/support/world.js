var World = function World(worldcallback) {
	var chai = require('chai'),
		expect = chai.expect,
		restify = require('restify'),
		settings = require(process.cwd() + '/settings'),
		models = require(process.cwd() + '/api/models'),
		self = this,
		_ = require('underscore'),
		webUrl = 'http://' + settings.HOST + ':' + settings.PORT + '/',
		apiProxy = require(process.cwd() + '/tests/features/support/apiProxy')(webUrl);

	chai.use(require('chai-as-promised'));

	this.expect = expect;
	this.emittersToDelete = [];
	this.identifiersToDelete = [];
	this.managerInvitationsToDelete = [];
	this.webApiClient = restify.createJsonClient({
		url: webUrl,
		version: '*'
	});
	this.webUrl = webUrl;
	this.defaultFbToken = 'fbToken';

	this.formatDate = function (date){
		var month = '0' + (date.getMonth() + 1),
			day = '0' + date.getDate();

		return day.substring(day.length - 2) + '/' + month.substring(month.length - 2) + '/' + date.getFullYear();
	}

	this.defaultDeal = {
		benefit: '30%',
		teaser: 'on your next meal',
		description: 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...',		
		beginDate: new Date(),
		expiration: new Date(new Date().valueOf() + (1000 * 60 * 60 * 24 * 3)),
		visibility: 'limited',
		imageRef: {}, //just give it a non-null value, we won't test further anyway
		termsOfUse: []
	}

	this.defaultAddress = {
		street: 'rue Morel',
		number: '4',
		postcode: '7500',
		city: 'Tournai',
		box: '8',
		country: 'Belgique',
		useMap: true,
		coords: [3.3214, 50.2365],
		created: new Date()
	};

	this.defaultEmitter = {
		name: 'e2etests',
		email: 'e2e@test.com',
		password: '1234567890',
		address: this.defaultAddress,
		website: 'www.e2etests.com',
		language: 'fr'
	};

	this.defaultInvoicingData = {
		companyName: 'company',
		juridicalForm: 'sprl',
		language: 'en',
		address: this.defaultAddress,
		hasNoVATNumber: false,
		VATCountryCode: 'be',
		VATNumber: '0830123456'
	};

	this.minimalisticEmitter = {
		name: 'minimalistic',
		email: 'minimalistic@test.com',
		password: '1234567890',
		language: 'fr'
	};

	this.emitterWithFullInfo = {
			name: 'e2etests',
			email: 'e2e@test.com',
			password: '0987654321',
			address: {
				street: 'rue moche',
				number: '13',
				box: 'C',
				postcode: '9800',
				city: 'Degueuville',
				country: 'Là-bas'
			},
			website: 'www.e2etests-full.com',
			language: 'en'
		}

	this.getDefaultDeal = function (done){
		apiProxy.getDeals(self.defaultEmitter, function (err, deals){
			if(err) return done(err);

			var foundDeal = _.find(deals, function (dealInRes){
				return dealInRes.teaser === self.defaultDeal.teaser &&
					dealInRes.benefit === self.defaultDeal.benefit &&
					dealInRes.description.indexOf(self.defaultDeal.description) !== -1
			});

			done(null, foundDeal);
		});
	};

	this.createDefaultDeal = function (done){
		self.createDealWithDefaultEmitter(self.defaultDeal, done);
	};

	this.createDealWithDefaultEmitter = function (deal, done){
		self.createDefaultEmitter(function (err){
			if(err) return done(err);

			apiProxy.createSession(self.defaultEmitter, function (err, sessionId, session){
				if(err) return done(err);

				self.createDeal(deal, sessionId, done);
			});
		});
	}

	this.verifyEmitter = function (emitter, done){
		apiProxy.createSession(emitter, function (err, sessionId, session){
			if(err) return done(err);

			try{
				self.expect(emitter.name).to.equal(session.emitter.name);
				if(emitter.address){
					self.expect(emitter.address.street).to.equal(session.emitter.address.street);
					self.expect(emitter.address.postcode.toString()).to.equal(session.emitter.address.postcode);
					self.expect(emitter.address.city).to.equal(session.emitter.address.city);
				}
				done();
			}
			catch(ex){
				return done(ex);
			}
		});
	};

	this.verifyEmitterExists = function (emitter, done){
		models.emitter.findOne({ name: emitter.name }, function (err, emitterFound){
			if(err) return done(err);
			if(!emitterFound) return done('Emitter not found.');

			if(emitter.address){
				try{
					self.expect(emitterFound.address).to.be.ok;
					self.expect(emitterFound.address.street).to.equal(emitter.address.street);
					self.expect(emitterFound.address.postcode).to.equal(emitter.address.postcode.toString());
					self.expect(emitterFound.address.city).to.equal(emitter.address.city);
					done();
				}
				catch(ex){
					done(ex);
				}
			}

		});
	};

	this.verifyDeal = function (emitter, deal, done){
		apiProxy.getDeals(emitter, function (err, deals){
			if(err) return done(err);

			var foundDeal = _.find(deals, function (dealInRes){
				var valid = dealInRes.teaser === deal.teaser &&
					dealInRes.benefit === deal.benefit &&
					dealInRes.description.indexOf(deal.description) !== -1 &&
					dealInRes.imageRef ? true : false === deal.imageRef ? true : false &&
					dealInRes.termsOfUse.length === deal.termsOfUse.length,
					touMatch = true;

				_.each(dealInRes.termsOfUse, function (termOfUseInRes){
					var touFound = false;
					_.each(deal.termsOfUse, function (termOfUse){
						if(termOfUse == termOfUseInRes){
							touFound = true;
						}
					});
					if(!touFound){
						touMatch = false;
					}
				});
				return valid && touMatch;
			});
			try{
				expect(foundDeal).to.be.ok;
				done();
			}
			catch(ex){
				return done(ex);
			}
		});
	};

	this.verifyEmitterImage = function (emitter, done){
		models.emitter.findOne({ name: emitter.name }, function (err, emitterFound){
			if(err) return done(err);

			try{
				expect(emitterFound.imageRef).to.be.ok;
				expect(emitterFound.imageRef.public_id).to.be.ok;
				expect(emitterFound.imageRef.format).to.be.ok;

				done();
			}
			catch(ex){
				return done(ex);
			}
		});
	};

	this.registerEmitterForDeletion = function (emitterName){
		self.emittersToDelete.push({ name: emitterName });
	};

	this.registerIdentifierForDeletion = function (identifier){
		self.identifiersToDelete.push(identifier);
	};

	this.createDefaultEmitter = function (done){
		self.createEmitter(self.defaultEmitter, done);
	};
	
	this.createEmitterAndSession = function (emitter, done){
		self.createEmitter(emitter, function (err){
			if(err) return done(err);

			apiProxy.createSession(emitter, done);
		});
	};

	this.createDefaultEmitterAndSession = function (done){
		self.createEmitterAndSession(self.defaultEmitter, done);
	};

	this.createEmitter = function (emitter, done){
		apiProxy.createEmitter(emitter, function (err, response){
			if(err) return done(err);

			self.registerEmitterForDeletion(emitter.name);
			self.registerIdentifierForDeletion(response.identifier);
			done();
		});
	};

	this.createDealWithOnRequest = function (deal, sessionId, done){
		self.createDeal(deal, sessionId, function (err, sessionId, dealSaved){
			if(err) return done(err);

			var distribute = {
				sessionId: sessionId,
				dealId: dealSaved._id,
				amountOnRequest: 1
			};

			self.webApiClient.post('/api/distribute', distribute, function (err, req, res, obj){
				if(err) return done(err);

				done();
			});
		});
	};

	this.createDeal = function (deal, sessionId, done){
		deal.sessionId = sessionId;
		self.webApiClient.post('/api/deal', deal, function(err, req, res, obj){
			if(err) return done(err);

			self.lastCreatedDealId = obj._id;
			
			done(null, sessionId, obj);
		});
	};

	this.distributeDeal = function (deal, done){
		self.createDealWithDefaultEmitter(deal, function (err, sessionId, dealSaved){
			if(err) return done(err);

			self.lastDistributedDealId = dealSaved._id;

			var distribute = {
				sessionId: sessionId,
				dealId: dealSaved._id,
				grants: [{
					channelType: 1,
					email: 'me@me.com'
				}]
			};

			self.webApiClient.post('/api/distribute', distribute, function(err, req, res, obj){
				if(err) return done(err);

				done();
			});
		});
	};

	this.login = function (emitter, done){
		browser.get(webUrl + 'login');
		element(by.model('newSession.email')).sendKeys(emitter.email);
		element(by.model('newSession.password')).sendKeys(emitter.password);

		element(by.id('createSessionButton')).click();
		browser.wait(function(){
			return browser.getLocationAbsUrl().then(function (url){ return url.indexOf('home') !== -1; });
		}, 5000);
		done();
	};

	this.nextWizardStep = function (){
		return element(by.css('button[data-ng-click="next()"]')).click();		
	};

	this.finishWizard = function(){
		return element(by.css('button[data-ng-click="finish()"]')).click();
	};

	this.loginAsDefaultEmitter = function (done){
		self.createDefaultEmitter(function (err){
			if(err) return done(err);

			self.login(self.defaultEmitter, done);
		});
	};

	this.checkGrants = function (dealId, grants, done){
		models.grant.find({ deal: dealId }, function (err, grantsFound){
			if(err) return done(err);

			expect(grants.length).to.equal(grantsFound.length);
			_.each(grants, function (grant){
				expect(_.find(grantsFound, function(grantFound){
					if(grantFound.channelType !== grant.channelType)
						return false;

					if(grantFound.channelType === 0){
						return grantFound.fbDestinatorUser.name === grant.name &&
							grantFound.fbDestinatorUser.tagId === grant.id &&
							grantFound.fbDestinatorUser.taggedMessage.indexOf(grant.taggedMessage) === 0
					} else {
						return grantFound.email === grant.name;
					}
				})).to.be.ok;
			});

			done();
		});
	};

	this.waitForPage = function(urlExcerpt, done){
		// wait for it with a browser.wait, because simply waiting for Angular does not work when non-Angular promises are used
		browser.wait(function(){
			return browser.getLocationAbsUrl().then(function (url){ return url.indexOf(urlExcerpt) !== -1; });
		}, 5000).then(function (){
				done();
			},
			function (){
				done('Timed out waiting for url "' + urlExcerpt + '" to be loaded.');
			});
	};

	this.enterValidEmitterField = function (fieldName, emitter){
		var valueToEnter,
			inputId;

		switch(fieldName){
			case 'business name':
				valueToEnter = emitter.name;
				inputId = 'inputName';
				break;
			case 'street':
				valueToEnter = emitter.address.street;
				inputId = 'inputStreet';
				break;
			case 'postcode':
				valueToEnter = emitter.address.postcode;
				inputId = 'inputPostcode';
				break;
			case 'city':
				valueToEnter = emitter.address.city;
				inputId = 'inputCity';
				break;
			case 'email':
				valueToEnter = emitter.email;
				inputId = 'inputEmail';
				break;
			case 'password':
				valueToEnter = emitter.password;
				inputId = 'inputPassword';
				break;
			case 'repeated password':
				valueToEnter = emitter.password;
				inputId = 'inputPasswordRepeat';
				break;
			default: throw 'unexpected emitter field name ' + fieldName;
		}
		return element(by.id(inputId)).sendKeys(valueToEnter);
	};

	this.enterValidDealField = function (fieldName, deal, world){
		var valueToEnter,
			inputId;
		if(fieldName !== 'description'){
			switch(fieldName){
				case 'benefit':
					valueToEnter = deal.benefit;
					inputId = 'inputBenefit';
					break;
				case 'teaser':
					valueToEnter = deal.teaser;
					inputId = 'inputTeaser';
					break;
				case 'begin date':
					valueToEnter = world.formatDate(deal.beginDate);
					inputId = 'inputBeginDate';
					break;
				case 'expiration date':
					valueToEnter = world.formatDate(deal.expiration);
					inputId = 'inputExpiration';
					break;
				default: throw 'unexpected field name ' + fieldName;
			}
			element(by.id(inputId)).clear();
			return element(by.id(inputId)).sendKeys(valueToEnter);
		} else {
			return browser.wait(function(){
				return browser.executeScript(function (){
					return tinyMCE.get('inputDescription') ? true : false;
				});
			}, 5000).then(
				function (){
					browser.executeScript(function (){
						tinyMCE.get('inputDescription').setContent(arguments[0]);
					}, deal.description);
				}
			);
		}
	};

	this.waitAddressGeoLocated = function (){
		return browser.wait(function(){
			return element(by.css('[data-ng-controller="EmitterLocationCtrl"]')).evaluate('address.coords');
		}, 5000);
	};

	this.setDefaultAddress = function (emitter, done){
		models.emitter.findOne({ name: emitter.name }, function (err, emitterFound){
			if(err) return done(err);

			self.setAddress(emitterFound.address, self.defaultAddress);

			emitterFound.save(done);
		});
	};

	this.setAddress = function (target, address){
		target.street = address.street;
		target.number = address.number;
		target.postcode = address.postcode;
		target.city = address.city;
		target.box = address.box;
		target.country = address.country;
		target.created = address.created;
	};

	this.setDefaultInvoicingData = function (identifierEmail, done){
		models.identifier.findOne({ email: identifierEmail }).populate('client').exec(function (err, identifier){
			if(err) return done(err);

			var client = identifier.client;

			client.invoicingData.companyName = self.defaultInvoicingData.companyName;
			client.invoicingData.juridicalForm = self.defaultInvoicingData.juridicalForm;
			client.invoicingData.language = self.defaultInvoicingData.language;
			client.invoicingData.hasNoVATNumber = self.defaultInvoicingData.hasNoVATNumber;
			client.invoicingData.VATCountryCode = self.defaultInvoicingData.VATCountryCode;
			client.invoicingData.VATNumber = self.defaultInvoicingData.VATNumber;
			self.setAddress(client.invoicingData.address, self.defaultInvoicingData.address);

			client.save(done);
		});
	}

	this.fillInAddress = function (address){
		element(by.id('inputStreet')).clear();
		element(by.id('inputNumber')).clear();
		element(by.id('inputBox')).clear();
		element(by.id('inputPostcode')).clear();
		element(by.id('inputCity')).clear();
		element(by.id('inputCountry')).clear();
		if(address){
			element(by.id('inputStreet')).sendKeys(address.street);
			element(by.id('inputNumber')).sendKeys(address.number);
			element(by.id('inputBox')).sendKeys(address.box);
			element(by.id('inputPostcode')).sendKeys(address.postcode);
			element(by.id('inputCity')).sendKeys(address.city);
			element(by.id('inputCountry')).sendKeys(address.country);
		}
	};

	this.inviteManager = function (email, sessionId, done){
		apiProxy.inviteManager(email, sessionId, function (err){
			if(err) return done(err);

			models.managerInvitation.findOne({ newManagerEmail: email }, function (err, request){
				if(err) return done(err);

				self.managerInvitationsToDelete.push({ uuid: request.uuid });

				done(null, request);
			});
		});
	};

  	worldcallback(this);
};
exports.World = World;