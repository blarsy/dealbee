module.exports = function (apiUrl){
	var restify = require('restify'),
		webApiClient = restify.createJsonClient({
			url: apiUrl,
			version: '*'
		}),
		self = this;

	function createSession (emitter, done){
		webApiClient.post('/api/session', { email: emitter.email, password: emitter.password }, function (err, req, res, session){
			if(err) return done(err);

			done(null, res.headers['session-id'], session);
		});
	};

	function getDealsForSession (sessionId, done){
		webApiClient.get('/api/deal?sessionId=' + sessionId, function (err, req, res, deals){
			if(err) return done(err);

			done(null, deals);
		});
	};
	return {
		createSession: createSession,
		getDeals: function (emitter, done){
			createSession (emitter, function (err, sessionId, session){
				if(err) return done(err);

				getDealsForSession(sessionId, function (err, deals){
					if(err) return done(err);

					done(null, deals);
				})
			});
		},
		createEmitter: function (emitter, done){
			webApiClient.post('/api/emitter', emitter, function (err, req, res, obj){
				if(err) return done(err);

				done(null, obj);
			});
		},
		inviteManager: function (email, sessionId, done){
			webApiClient.post('/api/manager', { email: email, sessionId: sessionId }, function (err, req, res, obj){
				if(err) return done(err);

				done(null, obj);
			});
		}
	};
};