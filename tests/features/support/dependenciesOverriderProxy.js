module.exports = function (){
	var result = {},
		restify = require('restify'),
		client = restify.createJsonClient({
			url: 'http://127.0.0.1:8085/',
			version: '*'
		}),
		pingAttempts = 0,
		MAX_PING_ATTEMPTS = 10,
		PING_ATTEMPS_INTERVAL = 1000,
		ready = false;

	function pingOverrider(done){
		client.get('/ping', function (err, req, res, obj){
			if(err) return done(false);

			done(true);
		});
	}

	function ensureReady(done){
		if(ready) return done();

		pingOverrider(function (success){
			if(!success){
				pingAttempts ++;
				if(pingAttempts <= MAX_PING_ATTEMPTS){
					setTimeOut(function (){
						ensureReady(done);
					}, PING_ATTEMPS_INTERVAL);
				} else {
					done(new Error('Timed out waiting for dependencies overrider to load.'));
				}
			} else {
				ready = true;
				done();
			}
		});
	};

	result.setupCall = function (objectName, method, args, returnValue, done){
		ensureReady(function (err){
			if(err) return done(err);

			client.post('/call', { objectName: objectName, method: method, args: args, returnValue: returnValue }, function (err, req, res, obj){
				if(err) return done(err);

				done();
			});
		});
	};

	return result;
}();