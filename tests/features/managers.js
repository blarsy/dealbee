module.exports = function(){
	var models = require(process.cwd() + '/api/models'),
		_ = require('underscore'),
		dependenciesOverriderProxy = require(process.cwd() + '/tests/features/support/dependenciesOverriderProxy'),
		nonMemberEmail = 'nonMember@test.com',
		nonMemberPassword = '9876543210',
		lastManagerInvitation,
		addManager = function (email, world, done){
			dependenciesOverriderProxy.setupCall(
				process.cwd() + '/api/lib/mailassets/mailSender', 
				'send', 
				['any', 'callback'], 
				[null, {}], function (err){
					if(err) return done(err);
					
					element(by.id('emitterMenuHead')).click();
					element(by.css('[data-dlb-localize="topMenu.managers"]')).click();
					element(by.id('inputEmailToAdd')).sendKeys(email);
					element(by.css('[data-dlb-localize="managers.addManagerButton"]')).click().then(done, done);
			});
		},
		verifyIsManager = function (world, emitterName, email, done){
			setTimeout(function (){
				models.emitter.findOne({ name: emitterName }).populate('identifiers', 'email').exec(function (err, emitter){
					if(err) return done(err);

					try{
						world.expect(_.find(emitter.identifiers, function (identifier) { return identifier.email === email; })).to.be.ok;
						done();
					}
					catch(ex){
						return done(err);
					}
				});
			}, 200);
		};
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^a manager of another emitter$/, function (done){
		this.createEmitter(this.minimalisticEmitter, done);
	});

	this.Given(/^I received a manager invitation$/, function (done){
		var world = this;
		this.createEmitterAndSession(this.defaultEmitter, function (err, sessionId){
			if(err) return done(err);

			world.inviteManager(nonMemberEmail, sessionId, function (err, request){
				if(err) return done(err);

				lastManagerInvitation = request;
				done();
			});
		});
	});

	this.When(/^I login as an identity managing another emitter$/, function (done){
		var world = this;
		this.createEmitter(this.minimalisticEmitter, function (err){
			if(err) return done(err);

			element(by.css('div[data-ng-show="emitter"] [data-dlb-localize="loginButton"]')).click();
			element(by.css('form[name="loginForm"] #inputEmail')).sendKeys(world.minimalisticEmitter.email);
			element(by.css('form[name="loginForm"] #inputPassword')).sendKeys(world.minimalisticEmitter.password);
			element(by.css('form[name="loginForm"] #createSessionButton')).click().then(done, done);
		});
	});

	this.Given(/^I open the manager invitation$/, function (done){
		browser.get(this.webUrl + 'managers/accept/' + lastManagerInvitation.uuid + '/fr');
		done();
	});

	this.When(/^I add the other manager as manager of my emitter$/, function (done){
		addManager(this.minimalisticEmitter.email, this, done);
	});

	this.When(/^I add someone I trust as manager of my emitter$/, function (done){
		addManager(nonMemberEmail, this, done);

	});

	this.When(/^I provide info for creating a new identity on Dealbee$/, function (done){
		element(by.css('[data-dlb-localize-alt="createIdentity.withEmailButtonAlt"]')).click();
		element(by.id('inputEmail')).sendKeys(nonMemberEmail);
		element(by.id('inputPassword')).sendKeys(nonMemberPassword);
		element(by.id('inputPasswordRepeat')).sendKeys(nonMemberPassword);
		this.registerIdentifierForDeletion({ email: nonMemberEmail });
		element(by.id('createProfileButton')).click().then(done, done);
	});

	this.Then(/^the other manager has becomen manager of my emitter$/, function (done){
		verifyIsManager(this, this.defaultEmitter.name, this.minimalisticEmitter.email, done);
	});

	this.Then(/^a manager invitation is sent$/, function (done){
		var world = this;
		models.identifier.findOne({ email: this.defaultEmitter.email }, function (err, identifier){
			if(err) return done(err);
			if(!identifier) return done('Identifier not found, this is not expected.')

			models.managerInvitation.findOne({ newManagerEmail: nonMemberEmail, requestor: identifier._id }, function (err, request){
				if(err) return done(err);

				try{
					world.expect(request).to.be.ok;
					world.managerInvitationsToDelete.push({ uuid: request.uuid });
					done();
				}
				catch(ex){
					return done(ex);
				}
			});

		});
	});

	this.Then(/^I have becomen manager of the emitter for which the invitation was sent$/, function (done){
		verifyIsManager(this, this.defaultEmitter.name, this.minimalisticEmitter.email, done);
	});

	this.Then(/^the new identity is manager of the emitter for which the invitation was sent$/, function (done){
		verifyIsManager(this, this.defaultEmitter.name, nonMemberEmail, done);
	});

	this.Then(/^I am logged in as the new manager$/, function (done){
		this.expect(element(by.id('connectedEmitterName')).getText()).to.eventually.equal(this.defaultEmitter.name)
			.and.notify(done);
	});

	this.Then(/^the new identity is created$/, function (done){
		var world = this;
		setTimeout(function (){
			models.identifier.findOne({ email: nonMemberEmail }, function (err, identifier){
				if(err) return done(err);

				try{
					world.expect(identifier).to.be.ok;
					done();
				} catch (ex){
					return done(ex);
				}
			});
		}, 1000);
	});
};