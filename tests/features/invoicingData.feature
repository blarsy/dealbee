@Active
Feature: Invoicing data
	As a merchant
	I want to set my invoicing data
	So that I can get invoices on my future payments

Background:
	Given An emitter with just a name

Scenario: Emitter data is prefilled
	Given I have an emitter address
	And I am logged in as the emitter with just a name
	When I request the credits-payments page
	Then the emitter name and emitter address are prefilled as invoicing data

Scenario: Invoicing data is required
	Given I am logged in as the emitter with just a name
	And I request the credits-payments page
	And I remove the prefilled company name
	When I save the invoicing data
	Then I see the invoicing data fields are required

Scenario: Cannot use the emitter address as invoicing address when no emitter address
	Given I am logged in as the emitter with just a name
	When I request the credits-payments page
	Then I cannot set the invoicing address as the emitter address

Scenario: Save invoicing data without VAT
	Given I am logged in as the emitter with just a name
	And I am on the credits-payments page
	And I provide my company info, having no vat
	When I save the invoicing data
	Then the invoicing data of the company without VAT is saved

Scenario: Save invoicing data with address as emitter address
	Given I have an emitter address
	And I am logged in as the emitter with just a name
	And I am on the credits-payments page
	And I provide company info and vat info
	When I save the invoicing data
	Then the invoicing data is saved with the same address as my emitter address