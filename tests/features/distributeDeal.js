module.exports = function(){
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	var models = require(process.cwd() + '/api/models'),
		dependenciesOverrider = require(process.cwd() + '/tests/features/support/dependenciesOverriderProxy'),
		email1 = 'me@me.com',
		emailList = 'me1@me1.be Michele Hinds\'s profile photo  Michele Hinds  me.me2@me.za',
		friend1 = 'Friend A',
		friend2 = 'Friend B',
		friend3 = 'Friend C',
		friend1TagId = '123',
		friend2TagId = '234',
		friend3TagId = '345',
		friend1FbId = 321,
		friend2FbId = 432,
		friend3FbId = 543,
		publishFbId = 'publishFbId',
		accessToken = 'accTok',
		defaultTaggedMessage = 'test',
		async = require('async'),
		_ = require('underscore'),
		settings = require(process.cwd() + '/settings'),
		self = this;

	this.When(/^I request a distribution$/, function (done){
		element(by.id('distributeButton')).click().then(done, done);
	});

	this.When(/^I request a distribution summary$/, function (done){
		element(by.css('button[data-ng-click="confirmDistribute()"]')).click().then(done, done);
	});

	this.Given(/^I request a distribution summary$/, function (done){
		element(by.css('button[data-ng-click="confirmDistribute()"]')).click();
		done();
	});

	this.Given(/^I have (.*) credits left$/, function (credits, done){
		models.identifier.findOne({ email: this.defaultEmitter.email }).populate('client').exec(function (err, identifierFound){
			if(err) return done(err);

			var client = identifierFound.client;
			client.packs[0].remaining = new Number(credits);

			client.save(done);
		});
	});

	this.When(/^I request the Facebook distribution interface$/, function (done){
		element(by.css('#distribute-tabset ul li:nth-child(3) a')).click().then(done, done);
	});

	this.Given(/^I request the Facebook distribution interface$/, function (done){
		element(by.css('#distribute-tabset ul li:nth-child(3) a')).click();
		done();
	});

	this.Given(/^I have some Facebook friends$/, function (done){
		this.fbMockDescriptor.methods.push({ 
			name: 'api', 
			arguments: ['/me/taggable_friends?limit=500&fields=name,picture,id&access_token=' + this.defaultFbToken, 'callback'], 
			returnValue: { data: [ { name: friend1, id: friend1TagId, picture: { data: { url:'x' } } }, { name: friend2, id: friend2TagId, picture: { data: { url:'x' } } }, { name: friend3, id: friend3TagId, picture: { data: { url:'x' } } } ] }
		});
		browser.removeMockModule('Dealbee.test.facebook');
		browser.addMockModule('Dealbee.test.facebook', this.mockModuleFacebook, JSON.stringify(this.fbMockDescriptor));
		done();
	});

	this.Given(/^I granted Dealbee publish rights on Facebook$/, function (done){
		this.fbMockDescriptor.methods.push({ 
			name: 'login', 
			arguments: ['callback', { scope: 'email, user_friends, publish_actions', return_scopes: true }], 
			returnValue: { status: 'connected', authResponse: { grantedScopes: 'publish_actions', accessToken: this.defaultFbToken } }
		});
		browser.removeMockModule('Dealbee.test.facebook');
		browser.addMockModule('Dealbee.test.facebook', this.mockModuleFacebook, JSON.stringify(this.fbMockDescriptor));
		done();
	});

	this.Then(/^I see my Facebook friends$/, function (done){
		var world = this;
		element.all(by.css('.friendsList > div > a > div > span')).then(function (elements){
			async.parallel([
				function (callback){
					try{
						world.expect(elements.length).to.equal(3);
						callback();
					}
					catch(ex){
						callback(ex);
					}
				},
				function (callback){
					world.expect(elements[0].getText())
						.to.eventually.equal(friend1).and.notify(callback);
				},
				function (callback){
					world.expect(elements[1].getText())
						.to.eventually.equal(friend2).and.notify(callback);
				},
				function (callback){
					world.expect(elements[2].getText())
						.to.eventually.equal(friend3).and.notify(callback);
				}
				], done);
		}, done);
	});

	this.Then(/^I see that some contacts are required$/, function (done){
		this.expect(element(by.css('[data-dlb-localize="distribute.pleaseSelectSomeContactToSendTo"]'))
			.isDisplayed()).to.eventually.be.true.and.notify(done);
	});

	this.Given(/^I provided an email contact$/, function (done){
		element(by.css('#distribute-tabset ul li:nth-child(2) a')).click();
		element(by.id('inputEmailToAdd')).sendKeys(email1);
		element(by.id('addEmailButton')).click();
		done();
	});

	this.Given(/^I pasted a list of emails$/, function (done){
		element(by.css('#distribute-tabset ul li:nth-child(2) a')).click();
		//use the single email input box as temporary container for the list of emails
		var emailInput = element(by.id('inputEmailToAdd'));
		emailInput.sendKeys(emailList);
		emailInput.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
		emailInput.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "x"));
		element(by.id('pasteListButton')).click();
		element(by.id('inputPasteEmails')).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "v"));
		done();
	});

	this.Given(/^I selected a Facebook friend$/, function (done){
		element(by.id('inputTaggedMessage')).sendKeys(defaultTaggedMessage);
		element(by.css('.friendsList > div:nth-child(1) > a')).click();
		done();
	});

	this.Given(/^I set (.*) deal available on request$/, function (dealsOnRequest, done){
		element(by.css('#distribute-tabset ul li:nth-child(1) a')).click();
		element(by.id('inputAmountDealsOnRequest')).sendKeys(dealsOnRequest);
		done();
	});

	this.Given(/^I selected some Facebook friends$/, function (done){
		var world = this;
		element(by.css('#distribute-tabset ul li:nth-child(3) a')).click();
		element(by.id('inputTaggedMessage')).sendKeys(defaultTaggedMessage);
		element(by.css('.friendsList > div:nth-child(1) > a')).click();
		element(by.css('.friendsList > div:nth-child(3) > a')).click();

		async.parallel([
			function (callback){
				dependenciesOverrider.setupCall('fb', 
					'api', 
					['oauth/access_token', { client_id: settings.FBAPPID, client_secret: settings.FBAPPSECRET, grant_type: 'fb_exchange_token', fb_exchange_token: world.defaultFbToken }, 'callback'],
					{ access_token: accessToken },
					callback);
			},
			function (callback){
				dependenciesOverrider.setupCall('fb', 
					'api', 
					['me', { fields:['id'], access_token: accessToken }, 'callback'],
					{ id: publishFbId },
					callback);
			}
		], done);
	});

	this.Given(/^I allow publishing on Facebook$/, function (done){
		element(by.css('a[data-ng-click="ensurePublishRights()"]')).click();
		done();
	});

	this.Then(/^I am requested publish permissions on Facebook$/, function (done){
		this.expect(element(by.css('li[data-ng-show="summary.publishRightsRequired"]')).isDisplayed())
			.to.eventually.be.true.and.notify(done);
	});

	this.Then(/^I am requested to buy credits$/, function (done){
		this.expect(element(by.css('li[data-ng-if="summary.missingFund > 0"]')).isDisplayed())
			.to.eventually.be.true.and.notify(done);
	});

	this.Then(/^the grants were created$/, function (done){
		var world = this;
		setTimeout(function (){
			world.checkGrants(world.lastCreatedDealId, [
				{ channelType: 1, name: email1 },
				{ channelType: 1, name: 'me1@me1.be' },
				{ channelType: 1, name: 'me.me2@me.za' },
				{ channelType: 0, name: friend1, id: friend1TagId, taggedMessage: defaultTaggedMessage },
				{ channelType: 0, name: friend3, id: friend3TagId, taggedMessage: defaultTaggedMessage }], done);
		}, 1000);
	});

	this.Then(/^I am requested to connect on Facebook$/, function (done){
		this.expect(element(by.css('a[data-ng-click="fbConnect()"]')).isDisplayed()).to.eventually.be.true.and.notify(done);
	});

	this.Then(/^(.*) deals on request are available$/, function (dealsOnRequest, done){
		browser.get(this.webUrl + 'viewdeal/onrequest/' + this.lastCreatedDealId.toString());
		this.expect(element(by.css('p.remaining span:first-child')).getText()).to.eventually.equal(dealsOnRequest)
			.and.notify(done);
	});
};