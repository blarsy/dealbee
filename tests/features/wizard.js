module.exports = function(){
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^I request the next step$/, function (done){
		this.nextWizardStep();
		done();
	});

	this.Given(/^I request to finish the operation$/, function (done){
		this.finishWizard();
		done();
	});

	this.When(/^I request the next step$/, function (done){
		this.nextWizardStep().then(done, done);
	});

	this.When(/^I request to finish the operation$/, function (done){
		this.finishWizard().then(done, done);
	});
};