module.exports = function(){
	var self = this,
		models = require(process.cwd() + '/api/models'),
		_ = require('underscore'),
		email = 'multiple@test.com',
		password = '1234567890';

	function createOtherEmitter(emitter, world, done){
		world.enterValidEmitterField('business name', emitter);
		world.nextWizardStep();
		world.enterValidEmitterField('postcode', emitter);
		world.enterValidEmitterField('street', emitter);
		world.enterValidEmitterField('city', emitter);
		world.waitAddressGeoLocated().then(function (){
			world.finishWizard();
			world.registerEmitterForDeletion(emitter.name);
			done();
		}, done);
	}
		
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;
	this.firstManagedEmitter = {
		name: 'managedEmitter1',
		email: email,
		password: password,
		address: {
			street: 'rue Morel',
			number: '4',
			postcode: 7500,
			city: 'Tournai',
			box: '8',
			country: 'Belgique',
			useMap: true,
			coords: [3.3214, 50.2365]
		},
		website: 'www.e2etests.com',
		language: 'fr'
	};
	this.secondManagedEmitter = {
		name: 'managedEmitter2',
		email: email,
		password: password,
		address: {
			street: 'rue Morel',
			number: '4',
			postcode: 7500,
			city: 'Tournai',
			box: '8',
			country: 'Belgique',
			useMap: true,
			coords: [3.3214, 50.2365]
		},
		website: 'www.e2etests2.com',
		language: 'fr'
	};
	this.Given(/^I am logged in on my existing emitter account$/, function (done){
		this.login(self.firstManagedEmitter, done);
	});

	this.Given(/^I am logged in as emitter 1$/, function (done){
		this.login(self.firstManagedEmitter, done);
	});

	this.Given(/^I manage an existing emitter$/, function (done){
		this.createEmitter(self.firstManagedEmitter, done);
	});

	this.Given(/^I enter my email$/, function (done){
		element(by.id('inputEmail')).sendKeys(email);
		done();
	});

	this.Given(/^I enter my password$/, function (done){
		element(by.id('inputPassword')).sendKeys(password);
		done();
	});

	this.Given(/^I enter my repeated password$/, function (done){
		element(by.id('inputPasswordRepeat')).sendKeys(password);
		done();
	});

	this.Given(/^I manage 2 emitters$/, function (done){
		var world = this;
		this.createEmitter(self.firstManagedEmitter, function (err){
			if(err) return done(err);

			world.login(self.firstManagedEmitter, function (err){
				if(err) return done(err);

				browser.get(world.webUrl + 'subscribe');
				createOtherEmitter(self.secondManagedEmitter, world, done);
			});
		});
	});


	this.When(/^I create a second emitter$/, function (done){
		createOtherEmitter(self.secondManagedEmitter, this, done);
	});

	this.When(/^I switch to the emitter 2$/, function (done){
		element(by.id('emitterMenuHead')).click();
		element(by.css('li[data-ng-repeat="emitter in account.identifier.emitters"] a')).click().then(done, done);
	});

	this.When(/^I request to create the emitter with my Facebook account$/, function (done){
		element(by.css('button[type="submit"]')).click().then(done, done);
	});

	this.Then(/^I see the home page of emitter 2$/, function (done){
		var world = this;
		this.waitForPage('home/', function (err){
			if(err) return done(err);

			world.expect(element(by.id('connectedEmitterName')).getText()).to.eventually.equal(self.secondManagedEmitter.name).and.notify(done);
		});
	});

	this.Then(/^I see I must login before creating other companies$/, function (done){
		this.expect(element(by.css('[data-dlb-localize="subscribe.duplicateEmail"]')).isDisplayed()).to.eventually.be.true.and.notify(done);
	});

	this.Then(/^I can switch between both emitters$/, function (done){
		var world = this;
		element.all(by.css('li[data-ng-repeat="emitter in account.identifier.emitters"] a')).then(
			function (elements){
				try{
					world.expect(elements.length).to.equal(1);
					world.expect(element(by.css('li[data-ng-repeat="emitter in account.identifier.emitters"] a')).getAttribute('innerText')).to.eventually.equal(self.firstManagedEmitter.name).and.notify(done);
				}
				catch(ex){
					return done(ex);
				}
			}, done);
	});

	this.Then(/^I am taken to the new emitter home page$/, function (done){
		var world = this;

		this.waitForPage('/home', function (err){
			if(err) return done(err);

			world.expect(element(by.id('connectedEmitterName')).getText()).to.eventually.equal(self.secondManagedEmitter.name).and.notify(done);
		});
	});

	this.Then(/^existing emitter is default emitter$/, function (done){
		var world = this;
		models.identifier.findOne({ email: self.firstManagedEmitter.email }, function (err, identifier){
			if(err) return done(err);
			if(!identifier) return done('no identifier found');
			if(!identifier.defaultEmitter) return done('identifier has no default emitter');

			models.emitter.findOne({ _id:identifier.defaultEmitter }, function (err, emitter){
				if(err) return done(err);
				if(!emitter) return done('could not find default emitter');

				try{
					world.expect(emitter.name).to.equal(self.firstManagedEmitter.name);
					done();
				}
				catch(ex){
					done(ex);
				}
			});
		});
	});

	this.Then(/^the new emitter exists$/, function (done){
		this.verifyEmitterExists(self.secondManagedEmitter, done);
	});
};