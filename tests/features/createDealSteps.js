module.exports = function(){
	var async = require('async');

	function createPreviewChecks(world){
		var asyncI = 0,
			checks = [
			function (callback){
				world.expect(element(by.css('#benefit span')).getText())
					.to.eventually.equal(world.defaultDeal.benefit).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('span.teaser span:first-child')).getText())
					.to.eventually.equal(world.defaultDeal.teaser).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('#seller p.emitterName')).getText())
					.to.eventually.equal(world.defaultEmitter.name).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('span[data-ng-bind="emitter.address.street"]')).getText())
					.to.eventually.equal(world.defaultEmitter.address.street).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('span[data-ng-bind="emitter.address.number"]')).getText())
					.to.eventually.equal(world.defaultEmitter.address.number).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('span[data-ng-bind="emitter.address.box"]')).getText())
					.to.eventually.equal(world.defaultEmitter.address.box).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('span[data-ng-bind="emitter.address.postcode"]')).getText())
					.to.eventually.equal(world.defaultEmitter.address.postcode.toString()).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('span[data-ng-bind="emitter.address.city"]')).getText())
					.to.eventually.equal(world.defaultEmitter.address.city).and.notify(callback);
			},
			function (callback){
				world.expect(element(by.css('a[data-dlb-localize="viewDeal.websiteLabel"]')).getAttribute('href'))
					.to.eventually.equal("http://" + world.defaultEmitter.website + '/').and.notify(callback);
			},
			function (callback){
				world.expect(element.all(by.css('#termsOfUseZone ul li')).count())
					.to.eventually.equal(world.defaultDeal.termsOfUse.length).and.notify(callback);
			}
		];

		for(var i = 0; i < world.defaultDeal.termsOfUse.length; i++){
			checks.push(function (callback){
				asyncI ++;
				world.expect(element(by.css('#termsOfUseZone ul li:nth-child(' + (asyncI).toString() + ')')).getText())
					.to.eventually.equal(world.defaultDeal.termsOfUse[asyncI - 1]).and.notify(callback);
			});
		}

		return checks;
	}; 

	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^I select a valid deal image$/, function (done){
		element(by.css('a[data-ng-click="openUploadBox()"]')).click();
		browser.wait(function (){
			return browser.driver.findElement(by.css('iframe[src*="cloudinary"]')).isDisplayed();
		}, 3000);
		browser.switchTo().frame(1);
		browser.wait(function (){
			return browser.driver.findElement(by.css('div.upload_button_holder')).isDisplayed();
		}, 5000);
		browser.driver.findElement(by.css('input.cloudinary_fileupload')).sendKeys(process.cwd() + '/tests/features/assets/logo.jpg');
		browser.driver.findElement(by.css('a.upload_cropped')).click();
		browser.switchTo().defaultContent();
		browser.wait(function (){
			return element(by.css('img[data-ng-show="imageRef && !oobImageSelected"]')).getSize().then(function (size){
				return size.width > 0;
			});
		}, 3000);
		done();
	});

	this.When(/^I request a preview before saving the deal$/, function (done){
		element(by.id('previewButton')).click().then(done);
	});

	this.When(/^I request a preview after saving the deal$/, function (done){
		element(by.css('a[data-ng-click="preview(deal)"]')).click().then(done, done);
	});

	this.Then(/^a deal exists$/, function (done){
		this.verifyDeal(this.defaultEmitter, this.defaultDeal, done);
	});

	this.Then(/^a preview is shown$/, function (done){
		var world = this;
		async.parallel(createPreviewChecks(world), function(err){
			if(err) return done(err);

			world.expect(element(by.css('#dealImageZone img')).getAttribute('width'))
				.to.eventually.not.equal('0').and.notify(done);
		});
	});

	this.Given(/^I return to the deal creation page$/, function (done){
		element(by.css('a[data-ng-click="returnToPreviousView()"]')).click();
		done();
	});

	this.Then(/^a preview with the selected image is shown$/, function (done){
		var world = this;
		async.parallel(createPreviewChecks(world), function(err){
			if(err) return done(err);

			world.expect(element(by.css('img[data-ng-show="deal.imageRef"]')).getAttribute('width'))
				.to.eventually.not.equal('0').and.notify(done);
		});
	});
};