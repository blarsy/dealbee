module.exports = function(){
	var fbEmail = 'me@fb.com',
		fbName = 'Sample User',
		fbId = 'fbTestUserId',
		_ = require('underscore'),
		self = this;

	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^I am logged in$/, function (done){
		this.loginAsDefaultEmitter(function (err){
			if(err) return done(err);

			done();
		});
	});

	this.Given(/^a valid deal$/, function (done){
		var world = this;
		this.createDefaultDeal(function(err){
			if(err) return done(err);

			world.login(world.defaultEmitter, done);
		});
	});

	this.Given(/^a distributed deal$/, function (done){
		var world = this;

		world.distributeDeal(world.defaultDeal, function (err){
			if(err) return done(err);

			world.login(world.defaultEmitter);
			done();
		});
	});

	this.Given(/^a public, distributed deal$/, function (done){
		var world = this;
			publicDeal = JSON.parse(JSON.stringify(this.defaultDeal));

		publicDeal.visibility = 'public';
		world.distributeDeal(publicDeal, done);
	});

	this.Given(/^I am connected on Facebook$/, function (done){
		var getLoginStatusMock = _.find(this.fbMockDescriptor.methods, function (method){
			return method.name === 'getLoginStatus';
		});
		getLoginStatusMock.returnValue = { status: 'connected', authResponse: { grantedScopes: '', accessToken: this.defaultFbToken } };

		this.fbMockDescriptor.methods.push({
			name: 'api', 
			arguments: ['/me', 'callback'], 
			returnValue: { email: fbEmail, name: fbName, id: fbId }
		});
		this.fbMockDescriptor.methods.push({
			name: 'api', 
			arguments: ['/me/picture?height=30&type=normal&redirect=false', 'callback'], 
			returnValue: { data: { url: '' } }
		});

		browser.removeMockModule('Dealbee.test.facebook');
		browser.addMockModule('Dealbee.test.facebook', this.mockModuleFacebook, JSON.stringify(this.fbMockDescriptor));
		done();
	});
};