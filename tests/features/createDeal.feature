@Active @Focus
Feature: Create deal
	As a merchant
	I want to create a deal
	So that I can send unique deals to my clients

Background:
	Given I am logged in

Scenario: Navigate to create deal
	When I request the create deal page
	Then I see the create deal page

Scenario: Missing benefit and teaser
	Given I am on the create deal page
	When I request the next step
	Then I see that a benefit is required
	And I see that a teaser is required

Scenario: Missing image
	Given I am on the create deal page
	And I enter a valid deal teaser
	And I enter a valid deal benefit
	And I request the next step
	When I request the next step
	Then I see that an image is required

Scenario: Missing description
	Given I am on the create deal page
	And I enter a valid deal teaser
	And I enter a valid deal benefit
	And I request the next step
	And I select a valid deal image
	And I request the next step
	And I enter a valid deal begin date
	And I enter a valid deal expiration date
	And I request the next step 
	When I request to finish the operation
	Then I see that a description is required

Scenario: Deal creation
	Given I am on the create deal page
	And I enter a valid deal teaser
	And I enter a valid deal benefit
	And I request the next step
	And I select a valid deal image
	And I request the next step
	And I enter a valid deal begin date
	And I enter a valid deal expiration date
	And I request the next step
	And I enter a valid deal description
	When I request to finish the operation
	Then I see the home page
	And a deal exists

Scenario: Preview Deal
	Given I am on the create deal page
	And I enter a valid deal teaser
	And I enter a valid deal benefit
	And I request the next step
	And I select a valid deal image
	And I request the next step
	And I enter a valid deal begin date
	And I enter a valid deal expiration date
	And I request the next step 
	And I enter a valid deal description
	When I request a preview before saving the deal
	Then a preview with the selected image is shown

Scenario: Preview Deal and create deal
	Given I am on the create deal page
	And I enter a valid deal teaser
	And I enter a valid deal benefit
	And I request the next step
	And I select a valid deal image
	And I request the next step
	And I enter a valid deal begin date
	And I enter a valid deal expiration date
	And I request the next step 
	And I enter a valid deal description
	And I request a preview before saving the deal
	And I return to the deal creation page
	When I request to finish the operation
	Then I see the home page
	And a deal exists

Scenario: Deal creation and preview
	Given I am on the create deal page
	And I enter a valid deal teaser
	And I enter a valid deal benefit
	And I request the next step
	And I select a valid deal image
	And I request the next step
	And I enter a valid deal begin date
	And I enter a valid deal expiration date
	And I request the next step 
	And I enter a valid deal description
	And I request to finish the operation
	When I request a preview after saving the deal
	Then a preview is shown

