module.exports = function(){
	var models = require(process.cwd() + '/api/models'),
		fillInEmitterInfo = function (emitter, world, done){
			element(by.id('emitterMenuHead')).click();
			element(by.css('[data-dlb-localize="topMenu.account"]')).click();
			element(by.id('inputName')).clear();
			element(by.id('inputName')).sendKeys(emitter.name);
			element(by.id('inputWebsite')).clear();
			if(emitter.website){
				element(by.id('inputWebsite')).sendKeys(emitter.website);
			}
			world.fillInAddress(emitter.address);
			element(by.css('[name="useMap"]')).getAttribute('checked').then(function (checked){
				if(new Boolean(checked).valueOf()) element(by.css('[name="useMap"]')).click();

				element(by.css('[data-dlb-localize="account.saveAccountButton"]')).click().then(function (err){
					if(err) return done(err);

					world.emittersToDelete.length = 0;
					world.registerEmitterForDeletion(emitter.name);
					done();
				}, done);
			});

		},
		verifyEmitter = function (emitter, world, done){
			setTimeout(function (){
				models.emitter.findOne({ name: emitter.name }, function (err, emitterFound){
					if(err) return done(err);

					try{
						world.expect(emitterFound.name).to.equal(emitter.name);
						world.expect(emitterFound.website).to.equal(emitter.website || '');
						world.expect(emitterFound.address.useMap).to.not.be.ok;
						if(emitter.address){
							world.expect(emitterFound.address.street).to.equal(emitter.address.street);
							world.expect(emitterFound.address.number).to.equal(emitter.address.number);
							world.expect(emitterFound.address.box).to.equal(emitter.address.box);
							world.expect(emitterFound.address.postcode).to.equal(emitter.address.postcode);
							world.expect(emitterFound.address.city).to.equal(emitter.address.city);
							world.expect(emitterFound.address.country).to.equal(emitter.address.country);							
						} else {
							world.expect(emitterFound.address.created).to.not.be.ok;
						}
						done();
					}
					catch(ex){
						return done(ex);
					}
				});				
			}, 500);
		};

	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^An emitter with full info$/, function (done){
		this.createEmitter(this.emitterWithFullInfo, done);
	});

	this.Given(/^I am logged in as the emitter having full info$/, function (done){
		this.login(this.emitterWithFullInfo, done);
	});

	this.When(/^I fill in all emitter info$/, function (done){
		fillInEmitterInfo(this.emitterWithFullInfo, this, done);
	});

	this.When(/^I fill in some other full emitter info$/, function (done){
		fillInEmitterInfo(this.defaultEmitter, this, done);
	});

	this.When(/^I fill minimalistic emitter info$/, function (done){
		fillInEmitterInfo(this.minimalisticEmitter, this, done);
	});

	this.Then(/^my emitter data has all emitter info$/, function (done){
		verifyEmitter(this.emitterWithFullInfo, this, done);
	});

	this.Then(/^my emitter data has all other emitter info$/, function (done){
		verifyEmitter(this.defaultEmitter, this, done);
	});

	this.Then(/^my emitter data has minimal emitter info$/, function (done){
		verifyEmitter(this.minimalisticEmitter, this, done);
	});
};