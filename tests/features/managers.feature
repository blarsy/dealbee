@Actve
Feature: Deal managers
	As a deal manager
	I want to monitor the deal managers of an emitter I manage
	So that I spread the load with people I trust

Scenario: Add a manager already member of Dealbee
	Given I am logged in
	And a manager of another emitter
	When I add the other manager as manager of my emitter
	Then the other manager has becomen manager of my emitter

Scenario: Send a manager request
	Given I am logged in
	When I add someone I trust as manager of my emitter
	Then a manager invitation is sent

Scenario: Accept a manager request with an existing emitter account
	Given I received a manager invitation
	And I open the manager invitation
	When I login as an identity managing another emitter
	Then I have becomen manager of the emitter for which the invitation was sent
	And I am logged in as the new manager

Scenario: Accept a manager request by creating an account
	Given I received a manager invitation
	And I open the manager invitation
	When I provide info for creating a new identity on Dealbee
	Then the new identity is created
	And the new identity is manager of the emitter for which the invitation was sent
	And I am logged in as the new manager
