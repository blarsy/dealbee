@Active
Feature: Payment
	As a merchant
	I want to set my invoicing data
	So that I can get invoices on my future payments

Background:
	Given An emitter with just a name

Scenario: Prompt for invoicing data and credit card data
	Given I am logged in as the emitter with just a name
	When I request to buy a pack
	Then I see the pack price
	And I am prompted for my invoicing data
	And I am prompted for my credit card info

Scenario: Invoicing data and credit card data are required
	Given I am logged in as the emitter with just a name
	And I requested to buy a pack
	When I confirm the payment
	Then invoicing data is required

Scenario: Invoicing data is not prompted when already present, prompt for credit card data
	Given I have valid invoicing data
	And I am logged in as the emitter with just a name
	When I request to buy a pack
	Then I am prompted for my credit card info
	And I am not prompted for my invoicing data

Scenario: Invoicing data is not required when already present, credit card data is required
	Given I have valid invoicing data
	And I am logged in as the emitter with just a name
	And I requested to buy a pack
	When I confirm the payment
	Then credit card info is required

Scenario: Payment accepted
	Given I have valid invoicing data
	And I have valid credit card info
	And I am logged in as the emitter with just a name
	And I requested to buy a pack
	When I confirm the payment
	Then the pack is added to my profile