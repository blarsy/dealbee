module.exports = function(){
	function waitAddressGeoLocated(){
		return browser.wait(function(){
			return element(by.css('[data-ng-controller="EmitterLocationCtrl"]')).evaluate('address.coords');
		}, 5000);
	};
	this.World = require(process.cwd() + '/tests/features/support/world.js').World;

	this.Given(/^I select a valid image$/, function (done){
		element(by.css('a[data-ng-click="openUploadBox()"]')).click();
		browser.wait(function (){
			return browser.driver.findElement(by.css('iframe[src*="cloudinary"]')).isDisplayed();
		}, 5000);
		browser.switchTo().frame(1);
		browser.wait(function (){
			return browser.driver.findElement(by.css('div.upload_button_holder')).isDisplayed();
		}, 5000);
		browser.driver.findElement(by.css('input.cloudinary_fileupload')).sendKeys(process.cwd() + '/tests/features/assets/logo.jpg');
		browser.switchTo().defaultContent();
		browser.wait(function (){
			return element(by.css('img[data-ng-show="imageRef"]')).getSize().then(function (size){
				return size.width > 0;
			});
		}, 5000);
		done();
	});

	this.Given(/^the address has been geolocated$/, function (done){
		//this is an exception: Given should not trigger actual browser execution, but I could not find any other way that works
		this.waitAddressGeoLocated().then(function() {
			done();
		}, done);
	});

	this.Given(/^I choose to use an email identity$/, function (done){
		element(by.css('div[data-ng-click="selectEmail()"]')).click();
		done();
	});

	this.Then(/^A valid emitter exists$/, function (done){
		var self = this;
		this.verifyEmitter(this.defaultEmitter , function (err){
				if(err) return done(err);

				self.registerEmitterForDeletion(self.defaultEmitter.name);
				self.registerIdentifierForDeletion({ email: self.defaultEmitter.email });
				done();
			}
		);
	});
	
	this.Then(/^the address has been geolocated$/, function (done){
		waitAddressGeoLocated().then(function() {
			done();
		}, done);
	});

	this.Then(/^the emitter has an image$/, function (done){
		this.verifyEmitterImage(this.defaultEmitter, done);
	});
};