@Active
Feature: Subscribe
	As a merchant
	I want to subscribe to Dealbee
	So that I can send unique deals to my clients

Scenario: Navigate to subscribe #1
	Given I am on the splash page
	When I request the subscription page
	Then I see the subscription page

Scenario: Navigate to subscribe #2
	Given I am on the splash page
	When I request the subscription page from the top menu
	Then I see the subscription page

Scenario: Missing business name
	Given I am on the subscription page
	When I request the next step
	Then I see that business name is required
	
Scenario: Missing city
	Given I am on the subscription page
	And I enter a valid emitter business name
	And I request the next step
	And I enter a valid emitter street
	When I request the next step
	Then I see that a city is required
	And I see that a postcode is required

Scenario: Missing postcode
	Given I am on the subscription page
	And I enter a valid emitter business name
	And I request the next step
	And I enter a valid emitter street
	When I request the next step
	Then I see that a postcode is required

Scenario: Missing street
	Given I am on the subscription page
	And I enter a valid emitter business name
	And I request the next step
	And I enter a valid emitter postcode
	When I request the next step
	Then I see that a street is required

Scenario: Geocode emitter address
	Given I am on the subscription page
	And I enter a valid emitter business name
	And I request the next step
	And I enter a valid emitter postcode
	And I enter a valid emitter street
	When I enter a valid emitter city
	Then the address has been geolocated

Scenario: Missing identification data
	Given I am on the subscription page
	And I enter a valid emitter business name
	And I request the next step
	And I enter a valid emitter postcode
	And I enter a valid emitter street
	And I enter a valid emitter city
	And I request the next step
	And I choose to use an email identity
	When I request to finish the operation
	Then I see that an email is required
	And I see that a password is required
	And I see that a repeated password is required

Scenario: Subscription without image
	Given I am on the subscription page
	And I enter a valid emitter business name
	And I request the next step
	And I enter a valid emitter postcode
	And I enter a valid emitter street
	And I enter a valid emitter city
	And the address has been geolocated
	And I request the next step
	And I choose to use an email identity
	And I enter a valid emitter email
	And I enter a valid emitter password
	And I enter a valid emitter repeated password
	When I request to finish the operation
	Then I see the home page
	And A valid emitter exists
	And I am connected

Scenario: Subscription with image
	Given I am on the subscription page
	And I enter a valid emitter business name
	And I select a valid image
	And I request the next step
	And I enter a valid emitter postcode
	And I enter a valid emitter street
	And I enter a valid emitter city
	And the address has been geolocated
	And I request the next step
	And I choose to use an email identity
	And I enter a valid emitter email
	And I enter a valid emitter password
	And I enter a valid emitter repeated password
	When I request to finish the operation
	Then I see the home page
	And A valid emitter exists
	And I am connected
	And the emitter has an image