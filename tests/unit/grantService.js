describe('grant service', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested;

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	chai.config.includeStack = true;

	describe('create', function (){
		var bizTime, uuidMock, aUuid = '321', now = new Date(2015, 3, 2, 1);

		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
			bizTime = require(process.cwd() + '/api/lib/bizTime');
			bizTime.now = sinon.stub().returns(now);

			uuidMock = { v4: sinon.stub().returns(aUuid) };
		});

		it('creates a facebook grant', function (done){
			var fbDestinatorUser = { name: 'fbDestinatorUser'},
				dealId = '54dd36d3e21d86000068cf59',
				nextNumber = 5,
				senderUid = 'senderUid';

			mockery.registerMock('node-uuid', uuidMock);
			tested = require(process.cwd() + '/api/lib/grantService');

			var grant = tested.create(dealId, null, fbDestinatorUser, senderUid, nextNumber);

			check(done, function (){
				expect(grant.deal.toString()).to.equal(dealId);
				expect(grant.channelType).to.equal(0);
				expect(grant.email).to.not.be.ok;
				expect(grant.fbDestinatorUser.name).to.equal(fbDestinatorUser.name);
				expect(grant.senderFbUid).to.equal(senderUid);
				expect(grant.uuid).to.equal(aUuid);
				expect(grant.created).to.equal(now);
				expect(grant.number).to.equal(nextNumber);
			});
		});

		it('creates an email grant', function (done){
			var email = 'email',
				dealId = '54dd36d3e21d86000068cf59',
				nextNumber = 5;

			mockery.registerMock('node-uuid', uuidMock);
			tested = require(process.cwd() + '/api/lib/grantService');

			var grant = tested.create(dealId, email, null, null, nextNumber);

			check(done, function (){
				expect(grant.deal.toString()).to.equal(dealId);
				expect(grant.channelType).to.equal(1);
				expect(grant.email).to.equal(email);
				expect(grant.fbDestinatorUser).to.be.undefined;
				expect(grant.senderFbUid).to.be.undefined;
				expect(grant.uuid).to.equal(aUuid);
				expect(grant.created).to.equal(now);
				expect(grant.number).to.equal(nextNumber);
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/grantService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
		});
	});

	describe('distribute', function (){
		var tested,
			facebook,
			mailComposer,
			joblogs;
		
		beforeEach(function (){
			facebook = {};
			mailComposer = {};
			joblogs = {};
			mockery.enable({ warnOnUnregistered: false });
			mockery.registerMock(process.cwd() + '/api/lib/facebook', facebook);
			mockery.registerMock(process.cwd() + '/api/lib/mailassets/mailComposer', mailComposer);
			mockery.registerMock(process.cwd() + '/api/lib/jobs/joblogs', joblogs);
			tested = require(process.cwd() + '/api/lib/grantService');
		});

		it('Handles exception', function (done){
			var grant = { channelType: 0 },
				exception = 'exception';

			facebook.sendDeal = sinon.stub().throws(exception);
			joblogs.handleJobError = sinon.stub();

			tested.distribute(grant, function (err){
				check(done, function (){
					expect(joblogs.handleJobError.calledWith('Distribute deals', 'Exception while distributing deal', sinon.match(function(value){ 
						return value.name === exception}
					))).to.be.true;
					expect(err.name).to.equal(exception);
				});
			});
		});

		it('Finalizes grant after Facebook publication', function (done){
			var grant = { channelType: 0 },
				error = 'error';

			facebook.sendDeal = sinon.stub().yields(error);
			tested.finalizeDealSend = sinon.stub().yields(null);

			tested.distribute(grant, function (err){
				check(done, function (){
					expect(facebook.sendDeal.calledWith(grant)).to.be.true;
					expect(tested.finalizeDealSend.calledWith(error, grant)).to.be.true;
				});
			});
		});

		it('Finalizes grant after sending gift mail', function (done){
			var grant = { channelType: 1, previousHolders: [{}] },
				error = 'error';

			mailComposer.giveDeal = sinon.stub().yields(error);
			tested.finalizeDealSend = sinon.stub().yields(null);

			tested.distribute(grant, function (err){
				check(done, function (){
					expect(mailComposer.giveDeal.calledWith(grant)).to.be.true;
					expect(tested.finalizeDealSend.calledWith(error, grant)).to.be.true;
				});
			});
		});

		it('Finalizes grant after sending grant mail', function (done){
			var grant = { channelType: 1 },
				error = 'error';

			mailComposer.distributeDeal = sinon.stub().yields(error);
			tested.finalizeDealSend = sinon.stub().yields(null);

			tested.distribute(grant, function (err){
				check(done, function (){
					expect(mailComposer.distributeDeal.calledWith(grant)).to.be.true;
					expect(tested.finalizeDealSend.calledWith(error, grant)).to.be.true;
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/grantService')];
		});
	});

	describe('finalizeDealSend', function (){
		var tested,
			joblogs,
			bizTime;
		
		beforeEach(function (){
			joblogs = {};
			bizTime = {};
			mockery.enable({ warnOnUnregistered: false });
			mockery.registerMock(process.cwd() + '/api/lib/jobs/joblogs', joblogs);
			mockery.registerMock(process.cwd() + '/api/lib/bizTime', bizTime);
			tested = require(process.cwd() + '/api/lib/grantService');
		});

		it('Handles first distribution error', function (done){
			var grant = { save: sinon.stub().yields(null) },
				error = 'error',
				now = new Date();

			bizTime.now = sinon.stub().returns(now);

			tested.finalizeDealSend(error, grant, function (err, success){
				check(done, function (){
					expect(grant.consecutiveSendErrors).to.equal(1);
					expect(grant.sendErrors.length).to.equal(1);
					expect(grant.sendErrors[0].date).to.equal(now);
					expect(grant.sendErrors[0].error).to.equal(error);
					expect(success).to.be.false;
				});
			});
		});

		it('Handles following distribution errors', function (done){
			var grant = { save: sinon.stub().yields(null), consecutiveSendErrors: 2, sendErrors: [{}, {}] },
				error = 'error',
				now = new Date();

			bizTime.now = sinon.stub().returns(now);

			tested.finalizeDealSend(error, grant, function (err, success){
				check(done, function (){
					expect(grant.consecutiveSendErrors).to.equal(3);
					expect(grant.sendErrors.length).to.equal(3);
					expect(grant.sendErrors[2].date).to.equal(now);
					expect(grant.sendErrors[2].error).to.equal(error);
					expect(success).to.be.false;
				});
			});
		});

		it('Handles distribution error objects', function (done){
			var grant = { save: sinon.stub().yields(null) },
				error = { a:'a', b: 'b' },
				now = new Date();

			bizTime.now = sinon.stub().returns(now);

			tested.finalizeDealSend(error, grant, function (err, success){
				check(done, function (){
					expect(grant.consecutiveSendErrors).to.equal(1);
					expect(grant.sendErrors.length).to.equal(1);
					expect(grant.sendErrors[0].date).to.equal(now);
					expect(grant.sendErrors[0].error).to.equal(JSON.stringify(error));
					expect(success).to.be.false;
				});
			});
		});

		it('Handles successful distribution', function (done){
			var grant = { save: sinon.stub().yields(null), consecutiveSendErrors: 1 },
				now = new Date();

			bizTime.now = sinon.stub().returns(now);

			tested.finalizeDealSend(null, grant, function (err, success){
				check(done, function (){
					expect(grant.consecutiveSendErrors).to.equal(0);
					expect(grant.sent).to.equal(now);
					expect(success).to.be.true;
				});
			});
		});

		it('Handles exception before or upon saving grant', function (done){
			var exception = 'exception',
				grant = { save: sinon.stub().throws(exception) },
				now = new Date();

			bizTime.now = sinon.stub().returns(now);

			tested.finalizeDealSend(null, grant, function (err, success){
				check(done, function (){
					expect(err.name).to.equal(exception);
				});
			});
		});

		it('Handles error while saving grant', function (done){
			var error = 'error',
				grant = { save: sinon.stub().yields(error) };

			bizTime.now = sinon.stub().returns(now);
			joblogs.errorLog = { error: sinon.stub() };

			tested.finalizeDealSend(null, grant, function (err, success){
				check(done, function (){
					expect(grant.save.called).to.be.true;
					expect(joblogs.errorLog.error.calledWith(sinon.match(function (value){
						return value.err === error && value.grant === grant && value.task === 'send deal' &&
							value.message === 'Deal sent, but saving the status of the grant failed';
					}))).to.be.true;
					expect(err).to.equal(error);
				});
			});
		});

		it('Handles exception while handling grant saving error', function (done){
			var error = 'error',
				exception = 'exception',
				grant = { save: sinon.stub().yields(error) };

			bizTime.now = sinon.stub().returns(now);
			joblogs.errorLog = { error: sinon.stub().throws(exception) };

			tested.finalizeDealSend(null, grant, function (err, success){
				check(done, function (){
					expect(grant.save.called).to.be.true;
					expect(err.name).to.equal(exception);
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/grantService')];
		});
	});

	
});