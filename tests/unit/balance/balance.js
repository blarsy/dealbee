describe('balance', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		jobName = 'job name',
		tested,
		bizTime,
		prices,
		now = new Date(2015, 0, 10, 1, 2, 3);

	chai.config.includeStack = true;

	describe('get balance', function (){;
		beforeEach(function (){
			tested = require(process.cwd() + '/api/lib/clientService');
			bizTime = require(process.cwd() + '/api/lib/bizTime')
			bizTime.now = sinon.stub().returns(now);
		});

		it('returns 0 when no pack', function (){
			var client = { packs: [] };
			expect(tested.getBalance(client)).to.equal(0);
		});

		it('return 0 when only expired packs', function (){
			var client = { 
				packs: [
					{ 
						begin: new Date(2014, 11, 9),
						end: new Date(2015, 0, 9),
						amount: 10,
						remaining: 10
					},
					{ 
						begin: new Date(2014, 11, 9),
						end: new Date(2014, 12, 9),
						amount: 20,
						remaining: 10
					}
				] 
			};
			expect(tested.getBalance(client)).to.equal(0);
		});

		it('return 0 when only future packs', function (){
			var client = { 
				packs: [
					{ 
						begin: new Date(2015, 1, 11),
						end: new Date(2015, 2, 11),
						amount: 10,
						remaining: 10
					},
					{ 
						begin: new Date(2015, 2, 11),
						end: new Date(2015, 3, 11),
						amount: 20,
						remaining: 10
					}
				] 
			};
			expect(tested.getBalance(client)).to.equal(0);
		});

		it('return sum of remaining in active packs', function (){
			var client = { 
				packs: [
					{ 
						begin: new Date(2014, 11, 11),
						end: new Date(2015, 0, 11),
						amount: 10,
						remaining: 10
					},
					{ 
						begin: new Date(2015, 0, 9),
						end: new Date(2015, 1, 11),
						amount: 20,
						remaining: 10
					}
				] 
			};
			expect(tested.getBalance(client)).to.equal(20);
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
		});
	});

	describe('setup subscription', function (){
		beforeEach(function (){
			tested = require(process.cwd() + '/api/lib/clientService');
			bizTime = require(process.cwd() + '/api/lib/bizTime')
			bizTime.now = sinon.stub().returns(now);
			prices = require(process.cwd() + '/api/lib/prices.json')
		});

		it('sets up an XS subscription and its initial pack, and returns a zero amount', function (){
			var client = {},
				aSubscription = {
					type: 'XS',
					price: 0,
					coupons: 10
				};
			prices.subscriptions = [ aSubscription ];

			tested.createPacksForSubscription = sinon.stub();
			
			var amount = tested.setupSubscription({ type: 'XS', paymentFrequency: 1 }, 'dummy', client);

			expect(client.subscription).to.be.ok;
			expect(client.subscription.kind).to.equal('XS');
			expect(client.subscription.price).to.equal(0);
			expect(client.subscription.latestRenewal).to.equal(now);
			expect(client.subscription.paymentFrequency).to.equal(1);

			expect(tested.createPacksForSubscription.calledWith(client, now, 1, aSubscription)).to.be.true;
			expect(amount).to.equal(0);
		});

		it('goes back from SM to XS without creating any pack when there is still an active XS subscription pack', function (){
			var packBeginDate = new Date(2015, 0, 1),
				client = {
					packs: [{ begin: packBeginDate, end: new Date(2015, 1, 1), subscriptionKind: 'XS' }]
				};

			tested.createPacksForSubscription = sinon.stub();

			var amount = tested.setupSubscription({ type: 'XS', paymentFrequency: 1 }, 'dummy', client);

			expect(client.subscription).to.be.ok;
			expect(client.subscription.kind).to.equal('XS');
			expect(client.subscription.price).to.equal(0);
			expect(client.subscription.latestRenewal).to.equal(packBeginDate);
			expect(client.subscription.paymentFrequency).to.equal(1);
			expect(tested.createPacksForSubscription.called).to.be.false;
			expect(amount).to.equal(0);
		});

		it('goes back from SM to XS', function (){
			var client = {
					packs: [
						{ begin: new Date(2014, 11, 1), end: new Date(2015, 0, 1), subscriptionKind: 'XS' }
					],
					subscription:{
						latestRenewal: now,
						kind: 'SM',
						paymentFrequency: 6
					}
				},
				aSubscription = {
					type: 'XS',
					price: 0,
					coupons: 10
				};

			prices.subscriptions = [ aSubscription ];

			tested.createPacksForSubscription = sinon.stub();

			var amount = tested.setupSubscription({ type: 'XS', paymentFrequency: 1 }, 'dummy', client);

			expect(client.subscription).to.be.ok;
			expect(client.subscription.kind).to.equal('XS');
			expect(client.subscription.latestRenewal).to.equal(now);
			expect(client.subscription.price).to.equal(0);
			expect(client.subscription.paymentFrequency).to.equal(1);

			expect(tested.createPacksForSubscription.calledWith(client, now, 1, aSubscription)).to.be.true;
			expect(amount).to.equal(0);
		});

		it('signals unexpected subscription type', function (){
			expect(function(){ tested.setupSubscription({ type: 'prout', paymentFrequency: 1 }, 'dummy', {}) }).to.throw((/Unexpected_Subscription_Name/));
		});

		it('updates the subscription and creates packs', function (){
			var client = {
					subscriptions: {
						latestRenewal: new Date(2014, 11, 1),
						kind: 'LG',
						paymentFrequency: 12
					}
				},
				paymentId = 'pay',
				aSubscription = {
					type: 'MD',
					price: 10,
					coupons: 100
				};

			prices.subscriptions = [ aSubscription ];

			tested.createPacksForSubscription = sinon.stub();

			var amount = tested.setupSubscription({ type: 'MD', paymentFrequency: 6 }, paymentId, client);

			expect(client.subscription).to.be.ok;
			expect(client.subscription.kind).to.equal('MD');
			expect(client.subscription.price).to.equal(10);
			expect(client.subscription.latestRenewal).to.equal(now);
			expect(client.subscription.paymentFrequency).to.equal(6);

			expect(tested.createPacksForSubscription.calledWith(client, now, 6, aSubscription, paymentId)).to.be.true;
			expect(amount).to.equal(60);
		});

		it('updates the subscription and creates 2 packs when paymentFrequency is 1 month', function (){
			var client = {
					subscriptions: {
						latestRenewal: new Date(2014, 11, 1),
						kind: 'LG',
						paymentFrequency: 12
					}
				},
				paymentId = 'pay',
				aSubscription = {
					type: 'MD',
					price: 10,
					coupons: 100
				};
			prices.subscriptions = [ aSubscription ];

			tested.createPacksForSubscription = sinon.stub();

			var amount = tested.setupSubscription({ type: 'MD', paymentFrequency: 1 }, paymentId, client);

			expect(client.subscription).to.be.ok;
			expect(client.subscription.kind).to.equal('MD');
			expect(client.subscription.latestRenewal).to.equal(now);
			expect(client.subscription.price).to.equal(10);
			expect(client.subscription.paymentFrequency).to.equal(1);

			expect(tested.createPacksForSubscription.calledWith(client, now, 2, aSubscription, paymentId)).to.be.true;
			expect(amount).to.equal(20);
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/prices.json')];
		});
	});

	describe('Balance modification during distribution', function (){
		beforeEach(function (){
			tested = require(process.cwd() + '/api/lib/clientService');
			bizTime = require(process.cwd() + '/api/lib/bizTime')
			bizTime.now = sinon.stub().returns(now);
		});

		it('errs when not enough balance', function (){
			var client = {};
			expect(function() { 
				tested.subtractFromBalance(client, 15); 
			}).to.throw(/Insufficient_Funds/);
		});
		it('subtract from active pack that expires first', function (){
			var expiredPack = { 
					begin: new Date(2014, 11, 9),
					end: new Date(2015, 0, 9),
					amount: 10,
					remaining: 10
				},
				futurePack = { 
					begin: new Date(2015, 0, 11),
					end: new Date(2015, 1, 11),
					amount: 20,
					remaining: 10
				},
				activePack1 = { 
					begin: new Date(2015, 0, 10),
					end: new Date(2015, 1, 10),
					amount: 10,
					remaining: 10
				},
				activePack2 = { 
					begin: new Date(2014, 11, 11),
					end: new Date(2015, 0, 11),
					amount: 10,
					remaining: 10
				},
				client = {
				packs:[
					expiredPack,
					futurePack,
					activePack1,
					activePack2
				]
			};
			tested.subtractFromBalance(client, 15);

			expect(expiredPack.remaining).to.equal(10);
			expect(futurePack.remaining).to.equal(10);
			expect(activePack1.remaining).to.equal(5);
			expect(activePack2.remaining).to.equal(0);
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
		});
	});

	describe('Pack creation', function (){
		beforeEach(function (){
			tested = require(process.cwd() + '/api/lib/clientService');
			bizTime = require(process.cwd() + '/api/lib/bizTime')
			bizTime.now = sinon.stub().returns(now);
			prices = require(process.cwd() + '/api/lib/prices.json')
		});

		it('rejects unexpected pack name', function (){
			expect(function (){
				tested.createPack('bla', now, {});
			}).to.throw(/Unexpected_Pack_Name/);
		});

		it('adds a pack', function (){	
			var beginDate = new Date(2015,5,5,5,5,5),
				end = new Date(),
				client = {
					packs: [
						{
							type: 'dummy'
						}
					]
				};

			prices.packs = [
				{
					type: 'pac1'
				},
				{
					type: 'pac2',
					price: 10,
					coupons: 100,
					daysValid: 30
				}
			];

			bizTime.addDays = sinon.stub().returns(end);

			tested.createPack('pac2', beginDate, client);

			expect(client.packs.length).to.equal(2);
			expect(client.packs[1].subscriptionKind).to.not.exist;
			expect(client.packs[1].begin).to.equal(beginDate);
			expect(client.packs[1].origin).to.equal('online');
			expect(client.packs[1].created).to.equal(now);
			expect(bizTime.addDays.calledWith(beginDate, 30)).to.be.true;
			expect(client.packs[1].end).to.equal(end);
			expect(client.packs[1].amount).to.equal(100);
			expect(client.packs[1].price).to.equal(10);
			expect(client.packs[1].remaining).to.equal(100);
		});

		it('adds a pack when no pack yet', function (){	
			var beginDate = new Date(2015,5,5,5,5,5),
				end = new Date(),
				client = {},
				paymentId = 'pay';

			prices.packs = [
				{
					type: 'pac1'
				},
				{
					type: 'pac2',
					price: 10,
					coupons: 100,
					daysValid: 30
				}
			];

			bizTime.addDays = sinon.stub().returns(end);

			tested.createPack('pac2', beginDate, client, paymentId);

			expect(client.packs.length).to.equal(1);
			expect(client.packs[0].subscriptionKind).to.not.exist;
			expect(client.packs[0].begin).to.equal(beginDate);
			expect(client.packs[0].origin).to.equal('online');
			expect(client.packs[0].created).to.equal(now);
			expect(bizTime.addDays.calledWith(beginDate, 30)).to.be.true;
			expect(client.packs[0].end).to.equal(end);
			expect(client.packs[0].amount).to.equal(100);
			expect(client.packs[0].price).to.equal(10);
			expect(client.packs[0].remaining).to.equal(100);
			expect(client.packs[0].payment).to.equal(paymentId);
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/prices.json')];
		});
	});

	describe('createPacksForSubscription', function (){
		beforeEach(function (){
			tested = require(process.cwd() + '/api/lib/clientService');
			bizTime = require(process.cwd() + '/api/lib/bizTime')
			bizTime.now = sinon.stub().returns(now);
		});
		it('creates 2 successive packs', function (){
			var client = {},
				begin = new Date(2016,1,1),
				date1 = new Date(2017,1,1),
				date2 = new Date(2018,1,1),
				paymentFrequency = 2,
				subscriptionData = {
					type: 'typ',
					coupons: 10
				},
				paymentId = 'pay';

			bizTime.addMonths = sinon.stub();
			bizTime.addMonths.withArgs(begin, 0).returns(begin);
			bizTime.addMonths.withArgs(begin, 1).returns(date1);
			bizTime.addMonths.withArgs(begin, 2).returns(date2);

			tested.createPacksForSubscription(client, begin, paymentFrequency, subscriptionData, paymentId);

			expect(client.packs.length).to.equal(2);
			expect(client.packs[0].begin).to.equal(begin);
			expect(client.packs[0].end).to.equal(date1);
			expect(client.packs[0].created).to.equal(now);
			expect(client.packs[0].origin).to.equal('renewal');
			expect(client.packs[0].subscriptionKind).to.equal(subscriptionData.type);
			expect(client.packs[0].amount).to.equal(subscriptionData.coupons);
			expect(client.packs[0].remaining).to.equal(subscriptionData.coupons);
			expect(client.packs[0].payment).to.equal(paymentId);

			expect(client.packs[1].begin).to.equal(date1);
			expect(client.packs[1].end).to.equal(date2);
			expect(client.packs[1].created).to.equal(now);
			expect(client.packs[1].origin).to.equal('renewal');
			expect(client.packs[1].subscriptionKind).to.equal(subscriptionData.type);
			expect(client.packs[1].amount).to.equal(subscriptionData.coupons);
			expect(client.packs[1].remaining).to.equal(subscriptionData.coupons);
			expect(client.packs[1].payment).to.equal(paymentId);
		});
		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
		});
	});

	describe('createPacks', function (){
		beforeEach(function (){
			tested = require(process.cwd() + '/api/lib/clientService');
			bizTime = require(process.cwd() + '/api/lib/bizTime')
			bizTime.now = sinon.stub().returns(now);
		});

		it('creates 3 packs', function (){
			var client = {},
				amount,
				begin = new Date(2015,6,6),
				paymentId = 'pay',
				packs = [
					{
						type: 'XS',
						quantity: 1
					},
					{
						type: 'SM',
						quantity: 2
					}
				];

			tested.createPack = sinon.stub();
			tested.createPack.withArgs('XS', begin, client).returns(10);
			tested.createPack.withArgs('SM', begin, client).returns(20);

			amount = tested.createPacks(client, begin, paymentId, packs);

			expect(tested.createPack.calledThrice).to.be.true;
			expect(tested.createPack.firstCall.calledWithExactly('XS', begin, client, paymentId)).to.be.true;
			expect(tested.createPack.secondCall.calledWithExactly('SM', begin, client, paymentId)).to.be.true;
			expect(tested.createPack.thirdCall.calledWithExactly('SM', begin, client, paymentId)).to.be.true;
			expect(amount).to.equal(50);
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
		});
	});	

	describe('registerOrder', function (){
		beforeEach(function (){
			tested = require(process.cwd() + '/api/lib/clientService');
			bizTime = require(process.cwd() + '/api/lib/bizTime')
			uuid = require('node-uuid');
			bizTime.now = sinon.stub().returns(now);
		});

		it('Should setup a gratis subscription', function (){
			var client = {},
				subscription = {},
				paymentId = 'bla',
				done = sinon.stub();

			uuid.v4 = sinon.stub().returns(paymentId);
			tested.charge = sinon.stub();
			tested.setupSubscription = sinon.stub().returns(0);

			tested.registerOrder(subscription, null, client, done);

			expect(done.calledWith(null)).to.be.true;
			expect(tested.setupSubscription.calledWith(subscription, paymentId, client)).to.be.true;
			expect(tested.charge.called).to.be.false;
		});

		it('Should setup and charge a subscription', function (){
			var client = { invoicingData: { VATCountryCode: 'SS' } },
				subscription = {},
				paymentId = 'bla',
				done = sinon.stub();

			uuid.v4 = sinon.stub().returns(paymentId);
			tested.registerPayment = sinon.stub().yields(null);
			tested.setupSubscription = sinon.stub().returns(10);

			tested.registerOrder(subscription, null, client, done);

			expect(done.calledWith(null)).to.be.true;
			expect(tested.setupSubscription.calledWith(subscription, paymentId, client)).to.be.true;
			expect(tested.registerPayment.calledWith(client, 10, 'online payment', paymentId)).to.be.true;
		});

		it('Should setup and charge a subscription and a pack', function (){
			var client = { invoicingData: { VATCountryCode: 'SS' } },
				subscription = {},
				paymentId = 'bla',
				packs = ['dummy'],
				done = sinon.stub();

			uuid.v4 = sinon.stub().returns(paymentId);
			tested.registerPayment = sinon.stub().yields(null);
			tested.setupSubscription = sinon.stub().returns(10);
			tested.createPacks = sinon.stub().returns(20);

			tested.registerOrder(subscription, packs, client, done);

			expect(done.calledWith(null)).to.be.true;
			expect(tested.setupSubscription.calledWith(subscription, paymentId, client)).to.be.true;
			expect(tested.createPacks.calledWith(client, now, paymentId, packs)).to.be.true;
			expect(tested.registerPayment.calledWith(client, 30, 'online payment', paymentId)).to.be.true;
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
			delete require.cache[require.resolve('node-uuid')];
		});		
	});

	describe('registerPayment', function (){
		var tested,
			bizTime;

		beforeEach(function(){
			tested = require(process.cwd() + '/api/lib/clientService');
			bizTime = require(process.cwd() + '/api/lib/bizTime')
			bizTime.now = sinon.stub().returns(now);
		});

		it('register payment without VAT', function (){
			var client = { invoicingData: {} },
				id = 'id',
				amount = 100,
				description = 'description';

			tested.charge = sinon.stub().yields(null);

			tested.registerPayment(client, amount, description, id, function (err){
				expect(err).to.not.be.ok;
				expect(client.payments.length).to.equal(1);
				expect(client.payments[0].date).to.equal(now);
				expect(client.payments[0].uuid).to.equal(id);
				expect(client.payments[0].amount).to.equal(amount);
				expect(client.payments[0].paid).to.equal(amount);
				expect(client.payments[0].vatRate).to.equal(1);
				expect(client.payments[0].description).to.equal(description);
				expect(tested.charge.calledWith(amount, id, client)).to.be.true;
			})
		});

		it('register payment with VAT', function (){
			var client = { invoicingData: { VATCountryCode: 'BE' } },
				id = 'id',
				amount = 100,
				description = 'description';

			tested.charge = sinon.stub().yields(null);

			tested.registerPayment(client, amount, description, id, function (err){
				expect(err).to.not.be.ok;
				expect(client.payments.length).to.equal(1);
				expect(client.payments[0].date).to.equal(now);
				expect(client.payments[0].uuid).to.equal(id);
				expect(client.payments[0].amount).to.equal(amount);
				expect(client.payments[0].vatRate).to.equal(1.21);
				expect(client.payments[0].paid).to.equal(121);
				expect(client.payments[0].description).to.equal(description);
				expect(tested.charge.calledWith(121, id, client)).to.be.true;
			})
		});

		it('handles error while charging', function (){
			var error = new Error('dummy'),
				client = { invoicingData: {} },
				id = 'id',
				amount = 100,
				description = 'description';

			tested.charge = sinon.stub().yields(error);

			tested.registerPayment(client, amount, description, id, function (err){
				expect(err).to.equal(error);
				expect(client.payments.length).to.equal(0);
			});
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
		});

	});
});