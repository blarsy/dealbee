describe('login', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested;

	function check( done, f ) {
		try {
			f();
			done();
		} catch( e ) {
			done( e );
		}
	}

	chai.config.includeStack = true;

	describe('login with fb', function (){
		var fbMock,
			modelMock,
			fbToken = 'dummy',
			error = 'error';

		beforeEach(function (){
			fbMock = {};
			modelMock = {};
			mockery.enable({ warnOnUnregistered: false });
			mockery.registerMock(process.cwd() + '/api/lib/facebook', fbMock);
			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			delete require.cache[require.resolve(process.cwd() + '/api/lib/authorize')];
			tested = require(process.cwd() + '/api/lib/authorize');
		});

		it('handles error when getting name from facebook', function (done){
			var fbToken = 'dummy';

			fbMock.getIdAndName = sinon.stub().yields(error);

			tested.loginFb({}, fbToken, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(fbMock.getIdAndName.calledWith(fbToken)).to.be.true;
				});
			});
		});


		it('handles error when finding identifier by fb uid', function (done){
			var fbUid = 123,
				fbName = 'name';

			fbMock.getIdAndName = sinon.stub().yields(null, { id: fbUid, name: fbName });

			modelMock.identifier = { findOne: sinon.stub().yields(error) };

			tested.loginFb({}, fbToken, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(modelMock.identifier.findOne.calledWith(sinon.match(function (value){
						return value.type === 0 && value.fbUid === fbUid;
					}))).to.be.true;
				});
			});
		});

		it('returns error when no identifier found with fbUid', function (done){
			var fbName = 'name',
				fbUid = 123;

			fbMock.getIdAndName = sinon.stub().yields(null, { id: fbUid, name: fbName });
			modelMock.identifier =  { findOne: sinon.stub().yields(null, null) };

			tested.loginFb({}, fbToken, function (err){
				check(done, function (){
					expect(err).to.equal('Identifier_Not_Found');
				});
			});
		});

		it('handles error when querying power users', function (done){
			var fbName = 'name',
				fbUid = 123,
				identifier = {};

			fbMock.getIdAndName = sinon.stub().yields(null, { id: fbUid, name: fbName });
			fbMock.getPowerUsers = sinon.stub().yields(error);

			modelMock.identifier = { findOne: sinon.stub().yields(null, identifier) };

			mockery.registerMock(process.cwd() + '/api/lib/facebook', fbMock);
			mockery.registerMock(process.cwd() + '/api/models', modelMock);

			tested = require(process.cwd() + '/api/lib/authorize');

			tested.loginFb({}, fbToken, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(fbMock.getPowerUsers.calledWith(fbToken));
				});
			});
		});

		it('create a session with the identifier', function (done){
			var fbName = 'name',
				fbUid = 123,
				identifier = {},
				req = {};

			fbMock.getIdAndName = sinon.stub().yields(null, { id: fbUid, name: fbName });
			fbMock.getPowerUsers = sinon.stub().yields(null, []);

			modelMock.identifier = { findOne: sinon.stub().yields(null, identifier) };

			tested.createSession = sinon.stub().yields(null);

			tested.loginFb(req, fbToken, function (err){
				check(done, function (){
					expect(err).to.be.null;
					expect(tested.createSession.calledWith(req, identifier, this));
				});
			});
		});

		it('create a session with the identifier as power user', function (done){
			var fbName = 'name',
				fbUid = 123,
				identifier = {},
				req = {};

			fbMock.getIdAndName = sinon.stub().yields(null, { id: fbUid, name: fbName });
			fbMock.getPowerUsers = sinon.stub().yields(null, [{ user:456, role:'role'}, { user: fbUid, role: 'role'}]);

			modelMock.identifier =  { findOne: sinon.stub().yields(null, identifier) };

			tested.createSession = sinon.stub().yields(null);

			tested.loginFb(req, fbToken, function (err){
				check(done, function (){
					expect(err).to.be.null;
					expect(identifier.powerUser).to.be.true;
					expect(identifier.god).to.not.be.ok;
				});
			});
		});

		it('create a session with the identifier as administrator', function (done){
			var fbName = 'name',
				fbUid = 123,
				identifier = {},
				req = {};

			fbMock.getIdAndName = sinon.stub().yields(null, { id: fbUid, name: fbName });
			fbMock.getPowerUsers = sinon.stub().yields(null, [{ user:456, role:'role'}, { user: fbUid, role: 'administrators'}]);

			modelMock.identifier = { findOne: sinon.stub().yields(null, identifier) };

			tested.createSession = sinon.stub().yields(null);

			tested.loginFb(req, fbToken, function (err){
				check(done, function (){
					expect(err).to.be.null;
					expect(identifier.powerUser).to.be.true;
					expect(identifier.god).to.be.true;
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/authorize')];
		});
	});

	describe('login with email', function (){
		var email = 'email',
			pwd = 'pwd',
			modelMock,
			cryptoMock,
			tested;

		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
			modelMock = {};
			cryptoMock = {};
			mockery.registerMock(process.cwd() + '/api/models', modelMock);			
			mockery.registerMock('crypto', cryptoMock);
			tested = require(process.cwd() + '/api/lib/authorize');
		});

		it('handles error when finding identifier by email', function (done){
			var error = 'error';
			
			modelMock.identifier = { findOne: sinon.stub().yields(error) };

			tested.loginEmail({}, email, pwd, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(modelMock.identifier.findOne.calledWith(sinon.match(function (value){
						return value.type === 1 && value.email === email;
					}))).to.be.true;
				});
			});
		});

		it('returns error when no identifier found by email', function (done){
			modelMock.identifier = { findOne: sinon.stub().yields(null, null) };

			tested.loginEmail({}, email, pwd, function (err){
				check(done, function (){
					expect(err).to.equal('LoginFailed');
				});
			});
		});

		it('handles error when deriving key', function (done){
			var identifier = { hashInfo: { salt: 'salt', iterations: 100, keySize: 200} },
				error = 'error';

			modelMock.identifier = { findOne: sinon.stub().yields(null, identifier) };
			cryptoMock.pbkdf2 = sinon.stub().yields(error);

			tested.loginEmail({}, email, pwd, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(cryptoMock.pbkdf2.calledWith(pwd, 'salt', 100, 200));
				});
			});
		});

		it('handles derived key did not match', function (done){
			var hash = 'hash',
				identifier = { hashInfo: { salt: 'salt', iterations: 100, keySize: 200, value: hash} };

			modelMock.identifier = { findOne: sinon.stub().yields(null, identifier) };
			cryptoMock.pbkdf2 = sinon.stub().yields(null, 'notthehash');

			tested.loginEmail({}, email, pwd, function (err){
				check(done, function (){
					expect(err).to.equal('LoginFailed');
				});
			});
		});

		it('create a session with the identifier', function (done){
			var hash = 'hash',
				req = {},
				identifier = { hashInfo: { salt: 'salt', iterations: 100, keySize: 200, value: hash} };

			modelMock.identifier = { findOne: sinon.stub().yields(null, identifier) };
			cryptoMock.pbkdf2 = sinon.stub().yields(null, hash);

			tested.createSession = sinon.stub().yields(null);

			tested.loginEmail(req, email, pwd, function (err){
				check(done, function (){
					expect(err).to.be.null;
					expect(tested.createSession.calledWith(req, identifier));
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/authorize')];
		});
	});

	describe('create session', function (){
		var email = 'email',
			pwd = 'pwd',
			identifierService,
			session;

		beforeEach(function (){
			identifierService = require(process.cwd() + '/api/lib/identifierService');
		});

		it('queries for all emitter managed by identifier', function (done){
			var identifierId = 'identifierId',
				identifier = { _id: identifierId, emitters: [] },
				req = {};

			identifierService.linkEmitters = sinon.stub().yields(null, identifier);

			tested = require(process.cwd() + '/api/lib/authorize');

			tested.createSession(req, identifier, function (err){
				check(done, function (){
					expect(identifierService.linkEmitters.calledWith(identifier)).to.be.true;
				});
			});
		});

		it('Handles error when querying for emitter', function (done){
			var identifierId = 'identifierId',
				identifier = { _id: identifierId },
				error = 'error';

			identifierService.linkEmitters = sinon.stub().yields(error);

			tested = require(process.cwd() + '/api/lib/authorize');

			tested.createSession({}, identifier, function (err){
				check(done, function (){
					expect(err).to.equal(error);
				});
			});
		});

		it('Handles no emitter found', function (done){
			var identifierId = 'identifierId',
				identifier = { _id: identifierId, emitters: [] };

			identifierService.linkEmitters = sinon.stub().yields(null, identifier);

			tested = require(process.cwd() + '/api/lib/authorize');

			tested.createSession({}, identifier, function (err){
				check(done, function (){
					expect(err).to.equal('No emitter linked to identifier, or default emitter not found');
				});
			});
		});

		it('Creates a session with default emitter', function (done){
			var identifierId = 'identifierId',
				defaultEmitterId = 'eid',
				defaultEmitter = { _id: defaultEmitterId };
				emitters = [ { _id: 'bla' }, defaultEmitter ],
				identifier = { _id: identifierId, save: sinon.stub().yields(null), defaultEmitter: defaultEmitterId, emitters: emitters },
				req = {};

			identifierService.linkEmitters = sinon.stub().yields(null, identifier);

			tested = require(process.cwd() + '/api/lib/authorize');
			tested.createSessionForEmitter = sinon.stub().yields(null);

			tested.createSession(req, identifier, function (err){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(tested.createSessionForEmitter.calledWith(req, defaultEmitter, identifier)).to.be.true;
					expect(identifier.emitters).to.equal(emitters);
				});
			});
		});

		it('Creates a session with first emitter', function (done){
			var identifierId = 'identifierId',
				firstEmitter = { _id: 'bla2' };
				emitters = [ firstEmitter, { _id: 'bla' } ],
				identifier = { _id: identifierId, save: sinon.stub().yields(null), emitters: emitters },
				req = {};

			identifierService.linkEmitters = sinon.stub().yields(null, identifier);

			tested = require(process.cwd() + '/api/lib/authorize');
			tested.createSessionForEmitter = sinon.stub().yields(null);

			tested.createSession(req, identifier, function (err){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(tested.createSessionForEmitter.calledWith(req, firstEmitter, identifier)).to.be.true;
					expect(identifier.emitters).to.equal(emitters);
				});
			});
		});
		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/identifierService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/authorize')];
		});
	});

	describe('create session for emitter', function (){
		var email = 'email',
			pwd = 'pwd',
			session,
			bizTime,
			modelMock,
			error = 'error',
			req = { session: {} };

		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
			session = {};
			bizTime = {};
			modelMock = {};
			mockery.registerMock(process.cwd() + '/api/configuredsession', session);
			mockery.registerMock(process.cwd() + '/api/lib/bizTime', bizTime);
			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/authorize');
		});

		it('handles error while finding client', function (done){
			var client = {},
				identifier = { client: client };
			
			modelMock.client = { findOne: sinon.stub().yields(error) };

			tested.createSessionForEmitter(req, {}, identifier, function (err){
				check(done, function (){
					expect(err).to.equal(err);
					expect(modelMock.client.findOne.calledWith(sinon.match(function(value){
						return value._id === client;
					}))).to.be.true;
				});
			});
		});

		it('handles error while loading identifier', function (done){
			var client = {},
				identifierId = '123',
				identifier = { _id: identifierId, client: client },
				clientFound = {};
			
			modelMock.client = { findOne: sinon.stub().yields(null, clientFound) };
			modelMock.identifier = { findOne: sinon.stub().yields(error) };

			tested.createSessionForEmitter(req, {}, identifier, function (err){
				check(done, function (){
					expect(err).to.equal(err);
					expect(modelMock.identifier.findOne.calledWith(sinon.match(function(value){
						return value._id === identifierId;
					}))).to.be.true;
				});
			});
		});

		it('handles error while saving identifier', function (done){
			var client = {},
				clientFound = {},
				identifierLoaded = { save: sinon.stub().yields(error) },
				identifier = { client: client };
			
			modelMock.client = { findOne: sinon.stub().yields(null, clientFound) };
			modelMock.identifier = { findOne: sinon.stub().yields(null, identifierLoaded) };

			tested.createSessionForEmitter(req, {}, identifier, function (err){
				check(done, function (){
					expect(err).to.equal(err);
					expect(identifierLoaded.save.called).to.be.true;
				});
			});
		});

		it('handles error while saving session', function (done){
			var client = {},
				clientFound = {},
				identifierLoaded = { save: sinon.stub().yields(null) },
				identifier = { client: client };
			
			session.save = sinon.stub().yields(error);
			modelMock.identifier = { findOne: sinon.stub().yields(null, identifierLoaded) };
			modelMock.client = { findOne: sinon.stub().yields(null, clientFound) };

			tested.createSessionForEmitter(req, {}, identifier, function (err){
				check(done, function (){
					expect(err).to.equal(err);
					expect(session.save.called).to.be.true;
				});
			});
		});

		it('handles error while creating sessionLog', function (done){
			var client = {},
				clientFound = {},
				identifierLoaded = { save: sinon.stub().yields(null) },
				identifier = { client: client },
				sessionSid = 'sessionSid',
				now = new Date(2014, 5, 3),
				emitter = {};
			
			req.session.sid = sessionSid;
			session.save = sinon.stub().yields(null);
			modelMock.identifier = { findOne: sinon.stub().yields(null, identifierLoaded) };
			modelMock.client = { findOne: sinon.stub().yields(null, clientFound) };
			modelMock.sessionLog = { create: sinon.stub().yields(error) };

			bizTime.now = sinon.stub().returns(now);

			tested.createSessionForEmitter(req, emitter, identifier, function (err){
				check(done, function (){
					expect(err).to.equal(err);
					expect(modelMock.sessionLog.create.calledWith(sinon.match(function (value){
						return value.identifier === identifier && value.id === sessionSid && value.created === now &&
							value.emitter === emitter && value.activity.length === 0;
					}))).to.be.true;
				});
			});
		});

		it('sets the session', function (done){
			var client = {},
				clientFound = {},
				identifierId = 321,
				emitters = [],
				name = 'name',
				powerUser = true,
				god = true,
				identifierLoaded = { save: sinon.stub().yields(null) },
				identifier = { 
					_id: identifierId,
					emitters: emitters,
					name: name,
					powerUser: powerUser,
					god: god,
					client: client
				}
				sessionSid = 'sessionSid',
				now = new Date(2014, 5, 3),
				emitter = {};
			
			req.session.sid = sessionSid;
			session.save = sinon.stub().yields(null);
			modelMock.identifier = { findOne: sinon.stub().yields(null, identifierLoaded) };
			modelMock.client = { findOne: sinon.stub().yields(null, clientFound) };
			modelMock.sessionLog = { create: sinon.stub().yields(null) };
			bizTime.now = sinon.stub().returns(now);

			tested.createSessionForEmitter(req, emitter, identifier, function (err){
				check(done, function (){
					expect(err).to.not.be.ok;

					expect(req.session.loggedInEmitter).to.equal(emitter);
					expect(req.session.loggedInClient).to.equal(clientFound);

					expect(req.session.loggedInIdentifier._id).to.equal(identifierId);
					expect(req.session.loggedInIdentifier.emitters).to.equal(emitters);
					expect(req.session.loggedInIdentifier.name).to.equal(name);
					expect(req.session.loggedInIdentifier.powerUser).to.equal(powerUser);
					expect(req.session.loggedInIdentifier.god).to.equal(god);
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/authorize')];
		});
	});

	describe('switchEmitter', function (){
		var identifierService;

		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
			identifierService = require(process.cwd() + '/api/lib/identifierService');
		});

		it('Handles invalid emitter Id', function (done){
			var emitters = [],
				req = { session: { loggedInIdentifier: { emitters: emitters } } };

			tested = require(process.cwd() + '/api/lib/authorize');

			tested.switchEmitter(req, 654, function (err){
				check(done, function (){
					expect(err).to.equal('Invalid_Emitter');
				});
			});
		});

		it('Handles error while querying for identifier', function (done){
			var emitterId = 'eid',
				emitters = [{ _id: emitterId }],
				error = 'error',
				identifierId = 'iid',
				req = { session: { loggedInIdentifier: { _id: identifierId, emitters: emitters } } },
				modelMock = { identifier: { findOne: sinon.stub().yields(error) } };

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/authorize');

			tested.switchEmitter(req, emitterId, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(modelMock.identifier.findOne.calledWith(sinon.match(function(value){
						return value._id === identifierId;
					}))).to.be.true;
				});
			});
		});

		it('Handles error while linking emitters to identifier', function (done){
			var emitterId = 'eid',
				emitters = [{ _id: emitterId }],
				error = 'error',
				identifierId = 'iid',
				identifier = { _id: identifierId },
				req = { session: { loggedInIdentifier: { _id: identifierId, emitters: emitters } } },
				modelMock = { identifier: { findOne: sinon.stub().yields(null, identifier) } };

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			identifierService.linkEmitters = sinon.stub().yields(error);
			tested = require(process.cwd() + '/api/lib/authorize');

			tested.switchEmitter(req, emitterId, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(identifierService.linkEmitters.calledWith(identifier)).to.be.true;
				});
			});
		});

		it('Handles error while querying for emitter', function (done){
			var emitterId = 'eid',
				emitters = [{ _id: emitterId }],
				error = 'error',
				identifierId = 'iid',
				identifier = { _id: identifierId },
				req = { session: { loggedInIdentifier: { _id: identifierId, emitters: emitters } } },
				modelMock = { 
					identifier: { findOne: sinon.stub().yields(null, identifier) },
					emitter: { findOne: sinon.stub().yields(error) }
				};

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			identifierService.linkEmitters = sinon.stub().yields(null);
			tested = require(process.cwd() + '/api/lib/authorize');

			tested.switchEmitter(req, emitterId, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(modelMock.emitter.findOne.calledWith(sinon.match(function(value){
						return value._id === emitterId;
					}))).to.be.true;
				});
			});
		});

		it('Handles error while creating session', function (done){
			var emitterId = 'eid',
				emitters = [{ _id: emitterId }],
				emitter = {},
				error = 'error',
				identifierId = 'iid',
				identifier = { _id: identifierId },
				req = { session: { loggedInIdentifier: { _id: identifierId, emitters: emitters } } },
				modelMock = { 
					identifier: { findOne: sinon.stub().yields(null, identifier) },
					emitter: { findOne: sinon.stub().yields(null, emitter) }
				};

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			identifierService.linkEmitters = sinon.stub().yields(null);
			tested = require(process.cwd() + '/api/lib/authorize');
			tested.createSessionForEmitter = sinon.stub().yields(error)

			tested.switchEmitter(req, emitterId, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(tested.createSessionForEmitter.calledWith(req, emitter, identifier)).to.be.true;
				});
			});
		});

		it('Creates a session', function (done){
			var emitterId = 'eid',
				emitters = [{ _id: emitterId }],
				emitter = {},
				error = 'error',
				identifierId = 'iid',
				identifier = { _id: identifierId },
				req = { session: { loggedInIdentifier: { _id: identifierId, emitters: emitters } } },
				modelMock = { 
					identifier: { findOne: sinon.stub().yields(null, identifier) },
					emitter: { findOne: sinon.stub().yields(null, emitter) }
				};

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			identifierService.linkEmitters = sinon.stub().yields(null);
			tested = require(process.cwd() + '/api/lib/authorize');
			tested.createSessionForEmitter = sinon.stub().yields(null)

			tested.switchEmitter(req, emitterId, function (err){
				check(done, function (){
					expect(err).to.not.be.ok;
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/authorize')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/identifierService')];
		});
	});
});