var sinon = require('sinon'),
	expect = require('chai').expect;

describe('bizTime', function(){
	var tested,
		sampleDate = new Date(2015, 5, 4, 3, 2, 1, 456);

	before(function(){
		tested = require(process.cwd() + '/api/lib/bizTime');
		tested.now = sinon.stub().returns(sampleDate);
	});

	describe('getDateHash', function(){
		it('should return date hash', function(){
			expect(tested.getDateHash(sampleDate)).to.equal('20150504');
		});
	});
	describe('getDateTimeHash', function(){
		it('should return dateTime hash', function(){
			expect(tested.getDateTimeHash(sampleDate)).to.equal('20150504-030201456');
		});
	});
	describe('addMonths', function (){
		it('should add monthes', function (){
			expect(tested.addMonths(sampleDate, 2).valueOf()).to.equal(new Date(2015, 7, 4, 3, 2, 1, 456).valueOf());
		});
		it('should add days', function (){
			expect(tested.addDays(sampleDate, 30).valueOf()).to.equal(new Date(2015, 5, 34, 3, 2, 1, 456).valueOf());
		});
	})
	after(function(){
		delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
	});
});
