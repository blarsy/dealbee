describe('identifier service', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested;

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	chai.config.includeStack = true;

	beforeEach(function (){
		delete require.cache[require.resolve(process.cwd() + '/api/lib/identifierService')];
	});

	describe('createIdentifierAndClient', function (){
		var tested,
			models,
			bizTime,
			error = 'error';

		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
			models = {};
			clientService = {};
			bizTime = {};
			mockery.registerMock(process.cwd() + '/api/models', models);
			mockery.registerMock(process.cwd() + '/api/lib/clientService', clientService);
			mockery.registerMock(process.cwd() + '/api/lib/bizTime', bizTime);
			tested = require(process.cwd() + '/api/lib/identifierService');
		});

		it('handles error when saving new identifier', function (done){
			var identifier = {};

			models.identifier = { create: sinon.stub().yields(error) };

			tested.createIdentifierAndClient(identifier, function (err){
				check(done, function (){
					expect(error).to.equal(err);
					expect(models.identifier.create.calledWith(identifier)).to.be.true;
				});
			});
		});

		it('handles error when saving new client', function (done){
			var identifier = {},
				identifierSaved = {},
				newClientMatcher,
				now = new Date(2016, 5, 9);

			newClientMatcher  = sinon.match(function (value){
				return value.created === now;
			});

			models.identifier = { create: sinon.stub().yields(null, identifierSaved) };
			clientService.setupSubscription = sinon.stub();
			models.client = { create: sinon.stub().yields(error) };
			bizTime.now = sinon.stub().returns(now);

			tested.createIdentifierAndClient(identifier, function (err){
				check(done, function (){
					expect(error).to.equal(err);
					expect(clientService.setupSubscription.calledWith(sinon.match(function (value){
						return value.paymentFrequency === 1 && value.type === 'XS';
					}), null, newClientMatcher)).to.be.true;
					expect(models.client.create.calledWith(newClientMatcher)).to.be.true;
				});
			});
		});

		it('handles error when saving identifier linked to new client', function (done){
			var identifier = {},
				identifierSaved = { save: sinon.stub(). yields(error) },
				newClient = {},
				newClientMatcher,
				now = new Date(2016, 5, 9);

			models.identifier = { create: sinon.stub().yields(null, identifierSaved) };
			clientService.setupSubscription = sinon.stub();
			models.client = { create: sinon.stub().yields(null, newClient) };
			bizTime.now = sinon.stub().returns(now);

			tested.createIdentifierAndClient(identifier, function (err){
				check(done, function (){
					expect(error).to.equal(err);
					expect(identifierSaved.save.called).to.be.true;
				});
			});
		});

		it('Yields identifier linked to new client', function (done){
			var identifier = {},
				identifierLinked = {},
				identifierSaved = { save: sinon.stub().yields(null, identifierLinked) },
				newClient = {};

			models.identifier = { create: sinon.stub().yields(null, identifierSaved) };
			clientService.setupSubscription = sinon.stub();
			models.client = { create: sinon.stub().yields(null, newClient) };
			bizTime.now = sinon.stub().returns(now);

			tested.createIdentifierAndClient(identifier, function (err, yielded){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(identifierSaved.save.called).to.be.true;
					expect(identifierSaved.client).to.equal(newClient);
					expect(yielded).to.equal(identifierLinked);
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/identifierService')];
		});	
	});

	describe('create', function (){
		var facebook,
			hash,
			models;
		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
			facebook = {};
			hasher = {};
			models = {};
			mockery.registerMock(process.cwd() + '/api/lib/facebook', facebook);
			mockery.registerMock(process.cwd() + '/api/lib/hasher', hasher);
			mockery.registerMock(process.cwd() + '/api/models', models);
			tested = require(process.cwd() + '/api/lib/identifierService');
		});

		it('handles error while making facebook identifier', function (done){
			var error = 'error',
				fbToken = 'fbToken',
				email = 'email',
				identifierInfo = { fbToken: fbToken, email: email };

			facebook.makeIdentifier = sinon.stub().yields(error);

			tested.create(identifierInfo, function (err, identifierSaved){
				check(done, function (){
					expect(err).to.equal(error);
					expect(facebook.makeIdentifier.calledWith(fbToken, email)).to.be.true;
				});
			});
		});

		it('handles error while querying for duplicate facebook identifier', function (done){
			var error = 'error',
				fbToken = 'fbToken',
				fbUid = 'fbUid',
				email = 'email',
				identifierInfo = { fbToken: fbToken, email: email };

			models.identifier = { findOne: sinon.stub().yields(error) };
			facebook.makeIdentifier = sinon.stub().yields(null, { fbUid: fbUid });

			tested.create(identifierInfo, function (err, identifierSaved){
				check(done, function (){
					expect(err).to.equal(error);
					expect(models.identifier.findOne.calledWith(sinon.match(function(value){
						return value.type === 0 && value.fbUid === fbUid;
					}))).to.be.true;
				});
			});
		});

		it('returns existing facebook identifier', function (done){
			var identifierFound = {},
				fbToken = 'fbToken',
				fbUid = 'fbUid',
				email = 'email',
				identifierInfo = { fbToken: fbToken, email: email };

			models.identifier = { findOne: sinon.stub().yields(null, identifierFound) };
			facebook.makeIdentifier = sinon.stub().yields(null, { fbUid: fbUid });

			tested.create(identifierInfo, function (err, identifierSaved){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(identifierSaved).to.equal(identifierFound);
				});
			});
		});

		it('creates identifier and client for facebook identifier', function (done){
			var fbToken = 'fbToken',
				fbUid = 'fbUid',
				email = 'email',
				identifierInfo = { fbToken: fbToken, email: email },
				identifierCreated = { fbUid: fbUid },
				identifierSaved = {};

			models.identifier = { findOne: sinon.stub().yields(null, null) };
			facebook.makeIdentifier = sinon.stub().yields(null, identifierCreated);
			tested.createIdentifierAndClient = sinon.stub().yields(null, identifierSaved);

			tested.create(identifierInfo, function (err, identifier){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(identifier).to.equal(identifierSaved);
					expect(tested.createIdentifierAndClient.calledWith(identifierCreated)).to.be.true;
				});
			});
		});

		it('handles error while finding duplicate identifier', function (done){
			var error = 'error',
				email = 'email',
				password = 'password',
				identifierInfo = { password: password, email: email };

			models.identifier = { findOne: sinon.stub().yields(error) };

			tested.create(identifierInfo, function (err, identifierSaved){
				check(done, function (){
					expect(err).to.equal(error);
					expect(models.identifier.findOne.calledWith(sinon.match(function (value){
						return value.email === email;
					}))).to.be.true;
				});
			});
		});

		it('handles error while making email identifier', function (done){
			var email = 'email',
				password = 'password',
				duplicateIdentifier = {},
				identifierInfo = { password: password, email: email };

			models.identifier = { findOne: sinon.stub().yields(null, duplicateIdentifier) };

			tested.create(identifierInfo, function (err, identifierSaved){
				check(done, function (){
					expect(err).to.equal('Email_Already_Used');
				});
			});
		});

		it('handles error while hashing the password', function (done){
			var error = 'error',
				email = 'email',
				password = 'password',
				identifierInfo = { password: password, email: email };

			models.identifier = { findOne: sinon.stub().yields(null, null) };
			hasher.hash = sinon.stub().yields(error);

			tested.create(identifierInfo, function (err, identifierSaved){
				check(done, function (){
					expect(err).to.equal(error);
					expect(hasher.hash.calledWith(password, email)).to.be.true;
				});
			});
		});

		it('creates identifier and client for email identifier', function (done){
			var error = 'error',
				email = 'email',
				password = 'password',
				identifierInfo = { password: password, email: email },
				identifierCreated = {},
				identifierSaved = {};

			models.identifier = { findOne: sinon.stub().yields(null, null) };
			hasher.hash = sinon.stub().yields(null, identifierCreated);
			tested.createIdentifierAndClient = sinon.stub().yields(null, identifierCreated);

			tested.create(identifierInfo, function (err, identifierSaved){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(tested.createIdentifierAndClient.calledWith(identifierCreated)).to.be.true;
					expect(identifierSaved).to.equal(identifierSaved);
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/identifierService')];
		});
	});

	describe('ensureHasDefaultEmitter', function (){
		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
		});

		it('handles error while querying for logged in identifier', function (done){
			var identifierId = 123,
				error = 'error',
				modelMock = { identifier: { findOne: sinon.stub().yields(error) } };

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/identifierService');

			tested.ensureHasDefaultEmitter(identifierId, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(modelMock.identifier.findOne.calledWith(sinon.match(function (value){
						return value._id === identifierId;
					}))).to.be.true;
				});
			});
		});

		it('yields no error when identifier has a default emitter', function (done){
			var identifierId = 123,
				identifier = { defaultEmitter: 'bla' },
				modelMock = { identifier: { findOne: sinon.stub().yields(null, identifier) } };

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/identifierService');

			tested.ensureHasDefaultEmitter(identifierId, function (err){
				check(done, function (){
					expect(err).to.not.be.ok;
				});
			});
		});

		it('handles error while querying emitters linked to the identity', function (done){
			var identifierId = 123,
				error = 'error',
				identifier = {},
				modelMock = { 
					identifier: { findOne: sinon.stub().yields(null, identifier) },
					emitter: { findOne: sinon.stub().yields(error) }
				};

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/identifierService');

			tested.ensureHasDefaultEmitter(identifierId, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(modelMock.emitter.findOne.calledWith(sinon.match(function (value){
						return value.identifiers === identifierId;
					}))).to.be.true;
				});
			});
		});

		it('handles error while saving identity', function (done){
			var identifierId = 123,
				error = 'error',
				identifier = { save: sinon.stub().yields(error) },
				modelMock = { 
					identifier: { findOne: sinon.stub().yields(null, identifier) },
					emitter: { findOne: sinon.stub().yields(null, { _id: 1234 }) }
				};

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/identifierService');

			tested.ensureHasDefaultEmitter(identifierId, function (err){
				check(done, function (){
					expect(err).to.equal(error);
					expect(identifier.save.called).to.be.true;
				});
			});
		});

		it('yields no error after setting identifier default emitter', function (done){
			var identifierId = 123,
				identifier = { save: sinon.stub().yields(null) },
				modelMock = { 
					identifier: { findOne: sinon.stub().yields(null, identifier) },
					emitter: { findOne: sinon.stub().yields(null, { _id: 1234 }) }
				};

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/identifierService');

			tested.ensureHasDefaultEmitter(identifierId, function (err){
				check(done, function (){
					expect(err).to.not.be.ok;
				});
			});
		});


		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/identifierService')];
		});
	});

	describe('makeDto', function (){
		it('returns a dto without name', function (){
			var email = 'email',
				type = 2,
				id = 'id';

			tested = require(process.cwd() + '/api/lib/identifierService');

			var result = tested.makeDto({ email: email, type: type, _id: id });

			expect(result._id).to.equal(id);
			expect(result.email).to.equal(email);
			expect(result.type).to.equal(type);
		});

		it('returns a dto with emitters', function (){
			var email = 'email',
				type = 2,
				id = 'id'
				idEmitter1 = 'em1',
				idEmitter2 = 'em2',
				nameEmitter1 = 'name1',
				nameEmitter2 = 'name2',
				emitters = [ { _id: idEmitter1, name: nameEmitter1 }, { _id: idEmitter2, name: nameEmitter2 } ];

			tested = require(process.cwd() + '/api/lib/identifierService');

			var result = tested.makeDto({ email: email, type: type, emitters: emitters, _id: id });

			expect(result.email).to.equal(email);
			expect(result._id).to.equal(id);
			expect(result.type).to.equal(type);
			expect(result.emitters.length).to.equal(2);
			expect(result.emitters[0]._id).to.equal(idEmitter1);
			expect(result.emitters[1]._id).to.equal(idEmitter2);
			expect(result.emitters[0].name).to.equal(nameEmitter1);
			expect(result.emitters[1].name).to.equal(nameEmitter2);
		});

		it('returns a dto with name', function (){
			var email = 'email',
				name = 'name',
				type = 2;

			tested = require(process.cwd() + '/api/lib/identifierService');

			var result = tested.makeDto({ email: email, name: name, type: type });

			expect(result.email).to.equal(email);
			expect(result.type).to.equal(type);
			expect(result.name).to.equal(name);
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/identifierService')];
		});
	});

	describe('linkEmitters', function (){
		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
		});

		it('handles error while finding emitters', function (){
			var error = 'error',
				leanQueryResult = { exec: sinon.stub().yields(error) },
				emitterQueryResult = { lean: sinon.stub().returns(leanQueryResult) },
				modelMock = { emitter: { find: sinon.stub().returns(emitterQueryResult) } },
				identifierId = 123,
				identifier = { _id: identifierId };
			
			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/identifierService');

			tested.linkEmitters(identifier, function (err){
				check(function(){
					expect(err).to.equal(error);
					expect(modelMock.emitter.find.calledWith(sinon.match(function (value){
						return value.identifiers === identifierId;
					})));
					expect(emitterQueryResult.lean.called).to.be.true;
					expect(leanQueryResult.exec.called).to.be.true;
				});
			});
		});

		it('sets emitters on the identifier', function (){
			var error = 'error',
				emitters = [],
				leanQueryResult = { exec: sinon.stub().yields(null, emitters) },
				emitterQueryResult = { lean: sinon.stub().returns(leanQueryResult) },
				modelMock = { emitter: { find: sinon.stub().returns(emitterQueryResult) } },
				identifierId = 123,
				identifier = { _id: identifierId };
			
			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			tested = require(process.cwd() + '/api/lib/identifierService');

			tested.linkEmitters(identifier, function (err){
				check(function(){
					expect(err).to.not.be.ok;
					expect(identifier.emitters).to.equal(emitters);
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/identifierService')];
		});
	});
});