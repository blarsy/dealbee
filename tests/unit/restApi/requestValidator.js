describe('requestValidator', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested;

	chai.config.includeStack = true;

	beforeEach(function (){
		tested = require(process.cwd() + '/api/lib/requestValidator')
	});

	describe('order post', function (){
		it('signals missing subscription and packs', function (){
			var msg = tested.orderPost({ body: {}});
			expect(msg).to.equal('Subscription_Or_Pack_Required');
		});
		it('signals missing subscription type', function (){
			var msg = tested.orderPost({ body: { subscription: { paymentFrequency: 2 }}});
			expect(msg).to.equal('Subscription_Type_Required');
		});
		it('signals missing subscription paymentFrequency', function (){
			var msg = tested.orderPost({ body: { subscription: { type: 'MD'}}});
			expect(msg).to.equal('Subscription_PaymentFrequency_Required');
		});
		it('signals unexpected subscription paymentFrequency', function (){
			var msg = tested.orderPost({ body: { subscription: { type: 'SM', paymentFrequency: 2 }}});
			expect(msg).to.equal('Invalid_PaymentFrequency');
			msg = tested.orderPost({ body: { subscription: { type: 'SM', paymentFrequency: 5 }}});
			expect(msg).to.equal('Invalid_PaymentFrequency');
			msg = tested.orderPost({ body: { subscription: { type: 'SM', paymentFrequency: 7 }}});
			expect(msg).to.equal('Invalid_PaymentFrequency');
			msg = tested.orderPost({ body: { subscription: { type: 'SM', paymentFrequency: 11 }}});
			expect(msg).to.equal('Invalid_PaymentFrequency');
			msg = tested.orderPost({ body: { subscription: { type: 'SM', paymentFrequency: 13 }}});
			expect(msg).to.equal('Invalid_PaymentFrequency');
		});
		it('allows valid subscription', function (){
			msg = tested.orderPost({ body: { subscription: { type: 'SM', paymentFrequency: 1 }}});
			expect(msg).to.be.null;
			msg = tested.orderPost({ body: { subscription: { type: 'SM', paymentFrequency: 6 }}});
			expect(msg).to.be.null;
			msg = tested.orderPost({ body: { subscription: { type: 'SM', paymentFrequency: 12 }}});
			expect(msg).to.be.null;
		});
		it('signals packs when not an array', function (){
			msg = tested.orderPost({ body: { packs: 'bla'}});
			expect(msg).to.equal('Expected_Array_Of_Packs');
		});
		it('signals missing pack quantity', function (){
			msg = tested.orderPost({ body: { packs: [ { type: 'SM' } ]}});
			expect(msg).to.equal('Pack_Quantity_Required');
		});
		it('signals missing pack type', function (){
			msg = tested.orderPost({ body: { packs: [ { type: 'SM', quantity: 23 }, { quantity: 2 } ]}});
			expect(msg).to.equal('Pack_Type_Required');
		});
		it('allow valid pack array', function (){
			msg = tested.orderPost({ body: { packs: [ { type: 'SM', quantity: 23 }, { quantity: 2, type: 'MD' } ]}});
			expect(msg).to.be.null;
		});
	});

	describe('client invoicing data', function (){
		var adate = new Date();
		it('signals missing invoicing address', function (){
			msg = tested.invoicingDataPost({ hasNoVATNumber: true });
			expect(msg).to.equal('InvoicingAddress_Required');
		});
		it('signals missing company name', function (){
			msg = tested.invoicingDataPost({ address: { street: 'bla', postcode: 'bla', city: 'bla', created: adate }, hasNoVATNumber: true });
			expect(msg).to.equal('CompanyName_required');
		});
		it('signals missing language', function (){
			msg = tested.invoicingDataPost({ address: { street: 'bla', postcode: 'bla', city: 'bla', created: adate }, hasNoVATNumber: true, companyName: 'bla', juridicalForm: 'bla' });
			expect(msg).to.equal('InvoicingLanguage_required');
		});
		it('signals missing invoicing address street', function (){
			msg = tested.invoicingDataPost({ address: { postcode: 'bla', city: 'bla', created: adate }, hasNoVATNumber: true, companyName: 'bla', juridicalForm: 'bla', language: 'bla' });
			expect(msg).to.equal('InvoicingAddress_Street_required');
		});
		it('signals missing invoicing address postcode', function (){
			msg = tested.invoicingDataPost({ address: { street: 'bla', city: 'bla', created: adate }, hasNoVATNumber: true, companyName: 'bla', juridicalForm: 'bla', language: 'bla' });
			expect(msg).to.equal('InvoicingAddress_Postcode_required');
		});
		it('signals missing invoicing address city', function (){
			msg = tested.invoicingDataPost({ address: { postcode: 'bla', street: 'bla', created: adate }, hasNoVATNumber: true, companyName: 'bla', juridicalForm: 'bla', language: 'bla' });
			expect(msg).to.equal('InvoicingAddress_City_required');
		});
		it('signals missing VAT number', function (){
			msg = tested.invoicingDataPost({ address: { postcode: 'bla', street: 'bla', city: 'bla', created: adate }, VATCountryCode: 'ABC', companyName: 'bla', juridicalForm: 'bla', language: 'bla' });
			expect(msg).to.equal('VATNumber_Required');
		});
		it('signals missing VAT country code', function (){
			msg = tested.invoicingDataPost({ address: { postcode: 'bla', street: 'bla', city: 'bla', created: adate }, VATNumber: '12345678901', companyName: 'bla', juridicalForm: 'bla', language: 'bla' });
			expect(msg).to.equal('VATCountryCode_Required');
		});
		it('allows client with invoicing address and VAT', function (){
			msg = tested.invoicingDataPost({ address: { postcode: 'bla', street: 'bla', city: 'bla', created: adate }, VATNumber: '12345', VATCountryCode: 'ABC', companyName: 'bla', juridicalForm: 'bla', language: 'bla' });
			expect(msg).to.be.null;
		});
	});

	describe('emitter post', function (){
		it('signals missing name', function (){
			msg = tested.emitterPost({ body: {} });
			expect(msg).to.equal('Name_required');
		});
		it('signals name too short', function (){
			msg = tested.emitterPost({ body: { name: '1' } });
			expect(msg).to.equal('Name_policy_failed');
		});
		it('signals missing street', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: {} } });
			expect(msg).to.equal('Street_required');
		});
		it('signals missing postcode', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: { street: 'street' } } });
			expect(msg).to.equal('Postcode_required');
		});
		it('signals missing city', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: { street: 'street', postcode: '1234' } } });
			expect(msg).to.equal('City_required');
		});
		it('signals missing longitude', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: { street: 'street', postcode: '1234', city: 'city', useMap: true, coords: [] } } });
			expect(msg).to.equal('Lng_required');
		});
		it('signals invalid longitude', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: { street: 'street', postcode: '1234', city: 'city', useMap: true, coords: ['bla'] } } });
			expect(msg).to.equal('Lng_Invalid');
		});
		it('signals missing latitude', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: { street: 'street', postcode: '1234', city: 'city', useMap: true, coords: ['1.23'] } } });
			expect(msg).to.equal('Lat_required');
		});
		it('signals invalid latitude', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: { street: 'street', postcode: '1234', city: 'city', useMap: true, coords: ['1.23', 'bla'] } } });
			expect(msg).to.equal('Lat_Invalid');
		});
		it('signals missing coordinates', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: { street: 'street', postcode: '1234', city: 'city', useMap: true } } });
			expect(msg).to.equal('Lng_required');
		});
		it('allows only a name', function (){
			msg = tested.emitterPost({ body: { name: 'name' } });
			expect(msg).to.be.null;
		});
		it('allows an address without map', function (){
			msg = tested.emitterPost({ body: { name: 'name', address: { street: 'street', postcode: '1234', city: 'city', useMap: false } } });
			expect(msg).to.be.null;
		});
	});

	describe('identifier post', function (){
		it('signals missing email', function (){
			msg = tested.identifierPost({ body: {} });
			expect(msg).to.equal('Email_required');
		});
		it('signals missing email, even if fbToken provided', function (){
			msg = tested.identifierPost({ body: { fbToken: 'token' } });
			expect(msg).to.equal('Email_required');
		});
		it('signals missing password', function (){
			msg = tested.identifierPost({ body: { email: 'email'} });
			expect(msg).to.equal('Password_required');
		});
		it('signals password too short', function (){
			msg = tested.identifierPost({ body: { email: 'email', password: '123456789' } });
			expect(msg).to.equal('Password_policy_failed');
		});
		it('allows valid email and password', function (){
			msg = tested.identifierPost({ body: { email: 'email', password: '1234567890' } });
			expect(msg).to.not.be.ok;
		});
		it('allows valid fbToken', function (){
			msg = tested.identifierPost({ body: { email: 'email', fbToken: 'token' } });
			expect(msg).to.not.be.ok;
		});
	});

	afterEach(function (){
		delete require.cache[require.resolve(process.cwd() + '/api/lib/requestValidator')];
	});

});