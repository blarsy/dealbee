describe('scraper', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested;

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	chai.config.includeStack = true;

	describe('getScrapeInfo', function (){
		it('detects "viewdeal/onrequest" route', function (){
			var req1 = { 
					url: '/otherroute/blabla',
					headers: { 'user-agent': 'facebookexternalhit/1.1 (+http(s)://www.facebook.com/externalhit_uatext.php)' } 
				},
				req2 = { 
					url: '/viewdeal/onrequest',
					headers: { 'user-agent': 'facebookexternalhit/1.1 (+http(s)://www.facebook.com/externalhit_uatext.php)' } 
				},
				req3 = { 
					url: '/viewdeal/onrequest/',
					headers: { 'user-agent': 'facebookexternalhit/1.1 (+http(s)://www.facebook.com/externalhit_uatext.php)' } 
				};

			tested = require(process.cwd() + '/api/lib/scraper');

			var result = tested.getScrapeInfo(req1);

			expect(result.type).to.equal('facebook');
			expect(result.dealId).to.be.undefined;

			result = tested.getScrapeInfo(req2);

			expect(result.type).to.equal('facebook');
			expect(result.dealId).to.be.undefined;

			result = tested.getScrapeInfo(req3);

			expect(result.type).to.equal('facebook');
			expect(result.dealId).to.be.undefined;
		});

		it('detects "viewdeal/:id" route', function (){
			var req1 = { 
					url: '/viewdeal/blabla',
					headers: { 'user-agent': 'facebookexternalhit/1.1 (+http(s)://www.facebook.com/externalhit_uatext.php)' } 
				},
				req2 = { 
					url: '/viewdeal/',
					headers: { 'user-agent': 'facebookexternalhit/1.1 (+http(s)://www.facebook.com/externalhit_uatext.php)' } 
				},
				req3 = { 
					url: '/viewdeal',
					headers: { 'user-agent': 'facebookexternalhit/1.1 (+http(s)://www.facebook.com/externalhit_uatext.php)' } 
				};

			tested = require(process.cwd() + '/api/lib/scraper');

			var result = tested.getScrapeInfo(req1);

			expect(result.type).to.equal('facebook');
			expect(result.dealId).to.equal('blabla');
			expect(result.onRequest).to.be.false;

			result = tested.getScrapeInfo(req2);

			expect(result.type).to.equal('facebook');
			expect(result.dealId).to.be.undefined;

			result = tested.getScrapeInfo(req3);

			expect(result.type).to.equal('facebook');
			expect(result.dealId).to.be.undefined;
		});

		it('identifies Facebook scraper', function (){
			var id = '123',
				req = { 
					url: '/viewdeal/onrequest/' + id,
					headers: { 'user-agent': 'facebookexternalhit/1.1 (+http(s)://www.facebook.com/externalhit_uatext.php)' } 
				};

			tested = require(process.cwd() + '/api/lib/scraper');

			var result = tested.getScrapeInfo(req);

			expect(result.type).to.equal('facebook');
			expect(result.dealId).to.equal(id);
			expect(result.onRequest).to.be.true;
		});

		it('identifies Google bot', function (){
			var id = '123',
				req = { 
					url: '/viewdeal/onrequest/' + id,
					headers: { 'user-agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)' } 
				};

			tested = require(process.cwd() + '/api/lib/scraper');

			var result = tested.getScrapeInfo(req);

			expect(result.type).to.equal('searchengine');
			expect(result.dealId).to.equal(id);
		});

		it('identifies Bing bot', function (){
			var id = '123',
				req = { 
						url: '/viewdeal/onrequest/' + id,
					headers: { 'user-agent': 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)' } 
				};

			tested = require(process.cwd() + '/api/lib/scraper');

			var result = tested.getScrapeInfo(req);

			expect(result.type).to.equal('searchengine');
			expect(result.dealId).to.equal(id);
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/scraper')];
		});
	});

	describe('scrape', function (){
		var facebook;

		beforeEach(function (){
			facebook = require(process.cwd() + '/api/lib/facebook');
			seo = require(process.cwd() + '/api/lib/seo');
		});

		it('scrapes a deal for Facebook', function (done){
			var dealId = 123,
				req = {},
				dealContent = 'dealContent';

			facebook.scrapeDeal = sinon.stub().yields(null, dealContent);
			tested = require(process.cwd() + '/api/lib/scraper');

			tested.scrape({ dealId: dealId, type: 'facebook' }, req, function (err, content){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(content).to.equal(dealContent);
					expect(facebook.scrapeDeal.calledWith(req, dealId)).to.be.true;
				});
			});
		});

		it('scrapes a deal on request for Facebook', function (done){
			var dealId = 123,
				req = {},
				dealContent = 'dealContent';

			facebook.scrapeDealOnRequest = sinon.stub().yields(null, dealContent);
			tested = require(process.cwd() + '/api/lib/scraper');

			tested.scrape({ dealId: dealId, type: 'facebook', onRequest: true }, req, function (err, content){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(content).to.equal(dealContent);
					expect(facebook.scrapeDealOnRequest.calledWith(req, dealId)).to.be.true;
				});
			});
		});

		it('scrapes the home page for Facebook', function (done){
			var req = {},
				dealContent = 'dealContent';

			facebook.scrapeHomePage = sinon.stub().yields(null, dealContent);
			tested = require(process.cwd() + '/api/lib/scraper');

			tested.scrape({ type: 'facebook' }, req, function (err, content){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(content).to.equal(dealContent);
					expect(facebook.scrapeHomePage.calledWith(req)).to.be.true;
				});
			});
		});

		it('scrapes a deal for search engines', function (done){
			var dealId = 123,
				req = {},
				dealContent = 'dealContent';

			seo.scrapeDeal = sinon.stub().yields(null, dealContent);
			tested = require(process.cwd() + '/api/lib/scraper');

			tested.scrape({ dealId: dealId, type: 'searchengine' }, req, function (err, content){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(content).to.equal(dealContent);
					expect(seo.scrapeDeal.calledWith(req, dealId)).to.be.true;
				});
			});
		});

		it('scrapes the home page for search engines', function (done){
			var req = {},
				dealContent = 'dealContent';

			seo.scrapeHomePage = sinon.stub().yields(null, dealContent);
			tested = require(process.cwd() + '/api/lib/scraper');

			tested.scrape({ type: 'searchengine' }, req, function (err, content){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(content).to.equal(dealContent);
					expect(seo.scrapeHomePage.calledWith(req)).to.be.true;
				});
			});
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/seo')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/facebook')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/scraper')];
		});
	});
});