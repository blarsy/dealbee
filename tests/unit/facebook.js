describe('login', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested;

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	chai.config.includeStack = true;

	describe('getIdAndName', function (done){
		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
			delete require.cache[require.resolve(process.cwd() + '/api/lib/facebook')];
		});

		it('handles fb api error', function (done){
			var error = 'error',
				fbMock = { api: sinon.stub().yields({ error: error }) },
				token = 'token';

			mockery.registerMock('fb', fbMock);

			tested = require(process.cwd() + '/api/lib/facebook');

			tested.getIdAndName(token, function (err){
				check(done, function (){
					expect(err).to.equal('ErrorContactingFacebook ' + JSON.stringify(error));
					expect(fbMock.api.calledWith('me', sinon.match(function(value){
						return value.fields[0] === 'id' && value.fields[1] === 'name' &&
							value.access_token === token;
					})));
				});
			});
		});

		it('handles no response from fb api', function (done){
			var fbMock = { api: sinon.stub().yields(null) },
				token = 'token';

			mockery.registerMock('fb', fbMock);

			tested = require(process.cwd() + '/api/lib/facebook');

			tested.getIdAndName(token, function (err){
				check(done, function (){
					expect(err).to.equal('No response from Facebook');
				});
			});
		});

		it('return fb info', function (done){
			var id = 'id',
				name = 'name',
				fbMock = { api: sinon.stub().yields({ id: id, name: name }) },
				token = 'token';

			mockery.registerMock('fb', fbMock);

			tested = require(process.cwd() + '/api/lib/facebook');

			tested.getIdAndName(token, function (err, result){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(result.id).to.equal(id);
					expect(result.name).to.equal(name);
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/facebook')];
		});
	});


});