describe('emitter service', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested;

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	chai.config.includeStack = true;

	describe('makeClientObject', function (){
		var identifierService, clientService;
		beforeEach(function (){
			mockery.enable({ warnOnUnregistered: false });
			identifierService = {};
			clientService = {};
			mockery.registerMock(process.cwd() + '/api/lib/identifierService', identifierService);
			mockery.registerMock(process.cwd() + '/api/lib/clientService', clientService);
		});

		it('makes the object', function (){
			var tested = require(process.cwd() + '/api/lib/emitterService'),
				emitter = {},
				identifier = {},
				emitterDto = {},
				client = {},
				identifierDto = {},
				clientDto = {};

			tested.makeDto = sinon.stub().returns(emitterDto);
			identifierService.makeDto = sinon.stub().returns(identifierDto);
			clientService.makeDto = sinon.stub().returns(clientDto);
			
			var result = tested.makeClientObject(emitter, identifier, client);

			expect(result.emitter).to.equal(emitterDto);
			expect(result.client).to.equal(clientDto);
			expect(result.identifier).to.equal(identifierDto);
			expect(tested.makeDto.calledWith(emitter)).to.be.true;
			expect(identifierService.makeDto.calledWith(identifier)).to.be.true;
			expect(clientService.makeDto.calledWith(client)).to.be.true;
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/emitterService')];
		});
	});
});