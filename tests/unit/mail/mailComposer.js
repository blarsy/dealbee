describe('mailComposer', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested,
		error = 'error',
		resMock = {};

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	chai.config.includeStack = true;

	describe('composeAndSend', function (){
		var tested, templateApplierMock, mailSenderMock;

		beforeEach(function (){
			templateApplierMock = {};
			mailSenderMock = {};
			delete require.cache[require.resolve(process.cwd() + '/api/lib/mailassets/mailComposer')];
			mockery.enable({ warnOnUnregistered: false });
			mockery.registerMock(process.cwd() + '/api/lib/templateApplier', templateApplierMock);
			mockery.registerMock(process.cwd() + '/api/res.json', resMock);
			mockery.registerMock(process.cwd() + '/api/lib/mailassets/mailSender', mailSenderMock);
			tested = require(process.cwd() + '/api/lib/mailassets/mailComposer');
		});

		it('handles error while applying html template', function (done){
			var from = 'from', to = 'to', subject = 'subject', htmlTemplate = 'htmlTemplate',
				textTemplate = 'textTemplate', resourceSetName = 'resourceSetName', lang = 'lang',
				mailData = {};

			templateApplierMock.createFromTemplate = sinon.stub().yields(error);

			tested.composeAndSend(from, to, subject, htmlTemplate, textTemplate, mailData, resourceSetName, lang, function(err){
				check(done, function (){
					expect(templateApplierMock.createFromTemplate.calledWith(htmlTemplate, lang, mailData, resMock, resourceSetName)).to.be.true;
					expect(err).to.equal(error);				
				});
			});
		});

		it('handles exception thrown while applying html template', function (done){
			var from = 'from', to = 'to', subject = 'subject', htmlTemplate = 'htmlTemplate',
				textTemplate = 'textTemplate', resourceSetName = 'resourceSetName', lang = 'lang',
				mailData = {}, exception = new Error(error);

			templateApplierMock.createFromTemplate = sinon.stub().throws(exception);

			tested.composeAndSend(from, to, subject, htmlTemplate, textTemplate, mailData, resourceSetName, lang, function(err){
				check(done, function (){
					expect(templateApplierMock.createFromTemplate.calledWith(htmlTemplate, lang, mailData, resMock, resourceSetName)).to.be.true;
					expect(err).to.equal(exception);				
				});
			});
		});


		it('handles error while applying text template', function (done){
			var from = 'from', to = 'to', subject = 'subject', htmlTemplate = 'htmlTemplate',
				textTemplate = 'textTemplate', resourceSetName = 'resourceSetName', lang = 'lang',
				mailData = {};

			templateApplierMock.createFromTemplate = sinon.stub();
			templateApplierMock.createFromTemplate.onFirstCall().yields(null, 'dummy');
			templateApplierMock.createFromTemplate.onSecondCall().yields(error);

			tested.composeAndSend(from, to, subject, htmlTemplate, textTemplate, mailData, resourceSetName, lang, function(err){
				check(done, function (){
					expect(templateApplierMock.createFromTemplate.getCall(1).calledWith(textTemplate, lang, mailData, resMock, resourceSetName)).to.be.true;
					expect(err).to.equal(error);				
				});
			});
		});

		it('handles exception thrown while applying text template', function (done){
			var from = 'from', to = 'to', subject = 'subject', htmlTemplate = 'htmlTemplate',
				textTemplate = 'textTemplate', resourceSetName = 'resourceSetName', lang = 'lang',
				mailData = {}, exception = new Error(error);

			templateApplierMock.createFromTemplate = sinon.stub();
			templateApplierMock.createFromTemplate.onFirstCall().yields(null, 'dummy');
			templateApplierMock.createFromTemplate.onSecondCall().throws(exception);

			tested.composeAndSend(from, to, subject, htmlTemplate, textTemplate, mailData, resourceSetName, lang, function(err){
				check(done, function (){
					expect(templateApplierMock.createFromTemplate.getCall(1).calledWith(textTemplate, lang, mailData, resMock, resourceSetName)).to.be.true;
					expect(err).to.equal(exception);				
				});
			});
		});

		it('handles exception thrown while sending the mail', function (done){
			var from = 'from', to = 'to', subject = 'subject', htmlTemplate = 'htmlTemplate',
				textTemplate = 'textTemplate', resourceSetName = 'resourceSetName', lang = 'lang',
				mailData = {}, exception = new Error(error);

			templateApplierMock.createFromTemplate = sinon.stub();
			templateApplierMock.createFromTemplate.onFirstCall().yields(null, 'dummy');
			templateApplierMock.createFromTemplate.onSecondCall().yields(null, 'dummy');
			mailSenderMock.send = sinon.stub().throws(exception);

			tested.composeAndSend(from, to, subject, htmlTemplate, textTemplate, mailData, resourceSetName, lang, function(err){
				check(done, function (){
					expect(err).to.equal(exception);				
				});
			});
		});

		it('Sends the mail', function (done){
			var from = 'from', to = 'to', subject = 'subject', htmlTemplate = 'htmlTemplate',
				textTemplate = 'textTemplate', resourceSetName = 'resourceSetName', lang = 'lang',
				mailData = {}, exception = new Error(error), htmlContent = 'htmlContent', 
				textContent = 'textContent';

			templateApplierMock.createFromTemplate = sinon.stub();
			templateApplierMock.createFromTemplate.onFirstCall().yields(null, htmlContent);
			templateApplierMock.createFromTemplate.onSecondCall().yields(null, textContent);
			mailSenderMock.send = sinon.stub().yields(null);

			tested.composeAndSend(from, to, subject, htmlTemplate, textTemplate, mailData, resourceSetName, lang, function(err){
				check(done, function (){
					expect(mailSenderMock.send.calledWith(sinon.match(function (value){
						return value.from === from && value.to === to && value.subject === subject &&
							value.html === htmlContent && value.text === textContent;
					}))).to.be.true;				
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
		});
	});
});