describe('subscriptionRenewer', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		tested,
		bizTime,
		joblogs;

	chai.config.includeStack = true;

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	describe('execute', function (){
		it('gathers ending subscriptions, charges and renews them', function (done){
			var endingSubscriptions = [],
				chargeResult = { nbRenewed: 10, nbChargeFailed: 20 },
				callback = sinon.stub();

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findEndingSubscriptions = sinon.stub().yields(null, endingSubscriptions);
			tested.chargeAndRenewSubscriptions = sinon.stub().yields(null, chargeResult);

			tested.execute(callback);

			expect(tested.findEndingSubscriptions.calledOnce).to.be.true;
			expect(tested.chargeAndRenewSubscriptions.calledWith(endingSubscriptions)).to.be.true;
			expect(callback.calledWith(null, 10, 20)).to.be.true;

			done();
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/subscriptionRenewer')];
		});
	});

	describe('findLatestPack', function (){
		it('return null when no pack of the right subscription kind', function (){
			var client = {
				subscription: { kind: 'SM' }, packs: [
						{ end: new Date(2015, 0, 12), subscriptionKind: 'XS' }, 
						{ end: new Date(2015, 0, 14), subscriptionKind: 'MD' }
				]
			};

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			expect(tested.findLatestPack(client)).to.be.null;
		});

		it('return the latest pack of the right subscription kind', function (){
			var latestPack = { end: new Date(2015, 2, 14), subscriptionKind: 'SM' },
				client = {
					subscription: { kind: 'SM' }, packs: [
						{ end: new Date(2015, 0, 12), subscriptionKind: 'XS' }, 
						{ end: new Date(2015, 3, 14), subscriptionKind: 'MD' },
						{ end: new Date(2015, 1, 14), subscriptionKind: 'SM' },
						latestPack
					]
				};

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			expect(tested.findLatestPack(client)).to.equal(latestPack);
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/subscriptionRenewer')];
		});
	});

	describe('gather ending subscriptions', function (){
		beforeEach(function (){
			bizTime = require(process.cwd() + '/api/lib/bizTime');
			joblogs = require(process.cwd() + '/api/lib/jobs/joblogs');
			mockery.enable({ warnOnUnregistered: false });
		});

		it('gathers ending subscriptions', function (done){
			var clientWithoutPack = { _id: 'dummy', subscription: { kind: 'XS' } },
				clientWithPackExpiringSoon = { _id: 'id', subscription: { kind: 'SM' } },
				clientWithPackExpiringIn10Days = { _id: 'dummy2', subscription: { kind: 'SM' } },
				clientWithPackExpiringSoonAndFailedChargeInLast24Hours = { _id: 'dummy3', subscription: { kind: 'MD', latestChargeFailure: new Date(2015, 0, 11, 12, 0, 0) } },
				clientWithPackExpiringSoonAndFailedChargeNotInLast24Hours = { _id: 'id2', subscription: { kind: 'MD', latestChargeFailure: new Date(2015, 0, 10) } },
				clients = [
					clientWithoutPack,
					clientWithPackExpiringSoon,
					clientWithPackExpiringIn10Days,
					clientWithPackExpiringSoonAndFailedChargeInLast24Hours,
					clientWithPackExpiringSoonAndFailedChargeNotInLast24Hours
				],
				now = new Date(2015,0,12),
				modelMock = { client: { find: sinon.stub().yields(null, clients) } };

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			bizTime.now = sinon.stub().returns(now);

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findLatestPack = sinon.stub();
			tested.findLatestPack.withArgs(clientWithoutPack).returns(null);
			tested.findLatestPack.withArgs(clientWithPackExpiringSoon).returns({ end: new Date(2015, 0, 14, 23, 59, 0) });
			tested.findLatestPack.withArgs(clientWithPackExpiringIn10Days).returns({ end: new Date(2015, 0, 22) });
			tested.findLatestPack.withArgs(clientWithPackExpiringSoonAndFailedChargeInLast24Hours).returns({ end: new Date(2015, 0, 14, 23, 59, 0) });
			tested.findLatestPack.withArgs(clientWithPackExpiringSoonAndFailedChargeNotInLast24Hours).returns({ end: new Date(2015, 0, 14, 23, 59, 0) });

			tested.findEndingSubscriptions(function (err, endingSubscriptions){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(endingSubscriptions.length).to.equal(2);
					expect(endingSubscriptions[0]._id).to.equal('id');
					expect(endingSubscriptions[1]._id).to.equal('id2');
				});
			});
		});

		it('handles exceptions while gathering ending subscriptions', function (done){
			var now = new Date(2015,0,15),
				exception = 'dummy',
				modelMock = { client: { find: sinon.stub().yields(exception) } };

			mockery.registerMock(process.cwd() + '/api/models', modelMock);

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findEndingSubscriptions(function (err, endingSubscriptions){
				check(done, function (){
					expect(err).to.equal(exception);
				});
			});
		});

		it('handles unexpected error when selecting clients to charge and renew', function (done){
			var clients = [
					{ _id: 'dummy', packs: {} }
				], //provoke an error via an client without subscription
				modelMock = { client: { find: sinon.stub().yields(null, clients) } };

			mockery.registerMock(process.cwd() + '/api/models', modelMock);
			joblogs.handleJobError = sinon.stub();

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findEndingSubscriptions(function (err, endingSubscriptions){
				check(done, function (){
					expect(err).to.not.be.ok;
					expect(endingSubscriptions.length).to.equal(0);
					expect(joblogs.handleJobError.called).to.be.true;
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/subscriptionRenewer')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/joblogs')];
		});
	});

	describe('charge and renew subscriptions', function(){
		var prices,
			clientService;

		beforeEach(function (){
			prices = require(process.cwd() + '/api/lib/prices.json');
			clientService = require(process.cwd() + '/api/lib/clientService');
			joblogs = require(process.cwd() + '/api/lib/jobs/joblogs');
			bizTime = require(process.cwd() + '/api/lib/bizTime');
			mockery.enable({ warnOnUnregistered: false });
		});

		it('renews a free subscription without charging it', function (done){
			var clientWithFreeSubscription = { subscription: { paymentFrequency: 3, kind: 'XS' }, save: sinon.stub().yields(null) },
				clients = [clientWithFreeSubscription],
				subscriptionInfo = { "type": "XS", "price": 0, "coupons": 10 },
				packEnd = new Date(2015, 0, 13);

			prices.subscriptions.length = 0;
			prices.subscriptions.push(subscriptionInfo);

			clientService.createPacksForSubscription = sinon.stub();

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findLatestPack = sinon.stub();
			tested.findLatestPack.withArgs(clientWithFreeSubscription).returns({ end: packEnd });

			tested.chargeAndRenewSubscriptions(clients, function (err, chargingResults){
				check(done, function (){
					expect(chargingResults.nbRenewed).to.equal(1);
					expect(chargingResults.nbChargeFailed).to.equal(0);
					expect(clientService.createPacksForSubscription.calledWith(clientWithFreeSubscription, packEnd, 3, subscriptionInfo)).to.be.true;
					expect(clientWithFreeSubscription.save.called).to.be.true;
				});
			})
		});

		it('handles saving error when renewing a free subscription', function (done){
			var error = new Error('dummy'),
				clientWithFreeSubscription = { subscription: { paymentFrequency: 3, kind: 'XS' }, save: sinon.stub().yields(error) },
				clients = [clientWithFreeSubscription],
				subscriptionInfo = { "type": "XS", "price": 0, "coupons": 10 },
				packEnd = new Date(2015, 0, 13);

			prices.subscriptions.length = 0;
			prices.subscriptions.push(subscriptionInfo);

			clientService.createPacksForSubscription = sinon.stub();
			joblogs.handleJobError = sinon.stub();

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findLatestPack = sinon.stub();
			tested.findLatestPack.withArgs(clientWithFreeSubscription).returns({ end: packEnd });

			tested.chargeAndRenewSubscriptions(clients, function (err, chargingResults){
				check(done, function (){
					expect(chargingResults.nbRenewed).to.equal(0);
					expect(chargingResults.nbChargeFailed).to.equal(0);
					expect(clientService.createPacksForSubscription.calledWith(clientWithFreeSubscription, packEnd, 3, subscriptionInfo)).to.be.true;
					expect(clientWithFreeSubscription.save.called).to.be.true;
					expect(joblogs.handleJobError.calledWith('Renew subscriptions', 'Charge and renew subscriptions - saving charged client', error)).to.be.true;
				});
			})
		});

		it('renews and charge a subscription', function (done){
			var clientWithFreeSubscription = { subscription: { paymentFrequency: 3, kind: 'XS' }, save: sinon.stub().yields(null) },
				clients = [clientWithFreeSubscription],
				subscriptionInfo = { "type": "XS", "price": 100, "coupons": 10 },
				packEnd = new Date(2015, 0, 13);

			prices.subscriptions.length = 0;
			prices.subscriptions.push(subscriptionInfo);

			clientService.createPacksForSubscription = sinon.stub();
			clientService.registerPayment = sinon.stub().yields(null);
			mockery.registerMock('node-uuid', { v4: sinon.stub().returns('paymentId') });

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findLatestPack = sinon.stub();
			tested.findLatestPack.withArgs(clientWithFreeSubscription).returns({ end: packEnd });

			tested.chargeAndRenewSubscriptions(clients, function (err, chargingResults){
				check(done, function (){
					expect(chargingResults.nbRenewed).to.equal(1);
					expect(chargingResults.nbChargeFailed).to.equal(0);
					expect(clientService.createPacksForSubscription.calledWith(clientWithFreeSubscription, packEnd, 3, subscriptionInfo)).to.be.true;
					expect(clientService.registerPayment.calledWith(clientWithFreeSubscription, 300, 'automatic renewal', 'paymentId')).to.be.true;
				});
			})
		});

		it('handles payment registration error while renewing and charging a subscription', function (done){
			var client = { subscription: { paymentFrequency: 3, kind: 'XS' }, save: sinon.stub().yields(null) },
				clients = [client],
				subscriptionInfo = { "type": "XS", "price": 100, "coupons": 10 },
				packEnd = new Date(2015, 0, 13),
				now = new Date(2015, 0, 14),
				error = new Error('dummy');

			prices.subscriptions.length = 0;
			prices.subscriptions.push(subscriptionInfo);

			clientService.createPacksForSubscription = sinon.stub();
			clientService.registerPayment = sinon.stub().yields(error);
			mockery.registerMock('node-uuid', { v4: sinon.stub().returns('paymentId') });
			bizTime.now = sinon.stub().returns(now);
			joblogs.handleJobError = sinon.stub();

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findLatestPack = sinon.stub();
			tested.findLatestPack.withArgs(client).returns({ end: packEnd });

			tested.chargeAndRenewSubscriptions(clients, function (err, chargingResults){
				check(done, function (){
					expect(chargingResults.nbRenewed).to.equal(0);
					expect(chargingResults.nbChargeFailed).to.equal(1);
					expect(client.subscription.latestChargeFailure).to.equal(now);
					expect(clientService.createPacksForSubscription.calledWith(client, packEnd, 3, subscriptionInfo)).to.be.true;
					expect(clientService.registerPayment.calledWith(client, 300, 'automatic renewal', 'paymentId')).to.be.true;
					expect(client.save.called).to.be.true;
					expect(joblogs.handleJobError.calledWith('Renew subscriptions', 'Charge and renew subscriptions', error)).to.be.true;
				});
			});
		});

		it('handles client saving error while renewing and charging a subscription', function (done){
			var error = new Error('dummy'),
				client = { subscription: { paymentFrequency: 3, kind: 'XS' }, save: sinon.stub().yields(error) },
				clients = [client],
				subscriptionInfo = { "type": "XS", "price": 100, "coupons": 10 },
				packEnd = new Date(2015, 0, 13),
				now = new Date(2015, 0, 14);

			prices.subscriptions.length = 0;
			prices.subscriptions.push(subscriptionInfo);

			clientService.createPacksForSubscription = sinon.stub();
			clientService.registerPayment = sinon.stub().yields(null);
			mockery.registerMock('node-uuid', { v4: sinon.stub().returns('paymentId') });
			bizTime.now = sinon.stub().returns(now);
			joblogs.handleJobError = sinon.stub();

			tested = require(process.cwd() + '/api/lib/jobs/subscriptionRenewer');

			tested.findLatestPack = sinon.stub();
			tested.findLatestPack.withArgs(client).returns({ end: packEnd });

			tested.chargeAndRenewSubscriptions(clients, function (err, chargingResults){
				check(done, function (){
					expect(chargingResults.nbRenewed).to.equal(0);
					expect(chargingResults.nbChargeFailed).to.equal(0);
					expect(client.subscription.latestChargeFailure).to.not.be.ok;
					expect(clientService.createPacksForSubscription.calledWith(client, packEnd, 3, subscriptionInfo)).to.be.true;
					expect(clientService.registerPayment.calledWith(client, 300, 'automatic renewal', 'paymentId')).to.be.true;
					expect(client.save.called).to.be.true;
					expect(joblogs.handleJobError.calledWith('Renew subscriptions', 'Charge and renew subscriptions - saving charged client', error)).to.be.true;
				});
			})
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/subscriptionRenewer')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/clientService')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/joblogs')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
			mockery.deregisterAll();
			mockery.disable();
		});
	});
});