describe('distributor', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		settings,
		jobName = 'job name';

	chai.config.includeStack = true;

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	describe('execute', function (){
		var grantService, 
			joblogs, 
			tested,
			queueFactory,
			distributionStatusFactory,
			error = 'error';

		beforeEach(function (){
			grantService = {};
			joblogs = {};
			distributionStatusFactory = {};
			queueFactory = {};
			mockery.enable({ warnOnUnregistered: false });
			mockery.registerMock(process.cwd() + '/api/lib/grantService', grantService);
			mockery.registerMock(process.cwd() + '/api/lib/jobs/joblogs', joblogs);
			mockery.registerMock(process.cwd() + '/api/lib/jobs/distributor/distributionStatusFactory', distributionStatusFactory);
			mockery.registerMock(process.cwd() + '/api/lib/jobs/distributor/queueFactory', queueFactory);
			tested = require(process.cwd() + '/api/lib/jobs/distributor/distributor');
		});

		it('handles error when getting grants to execute', function (done){
			grantService.getToDistribute = sinon.stub().yields(error);
			joblogs.handleJobError = sinon.stub();
			tested.execute(function (){
				check(done, function (){
					expect(grantService.getToDistribute.called).to.be.true;
					expect(joblogs.handleJobError.calledWith('Distribute deals', 'Get grants to distribute', error)).to.be.true;
				});
			});
		});

		it('Stops execution early when no grants found to distribute', function (done){
			grantService.getToDistribute = sinon.stub().yields(null, []);
			joblogs.info = sinon.stub();

			tested.execute(function (){
				check(done, function (){
					expect(joblogs.info.calledWith('Distribute deals', 'No grant found to be distributed. Stopping.')).to.be.true;
				});
			});
		});

		it('Creates a queue and pushes grants to distribute to it', function (done){
			var grants = [{}],
				distributionStatus = {},
				executeEnded = function(){
					check(done, function (){
						expect(distributionStatusFactory.create.calledWith(executeEnded)).to.be.true;
						expect(queueFactory.create.calledWith(100, distributionStatus)).to.be.true;
						expect(queuePush.calledWith(grants)).to.be.true;
					});
				},
				queue = { push: function(){}},
				queuePush = sinon.stub(queue, 'push', executeEnded);
			grantService.getToDistribute = sinon.stub().yields(null, grants);
			distributionStatusFactory.create = sinon.stub().returns(distributionStatus);
			queueFactory.create = sinon.stub().returns(queue);

			tested.execute(executeEnded);
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/distributor/distributor')];
		});
	});

	describe('Distribution status', function (){
		var joblogs, 
			tested;

		beforeEach(function (){
			joblogs = {};
			mockery.enable({ warnOnUnregistered: false });
			mockery.registerMock(process.cwd() + '/api/lib/jobs/joblogs', joblogs);
			tested = require(process.cwd() + '/api/lib/jobs/distributor/distributionStatusFactory');
		});

		it('Create distribution status', function (){
			var callback = {},
				status = tested.create(callback);

			expect(status.processed).to.equal(0);
			expect(status.errors).to.equal(0);
		});

		it('Processes feedback of distribution with failures', function (){
			var callback = sinon.stub(),
				status = tested.create(callback);

			joblogs.errorLog = { error: sinon.stub() };
			status.processed = 2;
			status.errors = 1;

			status.processFeedback();

			expect(joblogs.errorLog.error.calledWith(sinon.match(function (value){
				return value.task === 'Distribute deals' && value.message === 'Distribution ended with 1 errors out of 2 grants.';
			}))).to.be.true;
			expect(callback.called).to.be.true;
		});

		it('Processes feedback of sucessful distribution', function (){
			var callback = sinon.stub(),
				status = tested.create(callback);

			joblogs.info = sinon.stub();
			status.processed = 2;
			status.errors = 0;

			status.processFeedback();

			expect(joblogs.info.calledWith('Distribute deals', 'Distribution successfully sent 2 grants.')).to.be.true;
			expect(callback.called).to.be.true;
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/distributor/distributionStatusFactory')];
		});
	});

	describe('Queue factory', function (){
		var async,
			grantService, 
			tested;

		beforeEach(function (){
			async = {};
			grantService = {};
			mockery.enable({ warnOnUnregistered: false });
			mockery.registerMock(process.cwd() + '/api/lib/jobs/grantService', grantService);
			mockery.registerMock('async', async);
			tested = require(process.cwd() + '/api/lib/jobs/distributor/queueFactory');
		});

		it('Create initializes queue', function (){
			var concurrency = 123,
				distributionStatus = { processFeedback: {} };

			async.queue = sinon.stub().returns({});

			var queue = tested.create(concurrency, distributionStatus);

			expect(async.queue.calledWith(queue.distributeNonFailedGrant, concurrency)).to.be.true;
			expect(queue.drain).to.equal(distributionStatus.processFeedback);
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/distributor/queueFactory')];
		});
	});
	describe('distribution queue', function (){
		var tested, 
			distributionStatus, 
			grantService, 
			queueFactory;

		beforeEach(function (){
			grantService = {};
			mockery.enable({ warnOnUnregistered: false });
			distributionStatus = { processed: 0, errors: 0 }
			mockery.registerMock(process.cwd() + '/api/lib/grantService', grantService);
			queueFactory = require(process.cwd() + '/api/lib/jobs/distributor/queueFactory');
			tested = queueFactory.create(100, distributionStatus);
		});

		it('Skips distribution of grants having failed 5 times already', function (done){
			var grant = { consecutiveSendErrors: 5 };

			grantService.distribute = sinon.stub();

			tested.distributeNonFailedGrant(grant, function (err){
				check(done, function(){
					expect(grantService.distribute.called).to.be.false;
					expect(distributionStatus.processed).to.equal(0);
					expect(distributionStatus.errors).to.equal(0);
				});
			});
		});

		it('Handles error while distributing a grant', function (done){
			var grant = { consecutiveSendErrors: 2 },
				error = 'error';

			grantService.distribute = sinon.stub().yields(error);

			tested.distributeNonFailedGrant(grant, function (err){
				check(done, function(){
					expect(grantService.distribute.calledWith(grant)).to.be.true;
					expect(distributionStatus.processed).to.equal(1);
					expect(distributionStatus.errors).to.equal(1);
				});
			});
		});

		it('Handles successfully distributing a grant', function (done){
			var grant = {};

			grantService.distribute = sinon.stub().yields(null);

			tested.distributeNonFailedGrant(grant, function (err){
				check(done, function(){
					expect(grantService.distribute.calledWith(grant)).to.be.true;
					expect(distributionStatus.processed).to.equal(1);
					expect(distributionStatus.errors).to.equal(0);
				});
			});
		});

		afterEach(function (){
			mockery.deregisterAll();
			mockery.disable();
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/distributor/queueFactory')];
		});
	});
});