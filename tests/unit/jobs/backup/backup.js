describe('backup', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect
		mockery = require('mockery');

	chai.config.includeStack = true;

	function check( done, f ) {
		try {
			f()
			done()
		} catch( e ) {
			done( e )
		}
	}

	describe('backupDatabase', function (){
		var tested,
			settings,
			models;

		beforeEach(function (){
			settings = require(process.cwd() + '/settings');
			models = require(process.cwd() + '/api/models');
			bizTime = require(process.cwd() + '/api/lib/bizTime');
			//tested = require(process.cwd() + '/api/lib/backupDatabase');
			mockery.enable({ warnOnUnregistered: false });
			mockery.registerMock('settings', settings);
			mockery.registerMock(process.cwd() + '/api/models', models);
			mockery.registerMock('bizTime', bizTime);
			mockery.registerAllowable(process.cwd() + '/api/lib/backupDatabase');
		});

		it('backs up successfully', function (done){
			var fileName = 'filename1234',
				backupsPath = './backups/',
				writeStream = new Object(),
				conn = new Object(),
				now = new Date(),
				fsMock = { createWriteStream: sinon.stub().returns(writeStream) },
				mongoDump = { dump: sinon.stub().yields(null, { end: function (){} }) };

			settings.BACKUPSPATH = backupsPath;
			models.connectionInfo = { connection: { db: conn } }

			bizTime.now = sinon.stub().returns(now);
			bizTime.getDateTimeHash = sinon.stub().returns(fileName);
			mockery.registerMock('mongo-dump-stream', mongoDump);
			mockery.registerMock('fs', fsMock);
			writeStream.end = sinon.stub();

			tested = require(process.cwd() + '/api/lib/backupDatabase');

			tested.backup(function (err, filePath, backupFileName){
				try{
					expect(err).to.be.null;
					expect(filePath).to.equal(backupsPath + fileName);
					expect(fsMock.createWriteStream.calledWith(backupsPath + fileName)).to.be.true;
					expect(backupFileName).to.equal(fileName);
					expect(bizTime.getDateTimeHash.calledWith(now))
					expect(mongoDump.dump.calledOnce).to.be.true;
					expect(mongoDump.dump.calledWith(conn, writeStream)).to.be.true;
					expect(writeStream.end.calledOnce).to.be.true;					
				} catch(ex){
					return done(ex);
				}

				done();
			});
		});
		it('handles dump error', function (done){
			var dumpError = new Error('dumpError'),
				writeStream = new Object(),
				fsMock = { createWriteStream: sinon.stub().returns(writeStream) },
				mongoDump = { dump: sinon.stub().yields(dumpError) };

			mockery.registerMock('fs', fsMock);
			mockery.registerMock('mongo-dump-stream', mongoDump);

			tested = require(process.cwd() + '/api/lib/backupDatabase');

			tested.backup(function (err){
				expect(err).to.equal(dumpError);
				expect(fsMock.createWriteStream.called).to.be.true;
				done();
			})
		});
		it('handles unexpected error', function (done){
			var unexpectedError = new Error('dummy'),
				fsMock = { createWriteStream: sinon.stub().throws(unexpectedError) },
				mongoDump = { dump: sinon.stub() };

			mockery.registerMock('fs', fsMock);
			mockery.registerMock('mongo-dump-stream', mongoDump);

			tested = require(process.cwd() + '/api/lib/backupDatabase');

			tested.backup(function (err){
				try{
					expect(err).to.equal(unexpectedError);
					expect(mongoDump.dump.called).to.be.false;
				}
				catch(ex){
					return done(ex);
				}
				done(); 
			});
		});
		afterEach(function(){
			mockery.deregisterAll();
			mockery.disable();
			// delete require.cache[require.resolve('fs')];
			// fs.createWriteStream = require('fs').createWriteStream;
			delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
			// delete require.cache[require.resolve('mongo-dump-stream')]; 
			delete require.cache[require.resolve(process.cwd() + '/api/lib/backupDatabase')];
			delete require.cache[require.resolve(process.cwd() + '/settings')];
		});
	});
	describe('backupAgent', function (){
		describe('backup()', function (){
			var tested,
				backupDatabase,
				automatedTasks, 
				joblogs;

			beforeEach(function (){
				settings = require(process.cwd() + '/settings');
				settings.testing = true; 
				tested = require(process.cwd() + '/api/lib/jobs/backupAgent/backupAgent');
				backupDatabase = require(process.cwd() + '/api/lib/backupDatabase');
				automatedTasks = require(process.cwd() + '/api/lib/jobs/backupAgent/automatedTasks');
				joblogs = require(process.cwd() + '/api/lib/jobs/joblogs'); 
			}); 

			it('Should call backupDatase.backup(), then automatedTasks.execute()', function (done){
				var backupFilePath = 'backupFilePath',
					backupFileName = 'backupFileName';

				backupDatabase.backup = sinon.stub().yields(null, backupFilePath, backupFileName);
				automatedTasks.execute = sinon.stub().yields(null);
				joblogs.info = sinon.stub();

				tested.backup(function (){
					expect(automatedTasks.execute.calledOnce).to.be.true;
					expect(automatedTasks.execute.calledWith(backupFilePath, backupFileName)).to.be.true;
					expect(backupDatabase.backup.calledOnce).to.be.true;
					expect(backupDatabase.backup.calledWith()).to.be.true;
					expect(joblogs.info.calledOnce).to.be.true;
					done();
				});

			});

			it('Should handle error in backupDatase.backup()', function (done){
				var error = new Error('error');

				backupDatabase.backup = sinon.stub().yields(error);
				joblogs.handleJobError = sinon.stub().returns(true);
				automatedTasks.execute = sinon.stub();

				tested.backup(function (){
					expect(backupDatabase.backup.calledOnce).to.be.true;
					expect(backupDatabase.backup.calledWith()).to.be.true;
					expect(joblogs.handleJobError.calledWith(sinon.match.any, sinon.match.any, error)).to.be.true;
					done();
				});
			});

			it('Should handle error in automatedTasks.execute()', function (done){
				var error = new Error('error'),
					backupFilePath = 'backupFilePath',
					backupFileName = 'backupFileName';

				backupDatabase.backup = sinon.stub().yields(null, backupFilePath, backupFileName);
				automatedTasks.execute = sinon.stub().yields(error);
				joblogs.handleJobError = sinon.stub();

				tested.backup(function (){
					expect(automatedTasks.execute.calledOnce).to.be.true;
					expect(automatedTasks.execute.calledWith()).to.be.true;
					expect(joblogs.handleJobError.calledWith(sinon.match.any, sinon.match.any, error)).to.be.true;
					done();
				});
			});

			afterEach(function (){
				delete require.cache[require.resolve(process.cwd() + '/settings')];
				delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/backupAgent')];
				delete require.cache[require.resolve(process.cwd() + '/api/lib/backupDatabase')];
				delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/automatedTasks')];
				delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/joblogs')]; 
			});
		});
	});	
	describe('automatedTasks', function (){
		var tested,
			settings,
			joblogs,
			nFilesDeleted = 5;

		beforeEach(function (){
			settings = require(process.cwd() + '/settings');
			settings.testing = true; 
			joblogs = require(process.cwd() + '/api/lib/jobs/joblogs'); 
			remoteFolderProxy = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolderProxy');
			localBackupFolder = require(process.cwd() + '/api/lib/jobs/backupAgent/localBackupFolder');
			tested = require(process.cwd() + '/api/lib/jobs/backupAgent/automatedTasks');
		});

		it('connects to remote folder, copies backup if needed, cleans up remote folder, cleans up local folder', function (done){
			var copyBackupStub = sinon.stub().yields(null),
				cleanupOutdatedBackupsStub = sinon.stub().yields(null),
				backupFilePath = 'backupFilePath',
				backupFileName = 'backupFileName';

			remoteFolderProxy.connectBackupFolder = sinon.stub().yields(null, 
				{ 
					copyBackupIfNeeded: copyBackupStub, 
					cleanupOutdatedBackups: cleanupOutdatedBackupsStub
				});
			localBackupFolder.deleteOldBackups = sinon.stub().yields(null, nFilesDeleted);
			joblogs.handleJobError = sinon.stub().returns(false);
			joblogs.info = sinon.stub();

			tested.execute(backupFilePath, backupFileName, function (){
				try{
					expect(remoteFolderProxy.connectBackupFolder.calledOnce).to.be.true;
					expect(copyBackupStub.calledWith(backupFilePath, backupFileName)).to.be.true;
					expect(cleanupOutdatedBackupsStub.calledOnce).to.be.true;
					expect(localBackupFolder.deleteOldBackups.calledOnce).to.be.true;
					expect(joblogs.handleJobError.callCount).to.equal(4);
					expect(joblogs.info.calledOnce).to.be.true;
					done();
				}
				catch(ex){
					done(ex);
				}
			});
		});

		it('handles error while connecting to remote folder, and still cleans up local folder', function (done){
			var error = new Error('error');

			remoteFolderProxy.connectBackupFolder = sinon.stub().yields(error);
			joblogs.handleJobError = sinon.stub().returns(true);
			localBackupFolder.deleteOldBackups = sinon.stub().yields(null, nFilesDeleted);

			tested.execute('dummy', 'dummy', function (){
				expect(joblogs.handleJobError.calledWith(sinon.match.any, sinon.match.any)).to.be.true;
				expect(localBackupFolder.deleteOldBackups.calledOnce).to.be.true;
				done();
			});
		});

		it('handles error while copying to remote folder, and still cleans up remote and local folders', function (done){
			var error = new Error('error'),
				copyBackupStub = sinon.stub().yields(null),
				cleanupOutdatedBackupsStub = sinon.stub().yields(null);

			remoteFolderProxy.connectBackupFolder = sinon.stub().yields(null,
				{ 
					copyBackupIfNeeded: copyBackupStub, 
					cleanupOutdatedBackups: cleanupOutdatedBackupsStub
				});
			joblogs.handleJobError = sinon.stub();
			joblogs.handleJobError.onCall(0).returns(false);
			joblogs.handleJobError.onCall(3).returns(false);
			joblogs.info = sinon.stub();
			localBackupFolder.deleteOldBackups = sinon.stub().yields(null, nFilesDeleted);

			tested.execute('dummy', 'dummy', function (){
				expect(copyBackupStub.calledOnce).to.be.true;
				expect(localBackupFolder.deleteOldBackups.calledOnce).to.be.true;
				expect(joblogs.handleJobError.callCount).to.equal(4);
				expect(joblogs.info.calledOnce).to.be.true;
				done();
			});
		});

		it('handles error while cleaning up remote folder, and still cleans up local folder', function (done){
			var error = new Error('error'),
				copyBackupStub = sinon.stub().yields(null),
				cleanupOutdatedBackupsStub = sinon.stub().yields(error);

			remoteFolderProxy.connectBackupFolder = sinon.stub().yields(null,
				{ 
					copyBackupIfNeeded: copyBackupStub, 
					cleanupOutdatedBackups: cleanupOutdatedBackupsStub
				});
			joblogs.handleJobError = sinon.stub();
			joblogs.handleJobError.onCall(0).returns(false);
			joblogs.handleJobError.onCall(3).returns(false);
			joblogs.info = sinon.stub();
			localBackupFolder.deleteOldBackups = sinon.stub().yields(null, nFilesDeleted);

			tested.execute('dummy', 'dummy', function (){
				expect(joblogs.handleJobError.callCount).to.equal(4);
				expect(joblogs.info.calledOnce).to.be.true;
				expect(remoteFolderProxy.connectBackupFolder.calledOnce).to.be.true;
				expect(copyBackupStub.calledOnce).to.be.true;
				expect(cleanupOutdatedBackupsStub.calledOnce).to.be.true;
				expect(localBackupFolder.deleteOldBackups.calledOnce).to.be.true;
				done();
			});
		})

		it('handles error while cleaning up local folder', function (done){
			var error = new Error('error'),
				copyBackupStub = sinon.stub().yields(null),
				cleanupOutdatedBackupsStub = sinon.stub().yields(null);

			remoteFolderProxy.connectBackupFolder = sinon.stub().yields(null,
				{ 
					copyBackupIfNeeded: copyBackupStub, 
					cleanupOutdatedBackups: cleanupOutdatedBackupsStub
				});
			joblogs.handleJobError = sinon.stub();
			joblogs.handleJobError.onCall(0).returns(false);
			joblogs.handleJobError.onCall(3).returns(false);
			joblogs.info = sinon.stub();
			localBackupFolder.deleteOldBackups = sinon.stub().yields(error);

			tested.execute('dummy', 'dummy', function (){
				expect(joblogs.handleJobError.callCount).to.equal(4);
				expect(joblogs.info.calledOnce).to.be.true;
				expect(copyBackupStub.calledOnce).to.be.true;
				expect(cleanupOutdatedBackupsStub.calledOnce).to.be.true;
				expect(localBackupFolder.deleteOldBackups.calledOnce).to.be.true;
				done();
			});
		})

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/automatedTasks')];
			delete require.cache[require.resolve(process.cwd() + '/settings')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/joblogs')]; 
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolderProxy')]; 
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/localBackupFolder')]; 
		});
	});
	describe('remoteFolderProxy', function (){
		var kloudless,
			tested;

		beforeEach(function (){
			kloudless = sinon.createStubInstance(require('kloudless'));
			//black magic: Kloudless module returns a constructor, and the below is a way I have found to
			//ensure calling a stubbed version of this constructor gives back a stubbed kloudless object
			require.cache[require.resolve('kloudless')] = { exports: sinon.stub().returns(kloudless) };

			tested = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolderProxy');
		});

		it('handles error when gathering kloudless account', function (done){
			var error = new Error('error');

			kloudless.accounts = { base: sinon.stub().yields(error) };
			
			tested.getDropboxAccountId(function (err, accountId){
				check(done, function (){
					expect(kloudless.accounts.base.calledOnce).to.be.true;
					expect(err).to.be.equal(error);
				});
			});
		});

		it('handles missing dropbox account', function (done){
			var accounts = [
					{ id: 'prout', service: 'pleple' },
					{ id: 'dummy', service: 'blabla' }
				];
			kloudless.accounts = { base: sinon.stub().yields(null, { objects: accounts }) };
			tested.getDropboxAccountId(function (err, accountId){
				check(done, function (){
					expect(err).to.equal('No dropbox account found on kloudless.');
				});
			});
		});

		it('finds and uses the dropbox account', function (done){
			var dropboxId = '12345679',
				accounts = [
					{ id: dropboxId, service: 'dropbox' },
					{ id: 'dummy', service: 'blabla' }
				];

			kloudless.accounts = { base: sinon.stub().yields(null, { objects: accounts }) };
			tested.getDropboxAccountId(function (err, accountId){
				check(done, function (){
					expect(accountId).to.equal(dropboxId);
				});
			});
		});

		describe('find backup folder', function (){
			var dropboxId = '12345679'
				idProjectFolder = 'idProjectFolder',
				idBackupFolder = 'idBackupFolder',
				idEnvironmentFolder = 'idEnvironmentFolder',
				remoteFolder = null,
				error = null;

			beforeEach(function (){
				error = new Error('error');

				kloudless.folders = { create: sinon.stub() };
				remoteFolder = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder');
				tested.getDropboxAccountId = sinon.stub().yields(null, dropboxId);
			});

			it('finds the remote project folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: {} });
				kloudless.folders.create.onSecondCall().yields(error);

				tested.connectBackupFolder(function(err, folder){
					check(done, function (){
						expect(kloudless.folders.create.firstCall.calledWith({ account_id: dropboxId, parent_id: 'root', name: sinon.match.any })).to.be.true;
					});
				});
			});

			it('handles error while finding the remote project folder', function (done){
				kloudless.folders.create.onFirstCall().yields(error);

				tested.connectBackupFolder(function(err, folder){
					check(done, function (){
						expect(err).to.equal(error);
					});
				});
			});

			it('finds the root backup folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(null, { id: {} });
				kloudless.folders.create.onThirdCall().yields(error);

				tested.connectBackupFolder(function(err, folder){
					check(done, function (){
						expect(kloudless.folders.create.secondCall.calledWith({ account_id: dropboxId, parent_id: idProjectFolder, name: sinon.match.any })).to.be.true;
					});
				});
			});

			it('handles error while finding the root backup folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(error);

				tested.connectBackupFolder(function(err, folder){
					check(done, function (){
						expect(err).to.equal(error);
					});
				});

			});

			it('finds the environment-specific backup folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(null, { id: idBackupFolder });
				kloudless.folders.create.onThirdCall().yields(null, { id: {} });

				tested.connectBackupFolder(function(err, folder){
					check(done, function (){
						expect(kloudless.folders.create.thirdCall.calledWith({ account_id: dropboxId, parent_id: idBackupFolder, name: sinon.match.any })).to.be.true;
					});
				});
			});

			it('handles error while finding the environment-specific backup folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(null, { id: idBackupFolder });
				kloudless.folders.create.onThirdCall().yields(error);

				tested.connectBackupFolder(function(err, folder){
					check(done, function (){
						expect(err).to.equal(error);
					});
				});
			});

			it('returns the backup folder', function (done){
				var fakeFolder = {};

				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(null, { id: idBackupFolder });
				kloudless.folders.create.onThirdCall().yields(null, { id: idEnvironmentFolder });

				remoteFolder.create = sinon.stub().returns(fakeFolder);

				tested.connectBackupFolder(function(err, folder){
					check(done, function (){
						expect(remoteFolder.create.calledWith(dropboxId, idEnvironmentFolder));
						expect(folder).to.equal(fakeFolder);
					});
				});
			});

			afterEach(function (){
				delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder')];
			});
		});

		describe('find invoices folder', function (){
			var dropboxId = '12345679'
				idProjectFolder = 'idProjectFolder',
				idInvoiceFolder = 'idInvoiceFolder',
				idEnvironmentFolder = 'idEnvironmentFolder',
				remoteFolder = null,
				error = null;

			beforeEach(function (){
				error = new Error('error');

				kloudless.folders = { create: sinon.stub() };
				remoteFolder = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder');
				tested.getDropboxAccountId = sinon.stub().yields(null, dropboxId);
			});

			it('finds the remote project folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: {} });
				kloudless.folders.create.onSecondCall().yields(error);

				tested.connectInvoiceFolder(function(err, folder){
					check(done, function (){
						expect(kloudless.folders.create.firstCall.calledWith({ account_id: dropboxId, parent_id: 'root', name: sinon.match.any })).to.be.true;
					});
				});
			});

			it('handles error while finding the remote project folder', function (done){
				kloudless.folders.create.onFirstCall().yields(error);

				tested.connectInvoiceFolder(function(err, folder){
					check(done, function (){
						expect(err).to.equal(error);
					});
				});
			});

			it('finds the root backup folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(null, { id: {} });
				kloudless.folders.create.onThirdCall().yields(error);

				tested.connectInvoiceFolder(function(err, folder){
					check(done, function (){
						expect(kloudless.folders.create.secondCall.calledWith({ account_id: dropboxId, parent_id: idProjectFolder, name: sinon.match.any })).to.be.true;
					});
				});
			});

			it('handles error while finding the root backup folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(error);

				tested.connectInvoiceFolder(function(err, folder){
					check(done, function (){
						expect(err).to.equal(error);
					});
				});

			});

			it('finds the environment-specific backup folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(null, { id: idInvoiceFolder });
				kloudless.folders.create.onThirdCall().yields(null, { id: {} });

				tested.connectInvoiceFolder(function(err, folder){
					check(done, function (){
						expect(kloudless.folders.create.thirdCall.calledWith({ account_id: dropboxId, parent_id: idInvoiceFolder, name: sinon.match.any })).to.be.true;
					});
				});
			});

			it('handles error while finding the environment-specific backup folder', function (done){
				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(null, { id: idInvoiceFolder });
				kloudless.folders.create.onThirdCall().yields(error);

				tested.connectInvoiceFolder(function(err, folder){
					check(done, function (){
						expect(err).to.equal(error);
					});
				});
			});

			it('returns the backup folder', function (done){
				var fakeFolder = {};

				kloudless.folders.create.onFirstCall().yields(null, { id: idProjectFolder });
				kloudless.folders.create.onSecondCall().yields(null, { id: idInvoiceFolder });
				kloudless.folders.create.onThirdCall().yields(null, { id: idEnvironmentFolder });

				remoteFolder.create = sinon.stub().returns(fakeFolder);

				tested.connectInvoiceFolder(function(err, folder){
					check(done, function (){
						expect(remoteFolder.create.calledWith(dropboxId, idEnvironmentFolder));
						expect(folder).to.equal(fakeFolder);
					});
				});
			});

			afterEach(function (){
				delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder')];
			});
		});

		afterEach(function (){
			delete require.cache[require.resolve('kloudless')];
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolderProxy')]; 
		});
	});
	describe('remoteFolder', function (){
		describe('copyBackupIfNeeded', function (){
			var tested,
				kloudless,
				accountId = 'accId',
				folderId = 'folId',
				fileName = 'fileName',
				filePath = 'filePath',
				error,
				bizTime,
				settings;

			beforeEach(function (){
				kloudless = sinon.createStubInstance(require('kloudless'));
				//black magic: Kloudless module returns a constructor, and the below is a way I have found to
				//ensure calling a stubbed version of this constructor gives back a stubbed kloudless object
				require.cache[require.resolve('kloudless')] = { exports: sinon.stub().returns(kloudless) };
				fs = require('fs');
				settings = require(process.cwd() + '/settings');
				bizTime = require(process.cwd() + '/api/lib/bizTime');
				tested = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder').create(accountId, folderId);
				error = new Error('error');
			});

			it('Queries the remote folder\'s content', function (done){
				kloudless.folders = { contents: sinon.stub().yields(null, {}) };

				fs.readFile = sinon.stub().yields(error);

				tested.copyBackupIfNeeded(filePath, fileName, function (err){
					expect(kloudless.folders.contents.calledWith({ account_id: accountId, folder_id: folderId })).to.be.true;
					done()
				});
			});

			it('handle error while querying the remote folder\'s content', function (done){
				kloudless.folders = { contents: sinon.stub().yields(error) };

				tested.copyBackupIfNeeded(filePath, fileName, function (err){
					expect(err).to.equal(error);
					done()
				});
			});

			it('copies the backup when no file in remote folder', function (done){
				var backupFileContent = {};

				kloudless.folders = { contents: sinon.stub().yields(null, {}) };
				kloudless.files = { upload: sinon.stub().yields(null) };
				fs.readFile = sinon.stub().yields(null, backupFileContent);

				tested.copyBackupIfNeeded(filePath, fileName , function(err){
					expect(fs.readFile.calledWith(filePath)).to.be.true;
					expect(kloudless.files.upload.calledWith({ account_id: accountId, parent_id: folderId, file: backupFileContent, name: fileName })).to.be.true;
					expect(err).to.be.undefined;
					done();
				});
			});

			it('handles error while reading local backup file', function (done){
				var backupFileContent = {};

				kloudless.folders = { contents: sinon.stub().yields(null, {}) };
				fs.readFile = sinon.stub().yields(error);

				tested.copyBackupIfNeeded(filePath, fileName , function(err){
					expect(fs.readFile.calledWith(filePath)).to.be.true;
					expect(err).to.equal(error);
					done();
				});
			});

			it('handles error while uploading backup file', function (done){
				var backupFileContent = {};

				kloudless.folders = { contents: sinon.stub().yields(null, {}) };
				fs.readFile = sinon.stub().yields(null, backupFileContent);
				kloudless.files = { upload: sinon.stub().yields(error) };

				tested.copyBackupIfNeeded(filePath, fileName , function(err){
					expect(fs.readFile.calledWith(filePath)).to.be.true;
					expect(kloudless.files.upload.calledWith({ account_id: accountId, parent_id: folderId, file: backupFileContent, name: fileName })).to.be.true;
					expect(err).to.equal(error);
					done();
				});
			});

			it('copies the backup when newest file in remote folder is older than backup frequency', function (done){
				var backupFileContent = {};

				settings.REMOTEBACKUPUPDATEFREQUENCY = 12;
				bizTime.now = sinon.stub().returns(new Date(Date.UTC(2015, 0, 10)));
				kloudless.folders = { contents: sinon.stub().yields(null, { objects: [ { modified: '2015-01-09T11:59:00Z' }, { modified: '2015-01-09T11:00:00Z' } ] }) };
				kloudless.files = { upload: sinon.stub().yields(null) };
				fs.readFile = sinon.stub().yields(null, backupFileContent);

				tested.copyBackupIfNeeded(filePath, fileName , function(err){
					expect(fs.readFile.calledWith(filePath)).to.be.true;
					expect(kloudless.files.upload.calledWith({ account_id: accountId, parent_id: folderId, file: backupFileContent, name: fileName })).to.be.true;
					expect(err).to.be.undefined;
					done();
				});
			});

			it('Does not copy the backup when latest file in remote folder is not older than backup frequency', function (done){
				var backupFileContent = {};

				settings.REMOTEBACKUPUPDATEFREQUENCY = 12;
				bizTime.now = sinon.stub().returns(new Date(Date.UTC(2015, 0, 10)));
				kloudless.folders = { contents: sinon.stub().yields(null, { objects: [ { modified: '2015-01-09T12:01:00Z' }, { modified: '2015-01-09T11:00:00Z' } ] }) };
				kloudless.files = { upload: sinon.stub().yields(null) };
				fs.readFile = sinon.stub().yields(null, backupFileContent);

				tested.copyBackupIfNeeded(filePath, fileName , function(err){
					expect(fs.readFile.calledWith(filePath)).to.be.false;
					expect(err).to.be.undefined;
					done();
				});
			});

			afterEach(function (){
				delete require.cache[require.resolve('kloudless')];
				delete require.cache[require.resolve(process.cwd() + '/settings')];
				delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder')];
				delete require.cache[require.resolve('fs')];
				delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
			});
		});
		describe('cleanupOutdatedBackups', function (){
			var tested,
				kloudless,
				accountId = 'accId',
				folderId = 'folId',
				error,
				bizTime,
				settings;

			beforeEach(function (){
				kloudless = sinon.createStubInstance(require('kloudless'));
				//black magic: Kloudless module returns a constructor, and the below is a way I have found to
				//ensure calling a stubbed version of this constructor gives back a stubbed kloudless object
				require.cache[require.resolve('kloudless')] = { exports: sinon.stub().returns(kloudless) };
				settings = require(process.cwd() + '/settings');
				bizTime = require(process.cwd() + '/api/lib/bizTime');
				tested = require(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder').create(accountId, folderId);
				error = new Error('error');
			});

			it('Queries the remote folder\'s content', function (done){
				kloudless.folders = { contents: sinon.stub().yields(null, {}) };

				tested.cleanupOutdatedBackups(function (err){
					expect(kloudless.folders.contents.calledWith({ account_id: accountId, folder_id: folderId })).to.be.true;
					done()
				});
			});

			it('handles error while querying the remote folder\'s content', function (done){
				kloudless.folders = { contents: sinon.stub().yields(error) };

				tested.cleanupOutdatedBackups(function (err){
					expect(err).to.equal(error);
					done()
				});
			});

			it('deletes outdated remote backup files', function (done){
				var idOutdated1 = 'idOutdated1',
					idOutdated2 = 'idOutdated2',
					idToKeep1 = 'idToKeep1',
					idToKeep2 = 'idToKeep2';

				settings.REMOTEBACKUPSTORAGEDURATION = 3;
				bizTime.now = sinon.stub().returns(new Date(Date.UTC(2015, 0, 10)));
				kloudless.folders = { contents: sinon.stub().yields(null, { 
					objects: [ 
							{ modified: '2015-01-06T00:00:00Z', id: idOutdated1 }, 
							{ modified: '2015-01-06T23:59:00Z', id: idOutdated2 }, 
							{ modified: '2015-01-07T00:00:00Z', id: idToKeep1 }, 
							{ modified: '2015-01-08T00:00:00Z', id: idToKeep2 }
						] 
					}) 
				};
				kloudless.files = { delete: sinon.stub().yields(null) };

				tested.cleanupOutdatedBackups(function (err){
					expect(kloudless.files.delete.callCount).to.equal(2);
					expect(kloudless.files.delete.firstCall.calledWith({ account_id: accountId, file_id: idOutdated1 }));
					expect(kloudless.files.delete.secondCall.calledWith({ account_id: accountId, file_id: idOutdated2 }));
					done()
				});
			});

			it('handles errors when deleting outdated remote backup files', function (done){
				var idOutdated1 = 'idOutdated1',
					idOutdated2 = 'idOutdated2',
					idToKeep1 = 'idToKeep1',
					idToKeep2 = 'idToKeep2';

				settings.REMOTEBACKUPSTORAGEDURATION = 3;
				bizTime.now = sinon.stub().returns(new Date(Date.UTC(2015, 0, 10)));
				kloudless.folders = { contents: sinon.stub().yields(null, { 
					objects: [ 
							{ modified: '2015-01-06T00:00:00Z', id: idOutdated1 }, 
							{ modified: '2015-01-06T23:59:00Z', id: idOutdated2 }, 
							{ modified: '2015-01-07T00:00:00Z', id: idToKeep1 }, 
							{ modified: '2015-01-08T00:00:00Z', id: idToKeep2 }
						] 
					}) 
				};
				kloudless.files = { delete: sinon.stub() };
				kloudless.files.delete.onFirstCall().yields(null);
				kloudless.files.delete.onSecondCall().yields(error);

				tested.cleanupOutdatedBackups(function (err){
					expect(kloudless.files.delete.callCount).to.equal(2);
					expect(kloudless.files.delete.firstCall.calledWith({ account_id: accountId, file_id: idOutdated1 }));
					expect(kloudless.files.delete.secondCall.calledWith({ account_id: accountId, file_id: idOutdated2 }));
					expect(err).to.equal(error);
					done()
				});
			});

			afterEach(function (){
				delete require.cache[require.resolve('kloudless')];
				delete require.cache[require.resolve(process.cwd() + '/settings')];
				delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/remoteFolder')];
				delete require.cache[require.resolve(process.cwd() + '/api/lib/bizTime')];
			});
		});
	});
	describe('localBackupFolder', function (){
		var tested,
			fs,
			settings,
			error = new Error('error');

		beforeEach(function (){
			fs = require('fs');
			settings = require(process.cwd() + '/settings');
			tested = require(process.cwd() + '/api/lib/jobs/backupAgent/localBackupFolder');
		});

		it('handles error while listing files in local backup folder', function (done){
			fs.readdir = sinon.stub().yields(error);

			tested.deleteOldBackups(function (err){
				expect(fs.readdir.calledWith(settings.BACKUPSPATH)).to.be.true;
				expect(err).to.equal(error);
				done();
			});
		});

		it('Lists files in local backup folder, and collect their stats', function (done){
			var fileName1 = 'fileName1',
				fileName2 = 'fileName2';

			fs.readdir = sinon.stub().yields(null, [ fileName1, fileName2 ]);
			fs.stat = sinon.stub().yields(null, { ctime: new Date() });

			tested.deleteOldBackups(function (err){
				expect(fs.stat.firstCall.calledWith(settings.BACKUPSPATH + fileName1)).to.be.true;
				expect(fs.stat.secondCall.calledWith(settings.BACKUPSPATH + fileName2)).to.be.true;

				done();
			});
		});

		it('handles error when collecting stats of the files in local backup folder', function (done){
			var fileName1 = 'fileName1',
				fileName2 = 'fileName2';

			fs.readdir = sinon.stub().yields(null, [ fileName1, fileName2 ]);
			fs.stat = sinon.stub().withArgs(settings.BACKUPSPATH + fileName1).yields(null);
			fs.stat = sinon.stub().withArgs(settings.BACKUPSPATH + fileName2).yields(error);

			tested.deleteOldBackups(function (err){
				expect(err).to.equal(error);
				done();
			});
		});

		it('Deletes all backups except the configured amount of most recent backup files', function (done){
			var fileName1 = 'fileName1',
				fileName2 = 'fileName2',
				fileName3 = 'fileName3',
				fileName4 = 'fileName4';

			settings.NUMBEROFLOCALBACKUPSTOSTORE = 2;

			fs.readdir = sinon.stub().yields(null, [ fileName1, fileName2, fileName3, fileName4 ]);
			fs.stat = sinon.stub()
			fs.stat.withArgs(settings.BACKUPSPATH + fileName1).yields(null, { ctime: new Date(2015, 0, 1) });
			fs.stat.withArgs(settings.BACKUPSPATH + fileName2).yields(null, { ctime: new Date(2012, 0, 1) });
			fs.stat.withArgs(settings.BACKUPSPATH + fileName3).yields(null, { ctime: new Date(2010, 0, 1) });
			fs.stat.withArgs(settings.BACKUPSPATH + fileName4).yields(null, { ctime: new Date(2014, 0, 1) });
			fs.unlink = sinon.stub().yields(null);

			tested.deleteOldBackups(function (err, nFilesDeleted){
				expect(fs.unlink.firstCall.calledWith(settings.BACKUPSPATH + fileName2)).to.be.true;
				expect(fs.unlink.secondCall.calledWith(settings.BACKUPSPATH + fileName3)).to.be.true;
				expect(nFilesDeleted).to.equal(2);
				done();
			});
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/backupAgent/localBackupFolder')];
			delete require.cache[require.resolve(process.cwd() + '/settings')];
			delete require.cache[require.resolve('fs')];
		});
	});
});
