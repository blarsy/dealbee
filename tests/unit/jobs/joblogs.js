describe('joblogs', function (){
	var sinon = require('sinon'),
		chai = require('chai'),
		expect = chai.expect,
		settings,
		jobName = 'job name';

	chai.config.includeStack = true;

	beforeEach(function (){
		settings = require(process.cwd() + '/settings');
		settings.testing = true;
		bunyan = require('bunyan');
	});

	describe('handleJobError', function (){
		var opName = 'operation name';

		beforeEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/joblogs')];
		});

		it('logs error and returns true', function (){
			var errorLogger = sinon.stub(),
				err = new Error('error');

			bunyan.createLogger = sinon.stub().returns({ error: errorLogger });
			tested = require(process.cwd() + '/api/lib/jobs/joblogs');

			var result = tested.handleJobError(jobName, opName, err);

			expect(result).to.be.true;
			expect(errorLogger.calledOnce).to.be.true;
			expect(errorLogger.calledWith({ task: jobName, message: opName, err: err })).to.be.true;
		});

		it('return false when no error', function (){
			var	errorLogger = sinon.stub();
				bunyan.createLogger = sinon.stub().returns({ error: errorLogger })

			tested = require(process.cwd() + '/api/lib/jobs/joblogs');
			
			var result = tested.handleJobError(jobName, opName, null);

			expect(result).to.be.false;
		});

		afterEach(function (){
			delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/joblogs')];
		});
	});

	describe('info', function (){
		it('logs info', function (){
			var infoLogger = sinon.stub(),
				message = 'msg';
			bunyan.createLogger = sinon.stub().returns({ info: infoLogger });
			tested = require(process.cwd() + '/api/lib/jobs/joblogs');

			tested.info(jobName, message);

			expect(infoLogger.calledWith({ task: jobName, message: message }));
		});
	});

	afterEach(function (){
		delete require.cache[require.resolve(process.cwd() + '/settings')];
		delete require.cache[require.resolve(process.cwd() + '/api/lib/jobs/joblogs')];
		delete require.cache[require.resolve('bunyan')];
	});
});