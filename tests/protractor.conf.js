exports.config = {
  allScriptsTimeout: 11000,

  specs: [
    'features/*.feature'
  ],

  capabilities: {
    'browserName': 'chrome'
  },

  baseUrl: 'http://localhost:8081/',

  framework: 'cucumber',

  cucumberOpts: {
    // Require files before executing the features.
    //require: 'cucumber/stepDefinitions.js',
    // Only execute the features or scenarios with tags matching @dev.
    // This may be an array of strings to specify multiple tags to include.
    //tags: '@Active',
    tags: '@Focus',
    // How to format features (default: progress)
    format: 'pretty'
  }
};
