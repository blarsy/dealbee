#!/bin/env node
var restify = require('restify'),
    settings = require(process.cwd() + '/settings'),
    restApiServer = require(process.cwd() + '/api/restapiserver'),
    jobAgent = require(process.cwd() + '/api/lib/jobs/jobAgent'),
    dateUtils = require('date-utils');

/**
 *  Define the Dealbee application.
 */
 var Dealbee = function() {

    //Trick to allow terminating the process gracefully on Windows (dev machine)
    var readLine = require ('readline');
    if (process.platform === 'win32'){
    	var rl = readLine.createInterface ({
    		input: process.stdin,
    		output: process.stdout
    	});

    	rl.on ('SIGINT', function (){
    		console.log('Caught win-specific sigint.');
    		process.emit ('SIGINT');
    	});
    }

    //  Scope.
    var self = this;


    /*  ================================================================  */
    /*  Helper functions.                                                 */
    /*  ================================================================  */

    /**
     *  Set up server IP address and port # using env variables/defaults.
     */
     self.setupVariables = function() {
        //  Set the environment variables we need.
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;
        self.port      = process.env.OPENSHIFT_NODEJS_PORT || settings.PORT;

        if (typeof self.ipaddress === "undefined") {
            //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
            //  allows us to run/test the app locally.
            console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
            self.ipaddress = "127.0.0.1";
        };
    };

    /**
     *  terminator === the termination handler
     *  Terminate server on receipt of the specified signal.
     *  @param {string} sig  Signal to terminate on.
     */
     self.terminator = function(sig){
     	jobAgent.stop(function (){
     		if (typeof sig === "string") {
     			console.log('%s: Received %s - terminating Dealbee app ...',
     				Date(Date.now()), sig);
     			process.exit(1);
     		}
     		console.log('%s: Node server stopped.', Date(Date.now()) );
     	})
     };


    /**
     *  Setup termination handlers (for exit and a list of signals).
     */
     self.setupTerminationHandlers = function(){
        //  Process on exit and signals.
        process.on('exit', function() { self.terminator(); });

        // Removed 'SIGPIPE' from the list - bugz 852598.
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
        'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function(element, index, array) {
        	process.on(element, function() { self.terminator(element); });
        });
    };

    /*  ================================================================  */
    /*  App server functions (main app logic here).                       */
    /*  ================================================================  */

    /**
     *  Initialize the server and create the routes and register
     *  the handlers.
     */
     self.initializeServer = function() {
     	restApiServer.configure();
     	if(settings.AUTOSTARTJOBS){
     		jobAgent.start();
     	}
     };


    /**
     *  Initializes the Dealbee application.
     */
     self.initialize = function() {
     	self.setupVariables();
     	self.setupTerminationHandlers();

     	self.initializeServer();
     };


    /**
     *  Start the server (starts up the Dealbee application).
     */
     self.start = function() {
        //  Start the app on the specific interface (and port).
        restApiServer.start(self.port, self.ipaddress, function() {
        	console.log('%s: Node server started on %s:%d ...',
        		Date(Date.now() ), self.ipaddress, self.port);
        });
    };

};   /*  Dealbee Application.  */



/**
 *  main():  Main code.
 */
 var app = new Dealbee();
 app.initialize();
 app.start();