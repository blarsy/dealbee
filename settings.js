var webAppPath = './web/app/';

module.exports = function(){
	var settings = {
		WEBAPPPATH: webAppPath,
		ROOTPATH: './web/',
		DB: 'mongodb://localhost/dealbee',
		JOBDB: 'mongodb://localhost/dealbee',
		JOBPOLLINTERVAL: '5 seconds',
		FBAPPID: '1523173214607128',
		FBAPPSECRET: '343dab0186ab35775c73995cae73ce8a',
		FBNAMESPACE: 'dealbee-local',
		PORT: 8081,
		HOST: 'localhost',
		REDISHOST: '127.0.0.1',
		REDISPORT: 6379,
		REDISPASSWORD: '',
		LOGPATH: '/log/',
		SESSIONTTL: 14400,
		FORCESSL: false,
		MAILAPIKEY: 'key-271e24d65c68377c634d835c5801202c',
		MAILDOMAIN: 'sandbox81cd5b6aade341be9daf698f7c780cef.mailgun.org',
		WEBSITEURL: 'http://localhost:8081/',
		MAILGIFTREQUESTTTLMINUTES: 120,
		STRIPEAPIKEY: 'sk_test_XDLgeFcfUm1983hRdQYRwEgz',
		SUPPORTMAIL: 'support@dealbee.net',
		BACKUPSPATH: './../backups/',
		KLOUDLESSAPIKEY: 'Pc5jMC_fN5y_1SKF4kjBPfdsNWxzd9Rb_p6l3iXCg8nSt58m',
		ENVIRONMENT: 'local',
		AUTOSTARTJOBS: false,
		REMOTEBACKUPUPDATEFREQUENCY: 12, //hours
		REMOTEBACKUPSTORAGEDURATION: 5, //days
		NUMBEROFLOCALBACKUPSTOSTORE: 5,
		CLOUDINARYAPIKEY: '366435653146389',
		CLOUDINARYAPISECRET: 'UcGxwbdUEAL14OQ6jIW1bUWi6MU',
		VATMULTIPLIER: 1.21
	};

	if(process.env.OPENSHIFT_MONGODB_DB_URL){
		settings.DB = process.env.OPENSHIFT_MONGODB_DB_URL + process.env.OPENSHIFT_APP_NAME;
		settings.JOBDB = process.env.OPENSHIFT_MONGODB_DB_URL + process.env.OPENSHIFT_APP_NAME;
	}

	return settings;
}(); 